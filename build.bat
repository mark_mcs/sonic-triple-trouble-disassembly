@ECHO OFF

IF EXIST tt.o DEL tt.o
IF EXIST tt.sym DEL tt.sym
IF EXIST tt.sms DEL tt.sms

ECHO Assembling...
wla_dx_binaries_latest\wla-z80 -vo src/stt.asm tt.o > build_report.txt

IF %ERRORLEVEL% NEQ 0 GOTO assemble_fail
IF NOT EXIST tt.o GOTO assemble_fail

ECHO Linking...
wla_dx_binaries_latest\wlalink -rs stt.link tt.sms
IF %ERRORLEVEL% NEQ 0 GOTO link_fail

ECHO ==========================
ECHO Build Success.
ECHO ==========================

REM Use fcomp to compare with original ROM
REM ECHO Comparing with original:
REM fcomp tt.gg "Sonic the Hedgehog - Triple Trouble (UE).gg"


GOTO end

:assemble_fail
ECHO Error while assembling.
GOTO fail
:link_fail
ECHO Error while linking.
:fail

ECHO ==========================
ECHO Build failure."
ECHO ==========================

:end
PAUSE