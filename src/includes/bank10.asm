.incbin "unknown/bank10_0000.bin"

Art_NackExplosion:              ; $A370
.incbin "art/art_cmp_nack_explosion.bin"

.db 0

Art_InvincibilityStar:          ; $A546
.incbin "art/art_cmp_invincibility_star.bin"

.db 0, 0, 0, 0, 0, 0, 0

Art_Badnik_SpringTurtle:        ; $A586
.incbin "art/badnik/art_cmp_spring_turtle.bin"

.db 0, 0, 0, 0, 0, 0, 0, 0

Art_WaterSplash:                ; $A946
.incbin "art/art_cmp_water_splash.bin"


.db 0, 0, 0, 0, 0, 0, 0, 0
.db 0, 0, 0, 0, 0, 0, 0


Art_Spring:                     ; $AA86
.incbin "art/art_cmp_spring.bin"

.db 0

Art_MovingPlatform:             ; $AB06
.incbin "art/art_cmp_moving_platform.bin"

Art_SpringExtended:             ; $ABCA
.incbin "art/art_cmp_spring_extended.bin"

Art_LargeSpike:                 ; $AC8E
.incbin "art/art_cmp_large_spike.bin"

.db 0

Art_Numbers:                    ; $ACEA
.incbin "art/art_cmp_numbers.bin"

.db 0

Art_Unknown_B10_AF3A:           ; $AF3A
.incbin "art/art_cmp_unknown_b10_AF3A.bin"

.db 0

Art_Unknown_B10_AF5C:           ; $AF5C
.incbin "art/art_cmp_unknown_b10_AF5C.bin"

.db 0

Art_Unknown_B10_AF82:           ; $AF82
.incbin "art/art_cmp_unknown_b10_AF82.bin"

.db 0

Art_Unknown_B10_AFAA:           ; $AFAA
.incbin "art/art_cmp_unknown_b10_AFAA.bin"

.db 0

Art_Unknown_B10_AFD2:           ; $AFD2
.incbin "art/art_cmp_unknown_b10_AFD2.bin"

.db 0

Art_Unknown_B10_AFFA:           ; $AFFA
.incbin "art/art_cmp_unknown_b10_AFFA.bin"

.db 0

Art_Unknown_B10_B01C:           ; $B01C
.incbin "art/art_cmp_unknown_b10_B01C.bin"

.db 0

Art_Unknown_B10_B03C:           ; $B03C
.incbin "art/art_cmp_unknown_b10_B03C.bin"

.db 0, 0, 0, 0, 0, 0, 0

Art_Unknown_B10_B09C:           ; $B09C
.incbin "art/art_cmp_unknown_b10_B09C.bin"

Art_Unknown_B10_B0C5:           ; $B0C5
.incbin "art/art_cmp_unknown_b10_B0C5.bin"

.db 0, 0, 0, 0

Art_Unknown_B10_B304:           ; $B304
.incbin "art/art_cmp_unknown_b10_B304.bin"

.db 0

Art_Badnik_TPZ_Snake:           ; $B408
.incbin "art/badnik/art_cmp_tpz_snake.bin"

.db 0

Art_TPZ_Bubble:                 ; $B632
.incbin "art/art_cmp_tpz_bubble.bin"

.db 0, 0, 0, 0, 0, 0, 0, 0
.db 0

Art_Unknown_B10_BAD2:           ; $BAD2
.incbin "art/art_cmp_unknown_b10_BAD2.bin"

Art_Badnik_MechaHiyoko:         ; $BD26
.incbin "art/badnik/art_cmp_mecha_hiyoko.bin"

Art_NackSwitch:                 ; $BD9A
.incbin "art/art_cmp_nack_switch.bin"


.incbin "unknown/bank10_3e3c.bin"
