Logic_Sonic:                ; $8000
.dw Logic_Sonic_State00
.dw Logic_Sonic_State01
.dw Logic_Sonic_State00
.dw Logic_Sonic_State03
.dw Logic_Sonic_State04
.dw Logic_Sonic_State05     ; walk
.dw Logic_Sonic_State06
.dw Logic_Sonic_State07
.dw Logic_Sonic_State08
.dw Logic_Sonic_State09
.dw Logic_Sonic_State0A     ; jump
.dw Logic_Sonic_State0B
.dw Logic_Sonic_State0C
.dw Logic_Sonic_State0D
.dw Logic_Sonic_State0E
.dw Logic_Sonic_State0F
.dw Logic_Sonic_State10
.dw Logic_Sonic_State11
.dw Logic_Sonic_State12
.dw Logic_Sonic_State13
.dw Logic_Sonic_State14
.dw Logic_Sonic_State15
.dw Logic_Sonic_State16
.dw Logic_Sonic_State17
.dw Logic_Sonic_State00
.dw Logic_Sonic_State19
.dw Logic_Sonic_State1A
.dw Logic_Sonic_State1B
.dw Logic_Sonic_State1C
.dw Logic_Sonic_State1D
.dw Logic_Sonic_State1E
.dw Logic_Sonic_State1F
.dw Logic_Sonic_State20
.dw Logic_Sonic_State21
.dw Logic_Sonic_State22
.dw Logic_Sonic_State23
.dw Logic_Sonic_State24
.dw Logic_Sonic_State25
.dw Logic_Sonic_State26
.dw Logic_Sonic_State00 ; $27
.dw Logic_Sonic_State28
.dw Logic_Sonic_State00 ; $29
.dw Logic_Sonic_State2A
.dw Logic_Sonic_State2B
.dw Logic_Sonic_State2C
.dw Logic_Sonic_State2D
.dw Logic_Sonic_State2E
.dw Logic_Sonic_State2F
.dw Logic_Sonic_State30
.dw Logic_Sonic_State31
.dw Logic_Sonic_State32
.dw Logic_Sonic_State33
.dw LABEL_B12_880F      ; $34  FIXME: this points to code
.dw Logic_Sonic_State35
.dw Logic_Sonic_State36
.dw Logic_Sonic_State01 ; $37
.dw Logic_Sonic_State38
.dw Logic_Sonic_State00 ; $39
.dw Logic_Sonic_State3A
.dw Logic_Sonic_State3B
.dw Logic_Sonic_State3C
.dw Logic_Sonic_State3D
.dw Logic_Sonic_State3E
.dw Logic_Sonic_State3F
.dw Logic_Sonic_State40
.dw Logic_Sonic_State41
.dw Logic_Sonic_State42
.dw Logic_Sonic_State00 ; $43
.dw Logic_Sonic_State44
.dw Logic_Sonic_State45
.dw Logic_Sonic_State46
.dw Logic_Sonic_State47
.dw Logic_Sonic_State00 ; $48
.dw Logic_Sonic_State00 ; $49
.dw Logic_Sonic_State4A
.dw Logic_Sonic_State4B


Logic_Sonic_State00:        ; $8098
.db $FF, $01
    .dw $97BC
.db $FF, $00


Logic_Sonic_State01:        ; $809E
.db $3C, $5A
    .dw $98E8
.db $FE
    .db $3C, $5A
.db $FF, $0E
    .db $03
_Logic_Sonic_State01_j0:    ; $80A8
.db $FE
    .db $0F, $5B
.db $FE
    .db $0F, $5C
.db $FF, $0F
    .dw _Logic_Sonic_State01_j0
.db $FE
    .db $08, $5F
.db $FE
    .db $32, $5D
.db $FE
    .db $06, $5E
.db $FF, $0E
    .db $05
_Logic_Sonic_State01_j1:    ; $80BE
.db $FE
    .db $04, $52
.db $FE
    .db $04, $53
.db $FE
    .db $04, $54
.db $FE
    .db $04, $55
.db $FF, $0F
    .dw _Logic_Sonic_State01_j1
.db $FE
    .db $06, $5E
.db $FE
    .db $32, $5D
.db $FE
    .db $08, $5F
.db $FF, $00


LABEL_80D9:
    ld   a, ($D503)
    bit  2, a
    ret  z
    ld   hl, Logic_Sonic_State01
    ld   ($D50E), hl
    ld   (ix + $07), $01
    ret


Logic_Sonic_State19:        ; $80EA
.db $0C, $18
    .dw $A165
.db $FE
    .db $0C, $19
.db $FF, $00


Logic_Sonic_State03:        ; $80F3
.db $80, $16
    .dw $9945
.db $FF, $00


Logic_Sonic_State04:        ; $80F9
.db $E0, $56
    .dw $998C
.db $FF, $00


Logic_Sonic_State05:        ; $80FF
.db $FF, $05
    .dw $AC6E
    .dw $99B6
.db $FF, $00


Logic_Sonic_State06:        ; $8107
.db $FF, $0E
    .db $04
_Logic_Sonic_State06_j0:    ; $810A
.db $FF, $05
    .dw $AC6E
    .dw $9A7C
.db $FF, $0F
    .dw _Logic_Sonic_State06_j0
_Logic_Sonic_State06_j1:
.db $FF, $05
    .dw $ACDA
    .dw $9A7C
.db $FF, $07
    .dw _Logic_Sonic_State06_j1


Logic_Sonic_State07:        ; $811E
.db $08, $60
    .dw $9AAE
.db $FE
    .db $08, $61
.db $FE
    .db $E0, $62
.db $FF, $00


Logic_Sonic_State08:        ; $812A
.db $08, $60
    .dw $9AD9
.db $FE
    .db $08, $61
.db $FE
    .db $E0, $62
.db $FF, $00


Logic_Sonic_State09:        ; $8136
.db $FF, $05
    .dw $ACF0
    .dw $9B04
.db $FF, $00


Logic_Sonic_State1B:        ; $813E
.db $FF, $05
    .dw $ACF0
    .dw $9B10
.db $FF, $00


Logic_Sonic_State0A:        ; $8146
.db $FF, $05
    .dw $ACF0
    .dw $9B40
.db $FF, $05
    .dw $ACF0
    .dw $9B40
.db $FF, $00


Logic_Sonic_State0B:        ; $8154
.db $FF, $08
    .dw Logic_Sonic_State0B_Logic01
    .dw Logic_Sonic_State0B_Substate01
.db $FF, $0E
    .db $02
_Logic_Sonic_State08_j0:    ; $815D
.db $04, $10
    .dw $9B8E
.db $FE
    .db $04, $11
.db $FE
    .db $04, $12
.db $FE
    .db $04, $13
.db $FE
    .db $04, $14
.db $FE
    .db $04, $15
.db $FF, $0F
    .dw _Logic_Sonic_State08_j0
.db $FE
    .db $04, $10
.db $FE
    .db $04, $11
.db $FE
    .db $06, $12
.db $FE
    .db $06, $13
.db $FE
    .db $06, $14
.db $FE
    .db $08, $15
.db $FF, $00

Logic_Sonic_State0B_Substate01:     ; $8188
.db $E0, $0F
    .dw $9B8E
.db $FF, $07
    .dw Logic_Sonic_State0B_Substate01

Logic_Sonic_State0B_Logic01:        ; $8190
    ld   a, ($D3F0)
    rrca
    ret


Logic_Sonic_State1C:        ; $8195
.db $08, $10
    .dw $9BAB
.db $FE
    .db $08, $11
.db $FE
    .db $08, $12
.db $FE
    .db $08, $13
.db $FE
    .db $08, $14
.db $FE
    .db $08, $15
.db $FF, $00


Logic_Sonic_State0C:        ; $81AA
.db $FF, $05
    .dw $ADB7
    .dw Logic_vtable + $E4
.db $FF, $00


Logic_Sonic_State0D:        ; $81B2
.db $FF, $05
    .dw $ADB7
    .dw Logic_vtable + $EA
.db $FF, $00


Logic_Sonic_State13:        ; $81BA
.db $FF, $05
    .dw $ADB7
    .dw Logic_vtable + $E7
.db $FF, $00


Logic_Sonic_State14:        ; $81C2
.db $04, $10
    .dw $9CAD
.db $FE
    .db $04, $11
.db $FE
    .db $04, $12
.db $FE
    .db $04, $13
.db $FE
    .db $04, $14
.db $FE
    .db $04, $15
.db $FF, $00


Logic_Sonic_State0E:        ; $81D7
.db $08, $4D
    .dw $9C9C
.db $FE
    .db $08, $4E
.db $FE
    .db $08, $4F
.db $FE
    .db $08, $50
.db $FE
    .db $08, $51
.db $FE 
    .db $08, $4C 
.db $FF, $00

Logic_Sonic_State15:     ; $81EC
.db $06, $6A 
    .dw $9BC6
.db $FE
    .db $06, $6B
.db $FE
    .db $06, $6C
.db $FE
    .db $06 $6D
.db $FE
    .db $05 $6A
.db $FE
    .db $05 $6B
.db $FE
    .db $05 $6C
.db $FE
    .db $05 $6D
.db $FE
    .db $04 $6A
.db $FE
    .db $04 $6B
.db $FE
    .db $04 $6C
.db $FE
    .db $04 $6D
LABEL_B12_8211:
.db $FE
    .db $03 $6A
.db $FE
    .db $03 $6B
.db $FE
    .db $03 $6C
.db $FE
    .db $03 $6D
.db $FF $07
    .dw LABEL_B12_8211

Logic_Sonic_State1A:        ; $8221
.db $FF $01 
    .dw LABEL_B12_8243
.db $FF $0E
    .db $02 
.db $02 $52 
    .dw $9C06
.db $FE
    .db $02 $53
.db $FE
    .db $02 $54
.db $FE
    .db $02 $55
.db $FF $0F
    .dw LABEL_B12_8239
LABEL_B12_8239:
.db $FF $05
    .dw $ACDA
    .dw $9C06
.db $FF $07 
    .dw LABEL_B12_8239

LABEL_B12_8243:
    ld      hl, $D503
    set     7, (hl)
    set     1, (hl)
    ld      a, $1e
    ld      ($d3c2), a
    ret
    
Logic_Sonic_State0F:        ; $8250
.db $03 $57
    .dw $9C52
.db $FE
    .db $03 $58
.db $FE
    .db $03 $59
.db $FF $00

Logic_Sonic_State10:        ; $825C
.db $FF $05
    .dw $ACF0
    .dw $9C8A
.db $FF $00

Logic_Sonic_State11:        ; $8264
.db $FF $01
    .dw LABEL_B12_82D4
.db $04 $1B
    .dw $9D2B
.db $FE
    .db $04 $1F
.db $FE
    .db $04 $20
.db $FE
    .db $04 $21
.db $FE
    .db $04 $1B
.db $FE
    .db $04 $1C
.db $FE
    .db $04 $1F
.db $FE
    .db $04 $20
.db $FE
    .db $04 $21
.db $FE
    .db $04 $1C
.db $FE
    .db $04 $1B
.db $FE
    .db $04 $1C
LABEL_B12_828D:
.db $FF $09
    .db $36 $00
.db $07 $1B
    .dw $9D0D
.db $07 $1C
    .dw $9D0D
.db $FF $07 
    .dw LABEL_B12_828D

LABEL_B12_829D:
.db $FF $0E
    .db $02
.db $04 $1F
    .dw $9D2B
.db $FE 
    .db $04 $20
.db $FE 
    .db $04 $21
.db $FE 
    .db $04 $1B
.db $FE 
    .db $04 $1C
.db $FF $0F
    .dw $82A0
.db $FF $01
    .dw $82F8
.db $FF $01
    .dw $03D2
.db $FF $00

LABEL_B12_82BE:
.db $04 $1F
    .dw $9D2B
.db $FE
    .db $04 $20
.db $FE
    .db $04 $21
.db $FE
    .db $04 $1B
.db $FE
    .db $04 $1C
.db $04 $1C
    .dw $8306
.db $FF $00

LABEL_B12_82D4:
    ld    a, (CurrentLevel)
    cp    Level_ADZ 
    jr    nz, +
    ld    a, (CurrentAct)
    cp    Act_3 
    jr    z, ++

+:  ld    hl, $012C
    ld    ($D534),hl
    xor   a
    ld    ($D536),a
    ret
    
++: ld    hl, $1770
    ld    ($D534),hl
    xor   a
    ld    ($D536),a
    ret
    
LABEL_B12_82F8:
    call  LT_Engine_ChangeLevelMusic
    ld    (ix + Object.PowerUp), 0
    ld    hl, 1
    ld    ($dbe4),hl
    ret
    
    ld    hl,$828d
    ld    (Player + Object.LogicSeqPtr), hl
    ld    (ix + Object.FrameCounter), 1
    ret


Logic_Sonic_State12:        ; $8311
.db $04 $70
  .dw $A078
.db $FF $00

Logic_Sonic_State16:        ; $8317
.db $FF $01 
  .dw LABEL_B12_8325
LABEL_B12_831B:
.db $FF $05
  .dw $AEB2 
  .dw $BE2F
.db $FF $07
  .dw LABEL_B12_831B

LABEL_B12_8325:
    ld    hl,($d538)
    ld    a,($d537)
    cp    $ff
    jr    z,+
    ld    hl,$0000

+:  ld    ($d516),hl
    xor   a
    ld    ($d51e),a
    ld    ($d534),a
    ld    ($d535),a
    ld    ($d536),a
    ld    ($d537),a
    ld    hl,$0300
    ld    ($d518),hl
    ld    hl,($d511)
    ld    ($d53a),hl
    ret

Logic_Sonic_State17:              ; $8352
.db $08 $06 
  .dw $A590
.db $FE
  .db $08 $07
.db $FF $07 
  .dw Logic_Sonic_State17

Logic_Sonic_835D:
.db $08 $06 
  .dw $A5C4
.db $FE
  .db $08 $07
.db $FF $07
  .dw Logic_Sonic_835D

Logic_Sonic_8368:
.db $08 $06
  .dw $A60B
.db $FE
  .db $08 $06
.db $FF $07
  .dw Logic_Sonic_8368

Logic_Sonic_8373:
.db $FF $04
  .db $28
  .dw 0
  .dw 0
  .db $FE
.db $FF $04
  .db 7
  .dw 0
  .dw 0
  .db $00
Logic_Sonic_8383:
.db $10 $0D
  .dw $A64F
.db $FF $07
  .dw Logic_Sonic_8383

Logic_Sonic_838B:
.db $FF $00

Logic_Sonic_State25:          ; $838D
.db $FF $06
  .db $BC
.db $10 $67
  .dw LABEL_B12_8399
.db $FF $03
  .db $0E
.db $FF $00

LABEL_B12_8399:
    xor   a
    ld    ($D3CE), a
    ret

Logic_Sonic_State26:        ; $839E
.db $FF $01
  .dw LABEL_B12_83AD
LABEL_B12_83A2:
.db $07 $68
  .dw $9D06
.db $FE
  .db $07 $69
.db $FF $07
  .dw LABEL_B12_83A2

LABEL_B12_83AD:
    ld    hl, $012C
    ld    ($D534), hl
    xor   a
    ld    ($D536), a
    ret

Logic_Sonic_State1D:      ; $83B8
.db $FF $05 
  .dw $AD85
  .dw $9CBE
.db $FF $00 

Logic_Sonic_State1E:      ; $83C0
.db $FF $01
  .dw LABEL_B12_83CC
LABEL_B12_83C4:
.db $E0 $0D
  .dw $9CD6
.db $FF $07
  .dw LABEL_B12_83C4

LABEL_B12_83CC:
    ld    hl, $fc00
    ld    ($d518), hl
    res   1, (ix+$22)
    res   1, (ix+$23)
    ld    hl, ($d514)
    ld    de, $fffc
    add   hl, de
    ld    ($d514), hl
    ld    hl, ($d3cb)
    ld    a, h
    or    l
    jr    z, $22
    ld    a, (hl)
    cp    $22
    jr    z, 4
    cp    $2d
    jr    nz, $19
    push  hl
    pop   iy
    ld    (iy+2), $02
    ld    (iy+$1f), $ff
    ld    (iy+$19), $fe
    ld    (iy+$18), $00
    ld    hl, $0000
    ld    ($d3cb), hl
    ld    hl, ($d3c9)
    ld    a, h
    or    l
    ret   z
    ld    a, (hl)
    cp    $1b
    ret   nz
    ld    de, $0002
    add   hl, de
    ld    (hl), $02
    ld    hl, $0000
    ld    ($d3c9), hl
    ret


Logic_Sonic_State1F:            ; $8424
.db $FF $01
  .dw $843C
.db $FF $01
  .dw LT_LABEL_4CCE
LABEL_B12_842C:
.db $80 $0E
  .dw $A18D
.db $FF $07
  .dw LABEL_B12_842C 

LABEL_B12_8434:
    ld    hl, $fd80
    ld    ($d518), hl
    jr    6
    ld    hl, $fd00
    ld    ($d518), hl
    ld    hl, 0
    ld    ($d516), hl
    res   7, (ix+4)
    res   5, (ix+4)
    set   6, (ix+3)
    ret

Logic_Sonic_State28:         ; $8455
.db $FF $01 
  .dw $8434
.db $FF $01
  .dw $0312
LABEL_B12_845D:
.db $E0 $17
  .dw $A1D3
.db $FF $07
  .dw LABEL_B12_845D

Logic_Sonic_State2A:        ; $8465
.db $FF $01 
  .dw $BB86
LABEL_B12_8469:
.db $05 $52
  .dw $BB98
.db $FE
  .db $05 $53
.db $FE 
  .db $05 $54
.db $FE
  .db $05 $55
.db $FF $07
  .dw LABEL_B12_8469

Logic_Sonic_State2B:        ; $847A
.db $FF $05
  .dw $ACF0
  .dw $BBAB
.db $FF $00 

Logic_Sonic_State2C:        ; $8482
.db $06 $4C
  .dw $BBDC
.db $FE
  .db $06 $4D
.db $FE
  .db $06 $4E
.db $FE
  .db $06 $4F
.db $FE
  .db $06 $50
.db $FE
  .db $06 $51
.db $FF $00 

Logic_Sonic_State2D:        ; $8497
.db $FF $01
  .dw $BCB8
LABEL_B12_849B:
.db $E0 $0D
  .dw $BCC5
.db $FF $07
  .dw LABEL_B12_849B

Logic_Sonic_State2E:        ; $84A3
.db $1C $5A 
  .dw LT_ObjectLogic_DoNothing
.db $FF $0E
  .db $08
LABEL_B12_84AA:
.db $04 $57
  .dw LT_ObjectLogic_DoNothing
.db $FE
  .db $04 $58
.db $FE
  .db $04 $59
.db $FF $0F 
  .dw LABEL_B12_84AA
.db $FF $02
  .dw $0300
  .dw $FC00
LABEL_B12_84BE:
.db $06 $01
  .dw $BCF9
.db $FE
  .db $06 $02
.db $FE
  .db $06 $03
.db $FE
  .db $06 $04
.db $FE
  .db $06 $05
.db $FF $07
  .dw LABEL_B12_84BE

Logic_Sonic_State2F:            ; $84D2
.db $FF $01
  .dw $BD18
.db $30 $5A
  .dw $BD2B
.db $FE
  .db $0A $4C
.db $FE
  .db $0A $4D
.db $FE
  .db $0A $4C
.db $FE
  .db $0A $4D
.db $FE
  .db $08 $4E
.db $FE
  .db $08 $4F
.db $FE
  .db $07 $50
.db $FE
  .db $07 $51
.db $FF $03
  .db $2A
.db $FF $00 

Logic_Sonic_State20:          ; $84F7
.db $FF $05
  .dw $AC6E
  .dw LABEL_B12_84FF
.db $FF $00 

LABEL_B12_84FF:
  res 7, (ix+4)
  xor a
  ld (ix+24), a
  ld (ix+25), a
  res 4, (ix+4)
  ld de, ($d19a)
  ld l, (ix+17)
  ld h, (ix+18)
  xor a
  sbc hl, de
  ld de, $00f8
  ex de, hl
  xor a
  sbc hl, de
  jr nc, $18
  call $0312
  xor a
  ld hl, $0120
  sbc hl, de
  jr nc, $0d
  xor a
  ld (ix+22), a
  ld (ix+23), a
  ld hl, $d144
  set 4, (hl)
  ret

  ld a, (ix+1)
  cp $49
  jp z, $0381
  ld hl, ($d516)
  bit 7, h
  jr z, $06
  ld hl, $0000
  ld ($d516), hl
  ld a, h
  cp $06
  jr nc, $0a
  ld de, $0010
  add hl, de
  ld (ix+22), l
  ld (ix+23), h
  jp $0381


Logic_Sonic_State21:        ; $8563
.db $03 $01
  .dw $BD46
.db $FE
  .db $03 $02
.db $FE
  .db $03 $03
.db $FE
  .db $03 $04
.db $FE
  .db $03 $05
.db $FF $00

Logic_Sonic_State22:        ; $8575
.db $FF $01
  .dw $BD69
LABEL_B12_8579:
.db $03 $01
  .dw $BD4D
.db $FE
  .db $03 $02
.db $FE
  .db $03 $03
.db $FE
  .db $03 $04
.db $FE
  .db $03 $05
.db $FF $07 
  .dw LABEL_B12_8579

Logic_Sonic_State40:        ; $858D
.db $FF $01
  .dw $BDC6
.db $03 $01
  .dw $BDBB
.db $FE
  .db $03 $02
.db $FE
  .db $03 $01
.db $FE
  .db $03 $02
.db $E0 $01
  .dw LABEL_B12_85BD
.db $FF $00 


Logic_Sonic_85A4:
.db $FF $01
  .dw $BDBB
.db $03 $01
  .dw $0381
.db $FE
  .db $03 $02
LABEL_B12_85AF:
.db $03 $01
  .dw $BDF9
.db $FE
  .db $03 $02
.db $FE
  .db $03 $03
.db $FF $07
  .dw LABEL_B12_85AF


LABEL_B12_85BD:
    xor   a
    ld    ($D41B), a
    ld    (ix + Object.BgColFlags), a
    jp    $03BD


Logic_Sonic_State23:          ; $85C7
.db $FF $01
  .dw $A3CB
LABEL_B12_85CB:
.db $FF $05
  .dw $AF28
  .dw $A3E9
.db $FF $05
  .dw $AF55
  .dw $A3E9
.db $FF $07
  .dw LABEL_B12_85CB 

Logic_Sonic_State24:        ; $85DB
.db $FF $0E
  .db $02
LABEL_B12_85DE:
.db $03 $01
  .dw $A509
.db $FE
  .db $03 $02
.db $FE
  .db $03 $03
.db $FE
  .db $03 $04
.db $FE
  .db $03 $05
.db $FF $0F
  .dw LABEL_B12_85DE
.db $FF $01
  .dw $03D2
.db $FF $00 

Logic_Sonic_State30:        ; $85F8
.db $FF $01
  .dw LABEL_B12_8604
LABEL_B12_85FC:
.db $E0 $5A 
  .dw LABEL_B12_860E
.db $FF $07 
  .dw LABEL_B12_85FC

LABEL_B12_8604:
  ld    hl, 0
  ld    ($D516), hl
  ld    ($D518), hl
  ret

LABEL_B12_860E:
  xor a
  ld ($dbc0), a
  ld a, ($d139)
  and $30
  jp nz, $03ba
  ld a, ($d3c4)
  or a
  jp z, $03b7
  call $038d
  call $b7b6
  ld a, ($d502)
  cp $30
  ret nz
  ld a, $ff
  ld ($dbc0), a
  ret


Logic_Sonic_State31:        ; $8633
.db $FF $05
  .dw $0351
  .dw LABEL_B12_866E
LABEL_B12_8639:
.db $FF $06
  .db $C3
LABEL_B12_863C:
.db $04 $10
  .dw $A51C
.db $FE
  .db $04 $11
.db $FE
  .db $04 $12
.db $FE
  .db $04 $13
.db $FE
  .db $04 $14
.db $FE
  .db $04 $15
.db $FF $07
  .dw LABEL_B12_863C


Logic_Sonic_8653:
.db $FF $01
  .dw $A562
LABEL_B12_8657:
.db $04 $10
  .dw $A570
.db $FE
  .db $04 $11
.db $FE
  .db $04 $12
.db $FE
  .db $04 $13
.db $FE
  .db $04 $14
.db $FE
  .db $04 $15
.db $FF $07
  .dw LABEL_B12_8657


LABEL_B12_866E:
    ld    a, (CurrentLevel)
    add   a, a
    ld    hl, LABEL_B12_8685
    ld    d, 0
    ld    e, a
    add   hl, de
    ld    e, (hl)
    inc   hl
    ld    d, (hl)
    ld    (Player + Object.LogicSeqPtr), de
    ld    (ix + Object.FrameCounter), 1
    ret


LABEL_B12_8685:
.dw LABEL_B12_8639  
.dw Logic_Sonic_8653  
.dw LABEL_B12_8639  
.dw LABEL_B12_8639 
.dw LABEL_B12_8639 
.dw LABEL_B12_8639 


Logic_Sonic_State32:        ; $8691
.db $FF $01
  .dw $A667
LABEL_B12_8695:
.db $03 $01
  .dw $A672
.db $FE
  .db $03 $02
.db $FE
  .db $03 $03
.db $FE
  .db $03 $04
.db $FE
  .db $03 $05
.db $FF $07
  .dw LABEL_B12_8695


Logic_Sonic_86A9:
.db $FF $02
  .dw 0
  .dw $200
.db $02 $01
  .dw $0381
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $01
  .dw $A6C3
.db $02 $01
  .dw $0351
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $01
  .dw $A6A2
.db $FF $0E
  .db $02
LABEL_B12_86DA:
.db $02 $01
  .dw $0351
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $0F
  .dw LABEL_B12_86DA
.db $FF $01
  .dw $A6A8
.db $FF $02
  .dw 0 
  .dw $F700
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db 2
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db 3
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db $04
.db $02 $01
  .dw $0381
.db $FE
  .db $02 $02
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db $02
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db $03
.db $FF $04
  .db $39
  .dw 0
  .dw 0
  .db $04
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
LABEL_B12_8738:
.db $E0 $01
  .dw LABEL_B12_8740
.db $FF $07
  .dw LABEL_B12_8738 

LABEL_B12_8740:
    res   OBJ_F3_BIT6, (ix + Object.Flags03)
    jp    $03BD


Logic_Sonic_State33:      ; $8747
.db $02 $01 
  .dw LABEL_B12_879E
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw Logic_Sonic_State33 


Logic_Sonic_875B:
.db $FF $02
  .dw $FE80
  .dw $FC80
LABEL_B12_8761:
.db $02 $01
  .dw LABEL_B12_87AE
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8761 

LABEL_B12_8775:
.db $60 $5A 
  .dw $0351
.db $FF $06
  .db $B1
.db $FF $02
  .dw $0180
  .dw $FC80
LABEL_B12_8782:
.db $02 $01
  .dw LABEL_B12_87D7
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8782 


Logic_Sonic_8796:
.db $40 $5A
  .dw LABEL_B12_8807
.db $FF $07
  .dw Logic_Sonic_8796

LABEL_B12_879E:
  ld    a, ($d535)
  or    a
  ret   z
  ld    hl, $875b
  ld    ($d50e), hl
  ld    (ix+7), $01
  ret

LABEL_B12_87AE:
  ld hl,($d518)
  ld de,$0020
  add hl,de
  ld ($d518),hl
  call $0381
  bit 7,(ix+$19)
  ret nz
  call $0384
  bit 1,(ix+$22)
  ret z
  ld hl,$8775
  ld ($d50e),hl
  ld (ix+7),$01
  res 4,(ix+4)
  ret

LABEL_B12_87D7:
  ld hl,($d518)
  bit 7,h
  jr nz,$0c
  ld de,$0000
  ld ($d516),de
  ld a,h
  cp $06
  jr nc,$07
  ld de,$0020
  add hl,de
  ld ($d518),hl
  call $0381
  call $0384
  bit 1,(ix+$22)
  ret z
  ld hl,$8796
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8807:
  res 1,(ix+3)
  call $03d2
  ret

LABEL_B12_880F:             ; $880F
  ld a,(ix+4)
  xor $10
  ld (ix+4),a
  ret

Logic_Sonic_State35:        ; $8818
.db $FF $01
  .dw $A6CE
LABEL_B12_881C:
.db $04 $28
  .dw $A6D7
.db $FE
  .db $04 $29
.db $FE
  .db $04 $2A
.db $FE
  .db $04 $2B
.db $FE
  .db $04 $2C
.db $FE
  .db $04 $2D
.db $FE
  .db $04 $2E
.db $FF $07
  .dw LABEL_B12_881C

Logic_Sonic_State36:          ; $8836
.db $FF $01
  .dw $A70A
LABEL_B12_883A:
.db $04 $28
  .dw $A713
.db $FE
  .db $04 $29
.db $FE
  .db $04 $2A
.db $FE
  .db $04 $2B
.db $FE
  .db $04 $2C
.db $FE
  .db $04 $2D
.db $FF $07
  .dw LABEL_B12_883A

Logic_Sonic_State3D:          ; $8851
.db $FF $01
  .dw $A746
LABEL_B12_8855:
.db $08 $10
  .dw $A752
.db $FE
  .db $08 $11
.db $FE
  .db $08 $12
.db $FE
  .db $08 $13
.db $FE
  .db $08 $14
.db $FE
  .db $08 $15
.db $FF $07
  .dw LABEL_B12_8855

Logic_Sonic_State3E:          ; $886C
.db $FF $06
  .db $B4
.db $FF $01
  .dw $A76D
LABEL_B12_8873:
.db $02 $01
  .dw $A782
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8873

Logic_Sonic_State3F:          ; $8887
.db $FF $01
  .dw $A7D9
LABEL_B12_888B:
.db $E0 $0D
  .dw $A800
.db $FF $07
  .dw LABEL_B12_888B 


Logic_Sonic_State38:          ; $8893
.db $E0 $01
  .dw LABEL_B12_88B0
.db $FF $07
  .dw Logic_Sonic_State38 

Logic_Sonic_889B: 
.db $FF $01
  .dw LABEL_B12_88C2
LABEL_B12_889F:
.db $04 $52
  .dw LABEL_B12_88D6
.db $FE
  .db $04 $53
.db $FE
  .db $04 $54
.db $FE
  .db $04 $55
.db $FF $07
  .dw LABEL_B12_889F 

LABEL_B12_88B0:
  ld a,(ix+$34)
  or a
  ret z
  ld (ix+$0f),$88
  ld (ix+$0e),$9b
  ld (ix+7),$01
  ret

LABEL_B12_88C2:
  call $030f
  ld (ix+$17),$03
  ld (ix+$16),$00
  ld (ix+$15),$00
  ld (ix+$14),$ae
  ret

LABEL_B12_88D6:
  ld h,(ix+$17)
  ld l,(ix+$16)
  inc hl
  ld (ix+$17),h
  ld (ix+$16),l
  call $0381
  ld d,(ix+$12)
  ld e,(ix+$11)
  ld hl,$0600
  xor a
  sbc hl,de
  jr c,$0b
  ld hl,$0500
  xor a
  sbc hl,de
  ret nc
  call $0312
  ret

LABEL_B12_88FF:
  ld (ix+0),$02
  ld (ix+2),$39
  ret

Logic_Sonic_State3A:                ; $8908
.db $1C $00
  .dw $0351
.db $FF $01
  .dw LABEL_B12_893E
.db $FF $0E
  .db $05
LABEL_B12_8913:
.db $03 $52
  .dw $0381
.db $FE
  .db $03 $53
.db $FE
  .db $03 $54
.db $FE
  .db $03 $55
.db $FF $0F
  .dw LABEL_B12_8913
.db $FF $0B
  .db $04
  .db $EF
.db $FF $02
  .dw $0400
  .dw 0
LABEL_B12_892E:
.db $FE
  .db $03 $52
.db $FE
  .db $03 $53
.db $FE
  .db $03 $54
.db $FE
  .db $03 $55
.db $FF $07
  .dw LABEL_B12_892E


LABEL_B12_893E:
  set 4,(ix+4)
  ld hl,$fc00
  ld ($d516),hl
  ld a,$f0
  ld ($d511),a
  ld a,$70
  ld ($d514),a
  ret

Logic_Sonic_State3B:        ; $8953
.db $0C $5A
  .dw $0351
.db $FF $01
  .dw LABEL_B12_898D
.db $FE
  .db $0C $5A
.db $FF $01
  .dw LABEL_B12_89A9
.db $FE
  .db $0C $5A
.db $FF $01
  .dw LABEL_B12_89B9
.db $FF $0E
  .db $02
LABEL_B12_896C:
.db $FE
  .db $0A $5B
.db $FE
  .db $0A $5C
.db $FF $0F
  .dw LABEL_B12_896C
.db $FF $02
  .dw $0300
  .dw $0000
LABEL_B12_897C:
.db $03 $52
  .dw LABEL_B12_89C5
.db $FE
  .db $03 $53
.db $FE
  .db $03 $54
.db $FE
  .db $03 $55
.db $FF $07
  .dw LABEL_B12_897C


LABEL_B12_898D:
  ld hl,$0080
  ld ($d511),hl
  ld hl,$0090
  ld ($d514),hl
  di
  push ix
  ld a,($d145)
  add a,a
  add a,$05
  call $044d
  pop ix
  ei
  ret

LABEL_B12_89A9:
  di
  push ix
  ld a,($d145)
  add a,a
  add a,$06
  call $044d
  pop ix
  ei
  ret

LABEL_B12_89B9:
  di
  push ix
  ld a,$04
  call $044d
  pop ix
  ei
  ret

LABEL_B12_89C5:
  call    $0381
  ret

Logic_Sonic_State3C:          ; $89C9
.db $FF $01
  .dw LABEL_B12_8A50
LABEL_B12_89CD:
.db $FF $04
  .db $03
  .dw 0000
  .dw $FFF8
  .db $FF
.db $02 $49
  .dw LABEL_B12_8A8B
.db $FF $04
  .db $03
  .dw 0
  .dw 0
  .db $FF
.db $FE
  .db $02 $4A
.db $FF $04
  .db $03
  .dw 0
  .dw 8
  .db $FF
.db $FE
  .db $02 $49
.db $FF $04
  .db $03
  .dw 0
  .dw 0
  .db $FF
.db $FE
  .db $02 $4A
.db $FF $07
  .dw LABEL_B12_89CD

LABEL_B12_89FE:
.db $FF $06
  .db $C8
.db $10 $00
  .dw $0351
.db $FF $01
  .dw LABEL_B12_8A69
LABEL_B12_8A09:
.db $FF $04
  .db $03
  .dw 0
  .dw $FFF8
  .db $FF
.db $02 $52
  .dw LABEL_B12_8A8B
.db $FF $04
  .db $03
  .dw 0
  .dw 0
  .db $FF
.db $FE
  .db $02 $53
.db $FF $04
  .db $03
  .dw 0
  .dw 8
  .db $FF
.db $FE
  .db $02 $54
.db $FF $04
  .db $03 
  .dw 0
  .dw 0
  .db $FF
.db $FE
  .db $02 $55
.db $FF $07
  .dw LABEL_B12_8A09

LABEL_B12_8A3A:
.db $20 $00
  .dw $0351
.db $FF $01
  .dw LABEL_B12_8ABF
.db $FE
  .db $20 $00
.db $FF $01
  .dw LABEL_B12_8ADF
LABEL_B12_8A49:
.db $FE
  .db $E0 $00
.db $FF $07
  .dw LABEL_B12_8A49

LABEL_B12_8A50:
  ld hl,$0000
  ld ($d518),hl
  ld hl,$0800
  ld ($d516),hl
  ld hl,$0030
  ld ($d511),hl
  ld hl,$0088
  ld ($d514),hl
  ret

LABEL_B12_8A69:
  ld a,$01
  ld (ix+0),a
  set 4,(ix+4)
  ld hl,$0000
  ld ($d518),hl
  ld hl,$f800
  ld ($d516),hl
  ld hl,$00d4
  ld ($d511),hl
  ld hl,$0090
  ld ($d514),hl
  ret

LABEL_B12_8A8B:
  call $0381
  ld a,(ix+0)
  cp $01
  jr z,$15
  ld hl,($d511)
  ld de,$00f0
  xor a
  sbc hl,de
  ret c
  ld hl,$89fe
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8AAA:
  ld hl,($d511)
  ld de,$0020
  xor a
  sbc hl,de
  ret nc
  ld hl,$8a3a
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8ABF:
  di
  push ix
  ld a,$00
  call $044d
  ld a,$02
  call $044d
  ld a,$01
  call $044d
  ld a,$03
  call $044d
  pop ix
  ei
  ld a,$b6
  ld ($de05),a
  ret

LABEL_B12_8ADF:
  ld a,$0b
  ld ($d490),a
  ret

Logic_Sonic_State41:          ; $8AE5
.db $FF $01 
  .dw $A84D
LABEL_B12_8AE9:
.db $0C $6E 
  .dw $A862
.db $FE 
  .db $0C $6F
.db $FF $07
  .dw LABEL_B12_8AE9

Logic_Sonic_State42:          ; $8AF4
.db $10 $73
  .dw $A81E
.db $FF $01
  .dw $A824
.db $FF $0E
  .db $03
LABEL_B12_8AFF:
.db $06 $10
  .dw $A832
.db $FF $04
  .db $31
  .dw $FFF8
  .dw $0030
  .db $FD
.db $FE
  .db $06 $11
.db $FF $04
  .db $31
  .dw 0
  .dw $30
  .db $FD
.db $FE
  .db $06 $12
.db $FF $04
  .db $31
  .dw 8
  .dw $30
  .db $FD
.db $FE
  .db $06 $13
.db $FF $04
  .db $31
  .dw $FFF8
  .dw $30
  .db $FD
.db $FE
  .db $06 $14
.db $FF $04
  .db $31
  .dw 0
  .dw $30
  .db $FD
.db $FE
  .db $06 $15
.db $FF $04
  .db $31 
  .dw $FFF8
  .dw $30
  .db $FD
.db $FF $0F
  .dw LABEL_B12_8AFF
.db $E0 $15
  .dw $A84A
.db $FF $00

Logic_Sonic_State44:        ; $8B4C
.db $FF $02
  .dw 0
  .dw 0
.db $3C $5A 
  .dw $0351
.db $FF $01
  .dw $A8D2
.db $FF $01
  .dw $A915
LABEL_B12_8B5E:
.db $FF $05
  .dw $ACDA
  .dw $A91A
.db $FF $07
  .dw LABEL_B12_8B5E

LABEL_B12_8B68:
.db $E0 $16
  .dw $0351
.db $FF $07
  .dw LABEL_B12_8B68

LABEL_B12_8B70:
.db $FF $01
  .dw $A953
LABEL_B12_8B74:
.db $FF $05
  .dw $ACDA
  .dw $A95A
.db $FF $07
  .dw LABEL_B12_8B74

LABEL_B12_8B7E:
.db $E0 $16
  .dw $0351
.db $FF $07
  .dw LABEL_B12_8B7E

LABEL_B12_8B86:
.db $FF $02
  .dw 0
  .dw 0
LABEL_B12_8B8C:
.db $FF $05
  .dw $ACDA
  .dw $A993
.db $FF $07
  .dw LABEL_B12_8B8C

LABEL_B12_8B96:
.db $E0 $16
  .dw $0351
.db $FF $07
  .dw LABEL_B12_8B96


Logic_Sonic_State45:          ; $8B9E
.db $FF $01
  .dw LABEL_B12_8BE2
.db $FF $0C
  .db $04
  .db $10
.db $FF $01
  .dw LABEL_B12_8C81
LABEL_B12_8BAA:
.db $FF $05
  .dw $AC6E
  .dw LABEL_B12_8BEF
.db $FF $07
  .dw LABEL_B12_8BAA

LABEL_B12_8BB4:
.db $17 $5A 
  .dw LABEL_B12_8C03
.db $FE
  .db $17 $5B
.db $FE
  .db $17 $5C
.db $FF $07
  .dw LABEL_B12_8BB4
.db $FF $0B
  .db $04
  .db $EF
.db $FF $01
  .dw LABEL_B12_8C88
LABEL_B12_8BCA:
.db $FF $05
  .dw $AC6E
  .dw LABEL_B12_8C38
.db $FF $07
  .dw LABEL_B12_8BCA

LABEL_B12_8BD4:
.db $17 $5A
  .dw LABEL_B12_8C4C
.db $FE
  .db $17 $5B
.db $FE
  .db $17 $5C
.db $FF $07
  .dw LABEL_B12_8BD4

LABEL_B12_8BE2:
  ld hl,$0064
  ld ($d511),hl
  ld hl,$00a0
  ld ($d514),hl
  ret

LABEL_B12_8BEF:
  call $0381
  ld a,(ix+$11)
  cp $64
  ret nc
  ld hl,$8bb4
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8C03:
  ld a,($d139)
  ld b,a
  ld hl,$dbee
  bit 0,b
  jr z,$8
  ld a,(hl)
  cp $80
  jr z,$0d
  dec (hl)
  jr $0a
  bit 1,b
  jr z,$06
  ld a,(hl)
  cp $a2
  jr z,$01
  inc (hl)
  ld a,b
  and $30
  jr z,$05
  ld a,(hl)
  ld ($de06),a
  ret

LABEL_B12_8C2A:
  bit 3,b
  ret z
  ld hl,$8bc2
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8C38:
  call $0381
  ld a,(ix+$11)
  cp $a4
  ret c
  ld hl,$8bd4
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8C4C:
  ld a,($d139)
  ld b,a
  ld hl,$dbef
  bit 0,b
  jr z,$08
  ld a,(hl)
  cp $b0
  jr z,$0d
  dec (hl)
  jr $0a
  bit 1,b
  jr z,$06
  ld a,(hl)
  cp $dd
  jr z,$01
  inc (hl)
  ld a,b
  and $30
  jr z,$05
  ld a,(hl)
  ld ($de06),a
  ret

LABEL_B12_8C73:
  bit 2,b
  ret z
  ld hl,$8ba2
  ld ($d50e),hl
  ld (ix+7),$01
  ret

LABEL_B12_8C81:
  ld hl,$fe00
  ld ($d516),hl
  ret

LABEL_B12_8C88:
  ld hl,$0200
  ld ($d516),hl
  ret

Logic_Sonic_State46:        ; $8C8F
.db $FF $01 
  .dw $A9C4
LABEL_B12_8C93:
.db $02 $01
  .dw $A9EE
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8C93

LABEL_B12_8CA7:
.db $FF $05
  .dw $ACDA
  .dw $AA00
.db $FF $07
  .dw LABEL_B12_8CA7

LABEL_B12_8CB1:
.db $78 $5A
  .dw $0351
.db $FF $02
  .dw $0280
  .dw $FC00
LABEL_B12_8CBB:
.db $02 $01
  .dw $AA39
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8CBB

LABEL_B12_8CCF:
.db $FF $06
  .db $C1
LABEL_B12_8CD2:
.db $02 $01
  .dw $AA79
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8CD2

LABEL_B12_8CE6:
.db $FF $02
  .dw 0
  .dw 0
.db $80 $5A
  .dw $0351
LABEL_B12_8CF0:
.db $FF $05
  .dw $ACDA
  .dw $AAB7
.db $FF $07
  .dw LABEL_B12_8CF0

LABEL_B12_8CFA:
.db $FF $06
  .db $A1
.db $FF $02
  .dw $0180
  .dw $FC00
LABEL_B12_8D03:
.db $02 $01
  .dw $AAE6
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8D03

LABEL_B12_8D17:
.db $40 $5A
  .dw $0351
.db $FF $01
  .dw $AB41
LABEL_B12_8D1F:
.db $FF $05
  .dw $ACDA
  .dw $AB1E
.db $FF $07
  .dw LABEL_B12_8D1F

LABEL_B12_8D29:
.db $FF $02
  .dw $80
  .dw $FD00
LABEL_B12_8D2F:
.db $02 $01
  .dw $AB56
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8D2F

LABEL_B12_8D43:
.db $E0 $5A
  .dw $ABAE
.db $FF $07
  .dw LABEL_B12_8D43

LABEL_B12_8D4B:
.db $20 $5A
  .dw $0351
LABEL_B12_8D4F:
.db $FF $05
  .dw $AC6E
  .dw $ABDF
.db $FF $07
  .dw LABEL_B12_8D4F

LABEL_B12_8D59:
.db $1E $71
  .dw $0351
.db $FE
  .db $3C $72
LABEL_B12_8D60:
.db $C0 $72
  .dw $AC0C
.db $FF $07
  .dw LABEL_B12_8D60

LABEL_B12_8D68:
.db $10 $5A
  .dw $0351
.db $FF $02
  .dw $0200
  .dw 0
LABEL_B12_8D72:
.db $FF $05
  .dw $ACDA
  .dw $AC2C
.db $FF $07
  .dw LABEL_B12_8D72

LABEL_B12_8D7C:
.db $FF $02
  .dw $80
  .dw $FD00
LABEL_B12_8D82:
.db $02 $01
  .dw $AC51
.db $FE
  .db $02 $02
.db $FE
  .db $02 $03
.db $FE
  .db $02 $04
.db $FE
  .db $02 $05
.db $FF $07
  .dw LABEL_B12_8D82


Logic_Sonic_State47:        ; $8D96
.db $FF $01
  .dw LABEL_B12_8DC8
LABEL_B12_8D9A:
.db $15 $5A
  .dw LABEL_B12_8DE1
.db $FE
  .db $15 $5B
.db $FE
  .db $15 $5C
.db $FF $07
  .dw LABEL_B12_8D9A

LABEL_B12_8DA8:
.db $FF $05
  .dw $ACDA
  .dw $8E34
.db $FF $07
  .dw LABEL_B12_8DA8

LABEL_B12_8DB2:
.db $80 $0E
  .dw LABEL_B12_8DBA
.db $FF $07
  .dw LABEL_B12_8DB2

LABEL_B12_8DBA:
  ld hl,($d518)
  ld de,$0030
  add hl,de
  ld ($d518),hl
  call $0381
  ret

LABEL_B12_8DC8:
  res 4,(ix+4)
  set 3,(ix+3)
  xor a
  ld ($d536),a
  ld hl,$0080
  ld ($d511),hl
  ld hl,$00a0
  ld ($d514),hl
  ret

LABEL_B12_8DE1:
  ld a,($d539)
  inc a
  jr z,$2C
  ld a,($d139)
  and $b0
  ret z
  ld hl,$8da8
  ld a,($d500)
  cp $01
  jr z,$03
  ld hl,$974b
  ld ($d50e),hl
  ld (ix+7),$01
  ld hl,$0200
  ld ($d516),hl
  ld hl,$0000
  ld ($d518),hl
  ld a,$ff
  ld ($d536),a
  ret

LABEL_B12_8E13:
  ld hl,$8db2
  ld a,($d500)
  cp $01
  jr z,$03
  ld hl,$9755
  ld ($d50e),hl
  ld (ix+7),$01
  ld hl,$0000
  ld ($d516),hl
  ld hl,$fc00
  ld ($d518),hl
  ret

LABEL_B12_8E34:
  call $0381
  ld (ix+$37),$ff
  ret

Logic_Sonic_State4A:        ; $8E3C
.db $FF $01
  .dw LABEL_B12_8E58
.db $FF $02
  .dw 0
  .dw 0
LABEL_B12_8E46:
.db $FF $05
  .dw $AC6E
  .dw LABEL_B12_8E68
.db $FF $07 
  .dw LABEL_B12_8E46

LABEL_B12_8E50:
.db $3C $5A
  .dw LABEL_B12_8E5D
.db $FF $07
  .dw LABEL_B12_8E50

LABEL_B12_8E58:
  res 4, (ix+4)
  ret

LABEL_B12_8E5D:
  call $b42c
  ld a,($dbd7)
  or a
  ret nz
  jp $040e

LABEL_B12_8E68:
  call $b42c
  ld hl,($d518)
  ld de,$0010
  add hl,de
  ld ($d518),hl
  call $0381
  call $0384
  bit 1,(ix+$22)
  ret z
  ld hl,$8e50
  ld a,($d500)
  cp $01
  jr z,$03
  ld hl,$979e
  ld ($d50e),hl
  ld (ix+7),$01
  ret


Logic_Sonic_State4B:        ; $8E95
.db $0D $16
  .dw $0351
.db $FF $02
  .dw 0
  .dw $FC00
LABEL_B12_8E9F:
.db $1E $0D
  .dw $0381
.db $E0 $0D
  .dw LABEL_B12_8EAB
.db $FF $07
  .dw LABEL_B12_8E9F

LABEL_B12_8EAB:
  ld    hl, $D144
  set   4, (hl)
  ret


Logic_Tails:            ; $8EB1
.incbin "unknown/bank12.bin"

