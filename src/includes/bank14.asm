.incbin "unknown/bank14_1.bin"

.include "src/level_cam_headers.asm"

.db $00, $00, $00, $00

.incbin "unknown/bank14_2.bin"

.include "src/titlecard_palettes.asm"

Palette_Update:		;$B4D3
	;decrement the palette fade counter and compare
	;it to the expiry value
	ld   hl, PaletteFadeTimer
	inc  (hl)
	ld   a, (PaletteTimerExpiry)
	cp   (hl)
	;return if counter <= expiry value
	ret  nc

	;reset the counter 
	ld   (hl), $00
	ld   ix, BgPaletteControl
	ld   iy, WorkingCRAM
	ld   b, $02			;loop twice

	;save loop count
-:	push bc
		;save CRAM pointer
		push iy
			;check the palette control byte
			ld   a, (ix+$00)
			or   a
			;dont update palette if control byte == 0
			jr   z, +

			;update the palette in work RAM and flag
			;for a copy to the VDP
			call Palette_UpdateColours
			ld   a, $FF
			ld   (PaletteUpdatePending), a

			;move to the fg palette control byte
+:			ld   de, $0002
			add  ix, de
		
		;move to the fg palette (in work RAM)
		pop  iy
		ld   de, PaletteSize_Bytes
		add  iy, de
	pop  bc
	djnz -

	ld   hl, (RAM_D3E7)
	ld   a, h
	or   l
	ret  z

	ld   ix, $D4FA
	ld   iy, $DC58
	ld   b, $02
-:	push bc
		push iy
			ld   a, (ix+$00)
			or   a
			jr   z, +
			
			call Palette_UpdateColours
			ld   a, $FF
			ld   (RAM_D4F5), a
			
+:			ld   de, $0002
			add  ix, de
		pop  iy
	ld   de, PaletteSize_Bytes
	add  iy, de
	pop  bc
	djnz -
	ret

;---------- function start ----------
; (middle level)
Palette_UpdateColours:	;$B539
	bit  7, a
	jr   nz, Palette_FadeToColour
	bit  6, a
	jp   nz, Palette_FadeFromColour
	bit  5, a
	jr   nz, Palette_Reset
	ret

Palette_Reset:		;$B547
	call Palette_CalculateOffset
	;copy the palette data into work ram
	push iy
	pop  de
	ld   bc, PaletteSize_Bytes
	ldir
	;clear the palette control byte
	ld   (ix+$00), $00
	ret

Palette_FadeToColour:		;$B557
	;check the fade direction (from white or from black)
	bit  4, a
	jp   nz, Palette_FadeWhiteToColour
	
	;fade from black
	;FIXME: move this call before the jump to save some space
	call Palette_CalculateOffset
	
	;loop over all 16 colours in the palette
	ld   b, $10
	
-:	push bc
		;colour fade step value?
		ld   a, (ix+$00)
		and  $0F
		ld   c, a
		
		;read a byte from the palette
		ld   a, (hl)
		;extract the green component
		and  $F0
		rrca
		rrca
		rrca
		rrca
		;flip the lower 4 bits
		xor  $0F
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		;rotate the green component back into position
+:		rlca
		rlca
		rlca
		rlca
		and  $F0
		;store the colour value
		ld   (iy+$00), a
		
		;read the byte from the palette
		ld   a, (hl)
		;extract the red component
		and  $0F
		;flip the bits
		xor  $0F
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		;OR the red component with the blue component
+:		and  $0F
		or   (iy+$00)
		;store the colour value
		ld   (iy+$00), a

		;move to the next byte in the palette
		inc  hl
		;read the byte from the palette
		ld   a, (hl)
		;extract the blue component
		and  $0F
		xor  $0F
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		;store the colour value
+:		and  $0F
		ld   (iy+$01), a
		
		;move the colour storage pointer
		ld   de, $0002
		add  iy, de
		
		;move to the next colour in the palette
		inc  hl
	pop  bc
	djnz -
	
	;increment the fade step value
	inc  (ix+$00)
	ld   a, (ix+$00)
	and  $1F
	cp   $10
	ret  nz

	ld   (ix+$00), $00
	ret

Palette_FadeFromColour:		;$B5BD
	;check the direction flag (to black or to white)
	bit  4, a
	jp   nz, _LABEL_3B689_102
	
	;fade to black
	;FIXME: move this before the jump
	call Palette_CalculateOffset
	
	;loop over 16 colour entries
	ld   b, $10

	;read the red component from RAM
-:	ld   a, (iy+$00)
		and  $0F
		ld   c, a
		;read the green component from RAM
		ld   a, (iy+$00)
		and  $F0
		jr   z, +
		;fade the green component
		sub  $10
		and  $F0
		;recombine with the red component
		or   c
		;store in work RAM
		ld   (iy+$00), a

		;read the green component from RAM
+:		ld   a, (iy+$00)
		and  $F0
		ld   c, a
		;read the red component from RAM
		ld   a, (iy+$00)
		and  $0F
		jr   z, +
		;fade the red component
		dec  a
		;recombine with the green component
		or   c
		;store in work RAM
		ld   (iy+$00), a
		
		;read the blue component from RAM
+:		ld   a, (iy+$01)
		and  $0F
		jr   z, +
		;fade the blue component
		dec  a
		;store in work RAM
		ld   (iy+$01), a
		
		;move to the next colour entry
+:		ld   de, $0002
		add  iy, de
	djnz -
	
	inc  (ix+$00)
	ld   a, (ix+$00)
	and  $0F
	cp   $0F
	ret  nz

	ld   (ix+$00), $00
	ret

Palette_FadeWhiteToColour:		;$B610
	call Palette_CalculateOffset
	;loop over 16 colour entries
	ld   b, $10

	;read the palette control byte
-:	ld   a, (ix+$00)
	;extract the fade step value
	and  $0F
	ld   c, a
	
	push bc
		;read a byte from the palette
		ld   a, (hl)
		;extract the red component
		and  $0F
		ld   c, a
		;read a colour byte out of work RAM
		ld   a, (iy+$00)
		ld   b, a
		;extract the green component
		and  $F0
		ld   d, a
		;extract the red component
		ld   a, b
		and  $0F
		;compare the red component with the value
		;from the palette
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $01
		and  $0F
		;recombine with the green component
		or   d
		;store to work RAM
		ld   (iy+$00), a
		
		;read a byte from the palette
+:		ld   a, (hl)
		;extract the green component
		and  $F0
		ld   c, a
		;read a colour byte from work RAM
		ld   a, (iy+$00)
		ld   b, a
		;extract the red component
		and  $0F
		ld   d, a
		;extract the green component
		ld   a, b
		and  $F0
		;compare the green component with the value
		;from the palette
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $10
		and  $F0
		;recombine with the red component
		or   d
		;store to work RAM
		ld   (iy+$00), a
		
		;move to the next byte in the palette
+:		inc  hl
		;read a byte from the palette
		ld   a, (hl)
		;extract the blue component
		and  $0F
		ld   c, a
		;read a byte from work RAM
		ld   a, (iy+$01)
		ld   b, a
		
		and  $00	;FIXME: use "xor a"
		ld   d, a
		;compare the blue component with the value
		;from the palette
		ld   a, b
		and  $0F
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $01
		and  $0F
		or   d
		;store to work RAM
		ld   (iy+$01), a

		;increment the palette and work RAM pointers
+:		inc  iy
		inc  iy
		inc  hl
	pop  bc
	djnz -
	
	ld   a, (ix+$00)
	ld   c, a
	and  $F0
	ld   b, a
	ld   a, c
	and  $0F
	inc  a
	cp   $10
	jr   z, +
	
	or   b
	ld   (ix+$00), a
	ret

+:	ld   (ix+$00), $00
	ret

_LABEL_3B689_102:
	call Palette_CalculateOffset
	
	;loop over 16 colour entries
	ld   b, $10
	
	-:	push bc
			;read a colour byte from RAM
			ld   a, (iy+$00)
			ld   b, a
			;extract the green component
			and  $F0
			ld   c, a
			;extract the red component
			ld   a, b
			and  $0F
			;is the red component at full intensity?
			cp   $0F
			;jump if yes
			jr   z, +
			;increase the red intensity
			add  a, $01
			and  $0F
			;recombine with the red component
+:			or   c
			;store in work RAM
			ld   (iy+$00), a
			
			;read a colour byte from RAM
			ld   a, (iy+$00)
			ld   b, a
			;extract the red component
			and  $0F
			ld   c, a
			;extract the green component
			ld   a, b
			and  $F0
			;is the green component at full intensity?
			cp   $F0
			;jump if yes
			jr   z, +
			;increase the green intensity
			add  a, $10
			and  $F0
			;recombine with the red component
+:			or   c
			;store in work RAM
			ld   (iy+$00), a
			
			;read a colour byte from RAM
			ld   a, (iy+$01)
			and  $0F
			;is the blue component at full intensity?
			cp   $0F
			;jump if yes
			jr   z, +
			;increase red intensity
			add  a, $01		;FIXME: inc  a
			and  $0F
			;store in work RAM
+:			ld   (iy+$01), a

			;increment the RAM pointer
			ld   de, $0002
			add  iy, de
		pop  bc
	djnz -

	ld   a, (ix+$00)
	ld   b, a
	and  $F0
	ld   c, a
	ld   a, b
	inc  a
	and  $0F
	or   c
	ld   (ix+$00), a
	and  $0F
	cp   $0F
	ret  nz

	ld   (ix+$00), $00
	ret


Palette_CalculateOffset:		;$B6EC
	ld   a, (ix+$01)		;get the palette index
	ld   l, a				;calculate the address for the palette
	ld   h, $00
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	ld   de, Palettes
	add  hl, de
	ret


Palettes:
; Data from 3B6FC to 3BFFF (2308 bytes)
;.incbin "stt.gg.dat.ED"
.include "src/palettes.asm"

.org $3F1C
;level palettes
DATA_B14_BF1C:
;Great Turquoise Zone
.db $1E, $0F
.db $1E, $0F
.db $1E, $0F
;Sunset Park Zone
.db $1F, $10
.db $1F, $10
.db $1F, $10
;Meta Junglira Zone
.db $20, $11
.db $20, $11
.db $20, $11
;Robotnik Winter Zone
.db $21, $12
.db $21, $12
.db $21, $12
;Tidal Plant Zone
.db $22, $13
.db $22, $13
.db $29, $13
;Atomic Destroyer Zone
.db $23, $14
.db $24, $14
.db $25, $14

.db $26, $1D
.db $35, $1D
.db $36, $1D

.db $0C, $1B
.db $0C, $1B
.db $0C, $1B

.db $2B, $2F
.db $2C, $30
.db $2B, $2F

.db $26, $1D
.db $26, $1D
.db $26, $1D

.db $1E, $0F
.db $1E, $0F
.db $1E, $0F

;underwater palettes
DATA_B14_BF5E:
.db $3A, $3B
.db $3A, $3B
.db $3C, $3D
.db $3A, $3B
.db $3E, $3F
.db $3A, $3B
