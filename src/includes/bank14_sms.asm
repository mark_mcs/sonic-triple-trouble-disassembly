.incbin "unknown/bank14_1.bin"

.include "src/level_cam_headers.asm"

.db $00, $00, $00, $00

.incbin "unknown/bank14_2.bin"

.include "src/titlecard_palettes_sms.asm"

Palette_Update:		;$B4D3
	;decrement the palette fade counter and compare
	;it to the expiry value
	ld   hl, PaletteFadeTimer
	inc  (hl)
	ld   a, (PaletteTimerExpiry)
	cp   (hl)
	;return if counter <= expiry value
	ret  nc

	;reset the counter 
	ld   (hl), $00
	ld   ix, BgPaletteControl
	ld   iy, WorkingCRAM
	ld   b, $02			;loop twice

	;save loop count
-:	push bc
		;save CRAM pointer
		push iy
			;check the palette control byte
			ld   a, (ix+$00)
			or   a
			;dont update palette if control byte == 0
			jr   z, +

			;update the palette in work RAM and flag
			;for a copy to the VDP
			call Palette_UpdateColours
			ld   a, $FF
			ld   (PaletteUpdatePending), a

			;move to the fg palette control byte
+:			ld   de, $0002
			add  ix, de
		
		;move to the fg palette (in work RAM)
		pop  iy
		ld   de, PaletteSize_Bytes
		add  iy, de
	pop  bc
	djnz -


	ld   hl, (RAM_D3E7)
	ld   a, h
	or   l
	ret  z

	;update alternate palette
	ld   ix, AltBgPaletteControl
	ld   iy, AltWorkingCRAM
	ld   b, $02
-:	push bc
		push iy
			ld   a, (ix+$00)
			or   a
			jr   z, +
			
			call Palette_UpdateColours
			ld   a, $FF
			ld   (AltPaletteUpdatePending), a
			
+:			ld   de, $0002
			add  ix, de
		pop  iy
	ld   de, PaletteSize_Bytes
	add  iy, de
	pop  bc
	djnz -
	ret

;---------- function start ----------
; (middle level)
Palette_UpdateColours:	;$B539
	bit  7, a
	jr   nz, Palette_FadeToColour
	bit  6, a
	jp   nz, Palette_FadeFromColour
	bit  5, a
	jr   nz, Palette_Reset
	ret

Palette_Reset:		;$B547
	call Palette_CalculateOffset
	;copy the palette data into work ram
	push iy
	pop  de
	ld   bc, PaletteSize_Bytes
	ldir
	;clear the palette control byte
	ld   (ix+$00), $00
	ret

Palette_FadeToColour:		;$B557
	;check the fade direction (from white or from black)
	bit  4, a
	jp   nz, Palette_FadeWhiteToColour
	
	;fade from black
	;FIXME: move this call before the jump to save some space
	call Palette_CalculateOffset
	
	;loop over all 16 colours in the palette
	ld   b, $10
	
-:	push bc
		;colour fade step value?
		ld   a, (ix+$00)
		and  $03
		ld   c, a
		
		;read a byte from the palette
		ld   a, (hl)
		;extract the red component
		and  $03
		;flip the lower 4 bits
		xor  $03
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		
+:		and  $03
		;store the colour value
		ld   (iy+$00), a
		
		;read the byte from the palette
		ld   a, (hl)
		;extract the green component
		and  $0C
		;flip the bits
		xor  $0C
		;rotate into position
		rrca
		rrca
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		
+:		and  $03
		;rotate back into position
		rlca
		rlca
		;OR the red component with the blue component
		or   (iy+$00)
		;store the colour value
		ld   (iy+$00), a

		;read the byte from the palette
		ld   a, (hl)
		;extract the blue component
		and  $30
		xor  $30
		rrca
		rrca
		rrca
		rrca
		;subtract the step value
		ld   b, a
		ld   a, c
		sub  b
		;if (value < 0) value = 0;
		jr   nc, +
		xor  a
		
+:		and  $03
		;rotate into position
		rlca
		rlca
		rlca
		rlca
		;recombine with the red & green components
		or   (iy+$00)
		;store the colour value
		ld   (iy+$00), a
		
		;move the colour storage pointer
		inc iy
		
		;move to the next colour in the palette
		inc  hl
	pop  bc
	djnz -
	
	;increment the fade step value
	inc  (ix+$00)
	ld   a, (ix+$00)
	and  $07
	cp   $04
	ret  nz

	ld   (ix+$00), $00
	ret

Palette_FadeFromColour:		;$B5BD
	;check the direction flag (to black or to white)
	bit  4, a
	jp   nz, _LABEL_3B689_102
	
	;fade to black
	;FIXME: move this before the jump
	call Palette_CalculateOffset
	
	;loop over 16 colour entries
	ld   b, $10

	;read the colour from RAM
-:	ld   a, (iy+$00)
		
		;store the red + blue components in register C
		and  $33
		ld   c, a
		
		;read the green component from RAM
		ld   a, (iy+$00)
		and  $0C
		jr   z, +
		;fade the green component
		sub  $04
		and  $0C
		;recombine with the red & blue components
		or   c
		;store in work RAM
		ld   (iy+$00), a

		;read the colour from RAM
+:		ld   a, (iy+$00)
		;keep the blue and green components in register C
		and  $3C
		ld   c, a
		
		;read the red component from RAM
		ld   a, (iy+$00)
		and  $03
		jr   z, +
		;fade the red component
		dec  a
		;recombine with the green & blue components
		or   c
		;store in work RAM
		ld   (iy+$00), a
		
		;read the colour from RAM
+:		ld   a, (iy+$00)
		;store the red & green components in register C
		and  $0F
		ld   c, a
		
		ld   a, (iy+$00)
		and  $30
		jr   z, +
		;fade the blue component
		sub  $10
		;recombine with the red & green components
		or   c
		;store in work RAM
		ld   (iy+$00), a
		
		;move to the next colour entry
+:		inc  iy
	djnz -
	
	inc  (ix+$00)
	ld   a, (ix+$00)
	and  $03
	cp   $03
	ret  nz

	ld   (ix+$00), $00
	ret

Palette_FadeWhiteToColour:		;$B610
	call Palette_CalculateOffset
	;loop over 16 colour entries
	ld   b, $10

	;read the palette control byte
-:	ld   a, (ix+$00)
	;extract the fade step value
	and  $03
	ld   c, a
	
	push bc
		;read a byte from the palette
		ld   a, (hl)
		;extract the red component
		and  $03
		ld   c, a
		;read a colour byte out of work RAM
		ld   a, (iy+$00)
		ld   b, a
		;store the blue & green components in register D
		and  $3C
		ld   d, a
		;extract the red component
		ld   a, b
		and  $03
		;compare the red component with the value
		;from the palette
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $01
		and  $03
		;recombine with the blue & green components
		or   d
		;store to work RAM
		ld   (iy+$00), a


		;read a byte from the palette
+:		ld   a, (hl)
		;extract the green component
		and  $0C
		ld   c, a
		;read a colour byte from work RAM
		ld   a, (iy+$00)
		ld   b, a
		;store the red & blue components in register D
		and  $33
		ld   d, a
		;extract the green component
		ld   a, b
		and  $0C
		;compare the green component with the value
		;from the palette
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $04
		and  $0C
		;recombine with the red & blue components
		or   d
		;store to work RAM
		ld   (iy+$00), a


		;read a byte from the palette
+:		ld   a, (hl)
		;extract the blue component
		and  $30
		ld   c, a
		;read a byte from work RAM
		ld   a, (iy+$00)
		ld   b, a
		;store the red & green components in register D
		and  $0F
		ld   d, a
		;compare the blue component with the value
		;from the palette
		ld   a, b
		and  $30
		cp   c
		;jump if the values are equal
		jr   z, +
		;fade the colour value
		sub  $10
		and  $30
		or   d
		;store to work RAM
		ld   (iy+$00), a

		;increment the palette and work RAM pointers
+:		inc  iy
		inc  hl
	pop  bc
	djnz -
	
	ld   a, (ix+$00)
	ld   c, a
	and  $F0
	ld   b, a
	ld   a, c
	and  $07
	inc  a
	cp   $04
	jr   z, +
	
	or   b
	ld   (ix+$00), a
	ret

+:	ld   (ix+$00), $00
	ret

_LABEL_3B689_102:
	call Palette_CalculateOffset
	
	;loop over 16 colour entries
	ld   b, $10
	
-:		push bc
			;read a colour byte from RAM
			ld   a, (iy+$00)
			ld   b, a
			;extract the blue & green components
			and  $3C
			ld   c, a
			;extract the red component
			ld   a, b
			and  $03
			;is the red component at full intensity?
			cp   $03
			;jump if yes
			jr   z, +
			;increase the red intensity
			inc  a
			and  $03
			;recombine with the blue & green components
+:			or   c
			;store in work RAM
			ld   (iy+$00), a
			
	
			;read a colour byte from RAM
			ld   a, (iy+$00)
			ld   b, a
			;extract the red & blue components
			and  $33
			ld   c, a
			;extract the green component
			ld   a, b
			and  $0C
			;is the green component at full intensity?
			cp   $0C
			;jump if yes
			jr   z, +
			;increase the green intensity
			add  a, $04
			and  $0C
			;recombine with the red & blue components
+:			or   c
			;store in work RAM
			ld   (iy+$00), a


			;read a colour byte from RAM
			ld   a, (iy+$00)
			ld   b, a
			;extract the green & red components
			and  $0F
			ld   c, a
			;extract the blue component
			ld   a, b
			and  $30
			;is the blue component at full intensity?
			cp   $30
			;jump if yes
			jr   z, +
			;increase blue intensity
			add  a, $10
			and  $30
			;recombine with red & green components
+:			or   c
			;store in work RAM
			ld   (iy+$00), a

			;increment the RAM pointer
			inc  iy
		pop  bc
	djnz -

	ld   a, (ix+$00)
	ld   b, a
	and  $F0
	ld   c, a
	ld   a, b
	inc  a
	and  $03
	or   c
	ld   (ix+$00), a
	and  $03
	cp   $03
	ret  nz

	ld   (ix+$00), $00
	ret


Palette_CalculateOffset:		;$B6EC
	ld   a, (ix+$01)		;get the palette index
	ld   l, a				;calculate the address for the palette
	ld   h, $00
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	;add  hl, hl
	ld   de, Palettes
	add  hl, de
	ret


Palettes:
.include "src/palettes_sms.asm"

.org $3F1C
;level palettes
DATA_B14_BF1C:
;Great Turquoise Zone
.db $1E, $0F
.db $1E, $0F
.db $1E, $0F
;Sunset Park Zone
.db $1F, $10
.db $1F, $10
.db $1F, $10
;Meta Junglira Zone
.db $20, $11
.db $20, $11
.db $20, $11
;Robotnik Winter Zone
.db $21, $12
.db $21, $12
.db $21, $12
;Tidal Plant Zone
.db $22, $13
.db $22, $13
.db $29, $13
;Atomic Destroyer Zone
.db $23, $14
.db $24, $14
.db $25, $14

.db $26, $1D
.db $35, $1D
.db $36, $1D

.db $0C, $1B
.db $0C, $1B
.db $0C, $1B

.db $2B, $2F
.db $2C, $30
.db $2B, $2F

.db $26, $1D
.db $26, $1D
.db $26, $1D

.db $1E, $0F
.db $1E, $0F
.db $1E, $0F

;underwater palettes
DATA_B14_BF5E:
.db $3A, $3B
.db $3A, $3B
.db $3C, $3D
.db $3A, $3B
.db $3E, $3F
.db $3A, $3B
