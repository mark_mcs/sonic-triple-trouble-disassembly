Data_Art_Sega:		;$8000
.incbin "art/art_cmp_sega.bin"

.db $FF

Data_Art_Sega_Copyright:	;$839E
.incbin "art/art_cmp_sega_copyright.bin"


Mappings_Sega_Copyright:	;$84B0
.incbin "mappings/mappings_sega_copyright.bin"


Art_GTZ_LevelTiles:     ; $84C4
.incbin "art/level/gtz/art_cmp_gtz_level_tiles.bin"


Art_SPZ_LevelTiles:     ; $974A
.incbin "art/level/spz/art_cmp_spz_level_tiles.bin"


Mappings_TitleScreen:		;$AC72
.incbin "mappings/mappings_titlescreen.bin"

Art_Unknown_00:				;$AF42
.incbin "art/art_cmp_unknown.bin"

Mappings_Unknown:			;$B0F2
.incbin "mappings/mappings_unknown.bin"

Art_Cmp_Sonic_Tornado:		;$B152
.incbin "art/art_cmp_sonic_tornado.bin"

.db $FF

Art_Badnik_Unknown00:		;$BA9C
.incbin "art/art_cmp_badnik_unknown00.bin"


LABEL_BC52:		;$BC52
.dw DATA_BC5A
.dw DATA_BC60
.dw DATA_BC71
.dw DATA_BC82

DATA_BC5A:
.db $FF, $01, $8B, $BC, $FF, $00

DATA_BC60:
.db $FF, $02, $80, $FF, $00, $00, $08, $03
.db $B5, $BC, $FE, $08, $04, $FF, $07, $66
.db $BC

DATA_BC71:
.db $FF, $02, $00, $00, $00, $01, $08, $01
.db $90, $BC, $FE, $08, $02, $FF, $07, $77
.db $BC

DATA_BC82:
.db $04, $01, $C6, $BC, $FE, $04, $02, $FF
.db $00


LABEL_BC8B:
	ld   (ix+$02), $01
	ret

LABEL_BC90
	call Logic_vtable + $84
	bit  1, (ix+$22)
	jr   z, LABEL_BC9E
	ld   (ix+$02), $03
	ret

LABEL_BC9E:
	ld   h, (ix+$19)
	ld   a, h
	cp   $04
	jr   nc, LABEL_BCC3
	ld   l, (ix+$18)
	ld   de, $0010
	add  hl, de
	ld   (ix+$19), h
	ld   (ix+$18), l
	jr   LABEL_BCC3

LABEL_BCB5:
	ld   bc, $0010
	call Logic_vtable + $78
	or   a
	jr   z, LABEL_BCC3
	ld   (ix+$02), $02
	ret

LABEL_BCC3:
	call Logic_vtable + $81
	call Logic_vtable + $69
	jp   Logic_vtable + $63

LABEL_BCCC:
	xor  a
	ld   ($D145), a
	ld   ($D147), a
	ld   ($DBDB), a
	di
	ld   de, Data_LevelSelectEntries
	ld   hl, $390c
	ld   bc, $1413
	ld   a, $01
	ld   ($D3F1), a
	call Logic_vtable + $204
	ei
	call LABEL_BD01
	ld   a, ($DBDB)
	add  a, a
	ld   e, a
	ld   d, $00
	ld   hl, DATA_BF4A
	add  hl, de
	ld   a, (hl)
	ld   ($D145), a
	inc  hl
	ld   a, (hl)
	ld   ($D147), a
	ret

LABEL_BD01:
	call VF_Engine_WaitForInterrupt
	call LABEL_BD22
	call LABEL_BD69
	call LABEL_BD87
	call LABEL_BDA6
	ld   a, ($D139)
	bit  5, a
	jr   z, LABEL_BD01
	ld   a, $dd
	ld   ($DE05), a
	ld   a, $ff
	ld   ($D168), a
	ret

LABEL_BD22:
	ld   a, ($D139)
	bit  0, a
	jr   nz, LABEL_BD51
	bit  1, a
	jr   nz, LABEL_BD59
	ld   a, ($D137)
	and  $03
	jr   z, LABEL_BD4C
	ld   hl, $D116
	ld   a, (hl)
	inc  a
	ld   (hl), a
	cp   $28
	ret  c
	ld   a, $26
	ld   (hl), a
	ld   a, ($D137)
	bit  0, a
	jr   nz, LABEL_BD51
	bit  1, a
	jr   nz, LABEL_BD59
	ret

LABEL_BD4C:
	xor  a
	ld   ($D116), a
	ret

LABEL_BD51:
	ld   a, ($DBDB)
	or   a
	ret  z
	dec  a
	jr   LABEL_BD60

LABEL_BD59:
	ld   a, ($DBDB)
	cp   $11
	ret  nc
	inc  a
LABEL_BD60:
	ld   ($DBDB), a
	ld   a, $d8
	ld   ($DE05), a
	ret

LABEL_BD69:
	ld   a, ($DBDB)
	ld   l, a
	ld   h, $00
	ld   b, $06

-:	add  hl, hl
	djnz -

	ld   de, $3966
	add  hl, de
	ld   de, LABEL_BD81
	ld   bc, $0301
	jp   Logic_vtable + $14A

LABEL_BD81:
	jr   nz, +
	inc  a
+:	ld   bc, $0120
LABEL_BD87:
	ld   a, ($DBDB)
	ld   l, a
	ld   h, $00
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	add  hl, hl
	ld   de, $3972
	add  hl, de
	ld   de, DATA_BDA0
	ld   bc, $0301
	jp   Logic_vtable + $14A

DATA_BDA0:
.db $20, $01, $3E, $01, $20, $01


LABEL_BDA6:
	ld   a, ($DBDB)
	cp   $0c
	jr   c, LABEL_BDB4
	ld   a, $18
	ld   ($D3A4), a
	jr   LABEL_BDB9

LABEL_BDB4:
	ld   a, $00
	ld   ($D3A4), a
LABEL_BDB9:
	ld   a, ($D3A4)
	ld   b, a
	ld   a, ($D199)
	cp   b
	ret  z
	jr   c, LABEL_BDC9
	dec  a
	ld   ($D199), a
	ret

LABEL_BDC9:
	inc  a
	ld   ($D199), a
	ret


Data_LevelSelectEntries:		;$BDCE
.db "  - ZONE SELECT -  "
.db "                   "
.db "GREAT         ACT-1"
.ifeq SMS_VERSION 1
  .db "   TURQUOISE  ACT-2"
.else
  .db "   TURQUOIS   ACT-2"
.endif 
.db "              ACT-3"
.db "SUNSET        ACT-1"
.db "   PARK       ACT-2"
.db "              ACT-3"
.db "META          ACT-1"
.db "   JUNGLIRA   ACT-2"
.db "              ACT-3"
.db "ROBOTNIK      ACT-1"
.db "   WINTER     ACT-2"
.db "              ACT-3"
.db "TIDAL         ACT-1"
.db "   PLANT      ACT-2"
.db "              ACT-3"
.db "ATOMIC        ACT-1"
.db "  DESTROYER   ACT-2"
.db "              ACT-3"


DATA_BF4A:
.db $00, $00, $00, $01, $00, $02, $01, $00
.db $01, $01, $01, $02, $02, $00, $02, $01
.db $02, $02, $03, $00, $03, $01, $03, $02
.db $04, $00, $04, $01, $04, $02, $05, $00
.db $05, $01, $05, $02, $08, $00, $09, $00
.db $0A, $00, $0B, $00, $0C, $01, $00, $00

