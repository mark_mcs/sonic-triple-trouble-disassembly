
.incbin "unknown/bank23.bin"

Mappings_TitleScreen_00:
.incbin "mappings/mappings_titlescreen_00.bin"
Mappings_TitleScreen_01:
.incbin "mappings/mappings_titlescreen_01.bin"
Mappings_TitleScreen_02:
.incbin "mappings/mappings_titlescreen_02.bin"


LABEL_BFBB:
    ld   a, ($D168)
    cp   $00
    ret  z
    dec  a
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, DATA_BFCF
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    jp   (hl)


DATA_BFCF:
.dw LABEL_BFD5
.dw LABEL_BFE2
.dw LABEL_BFEF


LABEL_BFD5:
    ld   hl, Mappings_TitleScreen_00
.ifeq SMS_VERSION 1
	ld   de, $35A0
.else
    ld   de, $3720
.endif
    ld   bc, $0180
    call VF_VDP_CopyToVRAM
    ret
LABEL_BFE2:
    ld   hl, Mappings_TitleScreen_01
.ifeq SMS_VERSION 1
	ld   de, $35A0
.else
    ld   de, $3720
.endif
    ld   bc, $0180
    call VF_VDP_CopyToVRAM
    ret
LABEL_BFEF:
    ld   hl, Mappings_TitleScreen_02
.ifeq SMS_VERSION 1
	ld   de, $35A0
.else
    ld   de, $3720
.endif
    ld   bc, $0180
    call VF_VDP_CopyToVRAM
    ret
