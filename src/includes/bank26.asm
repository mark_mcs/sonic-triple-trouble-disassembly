.include "src/sound_pcm.asm"

DemoSequence_GTZ1:                  ; $9DF1
.incbin "demo/demo_gtz1.bin"

DemoSequence_SPZ2:                  ; $A4F1
.incbin "demo/demo_spz2.bin"

.incbin "unknown/bank_26_01.bin"

.include "src/zone_tilesets.asm"

.incbin "unknown/bank_26_02.bin"
