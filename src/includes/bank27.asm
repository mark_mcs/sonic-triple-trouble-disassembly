.incbin "unknown/bank_27.bin"

LABEL_B27_BD12:
	ld   a, (GameMode)
	cp   GameMode_TimeAttack
	jp   z, LABEL_B27_BDBA
	
	ld   a, $18
	ld   ($db37), a
	ld   a, $40
	ld   ($dbae), a
	ld   a, $18
	ld   ($db38), a
	ld   a, $48
	ld   ($dbb0), a
	ld   a, $18
	ld   ($db39), a
	ld   a, $50
	ld   ($dbb2), a
	ld   a, $18
	ld   ($db3a), a
	ld   a, $30
	ld   ($dbb4), a
	ld   a, $18
	ld   ($db3b), a
	ld   a, $38
	ld   ($dbb6), a
	ld   a, $18
	ld   ($db3a), a
	ld   a, $30
	ld   ($dbb4), a
	ld   a, $18
	ld   ($db3b), a
	ld   a, $38
	ld   ($dbb6), a
	ld   a, $28
	ld   ($db3c), a
	ld   a, $30
	ld   ($dbb8), a
	ld   a, $28
	ld   ($db3d), a
	ld   a, $38
	ld   ($dbba), a
	ld   a, $28
	ld   ($db3e), a
	ld   a, $40
	ld   ($dbbc), a
	ld   a, $28
	ld   ($db3f), a
	ld   a, $48
	ld   ($dbbe), a
	ld   a, $42
	ld   ($dbbb), a
	ld   a, $38
	ld   ($db35), a
	ld   ($db36), a
	ld   ($db33), a
	ld   ($db34), a
	ld   a, $30
	ld   ($dbaa), a
	ld   a, $38
	ld   ($dbac), a
	ld   a, $40
	ld   ($dba6), a
	ld   a, $48
	ld   ($dba8), a
	ld   a, $5e
	ld   ($dbab), a
	ld   a, $60
	ld   ($dbad), a
	ret

LABEL_B27_BDBA:
	ld   a, $28
	ld   ($db37), a
	ld   a, $40
	ld   ($dbae), a
	ld   a, $28
	ld   ($db38), a
	ld   a, $48
	ld   ($dbb0), a
	ld   a, $28
	ld   ($db39), a
	ld   a, $50
	ld   ($dbb2), a
	ld   a, $28
	ld   ($db3b), a
	ld   a, $38
	ld   ($dbb6), a
	ld   a, $28
	ld   ($db3a), a
	ld   a, $30
	ld   ($dbb4), a
	ld   a, $28
	ld   ($db3b), a
	ld   a, $38
	ld   ($dbb6), a
	ld   a, $18
	ld   ($db35), a
	ld   ($db36), a
	ld   ($db34), a
	ld   a, $58
	ld   ($dba8), a
	ld   a, $60
	ld   ($dbaa), a
	ld   a, $68
	ld   ($dbac), a
	ld   a, $18
	ld   ($db3c), a
	ld   a, $38
	ld   ($dbb8), a
	ld   a, $18
	ld   ($db3d), a
	ld   a, $40
	ld   ($dbba), a
	ld   a, $18
	ld   ($db3e), a
	ld   a, $48
	ld   ($dbbc), a
	ld   a, $18
	ld   ($db3f), a
	ld   a, $50
	ld   ($dbbe), a
	ld   a, $42
	ld   ($dbbb), a
	ld   a, $42
	ld   ($dba9), a
	ret

LABEL_B27_BE43:
	ld   a, $18
	ld   ($db3e), a
	ld   a, $3e
	ld   ($dbbc), a
	ld   a, $18
	ld   ($db3f), a
	ld   a, $46
	ld   ($dbbe), a
	ld   a, $18
	ld   ($db3b), a
	ld   a, $30
	ld   ($dbb6), a
	ld   a, $18
	ld   ($db3c), a
	ld   a, $38
	ld   ($dbb8), a
	ret

LABEL_B27_BE6C:
	xor  a
	ld   ($d3e9), a
	ld   a, ($d145)
	or   a
	jr   z, +
	ld   b, a
	xor  a

-:	add  a, $03
	djnz -

+:	ld   b, a
	ld   a, ($d147)
	add  a, b
	add  a, a
	ld   hl, DATA_B27_BEA7
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ld   ($d3e7), de
	ld   a, d
	or   e
	ret  z
	ld   a, ($d145)
	cp   $04
	jr   z, +
	or   a
	ret  nz

+:	ld   a, $24
	ld   ($d580), a
	ld   a, $05
	ld   ($d5bf), a
	ret

DATA_B27_BEA7:
.db $6D, $02, $4D, $02, $3D, $02, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $50, $01, $50, $01, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00


LABEL_B27_BE5F:
	ld   a, ($D145)
	or   a
	jr   z, $06
	ld   b, a
	xor  a
	add  a, $03
	djnz $FC
	ld   b, a
	ld   a, ($D147)
	add  a, b
	ld   hl, DATA_B27_BF14
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   a, (hl)
	or   a
	ret  z
	
	ld   ($D540), a
	ret


DATA_B27_BF14:
.db $00, $00, $00, $00, $00, $13, $00, $00
.db $00, $31, $31, $00, $00, $00, $00, $00
.db $00, $08, $08, $00, $00, $00, $00, $00
.db $00, $00, $00, $08, $08, $00, $19, $00
.db $00, $00, $00, $00, $00, $00, $00
