.incbin "unknown/bank_27.bin"

LABEL_B27_BD12:
	ld   a, (GameMode)
	cp   GameMode_TimeAttack
	jp   z, LABEL_B27_BDBA

	push ix
		push iy
			ld   b, $0D
			
			ld   hl, Data_HUDDefaults
			ld   ix, $DB33
			ld   iy, $DBA6
			
			-:	ld   a, (hl)
				ld   (ix+$00), a
				inc  hl
				
				ld   a, (hl)
				ld   (iy+$00), a
				inc  hl
				ld   a, (hl)
				ld   (iy+$01), a
				
				inc  hl
				inc  ix
				inc  iy
				inc  iy
			djnz -
		pop  iy
	pop  ix
	
	ret

Data_HUDDefaults:
;		hpos
;   vpos  |   char
;  |----|----|----|
.db $28, $20, $16	   ;life icon position
.db $28, $28, $28
.db $28, $10, $5E
.db $28, $18, $60

.db $08, $20, $2E	   ;ring icon position
.db $08, $28, $2E
.db	$08, $30, $2E
.db $08, $10, $10
.db $08, $18, $10

.db $18, $10, $10	   ;timer position
.db $18, $18, $42
.db $18, $20, $10
.db $18, $28, $10

.org $3DBA
LABEL_B27_BDBA:
	;ring icon position
	ld   a, $08
	ld   ($db37), a
	ld   a, $20
	ld   ($dbae), a
	
	ld   a, $08
	ld   ($db38), a
	ld   a, $28
	ld   ($dbb0), a
	
	ld   a, $08
	ld   ($db39), a
	ld   a, $30
	ld   ($dbb2), a
	
	ld   a, $08
	ld   ($db3b), a
	ld   a, $18
	ld   ($dbb6), a
	
	ld   a, $08
	ld   ($db3a), a
	ld   a, $10
	ld   ($dbb4), a
	
	ld   a, $28
	ld   ($db3b), a
	ld   a, $18
	ld   ($dbb6), a
	
	;time icon
	ld   a, $18
	ld   ($db35), a
	ld   ($db36), a
	ld   ($db34), a
	ld   a, $38
	ld   ($dba8), a
	ld   a, $40
	ld   ($dbaa), a
	ld   a, $48
	ld   ($dbac), a
	
	;time icon
	ld   a, $18
	ld   ($db3c), a
	ld   a, $18
	ld   ($dbb8), a
	ld   a, $18
	ld   ($db3d), a
	ld   a, $20
	ld   ($dbba), a
	ld   a, $18
	ld   ($db3e), a
	ld   a, $28
	ld   ($dbbc), a
	ld   a, $18
	ld   ($db3f), a
	ld   a, $30
	ld   ($dbbe), a
	ld   a, $42
	ld   ($dbbb), a
	ld   a, $42
	ld   ($dba9), a
	ret

LABEL_B27_BE43:
	ld   a, $18
	ld   ($db3e), a
	ld   a, $3e
	ld   ($dbbc), a
	ld   a, $18
	ld   ($db3f), a
	ld   a, $46
	ld   ($dbbe), a
	ld   a, $18
	ld   ($db3b), a
	ld   a, $30
	ld   ($dbb6), a
	ld   a, $18
	ld   ($db3c), a
	ld   a, $38
	ld   ($dbb8), a
	ret

LABEL_B27_BE6C:
	xor  a
	ld   ($d3e9), a
	ld   a, (CurrentLevel)
	or   a
	jr   z, +
	ld   b, a
	xor  a

-:	add  a, $03
	djnz -

+:	ld   b, a
	ld   a, (CurrentAct)
	add  a, b
	add  a, a
	ld   hl, DATA_B27_BEA7
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ld   ($d3e7), de
	ld   a, d
	or   e
	ret  z
	ld   a, (CurrentLevel)
	cp   $04
	jr   z, +
	or   a
	ret  nz

+:	ld   a, $24
	ld   ($d580), a
	ld   a, $05
	ld   ($d5bf), a
	ret

DATA_B27_BEA7:
.db $6D, $02, $4D, $02, $3D, $02, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $50, $01, $50, $01, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00


LABEL_B27_BE5F:
	ld   a, (CurrentLevel)
	or   a
	jr   z, $06
	ld   b, a
	xor  a
	add  a, $03
	djnz $FC
	ld   b, a
	ld   a, (CurrentAct)
	add  a, b
	ld   hl, DATA_B27_BF14
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   a, (hl)
	or   a
	ret  z
	
	ld   ($D540), a
	ret


DATA_B27_BF14:
.db $00, $00, $00, $00, $00, $13, $00, $00
.db $00, $31, $31, $00, $00, $00, $00, $00
.db $00, $08, $08, $00, $00, $00, $00, $00
.db $00, $00, $00, $08, $08, $00, $19, $00
.db $00, $00, $00, $00, $00, $00, $00
