
.include "src/level_dynamics.asm"


.org $1F14
Art_cmp_Titlescreen:	;$9F14
.incbin "art/art_cmp_titlescreen.bin"

.org $3CD0
Mappings_Unknown_B29_BCD0:
.db $AD, $01, $AE, $01, $AF, $01, $B0, $01
.db $B1, $01, $B2, $01, $B3, $01, $B4, $01
.db $B5, $01, $B6, $01, $B7, $01, $B8, $01
.db $7F, $01, $80, $01, $81, $01, $82, $01
.db $83, $01, $84, $01, $85, $01, $86, $01
.db $87, $01, $88, $01, $89, $01, $8A, $01
.db $B9, $01, $BA, $01, $BB, $01, $BC, $01
.db $BD, $01, $BE, $01, $BF, $01, $C0, $01
.db $C1, $01, $C2, $01, $C3, $01, $C4, $01

Art_cmp_Numbers:	;$BD18
.incbin "art/art_cmp_score_numbers.bin"
