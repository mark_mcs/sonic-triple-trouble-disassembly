.def	Music_GTZ			        $81
.def	Music_SPZ			        $82
.def	Music_MJZ			        $83
.def	Music_RWZ			        $84
.def	Music_Intro			      $85
.def	Music_Title			      $86
.def	Music_PlayerSelect	  $87
.def	Music_TitleCard		    $88
.def	Music_ActPassed		    $89
.def	Music_Boss			      $8A
.def	Music_FinalBoss		    $8B
.def	Music_ExtraLife		    $8C
.def	Music_LostLife		    $8D
.def	Music_GameOver		    $8E
.def	Music_RocketShoes	    $8F
.def  Music_PowerSneakers   $90

.def  Music_Invincibility   $92
.def	Music_SpzBoss		      $93
.def	Music_SeaFox		      $94
.def  Music_PropellerShoes  $95

.def	Music_NacksTheme	    $97
.def  Music_HeliTails       $98

.def	Music_TPZ			        $9A
.def	Music_ADZ			        $9B
.def  Music_Special2D       $9C
.def  Music_Special3D       $9D
.def  Music_MetalSonic      $9E
.def	Music_Continue		    $9F
.def  Music_ChaosEmerald    $A0
.def  Music_Ending1         $A1
.def	Music_Ending2		      $A2
.def	Music_A3			        $A3


.def  SFX_Ring            $C9

