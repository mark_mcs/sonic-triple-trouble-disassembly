.def    PlayerState_Idle            $01

.def    PlayerState_Walking         $05
.def    PlayerState_Running         $06

.def    PlayerState_Roll            $09
.def    PlayerState_Jumping         $0A
.def    PlayerState_VerticalSpring  $0B

.def    PlayerState_Falling         $0E
.def    PlayerState_SpinCharge      $0F
.def    PlayerState_SpinRelease     $10

.def    PlayerState_SPZCart         $16
.def	PlayerState_LostLife	    $1F

.def    PlayerState_EndOfLevel      $20

.def    PlayerState_FallingRoll     $24

.def	PlayerState_Drown		    $28

.def    PlayerState_TitleCard       $3B

.def    PlayerState_AfterGTZBoss1   $44

.def    PlayerState_AfterGTZBoss2   $4B
