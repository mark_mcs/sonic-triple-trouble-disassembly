; =============================================================================
;  LEVEL HEADER STRUCTURE
; -----------------------------------------------------------------------------
.struct Level
    ix00            db      ; $00
    ix01            db      ; $01
    ix02            db      ; $02
    ix03            db      ; $03
    metatileBank    db      ; $04
    layoutBank      db      ; $05
    metatilePtr     dw      ; $06
    layoutPtr       dw      ; $08
    widthTable      dw      ; $0A
    width           dw      ; $0C
    widthInChunks   dw      ; $0E
    verticalOffs    dw      ; $10
.endst

; ==============================================================================
;  Level flags 00 
; ------------------------------------------------------------------------------
.def  LEVEL_F00_BIT0            0   ; camera vertical movement dir flag
.def  LEVEL_F00_CAMERA_ENABLED  1   ; Set to 1 to enable camera updates
.def  LEVEL_F00_BIT2            2
.def  LEVEL_F00_BIT3            3
.def  LEVEL_F00_BIT4            4
.def  LEVEL_F00_BIT5            5
.def  LEVEL_F00_BIT6            6
.def  LEVEL_F00_BIT7            7

; ==============================================================================
;  OBJECT STRUCTURE & FLAGS
; ------------------------------------------------------------------------------
.struct Object
    ObjID           db      ; $00 - object number
    State           db      ; $01
    StateNext       db      ; $02
    Flags03         db      ; $03
    Flags04         db      ; $04
    SpriteCount     db      ; $05
    AnimFrame       db      ; $06
    FrameCounter    db      ; $07
    RightFacingIdx  db      ; $08
    LeftFacingIdx   db      ; $09
    ix0A            db      ; movement flags?
    ix0B            db
    LogicPtr        dw      ; $0C - Pointer to logic subroutine.
    LogicSeqPtr     dw      ; $0E - Pointer to logic sequence data
    SubPixelX       db      ; $10 - fractional part of xpos
    X               dw      ; $11 - x pos in level
    SubPixelY       db      ; $13 - fractional part of ypos
    Y               dw      ; $14 - y pos in level
    VelX            dw      ; $16 - x velocity (Q8.8)
    VelY            dw      ; $18 - y velocity (Q8.8)
    ScreenX         dw      ; $1A - x offset on screen
    ScreenY         dw      ; $1C - y offset on screen
    ix1E            db
    ix1F            db
    CollidingObj    db      ; $20 - index of colliding object
    SprColFlags     db      ; $21 - background collision flags
    BgColFlags      db      ; $22 - sprite collision flags
    ix23            db      ; $23 - bg + obj collision flags?
    ix24            db
    ix25            db
    ix26            db
    ix27            db
    SprMappgPtr     dw      ; $28 - pointer to sprite mapping data for current anim frame
    SprOffsets      dw      ; $2A - pointer to sprite SAT offsets (2 words in object anim data)
    Width           db      ; $2C
    Height          db      ; $2D
    FlashCounter    db      ; $2E - counter used to toggle sprite visibility
    ix2F            db
    D530            db
    ix31            db      ; $31 - slot index of parent/owning object
    PowerUp         db      ; $32 - current power-up 
    ix33            db
    ix34            db
    ix35            db
    ix36            db
    ix37            db
    ix38            db
    ix39            db
    InitialX        dw      ; $3A - initial x coordinate
    InitialY        dw      ; $3C - initial y coordinate
    ActvObjIdx      db      ; $3E - index of object within active objects array
    ix3F            db
.endst


; ---------------------------------------------------------
;  Object Flag Byte F3 bits
; ---------------------------------------------------------
.def OBJ_F3_IN_AIR              0
.def OBJ_F3_BIT1                1
.def OBJ_F3_BIT2                2
.def OBJ_F3_NO_STATE_CHANGE     3       ; set to disable logic updates
.def OBJ_F3_BIT4                4
.def OBJ_F3_BIT5                5
.def OBJ_F3_BIT6                6
.def OBJ_F3_BIT7                7

; ---------------------------------------------------------
;  Object Flag Byte F4 bits
; ---------------------------------------------------------
.def OBJ_F4_BIT0        0
.def OBJ_F4_BIT1        1
.def OBJ_F4_BIT2        2
.def OBJ_F4_BIT3        3
.def OBJ_F4_FACING_LEFT 4
.def OBJ_F4_FLASHING    5
.def OBJ_F4_BIT6        6
.def OBJ_F4_VISIBLE     7
