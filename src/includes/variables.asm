.ifeq SMS_VERSION 1
  .def    PaletteSize_Bytes       $10
  .def    ColourSize_Bytes        $01
.else
  .def    PaletteSize_Bytes       $20
  .def    ColourSize_Bytes        $02
.endif

;======================================================
.def    VdpRegister0            $D11E
.def    VdpRegister1            $D11F
.def    VdpRegister2            $D120
.def    VdpRegister3            $D121
.def    VdpRegister4            $D122
.def    VdpRegister5            $D123
.def    VdpRegister6            $D124
.def    VdpRegister7            $D125
.def    VdpRegister8            $D126
.def    VdpRegister9            $D127
.def    VdpRegisterA            $D128

.def    CurrentBank             $D12B

.def    SatUpdateTrigger        $D134 ; byte - causes refresh of SAT in vram

.def    DisableInput            $D136 ; flag to disable reading controller input
.def    ControllerData          $D137 ; input flags from the controller
.def    ControllerData_Prev     $D138 ; input flags from last frame

.def    PlayerObjectID          $D13F

.def    LifeCounter             $D140

.def    GameMode                $D143
;.def                            $D144 ; some sort of flags
.def    CurrentLevel            $D145
.def    CurrentAct              $D147

.def    ContinueCounter         $D14B

.def    RingCounter             $D159

.def    Timer_Seconds           $D15D

.def    DemoSequenceIdx         $D160 ; upper bit is demo mode flag, lower = demo number
.def    DemoTimer               $D161 ; word - timer for the duration of the demo sequence
.def    DemoSequencePtr         $D163 ; word - pointer to input sequence data
.def    DemoSequenceBank        $D165
.def    DemoFinishedFlag        $D166

.def    EmeraldCounter          $D17E

.def    LevelHeader             $D184   ;-.
                                        ;  |
.def    LevelLayoutBank         $D189   ;  > aux level header structure
                                        ;  |
.def    LevelLayoutPtr          $D18C   ;-'

.def    BackgroundXScroll       $D198
.def    BackgroundYScroll       $D199
.def    CameraLimit_X           $D19A
.def    CameraLimit_Y           $D19C

.def    Camera_minY             $D3A2   ; word - copied to D3AA
.def    Camera_maxY             $D3A4   ; word - copied to D3AC
.def    Camera_minX             $D3A6   ; word - copied to D3AE
.def    Camera_maxX             $D3A8   ; word - copied to D3B0
.def    Camera_minY_copy        $D3AA   ; word - copied from D3A2
.def    Camera_maxY_copy        $D3AC   ; word - copied from D3A4
.def    Camera_minX_copy        $D3AE   ; word - copied from D3A6
.def    Camera_maxX_copy        $D3B0   ; word - copied from D3A8
.def    Camera_X                $D3B2
.def    Camera_Y                $D3B4
.def    Camera_adjX             $D3B6   ; used to offset the camera around 
.def    Camera_adjY             $D3B7   ; the player
.def    Camera_adjX_centre      $D3B8
.def    Camera_adjX_right       $D3B9
.def    Camera_adjX_left        $D3BA
.def    Camera_adjY_centre      $D3BB
.def    Camera_adjY_bottom      $D3BC
.def    Camera_adjY_top         $D3BD
.def    CameraScrollOverride    $D3BE   ; used to index into an array of
                                        ; vdp scroll values
;.def                            $D3C0

.def    InvincibleTimer         $D3C2   ; ???

.def    TailsFlightTimer        $D3C7

.def    UnderWaterTimer         $D3CE

.def    RingArtFrame            $D3DC   ; current frame of ring art animation
.def    RingArtFrameNext        $D3DD   ; next frame index

.def    RingArtPtr              $D3DF   ; pointer to the ring art for the current level

.def    LevelRingArtVRAMPtr     $D3E1
.def    RingArtFrameCounter     $D3E3   ; timer counter for each anim frame. changes animation frame 
                                        ; when counter % 8 == 0

.def    Level_CameraLimitX      $D3F2   ; initial limit loaded from level header
.def    Level_CameraLimitY      $D3F4   ; initial limit loaded from level header

.def    CollisionDataPtr        $D3FA

.def    PatternLoadCue          $D436

.def    LevelDynamic1           $D490

.def    LevelDynamic2           $D498

.def    WorkingCRAM             $D4B3

.def    PaletteUpdatePending    $D4F3

.def    AltPaletteUpdatePending $D4F5
.def    BgPaletteControl        $D4F6
.def    BgPaletteIndex          $D4F7
.def    FgPaletteControl        $D4F8
.def    FgPaletteIndex          $D4F9
.def    AltBgPaletteControl     $D4FA
.def    AltBgPaletteIndex       $D4FB
.def    AltFgPaletteControl     $D4FC
.def    AltFgPaletteIndex       $D4FD
.def    PaletteTimerExpiry      $D4FE
.def    PaletteFadeTimer        $D4FF

; -- Start of Object Slots --------------------------------
.def    ObjectSlots             $D500
.def    Player                  $D500
.def    ObjectSlot_01           $D540   ; seems to be reserved like player slot
.def    ObjectSlot_02           $D580   ; first of the high priority dynamic slots

.def    ObjectSlotsBadniks      $D700   ; low priority dynamic slots
; ---------------------------------------------------------

.def    VDP_WorkingSAT          $DB00
.def    VDP_WorkingSAT_VPOS     VDP_WorkingSAT
.def    VDP_WorkingSAT_HPOS     VDP_WorkingSAT + 64


.def    AltWorkingCRAM          $DC58

.def    Sound_SFX_Trigger       $DE05
.def    Sound_Music_Trigger     $DE06

; ==========================================================
;  Input Flags Bits
; ----------------------------------------------------------
.def    INP_DEMO_MODE_BIT       7

