; ==============================================================================
;  Engine_ClearLevelAttributes
; ------------------------------------------------------------------------------
;  Clears the memory used for level structures & state.
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
Engine_ClearLevelAttributes:		;$428B
	ld   hl, LevelHeader
	ld   de, $D185
	ld   bc, $023C
	ld   (hl), $00
	ldir
	ret

; ==============================================================================
;  Engine_LoadLevelHeader
; ------------------------------------------------------------------------------
;  Loads the level header and initialises the level structure memory.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_LoadLevelHeader:		;$4299
	;calculate a pointer for the current level
	ld   a, (CurrentLevel)
	ld   l, a
	ld   h, $00
	add  hl, hl
	ld   de, Data_LevelHeaders
	add  hl, de
	;calculate the pointer for the current act
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ld   a, (CurrentAct)
	ld   l, a
	ld   h, $00
	add  hl, hl
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	;copy the pointer into the IY register
	push de
	pop  iy
	
	ld   ix, LevelHeader
	
	ld   a, (iy + $00)
	ld   (ix + Level.metatileBank), a	;$D188 - Bank number for metatile data.
	
	ld   a, (iy+$01)	;$D18A - Pointer to metatile data.
	ld   (ix + Level.metatilePtr), a
	ld   a, (iy+$02)
	ld   (ix + Level.metatilePtr+1), a
	
	ld   a, (iy+$03)	;$D189 - Bank number for level layout.
	ld   (ix + Level.layoutBank), a
	
	ld   a, (iy+$04)	;$D18C - Pointer to level layout data.
	ld   (ix + Level.layoutPtr), a
	ld   a, (iy+$05)
	ld   (ix + Level.layoutPtr+1), a
	
	ld   a, (iy+$06)	;$D192 - Level width in metatiles.
	ld   (ix + Level.widthInChunks), a
	ld   a, (iy+$07)
	ld   (ix + Level.widthInChunks+1), a
	
	ld   a, (iy+$08)	;$D190 - 2's comp. level width
	ld   (ix + Level.width), a
	ld   a, (iy+$09)
	ld   (ix + Level.width+1), a
	
	ld   a, (iy+$0A)	;$D194 - Vertical offset into layout data.
	ld   (ix + Level.verticalOffs), a
	ld   a, (iy+$0B)
	ld   (ix + Level.verticalOffs+1), a
	
	ld   l, (iy+$0C)	;minimum camera x position
	ld   h, (iy+$0D)
	ld   (Camera_minX), hl
	ld   (Camera_minX_copy), hl
	
	ld   l, (iy+$0E)	;minimum camera y position
	ld   h, (iy+$0F)
	ld   (Camera_minY), hl
	ld   (Camera_minY_copy), hl

	ld   l, (iy+$10)	;maximum camera x position
	ld   h, (iy+$11)
	ld   (Camera_maxX), hl
	ld   (Camera_maxX_copy), hl
	
	ld   l, (iy+$12)	;maximum camera y position
	ld   h, (iy+$13)
	ld   de, $FFE8		;adjust by -24
	add  hl, de
	ld   (Camera_maxY), hl
	ld   (Camera_maxY_copy), hl

	ld   a, (iy+$14)	;$D18E - Pointer to level width multiples.
	ld   (ix + Level.widthTable), a
	ld   a, (iy+$15)
	ld   (ix + Level.widthTable+1), a
	
	ld   a, $60
	ld   (Camera_adjX), a		;initial camera x adjustment
	ld   a, $78
	ld   (Camera_adjY), a		;initial camera y adjustment
	call Engine_CalculateCameraBounds
	
	ret


.include "src/level_headers.asm"


; =============================================================================
;  Engine_DecompressLevel()
; -----------------------------------------------------------------------------
;  Decompresses level data into work RAM.
; -----------------------------------------------------------------------------
;  Params:
;       None.
; -----------------------------------------------------------------------------
Engine_DecompressLevel:     ; $459F
	ld   ix, LevelHeader
	ld   a, (ix + Level.layoutBank)
	call Engine_SwapFrame2
	ld   l, (ix + Level.layoutPtr)
	ld   h, (ix + Level.layoutPtr + 1)
	ld   de, $C001

_Engine_DecompressLevel_ReadFlags:
	ld   c, (hl)
	inc  hl
	ld   b, $08


_Engine_DecompressLevel_NextFlag:
    ; read the next compression flag.
    ; 0 = no compression, 1 = compression
	rr   c
	jr   nc, _Engine_DecompressLevel_Decompress
    
    ; no compression - just copy a byte
	ldi
	inc  bc
	jr   +

_Engine_DecompressLevel_Decompress:
	push bc
    
    ; read the compression window pointer
	ld   c, (hl)
	inc  hl
	ld   b, (hl)
	inc  hl
    
    ; window of 0 = decompression complete
	ld   a, c
	or   b
	jr   z, _Engine_DecompressLevel_Finish
    
	push hl
        ; add the window offset to the source pointer
        ld   a, b
        or   $F0
        ld   h, a
        ld   l, c
        add  hl, de
        
        ; get the byte count
        ld   a, b
        and  $F0
        rrca
        rrca
        rrca
        rrca
        add  a, $03
        
        ; copy the data
        ld   c, a
        ld   b, $00
        ldir
	pop  hl
	pop  bc
    
+:  djnz _Engine_DecompressLevel_NextFlag
    jr   _Engine_DecompressLevel_ReadFlags

_Engine_DecompressLevel_Finish:
	pop  bc
	ret


