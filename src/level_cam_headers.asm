LevelCameraHeaders:
.dw _CamHeader_GTZ
.dw _CamHeader_SPZ
.dw _CamHeader_MJZ
.dw _CamHeader_RWZ
.dw _CamHeader_TPZ
.dw _CamHeader_ADZ
.dw _CamHeader_L06
.dw _CamHeader_L07
.dw _CamHeader_GTZ
.dw _CamHeader_L08
.dw _CamHeader_L09
.dw _CamHeader_GTZ
.dw _CamHeader_GTZ
.dw _CamHeader_GTZ
.dw _CamHeader_GTZ

_CamHeader_GTZ:     ; $9A27
.dw _CamHeader_GTZ1
.dw _CamHeader_GTZ2
.dw _CamHeader_GTZ3

_CamHeader_SPZ:     ; $9A2D
.dw _CamHeader_SPZ1
.dw _CamHeader_SPZ2
.dw _CamHeader_SPZ3

_CamHeader_MJZ:     ; $9A33
.dw _CamHeader_MJZ1
.dw _CamHeader_MJZ2
.dw _CamHeader_MJZ3

_CamHeader_RWZ:     ; $9A39
.dw _CamHeader_RWZ1
.dw _CamHeader_RWZ2
.dw _CamHeader_RWZ3

_CamHeader_TPZ:     ; $9A3F
.dw _CamHeader_TPZ1
.dw _CamHeader_TPZ2
.dw _CamHeader_TPZ3

_CamHeader_ADZ:     ; $9A45
.dw _CamHeader_ADZ1
.dw _CamHeader_ADZ2
.dw _CamHeader_ADZ3
.dw _CamHeader_ADZ4

_CamHeader_L06:     ; $9A4D
.dw _CamHeader_L061
.dw _CamHeader_L062
.dw _CamHeader_L063

_CamHeader_L07:     ; $9A53
.dw _CamHeader_L071
.dw _CamHeader_L071
.dw _CamHeader_L071

_CamHeader_L08:     ; $9A59
.dw _CamHeader_L081 
.dw _CamHeader_L081
.dw _CamHeader_L081

_CamHeader_L09:     ; $9A5F
.dw _CamHeader_L091
.dw _CamHeader_L091
.dw _CamHeader_L091


_CamHeader_GTZ1:    ; $9A65
;       .- Unknown
;      |       .- Unknown
;      |      |       .- Player X-pos
;      |      |      |     .- Player Y-pos
;  |------|------|------|-----|
.dw $0000, $0142, $0063, $01CE 
_CamHeader_GTZ2:    ; $9A6D
.dw $0016, $0092, $007E, $010E 
_CamHeader_GTZ3:    ; $9A75
.dw $0006, $0072, $006E, $00EE

_CamHeader_SPZ1:    ; $9A7D
.dw $0000, $0110, $004E, $0180
_CamHeader_SPZ2:    ; $9A85
.dw $0000, $0020, $0053, $0080
_CamHeader_SPZ3:    ; $9A8D
.dw $0EF0, $0152, $0F60, $01CE

_CamHeader_MJZ1:    ; $9A95
.dw $0000, $02B5, $004E, $032E
_CamHeader_MJZ2:    ; $9A9D
.dw $0006, $02D4, $006E, $034E
_CamHeader_MJZ3:    ; $9AA5
.dw $0000, $0012, $0064, $008E 

_CamHeader_RWZ1:    ; $9AAD
.dw $0000, $00F3, $0064, $0163
_CamHeader_RWZ2:    ; $9AB5
.dw $0000, $021C, $0056, $02AE
_CamHeader_RWZ3:    ; $9ABD
.dw $0043, $03FD, $00AB, $048E

_CamHeader_TPZ1:    ; $9AC5
.dw $0000, $0063, $006E, $00CE
_CamHeader_TPZ2:    ; $9ACD
.dw $0000, $0000, $006E, $006E
_CamHeader_TPZ3:    ; $9AD5
.dw $0000, $00FF, $005C, $016E

_CamHeader_ADZ1:    ; $9ADD
.dw $0003, $02F6, $0041, $036E
_CamHeader_ADZ2:    ; $9AE5
.dw $0045, $0094, $009D, $010E
_CamHeader_ADZ3:    ; $9AED
.dw $0004, $01DC, $004E, $026E
_CamHeader_ADZ4:    ; $9AF5
.dw $02E7, $01D2, $034F, $025E

_CamHeader_L061:    ; $9AFD
.dw $0000, $01B2, $0057, $022E
_CamHeader_L062:    ; $9B05
.dw $0000, $08D6, $004C, $094E
_CamHeader_L063:    ; $9B0D
.dw $0000, $013A, $0040, $01AE

_CamHeader_L071:    ; $9B15
.dw $0000, $002E, $0000, $0000

_CamHeader_L081:    ; $9B1D
.dw $10E0, $00A4, $1130, $012E

_CamHeader_L091:    ; $9B25
.dw $0003, $0095, $004A, $010E
