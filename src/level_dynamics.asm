; =============================================================================
;  Engine_UpdateLevelDynamics()
; -----------------------------------------------------------------------------
;  Processes the logic that updates the dynamic elements for the current
;  level.
; -----------------------------------------------------------------------------
;   Params:
;       None.
; -----------------------------------------------------------------------------
Engine_UpdateLevelDynamics:     ; $8000
	ld   a, (RAM_D3FE)
	or   a
	ret  nz

	ld   a, (LevelDynamic2)
	cp   $0A
	jr   z, +
	cp   $12
	jr   z, +
	ld   a, (RAM_D144)
	and  $14
	ret  nz

	ld   a, (BgPaletteControl)
	or   a
	ret  nz

+:  ld   iy, LevelDynamic1
	ld   b, $04
-:  push bc
	call _Dynamics_ProcessDynamic
	ld   bc, $0008
	add  iy, bc
	pop  bc
	djnz -
	ret


_Dynamics_ProcessDynamic:       ; $802E
	ld   a, (iy + $00)
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, _Data_Dynamics_Handlers
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)
	
	
;======================================================
_Data_Dynamics_Handlers:     ; $803E
.dw DynamicsHandler_DoNothing           ; $00
.dw DynamicsHandler_GTZ_Water           ; $01 - GTZ dynamic water art
.dw LABEL_B29_80FE
.dw LABEL_B29_8155
.dw LABEL_B29_8190
.dw LABEL_B29_81C9
.dw LABEL_B29_8220
.dw LABEL_B29_8221
.dw LABEL_B29_825F
.dw LABEL_B29_82B5
.dw LABEL_B29_83EC
.dw LABEL_B29_8425
.dw LABEL_B29_8470
.dw DynamicsHandler_ADZ_GreenFader	    ; $0D - ADZ green fader
.dw DynamicsHandler_ADZ_FlashingLights  ; $0E - ADZ flashing lights
.dw LABEL_B29_859C
.dw LABEL_B29_85F3
.dw LABEL_B29_8625
.dw LABEL_B29_86B8
.dw LABEL_B29_86F1

DynamicsHandler_DoNothing:     ; $8066
	ret

LABEL_B29_8067:
	inc  (iy+$03)
	ld   a, (iy+$03)
	cp   b
	jr   c, LABEL_B29_8081
	ld   (iy+$03), $00
	ld   a, (iy+$02)
	inc  a
	cp   c
	jr   c, +
	xor  a
+:	ld   (iy+$02), a
	xor  a
	ret

LABEL_B29_8081
	ld   a, $ff
	ret


; =============================================================================
;  Dynamics_IncrementCountersWithCheck(uint16 effect_ptr, 
;                                      uint8 c1_limit,
;                                      uint8 c2_limit)
; -----------------------------------------------------------------------------
;  Increments the counters for the current dynamic effect. Checks some game
;  state variable before incrementing the second counter.
; -----------------------------------------------------------------------------
;   Params:
;       IY - pointer to effect data structure.
;       B  - counter 1 limit.
;       C  - counter 2 limit.
;   Returns:
;       A  - 0 if second counter was incremented, otherwise $FF.
; -----------------------------------------------------------------------------
Dynamics_IncrementCountersWithCheck:        ; $8084
	inc  (iy + $03)
	ld   a, (iy + $03)
	cp   b
	jr   c, ++
    
	ld   hl, $d42a
	bit  7, (hl)
	ret  nz
    
	ld   (iy + $03), $00
	ld   a, (iy + $02)
	inc  a
	cp   c
	jr   c, +
	xor  a
+:	ld   (iy + $02), a
	xor  a
	ret

++: ld   a, $ff
	ret

; =============================================================================
;  DynamicsHandler_GTZ_Water()
; -----------------------------------------------------------------------------
;  Dynamics handler for the GTZ dynamic water art.
; -----------------------------------------------------------------------------
;
; -----------------------------------------------------------------------------
DynamicsHandler_GTZ_Water:      ; $80A7
	; increment the counters
    ld   b, $08
	ld   c, $04
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
    
    ; jump to the handler for the value in counter 2
	ld   a, (iy + $02)
	and  $03
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, _DynamicsHandler_GTZ_Water_StateTable
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

_DynamicsHandler_GTZ_Water_StateTable:      ; $80C2
.dw _DynamicsHandler_GTZ_Water_State1
.dw _DynamicsHandler_GTZ_Water_State2 
.dw _DynamicsHandler_GTZ_Water_State3
.dw _DynamicsHandler_GTZ_Water_State4

_DynamicsHandler_GTZ_Water_State1:      ; $80CA
	ld   hl, Data_Dynamics_GTZ_Water_Frame1
	ld   de, $2E60
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

_DynamicsHandler_GTZ_Water_State2:      ; $80D7
	ld   hl, Data_Dynamics_GTZ_Water_Frame2
	ld   de, $2E60
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

_DynamicsHandler_GTZ_Water_State3:      ; $80E4
	ld   hl, Data_Dynamics_GTZ_Water_Frame3
	ld   de, $2E60
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

_DynamicsHandler_GTZ_Water_State4:      ; $80F1
	ld   hl, Data_Dynamics_GTZ_Water_Frame4
	ld   de, $2E60
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret



LABEL_B29_80FE:
	ld   b, $0a
	ld   c, $04
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $03
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_8119
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_8119:
.dw LABEL_B29_8121
.dw LABEL_B29_812E 
.dw LABEL_B29_813B
.dw LABEL_B29_8148


LABEL_B29_8121:
	ld   hl, $8e14
	ld   de, $2d60
	ld   bc, $0120
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_812E:
	ld   hl, $8f74
	ld   de, $2d60
	ld   bc, $0120
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_813B:
	ld   hl, $90d4
	ld   de, $2d60
	ld   bc, $0120
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8148:
	ld   hl, $9234
	ld   de, $2d60
	ld   bc, $0120
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8155:
	ld   a, ($d48f)
	or   a
	ret  nz
	inc  (iy+$03)
	ld   a, (iy+$03)
	cp   $04
	ret  c
	ld   a, $ff
	ld   ($d4f3), a
	ld   (iy+$03), $00
	bit  0, (iy+$01)
	ld   a, (iy+$02)
	ld   ($d4d1), a
	jr   nz, LABEL_B29_8185
	inc  (iy+$02)
	ld   a, (iy+$02)
	cp   $0f
	ret  c
	inc  (iy+$01)
	ret

LABEL_B29_8185:
	dec  (iy+$02)
	ld   a, (iy+$02)
	ret  nz
	inc  (iy+$01)
	ret

LABEL_B29_8190:
	ld   b, $04
	ld   c, $02
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_81AB
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_81AB:
.dw LABEL_B29_81AF 
.dw LABEL_B29_81BC 


LABEL_B29_81AF:
	ld   hl, $9594
	ld   de, $2960
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_81BC:
	ld   hl, $9614
	ld   de, $2960
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_81C9:
	ld   b, $08
	ld   c, $04
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $03
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_81E4
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_81E4:
.dw LABEL_B29_81EC
.dw LABEL_B29_81F9 
.dw LABEL_B29_8206
.dw LABEL_B29_8213


LABEL_B29_81EC:
	ld   hl, $94d4
	ld   de, $2fc0
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_81F9:
	ld   hl, $9514
	ld   de, $2fc0
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8206:
	ld   hl, $9554
	ld   de, $2fc0
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8213:
	ld   hl, $9514
	ld   de, $2fc0
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8220:
	ret

LABEL_B29_8221:
	ld   a, ($d12f)
	or   a
	ret  nz
	ld   b, $03
	ld   c, $02
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_8241
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)


DATA_B29_8241:
.dw LABEL_B29_8245
.dw LABEL_B29_8252

LABEL_B29_8245:
	ld   hl, $9394
	ld   de, $3460
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8252:
	ld   hl, $93b4
	ld   de, $3460
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret


LABEL_B29_825F:
	ld   a, ($d12f)
	or   a
	ret  nz
	ld   b, $08
	ld   c, $02
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_827F
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_827F:
.dw LABEL_B29_8283
.dw LABEL_B29_829C


LABEL_B29_8283:
	ld   hl, $93d4
	ld   de, $37a0
	ld   bc, $0060
	call VF_VDP_CopyToVRAM
	ld   hl, $9494
	ld   de, $3360
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_829C:
	ld   hl, $9434
	ld   de, $37a0
	ld   bc, $0060
	call VF_VDP_CopyToVRAM
	ld   hl, $94b4
	ld   de, $3360
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_82B5:
	ld   a, (iy+$01)
	or   a
	jp   z, LABEL_B29_8380
	dec  (iy+$03)
	ret  nz
	ld   (iy+$03), $02
	ld   hl, ($d4b1)
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	ld   de, $9a14
	bit  0, (iy+$02)
	jr   z, +

	ld   de, $9ad4

+:	ld   bc, $0320
	call Logic_vtable + $14A
	ld   hl, ($d4b1)
	dec  hl
	dec  hl
	ld   ($d4b1), hl
	dec  (iy+$02)
	ld   a, (iy+$02)
	or   a
	jp   z, LABEL_B29_83E4
	ret


DATA_B29_82F0:
.dw $38CC
.dw $390C
.dw $394C
.dw $398C
.dw $39CC
.dw $3A0C
.dw $3A4C
.dw $3A8C
.dw $3ACC
.dw $3B0C
.dw $3B4C
.dw $3B8C
.dw $3BCC
.dw $3C0C
.dw $3C4C
.dw $3C8C
.dw $3CCC
.dw $3D0C
.dw $3D4C
.dw $3D8C
.dw $3DCC
.dw $3E0C
.dw $3E4C
.dw $3E8C
.dw $3ECC
.dw $3F0C
.dw $3F4C
.dw $3F8C
.dw $38CC
.dw $390C

DATA_B29_832C:
.dw $394C
.dw $398C
.dw $39CC

DATA_B29_8332:
.dw $3A0C
.dw $3A4C
.dw $3A8C
.dw $3ACC
.dw $3B0C
.dw $3B4C
.dw $3B8C
.dw $3BCC
.dw $3C0C
.dw $3C4C
.dw $3C8C
.dw $3CCC
.dw $3D0C
.dw $3D4C
.dw $3D8C
.dw $3DCC
.dw $3E0C
.dw $3E4C
.dw $3E8C
.dw $3ECC
.dw $3F0C
.dw $3F4C
.dw $3F8C
.dw $38CC
.dw $390C
.dw $394C
.dw $398C
.dw $39CC
.dw $3A0C
.dw $3A4C
.dw $3A8C
.dw $3ACC
.dw $3B0C
.dw $3B4C
.dw $3B8C
.dw $3BCC
.dw $3C0C
.dw $3C4C
.dw $3C8C


LABEL_B29_8380:
	ld   hl, LevelDynamic2
	ld   b, $18
	xor  a
-:	ld   (hl), a
	inc  hl
	djnz -
	ld   a, $cf
	ld   ($de05), a
	ld   a, ($0000)
	ld   hl, $d503
	res  3, (hl)
	ld   a, $4b
	ld   ($d502), a
	ld   a, $0a
	ld   (LevelDynamic2), a
	ld   a, $ff
	ld   (iy+$01), a
	ld   ($d4b0), a
	ld   (iy+$02), $1a
	ld   (iy+$03), $02
	ld   bc, ($d19a)
	ld   de, ($d19c)
	call Logic_vtable + $21
	ld   a, ($d199)
	srl  a
	srl  a
	srl  a
	add  a, a
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   hl, DATA_B29_832C
	ld   ($d4b1), hl
	ld   hl, $97d4
	ld   de, $1800
	ld   bc, $0140
	call VF_VDP_CopyToVRAM

	ld   c, $4c
	ld   h, $00
	call $0354
	ret


LABEL_B29_83E4:
	call $033f
	ld   (iy+$00), $00
	ret

LABEL_B29_83EC:
	ld   b, $02
	ld   c, $02
	call LABEL_B29_8067
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_8407
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_8407:
.dw LABEL_B29_840B
.dw LABEL_B29_8418

LABEL_B29_840B:
	ld   hl, $9914
	ld   de, $1840
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8418:
	ld   hl, $9994
	ld   de, $1840
	ld   bc, $0080
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_8425:
	ld   b, $20
	ld   c, $02
	call LABEL_B29_8067
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_8440
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)


DATA_B29_8440:
.dw LABEL_B29_8444
.dw LABEL_B29_8463

LABEL_B29_8444:
	ld   a, ($D168)
	add  a, a
	ld   hl, DATA_B29_845B
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ld   hl, $3C54
	ld   bc, $010C
	;copy screen mappings
	jp   Logic_vtable + $14A

DATA_B29_845B:
.ifeq SMS_VERSION 1
    .dw $BCD0
    .dw $BCD0
    .dw $BCD0
    .dw $BCD0
.else
    .dw $BCD0
    .dw $BD00
    .dw $BD00
    .dw $BD00
.endif


LABEL_B29_8463:
	ld   hl, $3c54
	ld   de, $bce8
	ld   bc, $010c
	call Logic_vtable + $14A
	ret

LABEL_B29_8470:
	ld   a, (iy+$03)
	or   a
	jr   z, LABEL_B29_847A
	dec  (iy+$03)
	ret

LABEL_B29_847A
	ld   (iy+$03), $03
	ld   a, (CurrentLevel)
	cp   $01
	jr   z, +
	or   a
	jp   nz, LABEL_B29_84AE

+:	ld   a, (iy+$02)
	or   a
	jr   nz, LABEL_B29_8497
	ld   hl, $0fff
	ld   ($d4f1), hl
	jr   LABEL_B29_849D

LABEL_B29_8497:
	ld   hl, ($dbdc)
	ld   ($d4f1), hl
LABEL_B29_849D:
	ld   a, $ff
	ld   ($d4f3), a
	inc  (iy+$02)
	ld   a, (iy+$02)
	cp   $02
	jp   nc, Logic_vtable + $09
	ret

LABEL_B29_84AE:
	ld   a, (iy+$02)
	or   a
	jr   nz, LABEL_B29_84BC
	ld   hl, $0fff
	ld   ($d4ef), hl
	jr   LABEL_B29_84C2

LABEL_B29_84BC:
	ld   hl, ($dbdc)
	ld   ($d4ef), hl
LABEL_B29_84C2:
	ld   a, $ff
	ld   ($d4f3), a
	inc  (iy+$02)
	ld   a, (iy+$02)
	cp   $02
	jp   nc, Logic_vtable + $09
	ret


; =============================================================================
;  DynamicsHandler_ADZ_GreenFader()
; -----------------------------------------------------------------------------
;  Handler for the ADZ green fading light effect.
; -----------------------------------------------------------------------------
;  
; -----------------------------------------------------------------------------
DynamicsHandler_ADZ_GreenFader:        ; $84D3
	ld   a, ($D12E)
	and  $07
	cp   $07
	ret  nz
	
.ifeq SMS_VERSION 1
	;SMS MOD - slow the effect down a bit
	ld   a, (iy + $01)
	and  $04
	;END SMS MOD
.else
    bit  0, (iy + $01)
.endif
	
	jr   nz, _DynamicsHandler_ADZ_GreenFader_FadeBack
	;copy the colour into HL
	
.ifeq SMS_VERSION 1
	;SMS MOD - replace with SMS colour
	ld   a, (WorkingCRAM + $0D)
	ld   de, $0005
	;END SMS MOD
.else
    ld   hl, ($D4CD)
	ld   de, $0262
.endif
	call VF_Engine_FadeFromColourToColour
	
	;copy the colour back to the palette
.ifeq SMS_VERSION 1
	;SMS MOD
	ld   ($D4C0), a
	;END SMS MOD
.else
    ld   ($D4CD), hl
.endif
	
	ld   a, $FE
	ld   (PaletteUpdatePending), a
.ifeq SMS_VERSION 1
    ld   a, (WorkingCRAM + $0D)
	sub  $05
	ret  c
.else
	ld   de, $0262
	xor  a
	sbc  hl, de
	ret  nz
.endif
	inc  (iy + $01)
	ret

_DynamicsHandler_ADZ_GreenFader_FadeBack:
.ifeq SMS_VERSION 1
	;SMS MOD
	ld   a, (WorkingCRAM + $0D)
	;END SMS MOD
.else
    ld   hl, ($d4cd)
.endif
	
	ld   de, $0000
	call VF_Engine_FadeFromColourToColour
	
.ifeq SMS_VERSION 1
	;SMS MOD
	ld   (WorkingCRAM + $0D), a
	or   a		;check for zero
	;END SMS MOD
.else
    ld   ($d4cd), hl
.endif
	
	ld   a, $FE
	ld   (PaletteUpdatePending), a
.ifeq SMS_VERSION 0
	ld   a, h
	or   l
.endif
	ret  nz
	inc  (iy + $01)
	ret


; =============================================================================
;  DynamicsHandler_ADZ_FlashingLights()
; -----------------------------------------------------------------------------
;  Handler for the ADZ flashing lights
; -----------------------------------------------------------------------------
;  
; -----------------------------------------------------------------------------
DynamicsHandler_ADZ_FlashingLights:     ; $8515
	ld   a, (iy + $03)
	or   a
	jr   z, +
	dec  (iy + $03)
	ret

+:	ld   a, (iy+$01)
	add  a, a
	ld   hl, _DynamicsHandler_ADZ_FlashingLights_EffectTable
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

_DynamicsHandler_ADZ_FlashingLights_EffectTable:       ; $852F
.dw _DynamicsHandler_ADZ_FlashingLights_Effect01
.dw _DynamicsHandler_ADZ_FlashingLights_Effect02
.dw _DynamicsHandler_ADZ_FlashingLights_Effect03
.dw _DynamicsHandler_ADZ_FlashingLights_Effect04 

_DynamicsHandler_ADZ_FlashingLights_Effect01:
.ifeq SMS_VERSION 1
    ld   (iy + $03), $30
	inc  (iy + $01)
	xor  a
	ld   (WorkingCRAM + $0E), a
	ld   (WorkingCRAM + $0F), a
	ld   a, $FF
	ld   (PaletteUpdatePending), a
.else
	ld   (iy+$03), $30
	inc  (iy+$01)
	ld   hl, $0000
	ld   ($d4cf), hl
	ld   hl, $0000
	ld   ($d4d1), hl
	ld   a, $ff
	ld   ($d4f3), a
.endif
    ret

_DynamicsHandler_ADZ_FlashingLights_Effect02:       ; $8550
.ifeq SMS_VERSION 1
	ld   (iy + $03), $03
	inc  (iy + $01)
	ld   a, $3F
	ld   (WorkingCRAM + $0E), a
	xor  a
	ld   (WorkingCRAM + $0F), a
	ld   a, $FF
	ld   (PaletteUpdatePending), a
.else
    ld   (iy+$03), $03
	inc  (iy+$01)
	ld   hl, $0fdd
	ld   ($d4cf), hl
	ld   hl, $0000
	ld   ($d4d1), hl
	ld   a, $ff
	ld   ($d4f3), a
.endif
	ret

_DynamicsHandler_ADZ_FlashingLights_Effect03:       ; $8569
.ifeq SMS_VERSION 1
    ld   (iy + $03), $30
	inc  (iy + $01)
	xor  a
	ld   (WorkingCRAM + $0E), a
	ld   (WorkingCRAM + $0F), a
	ld   a, $FF
	ld   (PaletteUpdatePending), a
.else
    ld   (iy+$03), $30
	inc  (iy+$01)
	ld   hl, $0000
	ld   ($d4cf), hl
	ld   hl, $0000
	ld   ($d4d1), hl
	ld   a, $ff
	ld   ($d4f3), a
.endif
	ret

_DynamicsHandler_ADZ_FlashingLights_Effect04:       ; $8582
.ifeq SMS_VERSION 1
	ld   (iy + $03), $03
	ld   (iy + $01), $00
	xor  a
	ld   (WorkingCRAM + $0E), a
	ld   a, $3F
	ld   (WorkingCRAM + $0F), a
	ld   a, $FF
	ld   (PaletteUpdatePending), a
.else
    ld   (iy+$03), $03
	ld   (iy+$01), $00
	ld   hl, $0000
	ld   ($d4cf), hl
	ld   hl, $0dff
	ld   ($d4d1), hl
	ld   a, $ff
	ld   ($d4f3), a
.endif
	ret


.org $059C
LABEL_B29_859C:
	ld   b, $08
	ld   c, $04
	call Dynamics_IncrementCountersWithCheck
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $03
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_85B7
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_85B7:
.dw LABEL_B29_85BF
.dw LABEL_B29_85CC
.dw LABEL_B29_85D9
.dw LABEL_B29_85E6

LABEL_B29_85BF:
	ld   hl, $96d4
	ld   de, $3360
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_85CC:
	ld   hl, $9714
	ld   de, $3360
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_85D9:
	ld   hl, $9754
	ld   de, $3360
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_85E6:
	ld   hl, $9794
	ld   de, $3360
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_85F3:
	ld   a, (iy+$03)
	or   a
	jr   z, LABEL_B29_85FD
	dec  (iy+$03)
	ret

LABEL_B29_85FD:
	ld   a, (iy+$01)
	or   a
	jr   nz, LABEL_B29_8617
	ld   (iy+$03), $01
	ld   (iy+$01), $ff
	ld   hl, $0fff
	ld   ($d4cb), hl
	ld   a, $01
	ld   ($d4f3), a
	ret

LABEL_B29_8617:
	ld   hl, $0000
	ld   ($d4cb), hl
	ld   a, $01
	ld   ($d4f3), a
	jp   $0309

LABEL_B29_8625:
	ld   a, (iy+$01)
	or   a
	jp   z, LABEL_B29_866D
	dec  (iy+$03)
	ret  nz
	ld   (iy+$03), $02
	ld   hl, ($d4b1)
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	ld   de, $9d94
	bit  0, (iy+$02)
	jr   z, +
	
	ld   de, $9e54
	
+:	ld   bc, $0320
	call Logic_vtable + $14A
	ld   hl, ($d4b1)
	inc  hl
	inc  hl
	ld   ($d4b1), hl
	dec  (iy+$02)
	ld   a, (iy+$02)
	or   a
	jp   z, LABEL_B29_8660
	ret

LABEL_B29_8660:
	call Logic_vtable + $3F
	ld   (iy+$00), $00
	ld   hl, $d144
	set  4, (hl)
	ret

LABEL_B29_866D:
	ld   hl, LevelDynamic2
	ld   b, $18
	xor  a
-:	ld   (hl), a
	inc  hl
	djnz -
	ld   a, $cf
	ld   ($de05), a
	ld   a, $12
	ld   (LevelDynamic2), a
	ld   a, $ff
	ld   (iy+$01), a
	ld   ($d3fc), a
	ld   ($d4b0), a
	ld   (iy+$02), $1a
	ld   (iy+$03), $02
	call Logic_vtable + $12
	ld   a, ($d199)
	srl  a
	srl  a
	srl  a
	add  a, a
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   hl, DATA_B29_8332
	ld   ($d4b1), hl
	ld   hl, $9b94
	ld   de, $1800
	ld   bc, $0100
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_86B8
	ld   b, $02
	ld   c, $02
	call LABEL_B29_8067
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_86D3
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_86D3:
.dw LABEL_B29_86D7
.dw LABEL_B29_86E4

LABEL_B29_86D7:
	ld   hl, $9C94 
	ld   de, $1800
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_86E4:
	ld   hl, $9d14
	ld   de, $1800
	ld   bc, $0040
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_86F1:
	ld   b, $02
	ld   c, $02
	call LABEL_B29_8067
	or   a
	ret  nz
	ld   a, (iy+$02)
	and  $01
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, DATA_B29_870C
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
	jp   (hl)

DATA_B29_870C:
.dw LABEL_B29_8710
.dw LABEL_B29_871D


LABEL_B29_8710:
	ld   hl, $9694
	ld   de, $2a60
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret

LABEL_B29_871D:
	ld   hl, $96b4
	ld   de, $2a60
	ld   bc, $0020
	call VF_VDP_CopyToVRAM
	ret


; =============================================================================
;  Engine_LoadDynamicsHeader()
; -----------------------------------------------------------------------------
;  Loads the level's dynamics header (collision, animated rings, cycling
;  palettes).
; -----------------------------------------------------------------------------
;  Params:
;       ($D145) - Current level.
;       ($D147) - Current act.
; -----------------------------------------------------------------------------
Engine_LoadDynamicsHeader:      ; $872A
    ; get a pointer to the ring headers for the current level
	ld   a, (CurrentLevel)
	add  a, a
	ld   l, a
	ld   h, $00
	ld   de, Data_RingArtHeaders
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
    
    ; get a pointer to the ring headers for the current act
	ld   a, (CurrentAct)
	add  a, a
	ld   l, a
	ld   h, $00
	add  hl, de
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	ex   de, hl
    
    ; get the collision data pointer
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	inc  hl
	ld   (CollisionDataPtr), de
    
    ; get the ring art source pointer
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	inc  hl
	ld   (RingArtPtr), de
    
    ; get the ring art vram dest pointer
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	inc  hl
	ld   (LevelRingArtVRAMPtr), de
    
    ; get the cycling palette indices
	ld   a, (hl)
	ld   (LevelDynamic1), a
	inc  hl
	ld   a, (hl)
	ld   (LevelDynamic2), a
	inc  hl
	ld   a, (hl)
	ld   ($d4a0), a
	inc  hl
	ld   a, (hl)
	ld   ($d4a8), a
	ret

; =============================================================================
;  Engine_AnimateRings()
; -----------------------------------------------------------------------------
;  Updates the animated background rings.
; -----------------------------------------------------------------------------
;  Params:
;       None.
; -----------------------------------------------------------------------------
Engine_AnimateRings:        ; $8770
	ld   hl, $D42A
	bit  7, (hl)
	ret  nz

    ; increment the counter. move to the next animation
    ; frame after 8 ticks of the counter
	ld   hl, RingArtFrameCounter
	inc  (hl)
	ld   a, (hl)
	and  $07
	ret  nz

    ; change animation frame
	ld   a, (RingArtFrame)
	inc  a
	cp   $04
	jr   c, +
	xor  a
    
    ; copy ring art to the vdp
+:  ld   (RingArtFrame), a
	ld   a, (RingArtFrame)
	ld   hl, Data_RingArtFrames
	ld   d, $00
	ld   e, a
	add  hl, de
	ld   a, (hl)
	ld   (RingArtFrameNext), a
    
	ld   a, (RAM_D3E4)
	or   a
	ret  nz

    ; calculate an index into the ring art for 
    ; the current animation frame
    ; hl = RingArtFrame * 32
	ld   a, (RingArtFrame)
	add  a, a
	add  a, a
	add  a, a
	add  a, a
	add  a, a
	ld   l, a
	ld   h, $00
    
    ; offset from the base pointer
	add  hl, hl
	add  hl, hl
	ld   de, (RingArtPtr)
	add  hl, de
    
    ; read the destination pointer
	ld   de, (LevelRingArtVRAMPtr)
	ld   a, d
	or   e
	ret  z

    ; set the vram pointer
	ld   a, e
	out  ($BF), a
	ld   a, d
	or   $40
	out  ($BF), a
    
    ; copy to the vdp
	ld   b, $80
	ld   c, $BE
	otir
	ret


; Data from 747C5 to 747D4 (16 bytes)
Data_HudRingCharCodes:		;$87C5
.db $10, $12, $14, $16, $1C, $1E, $18, $1A
Data_HudRingIconOffsets:		;$87CD
.ifeq SMS_VERSION 1
    .db $10, $10, $14, $10 
.else
    .db $30, $30, $34, $30
.endif
Data_RingArtFrames:     ; $87D1
.db $01, $02, $03, $04


; =============================================================================
;  Engine_AnimateHudRing()
; -----------------------------------------------------------------------------
;  Updates the HUD ring.
; -----------------------------------------------------------------------------
;  Params:
;       None.
; -----------------------------------------------------------------------------
Engine_AnimateHudRing:		;$87D5
	ld   a, (RingArtFrame)
	add  a, a
	ld   l, a
	ld   h, $00
	ld   bc, Data_HudRingCharCodes
	add  hl, bc
	;set the rotating ring frame number
	ld   a, (hl)
	ld   (VDP_WorkingSAT_HPOS + $75), a
	inc  hl
	ld   a, (hl)
	ld   (VDP_WorkingSAT_HPOS + $77), a
	
	ld   a, (RingArtFrame)
	ld   l, a
	ld   h, $00
	ld   de, Data_HudRingIconOffsets
	add  hl, de
	;set the icon position
	ld   a, (hl)
	ld   (VDP_WorkingSAT_HPOS + $74), a
	ret


Data_RingArtHeaders:		;$87F8
.dw Data_RingArtHeaders_GTZ
.dw Data_RingArtHeaders_SPZ
.dw Data_RingArtHeaders_MJZ
.dw Data_RingArtHeaders_RWZ
.dw Data_RingArtHeaders_TPZ
.dw Data_RingArtHeaders_ADZ
.dw Data_RingArtHeaders_L06
.dw Data_RingArtHeaders_L06
.dw Data_RingArtHeaders_L06
.dw Data_RingArtHeaders_L06
.dw Data_RingArtHeaders_GTZ
.dw Data_RingArtHeaders_L06

Data_RingArtHeaders_GTZ:        ; $8810
.dw Data_RingArtHeaders_GTZ1
.dw Data_RingArtHeaders_GTZ1
.dw Data_RingArtHeaders_GTZ1

Data_RingArtHeaders_SPZ:        ; $8816
.dw Data_RingArtHeaders_SPZ1
.dw Data_RingArtHeaders_SPZ1
.dw Data_RingArtHeaders_SPZ3

Data_RingArtHeaders_MJZ:        ; $881C
.dw Data_RingArtHeaders_MJZ1
.dw Data_RingArtHeaders_MJZ1
.dw Data_RingArtHeaders_MJZ3

Data_RingArtHeaders_RWZ:        ; $8822
.dw Data_RingArtHeaders_RWZ1
.dw Data_RingArtHeaders_RWZ1
.dw Data_RingArtHeaders_RWZ1

Data_RingArtHeaders_TPZ:        ; $8828
.dw Data_RingArtHeaders_TPZ1
.dw Data_RingArtHeaders_TPZ1
.dw Data_RingArtHeaders_TPZ1

Data_RingArtHeaders_ADZ:        ; $882E
.dw Data_RingArtHeaders_ADZ1
.dw Data_RingArtHeaders_ADZ1
.dw Data_RingArtHeaders_ADZ1

Data_RingArtHeaders_L06:        ; $8834
.dw Data_RingArtHeaders_L061
.dw Data_RingArtHeaders_L061
.dw Data_RingArtHeaders_L061


Data_RingArtHeaders_GTZ1:       ; $883A
.dw $8000, Art_ucmp_Rings, $2C60, $0001, $0000

Data_RingArtHeaders_SPZ1:       ; $8844
.dw $8000, Art_ucmp_Rings, $2580, $0302, $0000
Data_RingArtHeaders_SPZ3:       ; 884E
.dw $8000, Art_ucmp_Rings, $2580, $0000, $0000

Data_RingArtHeaders_MJZ1:       ; $8858
.dw $8000, Art_ucmp_Rings, $2EA0, $0807, $0000
Data_RingArtHeaders_MJZ3:       ; $8862
.dw $8000, Art_ucmp_Rings, $2EA0, $0000, $0000

Data_RingArtHeaders_RWZ1:       ; $886C
.dw $8000, Art_ucmp_Rings, $2DC0, $0005, $0000

Data_RingArtHeaders_TPZ1:       ; $8876
.dw $8000, Art_ucmp_Rings, $2B20, $0004, $0000

Data_RingArtHeaders_ADZ1:       ; $8880
.dw $8000, Art_ucmp_Rings, $2620, $0E0D, $0013

Data_RingArtHeaders_L061:       ; $888A
.dw $8000, Art_ucmp_Rings, $3220, $000F, $0000


;uncompressed ring art
Art_ucmp_Rings:		;$8894
.incbin "art/art_ucmp_rings.bin"

;compressed ring art
Art_cmp_Rings		;$8A94
.incbin "art/art_cmp_rings.bin"


;padding
.db $00, $00, $00, $00, $00, $00, $00, $00


; -- GTZ Dynamic Water Art ----------------------------------------------------
Data_Dynamics_GTZ_Water_Frame1:
.incbin "dynamics/gtz/art_ucmp_water_01.bin"
Data_Dynamics_GTZ_Water_Frame2:
.incbin "dynamics/gtz/art_ucmp_water_02.bin"
Data_Dynamics_GTZ_Water_Frame3:
.incbin "dynamics/gtz/art_ucmp_water_03.bin"
Data_Dynamics_GTZ_Water_Frame4:
.incbin "dynamics/gtz/art_ucmp_water_04.bin"
; -----------------------------------------------------------------------------


.incbin "unknown/stt.gg.dat.1D1"
