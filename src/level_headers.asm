Data_LevelHeaders:		;$434F
.dw LevelHeader_GTZ 
.dw LevelHeader_SPZ 
.dw LevelHeader_MJZ 
.dw LevelHeader_RWZ 
.dw LevelHeader_TPZ 
.dw LevelHeader_ADZ 
.dw LABEL_438D 
.dw LABEL_4393 
.dw LevelHeader_GTZ 
.dw LABEL_4399 
.dw LABEL_439F 
.dw LevelHeader_GTZ 

LevelHeader_GTZ:
.dw LevelHeader_GTZ1 
.dw LevelHeader_GTZ2 
.dw LevelHeader_GTZ3 

LevelHeader_SPZ:
.dw LevelHeader_SPZ1 
.dw LevelHeader_SPZ2 
.dw LevelHeader_SPZ3 

LevelHeader_MJZ:
.dw LevelHeader_MJZ1 
.dw LevelHeader_MJZ2 
.dw LevelHeader_MJZ3 

LevelHeader_RWZ:
.dw LevelHeader_RWZ1 
.dw LevelHeader_RWZ2 
.dw LevelHeader_RWZ3 

LevelHeader_TPZ:
.dw LevelHeader_TPZ1 
.dw LevelHeader_TPZ2 
.dw LevelHeader_TPZ3 

LevelHeader_ADZ:
.dw LevelHeader_ADZ1 
.dw LevelHeader_ADZ2 
.dw LevelHeader_ADZ3 
.dw LevelHeader_ADZ3 

LABEL_438D:
.dw DATA_4531 
.dw DATA_4547 
.dw DATA_455D 

LABEL_4393:
.dw DATA_4573 
.dw DATA_4573 
.dw DATA_4573 

LABEL_4399:
.dw DATA_4531 
.dw DATA_4531 
.dw DATA_4531 

LABEL_439F:
.dw DATA_4589 
.dw DATA_4589 
.dw DATA_4589 


LevelHeader_GTZ1:
    .db :Mappings32_GTZ     ;metatile bank
    .dw Mappings32_GTZ      ;metatile pointer
    .db :Layout_GTZ1        ;layout bank
    .dw Layout_GTZ1         ;layout pointer
    .dw $00A8               ;level width
    .dw $FF58               ;2's comp level width
    .dw $0498               ;vert offset into layout
    .dw $0000               ;min camera x pos
    .dw $0008               ;min camera y pos
    .dw $1430               ;max camera x pos
    .dw $0238               ;max camera y pos
    .dw MultTable_168
LevelHeader_GTZ2:
    .db :Mappings32_GTZ
    .dw Mappings32_GTZ
    .db :Layout_GTZ2
    .dw Layout_GTZ2
    .dw $00A8
    .dw $FF58
    .dw $0498
    .dw $0000
    .dw $0008
    .dw $1430
    .dw $0238
    .dw MultTable_168
LevelHeader_GTZ3:
    .db :Mappings32_GTZ
    .dw Mappings32_GTZ
    .db $1A
    .dw $B049
    .dw $0040
    .dw $FFC0
    .dw $01C0
    .dw $0000
    .dw $0008
    .dw $0730
    .dw $0238
    .dw MultTable_64

LevelHeader_SPZ1:
    .db :Mappings32_SPZ
    .dw Mappings32_SPZ
    .db :Layout_SPZ1
    .dw Layout_SPZ1
    .dw $00A8
    .dw $FF58
    .dw $0498
    .dw $0000
    .dw $0008
    .dw $1430
    .dw $0238
    .dw MultTable_168
LevelHeader_SPZ2:
    .db :Mappings32_SPZ
    .dw Mappings32_SPZ
    .db :Layout_SPZ2_3
    .dw Layout_SPZ2_3
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_SPZ3:
    .db :Mappings32_SPZ
    .dw Mappings32_SPZ
    .db :Layout_SPZ2_3
    .dw Layout_SPZ2_3
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128

LevelHeader_MJZ1:
    .db $1B
    .dw $8000
    .db :Layout_MJZ1
    .dw Layout_MJZ1
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_MJZ2:
    .db $1B
    .dw $8000
    .db :Layout_MJZ2
    .dw Layout_MJZ2
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_MJZ3:
    .db $1B
    .dw $8000
    .db $14
    .dw $B280
    .dw $0040
    .dw $FFC0
    .dw $01C0
    .dw $0000
    .dw $0008
    .dw $0730
    .dw $0338
    .dw MultTable_64

LevelHeader_RWZ1:
    .db $14
    .dw $8000
    .db $14
    .dw $B5A5
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_RWZ2:
    .db $14
    .dw $8000
    .db :Layout_RWZ2
    .dw Layout_RWZ2
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_RWZ3:
    .db $14
    .dw $8000
    .db :Layout_RWZ3
    .dw Layout_RWZ3
    .dw $0060
    .dw $FFA0
    .dw $02A0
    .dw $0000
    .dw $0008
    .dw $0B30
    .dw $0438
    .dw MultTable_96

LevelHeader_TPZ1:
    .db $14
    .dw $99E0
    .db :Layout_TPZ1
    .dw Layout_TPZ1
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_TPZ2:
    .db $14
    .dw $99E0
    .db :Layout_TPZ2
    .dw Layout_TPZ2
    .dw $0030
    .dw $FFD0
    .dw $0150
    .dw $0000
    .dw $0008
    .dw $0530
    .dw $0938
    .dw MultTable_48
LevelHeader_TPZ3:
    .db $14
    .dw $99E0
    .db :Layout_TPZ3
    .dw Layout_TPZ3
    .dw $0040
    .dw $FFC0
    .dw $01C0
    .dw $0000
    .dw $0008
    .dw $0730
    .dw $0138
    .dw MultTable_64

LevelHeader_ADZ1:
    .db $1B
    .dw $9C00
    .db :Layout_ADZ1
    .dw Layout_ADZ1
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128
LevelHeader_ADZ2:
    .db $1B
    .dw $9C00
    .db :Layout_ADZ2
    .dw Layout_ADZ2
    .dw $0060
    .dw $FFA0
    .dw $02A0
    .dw $0000
    .dw $0008
    .dw $0B30
    .dw $0438
    .dw MultTable_96
LevelHeader_ADZ3:
    .db $1B
    .dw $9C00
    .db :Layout_ADZ3
    .dw Layout_ADZ3
    .dw $00A8
    .dw $FF58
    .dw $0498
    .dw $0000
    .dw $0008
    .dw $1430
    .dw $0238
    .dw MultTable_168

DATA_4531:
    .db $19
    .dw $8340
    .db :Layout_Level06_1
    .dw Layout_Level06_1
    .dw $00A8
    .dw $FF58
    .dw $0498
    .dw $0000
    .dw $0008
    .dw $1430
    .dw $0238
    .dw MultTable_168
DATA_4547:
    .db $19
    .dw $8340
    .db :Layout_Level06_2
    .dw Layout_Level06_2
    .dw $0030
    .dw $FFD0
    .dw $0150
    .dw $0000
    .dw $0008
    .dw $0530
    .dw $0938
    .dw MultTable_48
DATA_455D:
    .db $19
    .dw $8340
    .db $1A
    .dw $B2AB
    .dw $0080
    .dw $FF80
    .dw $0380
    .dw $0000
    .dw $0008
    .dw $0F30
    .dw $0338
    .dw MultTable_128


DATA_4573:
    .db :Mappings32_GTZ
    .dw Mappings32_GTZ
    .db :Layout_Level07
    .dw Layout_Level07
    .dw $0040
    .dw $FFC0
    .dw $01C0
    .dw $0000
    .dw $0008
    .dw $0730
    .dw $0038
    .dw MultTable_64
DATA_4589:
    .db :Mappings32_GTZ
    .dw Mappings32_GTZ
    .db :Layout_TimeAttack
    .dw Layout_TimeAttack
    .dw $00A8
    .dw $FF58
    .dw $0498
    .dw $0000
    .dw $0008
    .dw $1430
    .dw $0238
    .dw MultTable_168
