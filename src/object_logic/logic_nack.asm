;
; Logic for Nack/Fang
;

Logic_Nack:                       ; $B9CD
.dw LABEL_B22_B9F9
.dw LABEL_B22_B9FF
.dw LABEL_B22_BA26
.dw LABEL_B22_BA2E
.dw LABEL_B22_BA34
.dw LABEL_B22_BA40
.dw LABEL_B22_BA4C
.dw LABEL_B22_BA58
.dw LABEL_B22_BA64
.dw LABEL_B22_BA70
.dw LABEL_B22_BA7C
.dw LABEL_B22_BA82
.dw LABEL_B22_BAC0
.dw LABEL_B22_BB4C
.dw LABEL_B22_BB6A
.dw LABEL_B22_BB70
.dw LABEL_B22_BB78
.dw LABEL_B22_BBA7
.dw LABEL_B22_BBC4
.dw LABEL_B22_BBD8
.dw LABEL_B22_BBE2
.dw LABEL_B22_BBF0

LABEL_B22_B9F9:
.db $FF $01
  .dw LABEL_B22_BC08
.db $FF $00

LABEL_B22_B9FF:
.db $08 $16 
  .dw $0351
.db $FE $08 $17
.db $FE $08 $16
.db $FE $08 $17
.db $FE $08 $16
.db $FE $08 $17
.db $FE $08 $16
.db $FE $08 $17
.db $FE $08 $16
.db $FE $08 $17 
LABEL_B22_BA1E:
.db $E0 $17
  .dw LABEL_B22_BCDB
.db $FF $07
  .dw LABEL_B22_BA1E


LABEL_B22_BA26:
.db $FF $05
  .dw LABEL_B22_BD7D
  .dw LABEL_B22_BD9C
.db $FF $00

LABEL_B22_BA2E:
.db $08 $15
  .dw LABEL_B22_BDEF
.db $FF $00

LABEL_B22_BA34:
.db $FF $01
  .dw LABEL_B22_BE1A
.db $FF $05
  .dw LABEL_B22_BFAC
  .dw LABEL_B22_BCDB
.db $FF $00

LABEL_B22_BA40:
.db $FF $01
  .dw LABEL_B22_BE1F
.db $FF $05
  .dw LABEL_B22_BFAC
  .dw LABEL_B22_BCDB 
.db $FF $00

LABEL_B22_BA4C:
.db $FF $01
  .dw LABEL_B22_BE24
.db $FF $05
  .dw LABEL_B22_BFAC
  .dw LABEL_B22_BCDB
.db $FF $00

LABEL_B22_BA58:
.db $FF $05
  .dw LABEL_B22_BD7D
  .dw LABEL_B22_BDAE 
.db $FF $01
  .dw LABEL_B22_BE29
.db $FF $00

LABEL_B22_BA64:
.db $10 $00
  .dw $0351
LABEL_B22_BA68:
.db $E0 $00
  .dw LABEL_B22_BE5F
.db $FF $07
  .dw LABEL_B22_BA68

LABEL_B22_BA70:
.db $FF $01
  .dw LABEL_B22_BEA2
LABEL_B22_BA74:
.db $E0 $00
  .dw $0351
.db $FF $07
  .dw LABEL_B22_BA74

LABEL_B22_BA7C:
.db $FF $01
  .dw LABEL_B22_BEAF
.db $FF $00

LABEL_B22_BA82:
.db $08 $1A
  .dw $0351
.db $FE $08 $1B
.db $FE $08 $1A
.db $FE $08 $1B
.db $FE $04 $1C
.db $FF $02
  .dw $FF80
  .dw $FC00 
.db $08 $1C
  .dw LABEL_B22_BF04
LABEL_B22_BA9C:
.db $08 $1A
  .dw LABEL_B22_BF04
.db $FE $08 $1B
.db $FF $07
  .dw LABEL_B22_BA9C

LABEL_B22_BAA7:
.db $04 $1C
  .dw $0351
.db $FF $02
  .dw $0080
  .dw $FC00
.db $08 $1C
  .dw LABEL_B22_BF2D
LABEL_B22_BAB5:
.db $08 $1A
  .dw LABEL_B22_BF2D 
.db $FE $08 $1B
.db $FF $07
  .dw LABEL_B22_BAB5

LABEL_B22_BAC0:
.db $FF $0E
  .db $02
LABEL_B22_BAC3:
.db $08 $07
  .dw $0351
.db $08 $08
  .dw $0351
.db $FF $0F
  .dw LABEL_B22_BAC3
.db $FF $06
  .db $CF
.db $FF $04
  .db $07
  .dw $FFFC
  .dw $0060
  .db $06
.db $04 $07
  .dw $0351
.db $FF $04
  .db $07
  .dw $0004
  .dw $0060
  .db $06
.db $04 $07
  .dw $0351
.db $FF $04
  .db $07
  .dw $FFFC
  .dw $0060
  .db $06
.db $04 $08
  .dw $0351
.db $FF $04
  .db $07
  .dw $0004
  .dw $0060
  .db $06
.db $04 $08
  .dw $0351
.db $FF $04
  .db $07
  .dw $FFFC
  .dw $0060
  .db $06
.db $FF $0E
  .db $06
LABEL_B22_BB0D:
.db $FF $04
  .db $07
  .dw $FFFC
  .dw $0060
  .db $06
.db $04 $09
  .dw $0351
.db $FF $04
  .db $07
  .dw $0004
  .dw $0060
  .db $06
.db $04 $09
  .dw $0351
.db $FF $04
  .db $07 
  .dw $FFFC
  .dw $0060
  .db $06
.db $04 $0A
  .dw $0351
.db $FF $04
  .db $07
  .dw $0004
  .dw $0060
  .db $06
.db $04 $0A
  .dw $0351
.db $FF $0F
  .dw LABEL_B22_BB0D
LABEL_B22_BB41:
.db $20 $0A
  .dw LABEL_B22_BB49
.db $FF $07
  .dw LABEL_B22_BB41


LABEL_B22_BB49:
    jp    LABEL_B22_BCDB


LABEL_B22_BB4C:
.db $FF $0E
  .db $04
LABEL_B22_BB4F:
.db $FF $01
  .dw LABEL_B22_BF4A
.db $08 $09
  .dw $0351
.db $08 $0A
  .dw $0351
.db $FF $0F
  .dw LABEL_B22_BB4F
LABEL_B22_BB5F:
.db $E0 $0A
  .dw LABEL_B22_BB67
.db $FF $07
  .dw LABEL_B22_BB5F


LABEL_B22_BB67:
    jp    LABEL_B22_BCDB


LABEL_B22_BB6A:
.db $FF $01 
  .dw LABEL_B22_BF59
.db $FF $00

LABEL_B22_BB70:
.db $E0 $00
  .dw LABEL_B22_BF65
.db $FF $07
  .dw LABEL_B22_BB70

LABEL_B22_BB78:
.db $0C $01
  .dw LABEL_B22_BB8F
.db $FE $0E $0B
.db $FE $18 $0C
.db $10 $0D
  .dw $0351
.db $FE $0E $0E
.db $E0 $0E
  .dw LABEL_B22_BCDB
.db $FF $00

LABEL_B22_BB8F:
    ld a, ($d12e)
    rrca
    ret   nc
    ld    hl, ($d551)
    bit   4, (ix+4)
    jr    z, +
    inc   hl
    ld    ($d551), hl
    ret

+:  dec   hl
    ld    ($d551), hl
    ret


LABEL_B22_BBA7:
.db $FF $02
  .dw 0
  .dw 0
LABEL_B22_BBAD:
.db $08 $01
  .dw LABEL_B22_BF94
.db $FE $08 $02
.db $FE $08 $03
.db $FE $08 $04
.db $FE $08 $05
.db $FE $08 $06
.db $FF $07
  .dw LABEL_B22_BBAD


LABEL_B22_BBC4:
.db $FF $0E
  .db $06
LABEL_B22_BBC7:
.db $06 $07
  .dw $0351
.db $FE $06 $08
.db $FF $0F
  .dw LABEL_B22_BBC7
.db $E0 $07
  .dw LABEL_B22_BCDB
.db $FF $00

LABEL_B22_BBD8:
.db $FF $01
  .dw LABEL_B22_BFB1
.db $E0 $07
  .dw LABEL_B22_BCDB
.db $FF $00


LABEL_B22_BBE2:
.db $24 $0E
  .dw $0351
.db $FF $01
  .dw LABEL_B22_BFBE
.db $E0 $0E
  .dw LABEL_B22_BCDB
.db $FF $00

LABEL_B22_BBF0:
.db $FF $0E
  .db $06
LABEL_B22_BBF3:
.db $10 $12
  .dw $04FE
.db $FE $10 $13
.db $FF $0F
  .dw LABEL_B22_BBF3
.db $28 $14
  .dw $04FE
.db $E0 $14
  .dw LABEL_B22_BCCF
.db $FF $00

LABEL_B22_BC08:
    call  LABEL_B22_BC20
    push  ix
    pop   hl
    ld    ($D3E5), hl
    ld    a, 16
    ld    ($D548), a
    ld    ($D549), a
    xor   a
    ld    ($D55E), a
    jp    LABEL_B22_BCDB

LABEL_B22_BC20:
    ld    a, (CurrentLevel)
    or    a
    jr    z, +

    ; currentLevel * 12
    ld    b, a
    xor   a
    ld    c, 12
-:  add   a, c
    djnz  -

+:  ld    b, a
    ld    a, (CurrentAct)
    add   a, a
    add   a, a
    add   a, b
    ld    hl, DATA_B22_BC4B
    ld    d, $00
    ld    e, a
    add   hl, de
    ; set the object X pos
    ld    e, (hl)
    inc   hl
    ld    d, (hl)
    inc   hl
    ld    (ObjectSlot_01 + Object.X), de
    ; set the object Y pos
    ld    e, (hl)
    inc   hl
    ld    d, (hl)
    ld    (ObjectSlot_01 + Object.Y), de
    ret

DATA_B22_BC4B:
; GTZ1
.dw 0
.dw 0
; GTZ2
.dw 0
.dw 0
; GTZ3
.dw 0
.dw 0

; SPZ1
.dw 0
.dw 0
; SPZ2
.dw 0
.dw 0
; SPZ3
.dw 0
.dw 0

; MJZ1
.dw 0
.dw 0
; MJZ2
.dw 0
.dw 0
; MJZ3
.dw 0
.dw 0

; RWZ1
.dw 0
.dw 0
; RWZ2
.dw 0
.dw 0
; RWZ3
.dw 0
.dw 0

; TPZ1
.dw 0
.dw 0
; TPZ2
.dw 0
.dw 0
; TPZ3
.dw 0
.dw 0

; ADZ1
.dw 0
.dw 0
; ADZ2
.dw 0
.dw 0
; ADZ3
.dw $07AE
.dw $024E

; Level 6-1
.dw $138C
.dw $00EE
; Level 6-2
.dw 0
.dw 0
; Level 6-3
.dw 0
.dw 0

; Level 7-1
.dw 0
.dw 0
; Level 7-2
.dw 0
.dw 0
; Level 7-3
.dw 0
.dw 0

; Level 8-1
.dw 0
.dw 0
; Level 8-2
.dw 0
.dw 0
; Level 8-3
.dw 0
.dw 0

; Level 9-1
.dw $1180
.dw $012E
; Level 9-2
.dw $1180
.dw $012E
; Level 9-3
.dw 0
.dw 0

; Level 10-1
.dw $0080
.dw $0090
; Level 10-2
.dw 0
.dw 0
; Level 10-3
.dw 0
.dw 0


LABEL_B22_BCCF:
    ld    hl, ($d19c)
    ld    (Camera_maxY), hl
    ld    (ix + $35), h
    ld    (ix + $34), l
LABEL_B22_BCDB:
    ; FIXME: is this a debug register?
    ld    a, ($0001)

    ; currentLevel * 6
    ld    a, (CurrentLevel)
    or    a
    jr    z, +
    ld    b, a
    xor   a
    ld    c, $06
-:  add   a, c
    djnz  -

+:  ld    b, a
    ld    a, (CurrentAct)
    add   a, a
    add   a, b
    ld    hl, LABEL_B22_BD0A
    ld    d, $00
    ld    e, a
    add   hl, de

    ; read the pointer
    ld    e, (hl)
    inc   hl
    ld    d, (hl)
    ; index with offset $1E
    ld    a, (ObjectSlot_01 + Object.ix1E)
    ld    h, $00
    ld    l, a
    add   hl, de
    ld    a, (hl)
    ld    (ix + Object.StateNext), a
    inc   (ix + Object.ix1E)
    ret

LABEL_B22_BD0A:
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD58
.dw LABEL_B22_BD4C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD63
.dw LABEL_B22_BD63
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD6E
.dw LABEL_B22_BD7C
.dw LABEL_B22_BD7C


LABEL_B22_BD4C:
.db $08 $0A $04 $15 $01 $0B $01 $0C $05 $07 $06 $09

LABEL_B22_BD58:
.db $08 $0A $04 $15 $01 $0D $05 $0D $04 $07 $09

LABEL_B22_BD63:
.db $15 $01 $05 $02 $0E $0F $05 $11 $10 $07 $09

LABEL_B22_BD6E:
.db $13 $04 $01 $12 $12 $12 $12 $12 $12 $05 $14 $10 $07 $09

LABEL_B22_BD7C:
.db $09


LABEL_B22_BD7D:
    ld    a, (ix+23)
    bit   7, a
    jr    z, +
    neg   
+:  ld    b, a
    ld    a, $06
    sub   b
    ld    (ix+7), a
    ld    a, (ix+6)
    inc   a
    ld    (ix+6), a
    cp    $07
    ret   c

    ld    (ix+6), $01
    ret

LABEL_B22_BD9C:
    ld    hl, ($d551)
    ld    de, $11d0
    xor   a
    sbc   hl, de
    jr    c, LABEL_B22_BDAE
    xor   a
    ld    ($dbd7), a
    jp    $bcdb

LABEL_B22_BDAE:
    ld    a, ($d147)
    cp    $00
    jr    nz, +
    xor   a
    ld    ($dbd7), a
+:  call  $0381
    call  $0384
    ld    de, ($d556)
    bit   4, (ix+4)
    jr    z, LABEL_B22_BDDC

    bit   7, d
    jr    nz, +

    ld    hl, $0400
    xor   a
    sbc   hl, de
    ret   c
+:  ld    hl, $0020

    add   hl, de
    ld    ($d556), hl
    ret   

LABEL_B22_BDDC:
    bit   7, d
    jr    z, +
    ld    hl, $fc00
    xor   a
    sbc   hl, de
    ret   nc

+:  ld    hl, $ffe0
    add   hl, de
    ld    ($d556), hl
    ret


LABEL_B22_BDEF:
    call  $0381
    call  $0384
    ld    hl, ($d556)
    ld    de, $0040
    bit   7, h
    jr    z, LABEL_B22_BE08
    xor   a
    adc   hl, de
    jr    c, LABEL_B22_BE10
    ld    ($d556), hl
    ret


LABEL_B22_BE08:
    xor   a
    sbc   hl, de
    jr    nc, LABEL_B22_BE10

    ld    ($d556), hl
    ret


LABEL_B22_BE10:
    ld    hl, $0000
    ld    ($d556), hl
    jp    LABEL_B22_BCDB


LABEL_B22_BE1A:
    res   4, (ix+4)
    ret


LABEL_B22_BE1F:
    set   4, (ix+4)
    ret


LABEL_B22_BE24:
    ld    (ix + Object.ix1E), 0
    ret


LABEL_B22_BE29:
    bit   6, (ix+4)
    ret   z
    ld    (ix+2), $09
    ld    hl, ($d19a)
    ld    (Camera_minX), hl
    call  $0324
    ld    a, (CurrentLevel)
    cp    Level_ADZ
    jr    z, LABEL_B22_BE58
    ld    hl, (Camera_maxX_copy)
    ld    (Camera_maxX), hl
    ld    hl, (Camera_minY_copy)
    ld    (Camera_minY), hl
    ld    h, (ix + Object.ix35)
    ld    l, (ix + Object.ix34)
    ld    (Camera_maxY), hl
    ret


LABEL_B22_BE58:
    ld    hl, $07a5
    ld    (Camera_maxX), hl
    ret


LABEL_B22_BE5F:
    ld    a, (CurrentLevel)
    cp    $06
    jr    nz, +
    ld    a, (CurrentAct)
    cp    $00
    jr    nz, +
    ld    bc, $0180
    call  $0378
    or    a
    ret   z
    call  $03b4

+:  ld    bc, $00c0
    call  $0378
    or    a
    ret   z
    ld    bc, $00a0
    call  $037b
    or    a
    ret   z
    ld    a, Music_NacksTheme
    ld    (Sound_Music_Trigger), a
    call  $04fe
    ld    a, (CurrentAct)
    cp    Act_3
    jp    z, LABEL_B22_BCDB

    ld    c, $0b
    ld    h, $00
    call  $0354
    jp    LABEL_B22_BCDB


LABEL_B22_BEA2:
    ld    (ix+0), $ff
    di
    ld    a, $ff
    ld    ($dbda), a
    jp    $04a1


LABEL_B22_BEAF:
    ld    a, $ff
    ld    ($d182), a
    ld    a, (CurrentAct)
    cp    Act_3
    jr    z, LABEL_B22_BEF8

    ld    de, $1368           ; x
    ld    hl, $00ee           ; y
    ld    c, $00              ; $31
    ld    b, Object_38        ; objID
    call  LT_Engine_AllocateLinkedObjectXY

    call  LT_Engine_LoadNackSwitchArt
    call  LT_Engine_LoadNackExplosionArt2
    ld    h, (ix+21)
    ld    l, (ix+20)
    ld    de, $0050
    xor   a
    sbc   hl, de
    ld    ($d11c), hl
    ld    h, (ix+18)
    ld    l, (ix+17)
    ld    de, $00b0
    xor   a
    sbc   hl, de
    push  hl
    pop   bc
    ld    de, ($d11c)
    call  $0321
    call  $03b4
    jp    LABEL_B22_BCDB


LABEL_B22_BEF8:
    ld    bc, $0700
    ld    de, $01d0
    call  $0321
    jp    LABEL_B22_BCDB


LABEL_B22_BF04:
    ld    hl, ($d558)
    ld    de, $0020
    add   hl, de
    ld    ($d558), hl
    call  $0381
    bit   7, (ix+25)
    ret   nz
    call  $0384
    bit   1, (ix+34)
    ret   z
    ld    (ix+ Object.ix1F), $ff
    ld    hl, LABEL_B22_BAA7
    ld    (ObjectSlot_01 + Object.LogicSeqPtr), hl
    ld    (ix+7), $01
    ret


LABEL_B22_BF2D:
    ld    hl, ($d558)
    ld    de, $0020
    add   hl, de
    ld    ($d558), hl
    call  $0381
    bit   7, (ix+25)
    ret   nz
    call  $0384
    bit   1, (ix+34)
    ret   z
    jp    LABEL_B22_BCDB


LABEL_B22_BF4A:
    xor   a
    ld    ($dbd7), a
    ld    a, $04
    ld    (CameraScrollOverride), a
    ld    a, $b2
    ld    (Sound_SFX_Trigger), a
    ret

LABEL_B22_BF59:
    di
    ld    a, $ff
    ld    ($dbda), a
    call  $04a1
    jp    LABEL_B22_BCDB


LABEL_B22_BF65:
    ld    a, ($d3ff)
    or    a
    ret   z
    ld    a, $ff
    ld    ($dbd9), a
    ld    hl, ($d3e5)
    push  hl
    pop   iy
    ld    a, (iy+18)
    ld    (ix+18), a
    ld    a, (iy+17)
    ld    (ix+17), a
    ld    a, (iy+21)
    ld    (ix+21), a
    ld    a, (iy+20)
    ld    (ix+20), a
    xor   a
    ld    ($dbda), a
    jp    LABEL_B22_BCDB


LABEL_B22_BF94:
    ld    hl, ($d558)
    ld    de, $0010
    add   hl, de
    ld    ($d558), hl
    call  $0381
    call  $0384
    bit   1, (ix+34)
    ret   z
    jp LABEL_B22_BCDB


LABEL_B22_BFAC:
    ld    (ix+7), $02
    ret


LABEL_B22_BFB1:
    ld    hl, $0080
    ld    ($d551), hl
    ld    hl, $00a0
    ld    ($d554), hl
    ret


LABEL_B22_BFBE:
    ld    a, (EmeraldCounter)
    ld    b, a
    ld    a, $06
    sub   b
    ret   z
    ret   c
    ld    b, a
    ld    h, $0e
-:  push  bc
    ld    c, $30
    call  $0354
    push  hl
    call  LABEL_B22_BFDD
    pop   hl
    inc   h
    pop   bc
    djnz  -
    call  $033f
    ret


LABEL_B22_BFDD:
    ld    a, h
    sub   $0e
    add   a, a
    ld    hl, DATA_B22_BFF2
    ld    d, $00
    ld    e, a
    add   hl, de
    ld    e, (hl)
    inc   hl
    ld    d, (hl)
    ld    (iy + Object.VelX + 1), d
    ld    (iy + Object.VelX), e
    ret


DATA_B22_BFF2:
.dw $FF00
.dw $FF80
.dw $0000
.dw $0080
.dw $0100
.dw $0180

