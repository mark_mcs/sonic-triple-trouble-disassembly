DATA_9245:
.dw DATA_924B
.dw DATA_9251
.dw DATA_9263


DATA_924B:
.db $FF, $01 
    .dw LABEL_9269
.db $FF, $00

DATA_9251:
.db $07, $01
    .dw LABEL_9296
.db $07, $02
    .dw LABEL_9296
.db $07, $03
    .dw LABEL_9296
.db $07, $04
    .dw LABEL_9296
.db $FF, $00

DATA_9263:
.db $E0, $00
    .dw $988B
.db $FF, $00


LABEL_9269:
    ld   a, $10
    ld   (ix + Object.RightFacingIdx), a
    ld   (ix + Object.LeftFacingIdx), a
    
    ld   (ix + Object.StateNext), $01
    
    ld   (ix + Object.VelX + 1), $FE
    ld   (ix + Object.VelX), $00
    
    ld   (ix + Object.VelY + 1), $00
    ld   (ix + Object.VelY), $00
    
    ld   (ix + Object.X + 1), $10
    ld   (ix + Object.X), $00
    
    ld   (ix + Object.Y + 1), $01
    ld   (ix + Object.Y), $CE
    ret


LABEL_9296:
    call  Logic_vtable + $81
    call  Logic_vtable + $72        ; check collisions
    or    a
    ;ret   z
    nop
    ld    (ix + Object.StateNext), $02
    ld    a, SFX_Ring
    ld    (Sound_SFX_Trigger), a
    ld    a, ($D159)
    add   a, 1
    daa
    ld    ($D159), a
    ret
    