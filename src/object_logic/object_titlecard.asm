DATA_91AE:
.dw DATA_91B4
.dw DATA_91BA
.dw DATA_91CA


DATA_91B4:
.db $FF, $01 
    .dw LABEL_91D2
.db $FF, $00


DATA_91BA:
.db $FF, $05 
    .dw LABEL_91F7
    .dw Logic_vtable + $51
DATA_91C0:
.db $FF, $05
    .dw LABEL_91F7
    .dw LABEL_9203
.db $FF, $07 
    .dw DATA_91C0
    

DATA_91CA:
.db $FF, $05 
    .dw LABEL_91F7
    .dw Logic_vtable + $51
.db $FF, $00


LABEL_91D2:
    ld    (ix + Object.StateNext), $01
    ld    (ix + Object.RightFacingIdx), $10
    ld    (ix + Object.LeftFacingIdx), $10
    ld    (ix + Object.X + 1), $01
    ld    (ix + Object.X), $00
    ld    (ix + Object.Y), $67
    ld    (ix + Object.VelX + 1), $FC
    ld    (ix + Object.VelX), $00
    ld    (ix + Object.ix1E), $00
    ret
    
LABEL_91F7:
    ld    a, ($D147)
    inc   a
    ld    (ix + Object.AnimFrame),a
    ld    (ix + Object.FrameCounter), $20
    ret

LABEL_9203:
    ld    a, (ix + Object.ix1E)
    or    a
    jr    z, +
    ld    h, (ix + Object.VelX + 1)
    ld    l, (ix + Object.VelX)
    ld    de, $FFC0
    add   hl, de
    ld    (ix + Object.VelX + 1), h
    ld    (ix + Object.VelX), l
    
+:  call  Logic_vtable + $81
    bit   7, (ix + Object.VelX + 1)
    ret   z
    ld    h, (ix + Object.X + 1)
    ld    l, (ix + Object.X)
    ld    de, $00B8
    xor   a
    sbc   hl, de
    ret   nc
    ld    a, (ix + Object.ix1E)
    or    a
    jr    nz, +
    
    ld    (ix + Object.VelX + 1), $03
    ld    (ix + Object.VelX), $00
    ld    (ix + Object.ix1E), $FF
    ret
    
+:  inc (ix + Object.StateNext)
    ret