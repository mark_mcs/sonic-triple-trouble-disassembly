sound_play_pcm:
    push  bc
    push  de
    push  hl
    
    ld    l, $13
    ld    de, data_sound_sega_pcm
    ld    bc, $1D90
    jp    _sound_play_pcm_1

_sound_play_pcm_1:
    ld    a, $81
    out   ($7F), a
    ld    a, $00
    out   ($7F), a

    ld    a, $A1
    out   ($7F), a
    ld    a, $00
    out   ($7F), a

    ld    a, $C1
    out   ($7F), a
    ld    a, $00
    out   ($7F), a

--: push  bc
        ld    a, (de)
        cpl
        rrca
        rrca
        rrca
        rrca
        ld    b, l
-:      djnz  -

        and   $0F
        or    $90
        out   ($7F), a
        add   a, $20
        out   ($7F), a
        add   a, $20
        out   ($7F), a

        ld    b, l
-:      djnz  -

        ld    a, (de)
        cpl
        and   $0F
        or    $90
        out   ($7F), a
        add   a, $20
        out   ($7F), a
        add   a, $20
        out   ($7F), a

        inc   de
    pop   bc

    dec   bc
    ld    a, c
    or    b
    jp    nz, --

    xor   a
    ld    ($DE04), a
    pop   hl
    pop   de
    pop   bc

    ret

data_sound_sega_pcm:
.incbin "sound/sega_sound.bin"

