.def SMS_VERSION        0

.include "src/includes/sms.asm"
.include "src/includes/variables.asm"
.include "src/includes/variables_level.asm"
.include "src/includes/variables_gamemode.asm"
.include "src/includes/defines_powerups.asm"
.include "src/includes/defines_player_states.asm"
.include "src/includes/defines_music.asm"
.include "src/includes/objects.asm"
.include "src/includes/structures.asm"
.include "src/includes/macros.asm"


.MEMORYMAP
SLOTSIZE $4000
SLOT 0 $0000
SLOT 1 $4000
SLOT 2 $8000
SLOTSIZE $2000
SLOT 3 $C000
DEFAULTSLOT 2
.ENDME

.ROMBANKMAP
BANKSTOTAL 32
BANKSIZE $4000
BANKS 32
.ENDRO

.EMPTYFILL $FF

; I/O-port access
;   port $00: read      
;   port $7E: read      
;   port $7F:      write
;   port $BE:      write
;   port $BF: read write
;   port $DC: read      

; RAM access
.def RAM_D3F1 $D3F1 ; byte
.def RAM_DC26 $DC26 ; byte
.def RAM_DC29 $DC29 ;      word
.def RAM_DC2B $DC2B ;      word
.def RAM_DC28 $DC28 ; byte
.def RAM_DC27 $DC27 ; byte word
.def RAM_DC31 $DC31 ; byte
.def RAM_D18B $D18B ; byte
.def RAM_D18A $D18A ; byte
.def RAM_D188 $D188 ; byte
.def RAM_DBEB $DBEB ; byte
.def RAM_D428 $D428 ; byte
.def RAM_DBC8 $DBC8 ; byte
.def RAM_DBC7 $DBC7 ; byte
.def RAM_DBAD $DBAD ; byte
.def RAM_DBAB $DBAB ; byte
.def RAM_DBC6 $DBC6 ; byte
.def RAM_DBBF $DBBF ; byte
.def RAM_DBBD $DBBD ; byte
.def RAM_D15D $D15D ; byte word
.def RAM_DBB9 $DBB9 ; byte
.def RAM_D15E $D15E ; byte
.def RAM_D3CF $D3CF ; byte
.def RAM_D181 $D181 ; byte
.def RAM_D182 $D182 ; byte
.def RAM_DE15 $DE15 ; byte
.def RAM_DE01 $DE01 ; byte
.def RAM_DE0D $DE0D ; byte
.def RAM_DE10 $DE10 ; byte
.def RAM_DE0B $DE0B ; byte
.def RAM_DE0A $DE0A ; byte
.def RAM_DE0F $DE0F ; byte
.def RAM_DE11 $DE11 ; byte
.def RAM_DE09 $DE09 ; byte
.def RAM_DE02 $DE02 ; byte
.def RAM_DE04 $DE04 ; byte
.def RAM_D3EC $D3EC ; byte
.def RAM_D12E $D12E ; byte
.def RAM_DC51 $DC51 ; byte
.def RAM_DC52 $DC52 ; byte
.def RAM_D17E $D17E ; byte
.def RAM_D3FE $D3FE ; byte
.def RAM_DBB4 $DBB4 ; byte
.def RAM_DBB7 $DBB7 ; byte
.def RAM_DBB5 $DBB5 ; byte
.def RAM_D546 $D546 ; byte
.def RAM_D42F $D42F ; byte
.def RAM_DBDA $DBDA ; byte
.def RAM_D540 $D540 ; byte
.def RAM_D506 $D506 ; byte
.def RAM_D42C $D42C ; byte
.def RAM_DBC3 $DBC3 ; byte
.def RAM_D110 $D110 ;      word
.def RAM_D504 $D504 ; byte
.def RAM_D505 $D505 ; byte
.def RAM_D48D $D48D ;      word
.def RAM_D48B $D48B ;      word
.def RAM_D4FE $D4FE ; byte
.def RAM_D42E $D42E ; byte
.def RAM_D544 $D544 ; byte
.def RAM_D42D $D42D ; byte
.def RAM_D500 $D500 ; byte
.def RAM_D42B $D42B ; byte
.def RAM_D42A $D42A ; byte
.def RAM_D13B $D13B ; byte
.def RAM_D13A $D13A ; byte
.def RAM_D139 $D139 ; byte
.def RAM_D3E4 $D3E4 ; byte
.def RAM_D4B0 $D4B0 ; byte
.def RAM_DBC0 $DBC0 ; byte
.def RAM_D4F5 $D4F5 ; byte
.def RAM_D147 $D147 ; byte
.def RAM_D145 $D145 ; byte
.def RAM_DBCA $DBCA ; byte
.def RAM_D12F $D12F ; byte
.def RAM_D12C $D12C ; byte
.def RAM_DC9A $DC9A ; byte
.def RAM_D131 $D131 ; byte
.def RAM_D4F4 $D4F4 ; byte
.def RAM_D3E7 $D3E7 ;      word
.def RAM_D3EA $D3EA ; byte
.def RAM_D130 $D130 ; byte
.def RAM_D19C CameraLimit_Y ; byte word
.def RAM_D19A $D19A ; byte word
.def RAM_D199 $D199 ; byte
.def RAM_D198 $D198 ; byte
.def RAM_D144 $D144 ; byte
.def RAM_DE03 $DE03 ; byte
.def RAM_DE91 $DE91 ; byte
.def RAM_D12D $D12D ; byte
.def RAM_D12B $D12B ; byte
.def RAM_D12A $D12A ; byte




.BANK 0 SLOT 0
.ORG $0000

;---------- function start ----------
; (high level)
_START:
  di          ;set interrupt mode and stack pointer
  im   1
  ld   sp, $DFF0
  
  ld   a, $00     ;set up default banking
  ld   ($FFFC), a
  ld   a, $00
  ld   ($FFFD), a
  inc  a
  ld   ($FFFE), a
  inc  a
  ld   ($FFFF), a

  ;wait for the VDP
_RST_18H:
  in   a, ($7E)   ;read current scanline
  cp   $B0      ;wait for scanline == 176
  jr   nz, _RST_18H

  ld   hl, $C001    ;clear work RAM ($C001 to $DFEF)
  ld   de, $C002
  ld   bc, $1FEE
  ld   (hl), $00
  ldir

  ld   a, $01       ;bank number 1 in slot 1
  ld   (RAM_D12A), a    ;$D12A = copy of RAM register $FFFE
  ld   a, $02       ;bank number 2 in slot 2
  ld   (RAM_D12B), a    ;$D12B = copy of RAM register $FFFF
  jp   Engine_Initialise

;---------- function start ----------
; (middle level)
_IRQ_HANDLER:
  di
  push af
  
  ;rotate the vblank flag into the carry
  in   a, ($BF)
  rlca
  ;jump if the interrupt was a vblank
  jp   c, Engine_VBlankInterrupt  ;jump if frame interrupt pending

  ;jump to line interrupt handler
  jp   Engine_LineInterrupt


; Data from 43 to 65 (35 bytes)
; (only NULL-bytes)
.db $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00
.db $00

;Triggers debug code if bit 0 of this byte is set
Data_DebugSwitch: ;$0064
.db $00


.db $00

;---------- function start ----------
; (high level)
_NMI_HANDLER:
  jp   PauseInterruptHandler


; Data from 69 to 6F (7 bytes)
; (only NULL-bytes)
.db $00, $00, $00, $00, $00, $00, $00

Engine_ErrorTrap:   ;$0070
  di
  call VDP_ClearLevelTilesAndSAT
  call LABEL_76D0
  ld   a, $01
  ld   (RAM_D3F1), a
  ld   hl, $3A4C      ;copy to this VRAM address
  ld   de, _error_msg
  ld   bc, 5
  call VDP_DrawText
  jr   +

_error_msg:
.db "ERROR"

+:  ld   hl, BgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $02
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $02
  ld   b, $B4
  call Engine_WaitForCountInterrupts
  ld   ($0000), a
  jp   _START


;padding
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00, $00, $00
.db $00, $00, $00, $00, $00, $00

.db "GG SONIC THE HEDGEHOG   Ver 1.00"
.db "1994/08/02 @SEGA/Aspect Co.,Ltd "

;****************************************************************
;*  The following table serves as a lookup table that is used   *
;*  by tile loading routines to mirror tiles horizontally on  *
;*  the-fly. Each byte represents the mirrored data for each  *
;*  possible byte of tile data. For example, if the byte of   *
;*  tile data was $D6 (1101 0110 in binary), its mirrored value *
;*  would be $6B (0110 1011 in binary).             *
;*  This array needs to be aligned to a $100 byte boundary in   *
;*  order for the addressing mechanism to work properly.    *
;****************************************************************
.ORGA $100
Data_TileMirroringValues:    ;$0100
.db $00, $80, $40, $C0, $20, $A0, $60, $E0
.db $10, $90, $50, $D0, $30, $B0, $70, $F0
.db $08, $88, $48, $C8, $28, $A8, $68, $E8
.db $18, $98, $58, $D8, $38, $B8, $78, $F8
.db $04, $84, $44, $C4, $24, $A4, $64, $E4
.db $14, $94, $54, $D4, $34, $B4, $74, $F4
.db $0C, $8C, $4C, $CC, $2C, $AC, $6C, $EC
.db $1C, $9C, $5C, $DC, $3C, $BC, $7C, $FC
.db $02, $82, $42, $C2, $22, $A2, $62, $E2
.db $12, $92, $52, $D2, $32, $B2, $72, $F2
.db $0A, $8A, $4A, $CA, $2A, $AA, $6A, $EA
.db $1A, $9A, $5A, $DA, $3A, $BA, $7A, $FA
.db $06, $86, $46, $C6, $26, $A6, $66, $E6
.db $16, $96, $56, $D6, $36, $B6, $76, $F6
.db $0E, $8E, $4E, $CE, $2E, $AE, $6E, $EE
.db $1E, $9E, $5E, $DE, $3E, $BE, $7E, $FE
.db $01, $81, $41, $C1, $21, $A1, $61, $E1
.db $11, $91, $51, $D1, $31, $B1, $71, $F1
.db $09, $89, $49, $C9, $29, $A9, $69, $E9
.db $19, $99, $59, $D9, $39, $B9, $79, $F9
.db $05, $85, $45, $C5, $25, $A5, $65, $E5
.db $15, $95, $55, $D5, $35, $B5, $75, $F5
.db $0D, $8D, $4D, $CD, $2D, $AD, $6D, $ED
.db $1D, $9D, $5D, $DD, $3D, $BD, $7D, $FD
.db $03, $83, $43, $C3, $23, $A3, $63, $E3
.db $13, $93, $53, $D3, $33, $B3, $73, $F3
.db $0B, $8B, $4B, $CB, $2B, $AB, $6B, $EB
.db $1B, $9B, $5B, $DB, $3B, $BB, $7B, $FB
.db $07, $87, $47, $C7, $27, $A7, $67, $E7
.db $17, $97, $57, $D7, $37, $B7, $77, $F7
.db $0F, $8F, $4F, $CF, $2F, $AF, $6F, $EF
.db $1F, $9F, $5F, $DF, $3F, $BF, $7F, $FF

;multiplication tables?
DATA_200:
.db $00, $03, $06, $09, $0C, $0F, $12, $15
.db $19, $1C, $1F, $22, $25, $28, $2B, $2E
.db $31, $34, $36, $39, $3C, $3F, $42, $44
.db $47, $49, $4C, $4F, $51, $53, $56, $58
.db $5A, $5C, $5F, $61, $63, $65, $67, $68
.db $6A, $6C, $6E, $6F, $71, $72, $73, $75
.db $76, $77, $78, $79, $7A, $7B, $7C, $7D
.db $7D, $7E, $7E, $7F, $7F, $7F, $7F, $7F
.db $7F, $7F, $7F, $7F, $7F, $7E, $7E, $7D
.db $7D, $7C, $7B, $7B, $7A, $79, $78, $77
.db $75, $74, $73, $71, $70, $6E, $6D, $6B
.db $69, $68, $66, $64, $62, $60, $5E, $5B
.db $59, $57, $55, $52, $50, $4D, $4B, $48
.db $46, $43, $40, $3D, $3B, $38, $35, $32
.db $2F, $2C, $29, $26, $23, $20, $1D, $1A
.db $17, $14, $11, $0E, $0B, $07, $04, $01
.db $00, $FF, $FC, $F9, $F5, $F2, $EF, $EC
.db $E9, $E6, $E3, $E0, $DD, $DA, $D7, $D4
.db $D1, $CE, $CB, $C8, $C5, $C3, $C0, $BD
.db $BA, $B8, $B5, $B3, $B0, $AE, $AB, $A9
.db $A7, $A5, $A2, $A0, $9E, $9C, $9A, $98
.db $97, $95, $93, $92, $90, $8F, $8D, $8C
.db $8B, $89, $88, $87, $86, $85, $85, $84
.db $83, $83, $82, $82, $81, $81, $81, $81
.db $81, $81, $81, $81, $81, $81, $82, $82
.db $83, $83, $84, $85, $86, $87, $88, $89
.db $8A, $8B, $8D, $8E, $8F, $91, $92, $94
.db $96, $98, $99, $9B, $9D, $9F, $A1, $A4
.db $A6, $A8, $AA, $AD, $AF, $B1, $B4, $B7
.db $B9, $BC, $BE, $C1, $C4, $C7, $CA, $CC
.db $CF, $D2, $D5, $D8, $DB, $DE, $E1, $E4
.db $E7, $EB, $EE, $F1, $F4, $F7, $FA, $FD

Logic_vtable:
VF_Engine_WaitForInterrupt:                     ;$300
  jp Engine_WaitForInterrupt
VF_Logic_DoNothing:                             ;$303
  jp Logic_DoNothing
  jp LABEL_5455                                 ;$306
  jp LABEL_25CE                                 ;$309
VF_VDP_CopyToVRAM:
  jp VDP_CopyToVRAM                             ;$30C
  jp LABEL_4CC3                                 ;$30F
LT_LABEL_4CCE:
  jp LABEL_4CCE                                 ;$312
  jp VDP_EnableLineInterruptIfFading            ;$315
  jp VDP_DisableLineInterrupt                   ;$318
  jp LABEL_4D00                                 ;$31B
  jp LABEL_4D20                                 ;$31E
  jp LABEL_4CDA                                 ;$321
  jp LABEL_4CF2                                 ;$324
  jp LABEL_2F36                                 ;$327
  jp LABEL_30B8                                 ;$32A
  jp LABEL_52E8                                 ;$32D
  jp LABEL_54FA                                 ;$330
  jp LABEL_50CC                                 ;$333
  jp LABEL_50D1                                 ;$336
  jp LABEL_515D                                 ;$339
  jp LABEL_259E                                 ;$33C
  jp Engine_FadeMusicOut                        ;$33F
  jp Engine_Add100Rings                         ;$342
  jp Engine_AddRing                             ;$345
  jp LABEL_6580                                 ;$348
  jp LABEL_6557                                 ;$34B
LT_Engine_ChangeLevelMusic:                     ;$34E
  jp Engine_ChangeLevelMusic
LT_ObjectLogic_DoNothing:                       ;$351
  jp ObjectLogic_DoNothing
LT_Engine_AllocateLinkedObject:
  jp Engine_AllocateLinkedObject                ;$354
LT_Engine_AllocateLinkedObjectXY:
  jp Engine_AllocateLinkedObjectXY              ;$357
LT_Engine_GetObjectSlotNumber:
  jp Engine_GetObjectSlotNumber                 ;$35A
  jp LABEL_52D9                                 ;$35D
VF_Engine_AllocateObjectLowPriority:
  jp Engine_AllocateObjectLowPriority           ;$360
  jp LABEL_51B8                                 ;$363
  jp LABEL_51B1                                 ;$366
  jp LABEL_58AC                                 ;$369
  jp LABEL_51FF                                 ;$36C
  jp LABEL_59DB                                 ;$36F
  jp LABEL_53FD                                 ;$372
  jp LABEL_550E                                 ;$375
  jp LABEL_5424                                 ;$378
  jp LABEL_5430                                 ;$37B
  jp LABEL_543A                                 ;$37E
  jp LABEL_5359                                 ;$381
  jp LABEL_713D                                 ;$384
  jp LABEL_709A                                 ;$387
  jp LABEL_5F53                                 ;$38A
  jp LABEL_6E16                                 ;$38D
  jp LABEL_6780                                 ;$390
  jp LABEL_5F14                                 ;$393
  jp LABEL_5669                                 ;$396
  jp LABEL_5677                                 ;$399
  jp LABEL_552C                                 ;$39C
  jp LABEL_55DB                                 ;$39F
LT_Engine_MoveAboveBelowPlayer:
  jp Engine_MoveAboveBelowPlayer                ;$3A2
  jp LABEL_53CD                                 ;$3A5
  jp LABEL_53E7                                 ;$3A8
  jp LABEL_551D                                 ;$3AB
  jp LABEL_5607                                 ;$3AE
  jp LABEL_5186                                 ;$3B1
  jp LABEL_53BB                                 ;$3B4
  jp LABEL_3B47                                 ;$3B7
  jp LABEL_3AB8                                 ;$3BA
  jp LABEL_3D99                                 ;$3BD
  jp LABEL_3E5C                                 ;$3C0
  jp LABEL_3DB3                                 ;$3C3
  jp LABEL_3DE4                                 ;$3C6
  jp LABEL_3ED0                                 ;$3C9
  jp LABEL_3ED6                                 ;$3CC
  jp LABEL_3EBA                                 ;$3CF
  jp LABEL_3B62                                 ;$3D2
  jp LABEL_3E7A                                 ;$3D5
  jp LABEL_3E9D                                 ;$3D8
  jp LABEL_3E79                                 ;$3DB
VF_Player_SetSate_Dead:
  jp Player_SetSate_Dead                        ;$3DE
  jp LABEL_3903                                 ;$3E1
  jp LABEL_3542                                 ;$3E4
  jp LABEL_35B7                                 ;$3E7
  jp LABEL_36B3                                 ;$3EA
  jp Logic_DoNothing                            ;$3ED
  jp LABEL_34F1                                 ;$3F0
  jp LABEL_365B                                 ;$3F3
  jp LABEL_3A8D                                 ;$3F6
  jp LABEL_3BCF                                 ;$3F9
  jp LABEL_3BDA                                 ;$3FC
  jp LABEL_6FA6                                 ;$3FF
  jp LABEL_3AD6                                 ;$402
  jp LABEL_3CFC                                 ;$405
  jp LABEL_3C91                                 ;$408
  jp LABEL_3BF2                                 ;$40B
  jp LABEL_3A3A                                 ;$40E
  jp LABEL_3C6E                                 ;$411
  jp LABEL_3C4A                                 ;$414
  jp LABEL_3ECA                                 ;$417
  jp LABEL_3EC4                                 ;$41A
  jp LABEL_3D5B                                 ;$41D
  jp LABEL_3D48                                 ;$420
  jp LABEL_3D6E                                 ;$423
  jp LABEL_3D3B                                 ;$426
  jp LABEL_3C12                                 ;$429
  jp LABEL_33BA                                 ;$42C
  jp LABEL_37B5                                 ;$42F
  jp Engine_Add10Rings                          ;$432
  jp LABEL_3EE3                                 ;$435
  jp LABEL_561B                                 ;$438
  jp Engine_CopyScreenMappings                  ;$43B
  jp LABEL_1D3C                                 ;$43E
  jp LABEL_1D66                                 ;$441
  jp LABEL_1D90                                 ;$444
  jp LABEL_1DBA                                 ;$447
  jp LABEL_2C3B                                 ;$44A
  jp LABEL_23A3                                 ;$44D
  jp LABEL_572E                                 ;$450
  jp LABEL_5860                                 ;$453
  jp LABEL_5854                                 ;$456
  jp LABEL_797B                                 ;$459
  jp LABEL_3F18                                 ;$45C
  jp LABEL_5379                                 ;$45F
  jp LABEL_539A                                 ;$462
LT_Engine_LoadNackSwitchArt:
  jp Engine_LoadNackSwitchArt                   ;$465
  jp LABEL_6D91                                 ;$468
  jp LABEL_3F1E                                 ;$46B
  jp LABEL_740F                                 ;$46E
  jp LABEL_73E7                                 ;$471
  jp LABEL_7437                                 ;$474
  jp LABEL_745F                                 ;$477
  jp LABEL_51CB                                 ;$47A
  jp LABEL_3F29                                 ;$47D
  jp LABEL_3F42                                 ;$480
  jp LABEL_3B6F                                 ;$483
  jp LABEL_5F27                                 ;$486
  jp LABEL_7A48                                 ;$489
  jp LABEL_7AA6                                 ;$48C
LT_Engine_LoadNackExplosionArt2:
  jp Engine_LoadNackExplosionArt2               ;$48F
  jp LABEL_7A2C                                 ;$492
  jp LABEL_7A33                                 ;$495
  jp Engine_Wait                                ;$498
  jp LABEL_2430                                 ;$49B
  jp LABEL_7AAD                                 ;$49E
  jp Engine_LoadCompressedRingArt               ;$4A1
  jp LABEL_3E0C                                 ;$4A4
  jp LABEL_3DFF                                 ;$4A7
  jp LABEL_7B6E                                 ;$4AA
  jp LABEL_7A41                                 ;$4AD
  jp LABEL_1CF4                                 ;$4B0
  jp LABEL_1CFD                                 ;$4B3
  jp LABEL_1D06                                 ;$4B6
  jp LABEL_1D0F                                 ;$4B9
  jp LABEL_1D18                                 ;$4BC
  jp LABEL_1D21                                 ;$4BF
  jp LABEL_1D2A                                 ;$4C2
  jp LABEL_1D33                                 ;$4C5
  jp LABEL_3F48                                 ;$4C8
  jp LABEL_7228                                 ;$4CB
  jp LABEL_5986                                 ;$4CE
  jp LABEL_7AB4                                 ;$4D1
  jp LABEL_3F53                                 ;$4D4
  jp LABEL_7A25                                 ;$4D7
  jp LABEL_7A2C                                 ;$4DA
VF_Engine_FadeFromColourToColour:
  jp Engine_FadeFromColourToColour              ;$4DD
  jp LABEL_26C0                                 ;$4E0
  jp LABEL_5A3B                                 ;$4E3
  jp LABEL_2216                                 ;$4E6
  jp LABEL_3342                                 ;$4E9
  jp LABEL_6A92                                 ;$4EC
  jp LABEL_242D                                 ;$4EF
  jp LABEL_51D0                                 ;$4F2
  jp LABEL_390D                                 ;$4F5
  jp LABEL_7A9F                                 ;$4F8
  jp LABEL_3035                                 ;$4FB
  jp LABEL_3F76                                 ;$4FE
  jp LABEL_79C0                                 ;$501
  jp LABEL_2C75                                 ;$504
  jp LABEL_7B97                                 ;$507
  jp LABEL_58A7                                 ;$50A
  jp LABEL_5888                                 ;$50D
  jp LABEL_7A3A                                 ;$510

Logic_DoNothing:  ;$513
  ret


; =============================================================================
;  Engine_Reset()
; -----------------------------------------------------------------------------
;  Soft reset. Executed if the reset button is pressed.
; -----------------------------------------------------------------------------
;  In:
;    None.
;  Out:
;    None.
; -----------------------------------------------------------------------------
Engine_Reset:       ; $514
  di
  ld   sp, $DFF0
    
    ; init the VDP
  call VDP_DisableDisplay
  
    ; initialise work RAM
    ld   hl, $C001
  ld   de, $C002
  ld   bc, $1FEE
  ld   (hl), $00
  ldir
    
    ; reset the paging registers
  ld   a, $00
  ld   ($FFFC), a
  ld   a, $00
  ld   ($FFFD), a
  inc  a
  ld   ($FFFE), a
  inc  a
  ld   ($FFFF), a
    ; FALL THROUGH

; =============================================================================
;  Engine_Initialise()
; -----------------------------------------------------------------------------
;  Initialises the hardware.
; -----------------------------------------------------------------------------
;  In:
;    None.
;  Out:
;    None.
; -----------------------------------------------------------------------------
Engine_Initialise:      ; $053A
  call _LABEL_2533_4
  ;bank $02 in slot 1
  ld   a, $02
  ld   a, $02
  ld   (RAM_D12A), a
  ld   ($FFFE), a
  ;bank $03 in slot 2
  ld   a, $03
  ld   (RAM_D12B), a
  ld   ($FFFF), a
  
    ; init the sound driver
  call _LABEL_908F_8
  
  ;put bank $01 in slot 1
  ld   a, $01
  ld   (RAM_D12A), a
  ld   ($FFFE), a
  
  ;Initialise VDP memory
  call VDP_InitRegisters
  call VDP_ClearPalettes
  call VDP_ClearVRAM
  call VDP_EnableFrameInterrupt
  call VDP_EnableDisplay
  
    ; reset the game mode
  xor  a
  ld   (GameMode), a
  ld   (RAM_D144), a
  ld   (DemoSequenceIdx), a
  jp   Engine_ChangeGameMode

Engine_VBlankInterrupt:   ;$0576
  ld   a, (RAM_DC9A)
  or   a
  jp   nz, _LABEL_77C_35
  ex   af, af'
  push af
  push bc
  push de
  push hl
  exx
  push bc
  push de
  push hl
  push ix
  push iy
  ld   a, (RAM_D12C)
  or   a
  jp   nz, _LABEL_713_36
  ld   a, (GameMode)
  cp   $0D
  jp   z, _LABEL_78D_37
  cp   $14
  jp   z, _LABEL_785_38
_LABEL_59E_165:
  di
  xor  a
  ld   (RAM_D12F), a
  xor  a
  ld   (RAM_DBCA), a
  ld   hl, (RAM_D3E7)
  ld   a, l
  or   h
  jp   z, _LABEL_616_39
  
  ld   a, (CurrentLevel)
  cp   Level_TPZ
  jp   nz, _LABEL_5BF_40
  
  ld   a, (CurrentAct)
  cp   Act_3
  jp   z, _LABEL_645_41
  
_LABEL_5BF_40:
  ld   a, (AltPaletteUpdatePending)
  or   a
  jr   z, _LABEL_5DB_42
  xor  a
  ld   (AltPaletteUpdatePending), a
  
  ld   de, (RAM_D19C)
  xor  a
  sbc  hl, de
  jr   nc, _LABEL_5F0_43
  ld   a, $FF
  ld   (RAM_DBCA), a
  ld   b, $02
  jr   _LABEL_5F9_44

_LABEL_5DB_42:
  ;secondary palette stuff
  ld   de, (RAM_D19C)
  xor  a
  sbc  hl, de
  jp   c, _LABEL_645_41
  ld   a, (RAM_D4F4)
  ld   b, a
  ld   a, (PaletteUpdatePending)
  or   b
  ld   (PaletteUpdatePending), a
_LABEL_5F0_43:
  ld   b, l
  ld   de, $00A8
  xor  a
  sbc  hl, de
  jr   nc, _LABEL_645_41

_LABEL_5F9_44:
  ld   a, b
  out  ($BF), a
  ld   a, $8A
  out  ($BF), a
  ld   a, (VdpRegister0)
  or   $10
  ld   (VdpRegister0), a
  out  ($BF), a
  ld   a, $80
  out  ($BF), a
  ld   a, $FF
  ld   (RAM_D12F), a
  jp   _LABEL_645_41

_LABEL_616_39:
    ld   a, (GameMode)
    cp   $06
    jp   z, _LABEL_645_41

    ld   a, (RAM_D3EA)
    or   a
    jp   z, _LABEL_645_41

    xor  a
    ld   (RAM_D131), a

    ; set the line interrupt value
    ld   a, (DATA_9E4)
    out  (Ports_VDP_Control), a
    ld   a, VDP_Reg_0A
    out  (Ports_VDP_Control), a
    ; enable line interrupts
    ld   a, (VdpRegister0)
    or   VDP_LineInterruptsBit
    ld   (VdpRegister0), a
    out  (Ports_VDP_Control), a
    ld   a, VDP_Reg_00
    out  (Ports_VDP_Control), a

    ld   a, $FE
    ld   (RAM_D12F), a
_LABEL_645_41:
    xor  a
    ld   (RAM_D4F4), a

    ld   a, (BgPaletteControl)
    or   a
    jr   z, +
    ld   a, $FF
    ld   (PaletteUpdatePending), a

+:  ld   a, ($d133)
    or   a
    jp   nz, _LABEL_77F_46

    call VDP_DisableDisplay
    ld   bc, $0000
    ld   a, (CameraScrollOverride)
    or   a
    jp   z, +
    
    dec  a
    ld   (CameraScrollOverride), a
    and  $06
    ld   e, a
    ld   d, $00
    ld   hl, _ScrollOverrideValues
    add  hl, de
    ld   c, (hl)
    inc  hl
    ld   b, (hl)

+:  ld   a, (BackgroundXScroll)
    add  a, b
    out  (Ports_VDP_Control), a
    ld   a, VDP_Reg_08
    out  (Ports_VDP_Control), a

    ld   a, (BackgroundYScroll)
    add  a, c
    out  (Ports_VDP_Control), a
    ld   a, VDP_Reg_09
    out  (Ports_VDP_Control), a

    ld   a, (RAM_D12F)
    cp   $FE
    jr   z, _LABEL_6BD_48

    ld   a, (LevelHeader)
    bit  6, a
    jr   z, _LABEL_6BD_48
    res  6, a
    ld   (LevelHeader), a
    ld   ix, LevelHeader
    bit  4, (ix+0)
    call nz, _LABEL_46F0_49
    bit  5, (ix+0)
    call nz, _LABEL_4644_55
    ld   hl, (Camera_X)
    ld   (RAM_D19A), hl
    ld   hl, (Camera_Y)
    ld   (RAM_D19C), hl
_LABEL_6BD_48:
    call _LABEL_27A0_59
    ld   a, (RAM_D144)
    bit  0, a
    jr   nz, _LABEL_6D2_71
    
    ld   a, :Bank29
    ld   ($FFFF), a
    ld   a, (RAM_D4B0)
    call z, Engine_AnimateRings
  
_LABEL_6D2_71:
  call VDP_EnableDisplay
  call VDP_CopyPalettes
  call Engine_ReadInput
  call _LABEL_A0C_83
  ld   a, (RAM_D144)
  bit  0, a
  jr   nz, _LABEL_713_36
  ld   a, $0E
  ld   ($FFFF), a
  call Palette_Update
    
  ld   a, (RAM_D144)
  bit  1, a
  jr   z, +
    
  ld   a, (RAM_D12F)
  or   a
  jr   nz, ++
    
  call LABEL_2A14
  call LABEL_B01
  call LABEL_B2E
    
++: ld   a, :Bank29
  ld   ($FFFF), a
  call Engine_AnimateHudRing
    
+:  ld   a, :Engine_UpdateLevelDynamics   ;FIXME: unnecessary
  ld   ($FFFF), a
  call Engine_UpdateLevelDynamics
  
_LABEL_713_36:
  ld   a, (RAM_D3EC)
  or   a
  jr   z, _LABEL_720_257
  ld   (Sound_Music_Trigger), a
  xor  a
  ld   (RAM_D3EC), a
_LABEL_720_257:
  ld   a, $02
  ld   ($FFFE), a
  ld   a, $03
  ld   ($FFFF), a
  ld   a, $FF
  ld   (RAM_DC9A), a
  ei
  call _LABEL_8C42_170
  di
  xor  a
  ld   (RAM_DC9A), a
  call _LABEL_2F5F_258
  ld   hl, $D12E
  inc  (hl)
  ld   a, (CurrentLevel)
  cp   $01
  jr   nz, _LABEL_75F_267
  ld   a, (CurrentAct)
  cp   $02
  jr   nz, _LABEL_75F_267
  ld   a, (GameMode)
  cp   $04
  jr   z, _LABEL_75F_267
  cp   $06
  jr   z, _LABEL_75F_267
  cp   $07
  jr   z, _LABEL_75F_267
  call _LABEL_1C72_268
_LABEL_75F_267:
  ld   a, (RAM_D12A)
  ld   ($FFFE), a
  ld   a, (RAM_D12B)
  ld   ($FFFF), a
  ld   hl, $D132
  inc  (hl)
  pop  iy
  pop  ix
  pop  hl
  pop  de
  pop  bc
  exx
  pop  hl
  pop  de
  pop  bc
  pop  af
  ex   af, af'
_LABEL_77C_35:
  pop  af
  ei
  ret

_LABEL_77F_46:
  call VDP_CopyPalettes
  jp   _LABEL_713_36

_LABEL_785_38:
  ld   a, (RAM_D17E)
  cp   $06
  jp   nz, _LABEL_59E_165
_LABEL_78D_37:
  ld   a, (RAM_DC52)
  cp   $FF
  jr   nz, _LABEL_7BA_166
  ld   a, (CurrentAct)
  or   a
  jr   nz, _LABEL_7BA_166
  ld   a, (RAM_DC51)
  out  ($BF), a
  ld   a, $8A
  out  ($BF), a
  ld   a, (VdpRegister0)
  or   $10
  ld   (VdpRegister0), a
  out  ($BF), a
  ld   a, $80
  out  ($BF), a
  ld   a, $FF
  ld   (RAM_D12F), a
  xor  a
  ld   (RAM_D131), a
_LABEL_7BA_166:
  ld   a, ($d133)
  or   a
  jp   nz, _LABEL_846_167
  call VDP_DisableDisplay
  ld   a, (RAM_D198)
  out  ($BF), a
  ld   a, $88
  out  ($BF), a
  ld   a, (BackgroundYScroll)
  out  ($BF), a
  ld   a, $89
  out  ($BF), a
  ld   a, (RAM_D12E)
  rrca
  call nc, _LABEL_27A0_59
  call VDP_EnableDisplay
  call VDP_CopyPalettes
  ld   a, (RAM_D144)
  bit  0, a
  jr   nz, _LABEL_7F9_168
  ld   a, (RAM_D12E)
  rrca
  call c, LABEL_2A14
  ld   a, $0E
  ld   ($FFFF), a
  call Palette_Update
_LABEL_7F9_168:
  call Engine_ReadInput
  di
  ld   a, (RAM_D3EC)
  or   a
  jr   z, _LABEL_80A_169
  ld   (RAM_DE04), a
  xor  a
  ld   (RAM_D3EC), a
_LABEL_80A_169:
  ld   a, $02
  ld   ($FFFE), a
  ld   a, $03
  ld   ($FFFF), a
  ld   a, $FF
  ld   (RAM_DC9A), a
  ei
  call _LABEL_8C42_170
  di
  xor  a
  ld   (RAM_DC9A), a
  ld   a, (RAM_D12A)
  ld   ($FFFE), a
  ld   a, (RAM_D12B)
  ld   ($FFFF), a
  ld   hl, $D132
  inc  (hl)
  ld   hl, $D12E
  inc  (hl)
  pop  iy
  pop  ix
  pop  hl
  pop  de
  pop  bc
  exx
  pop  hl
  pop  de
  pop  bc
  pop  af
  ex   af, af'
  pop  af
  ei
  ret

_LABEL_846_167:
  call VDP_CopyPalettes
  jp   _LABEL_7F9_168


Engine_WaitForInterrupt:  ;$84C
  ;make sure that interrupts are enabled
    ei
    xor  a
    ld   ($d133), a
  
    ;wait for $D132 to become non-zero
    ld   hl, $D132
-:  ld   a, (hl)
    or   a
    jr   z, -
  
    ;clear $D132
    ld   (hl), $00
    ret


Engine_LineInterrupt:
  push hl
  push de
  
  ;clear the line interrupt flag
  in   a, ($BF)
  
  ld   a, (RAM_D130)
  or   a
  jp   nz, _LABEL_999_30
  ld   a, (GameMode)
  cp   $0D
  jp   z, _LABEL_95A_31
  cp   $14
  jp   z, _LABEL_95A_31
  ld   a, (RAM_D3EA)
  or   a
  jp   nz, _LABEL_930_32

  
  ld   a, (VdpRegister0)
  ;disable line interrupts
  and  $EF
  ld   (VdpRegister0), a
  out  ($BF), a
  ld   a, $80
  out  ($BF), a
  
  ;copy the secondary palette into CRAM
  ld   hl, AltWorkingCRAM
  
  push bc
    ;set up to write to CRAM
    ld   a, $00
    out  ($BF), a
    ld   a, $C0
    out  ($BF), a
    
    ld   c, $BE

.ifeq SMS_VERSION 1
  .rept 32
    outi
    push  bc
    pop   bc
  .endr
.else
  .rept 64
    outi
  .endr
.endif
  pop  bc
  
  ld   hl, (RAM_D19C)
  ld   de, $0020
  add  hl, de
  ld   de, (RAM_D3E7)
  xor  a
  sbc  hl, de
  jp   nc, _LABEL_994_33
  ld   a, $FF
  ld   (RAM_D4F4), a
  jp   _LABEL_994_33

_LABEL_930_32:
  ld   a, (RAM_D131)
  ld   hl, DATA_9E5
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   a, (hl)
  out  ($BF), a
  ld   a, $8A
  out  ($BF), a
  ld   a, (RAM_D131)
  inc  a
  ld   (RAM_D131), a
  cp   $07
  jr   z, _LABEL_999_30
  ld   hl, $DBE7
  add  hl, de
  ld   a, (hl)
  out  ($BF), a
  ld   a, $88
  out  ($BF), a
  jp   _LABEL_994_33

_LABEL_95A_31:
  ld   a, $01
  ld   (PaletteUpdatePending), a
  ld   hl, DATA_9EA
  ld   a, (GameMode)
  cp   $14
  jr   nz, _LABEL_96C_34
  ld   hl, $DC26
_LABEL_96C_34:
  ld   a, (RAM_D131)
  add  a, a
  add  a, a
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   a, (hl)
  out  ($BF), a
  ld   a, $8A
  out  ($BF), a

  inc  hl
  inc  hl
  ld   a, $00
  out  ($BF), a
  ld   a, $C0
  out  ($BF), a
  ld   a, (hl)
  out  ($BE), a
.ifeq SMS_VERSION 0
  inc  hl
  ld   a, (hl)
  out  ($BE), a
.endif
  ld   hl, $D131
  inc  (hl)
  jp   _LABEL_994_33

_LABEL_994_33:
  pop  de
  pop  hl
  pop  af
  ei
  ret

_LABEL_999_30:
  xor  a
  ld   (RAM_D131), a
  out  ($BF), a
  ld   a, $88
  out  ($BF), a
  ld   a, (VdpRegister0)
  and  $EF
  ld   (VdpRegister0), a
  out  ($BF), a
  ld   a, $80
  out  ($BF), a
  pop  de
  pop  hl
  pop  af
  ei
  ret


LABEL_9B6:
  ld   a, ($D198)
  out  ($BF), a
  ld   a, $8A
  out  ($BF), a
  xor  a
  ld   ($D131), a
  out  ($BF), a
  ld   a, $88
  out  ($BF), a
  ld   a, (VdpRegister0)
  and  $EF
  ld   (VdpRegister0), a
  out  ($BF), a
  ld   a, $80
  out  ($BF), a
  pop  de
  pop  hl
  pop  af
  ei
  ret


_ScrollOverrideValues:          ;$09DC
.db $FE, $00
.db $FE, $FE
.db $02, $02
.db $00, $02

DATA_9E4:
.db $1B

DATA_9E5:
.db $00, $21, $1F, $20, $FF
DATA_9EA:
.db $02, $00, $70, $0D, $04, $00, $60, $0D
.db $06, $00, $50, $0D, $08, $00, $40, $0D
.db $0A, $00, $30, $0D, $0C, $00, $20, $0D
.db $FF, $00, $10, $0D, $FF, $00, $00, $0C


;Pause interrupt not present on GG
PauseInterruptHandler:
.ifeq SMS_VERSION 1
  push hl
  push af
  
  ld   a, ($D139)
  or   $80
  ld   ($D139), a
  
  pop  af
  pop  hl
.endif
  
  retn


;---------- function start ----------
; (middle level)
_LABEL_A0C_83:
  call _LABEL_A6C_84
  jp   _LABEL_A12_91

_LABEL_A12_91:
  ld   a, (RAM_D42D)
  and  $A0
  cp   $A0
  ret  nz

  ld   a, (RAM_D544)
  bit  6, a
  ret  nz

  ld   a, (RAM_D42E)
  or   a
  jp   z, _LABEL_AC9_92
  ld   l, a
  ld   h, $00
  add  hl, hl
  add  hl, hl
  ld   de, $8348
  add  hl, de
  ld   a, $1F
  ld   ($FFFF), a
  ld   a, (hl)
  inc  hl
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  inc  hl
  ld   b, (hl)
  ld   ($FFFF), a
  ld   a, (RAM_D42D)
  bit  6, a
  jp   nz, _LABEL_A58_93
  ld   a, $00
  out  ($BF), a
  ld   a, $42
  out  ($BF), a
  call _LABEL_B74_88
  ld   hl, $D42D
  res  7, (hl)
  ret

_LABEL_A58_93:
  ld   a, $00
  out  ($BF), a
  ld   a, $42
  out  ($BF), a
  ex   de, hl
  ld   d, $01
  call _LABEL_CB9_89
  ld   hl, $D42D
  res  7, (hl)
  ret

;---------- function start ----------
; (middle level)
_LABEL_A6C_84:
  ld   a, (RAM_D42A)
  and  $A0
  cp   $A0
  ret  nz

  ld   a, (RAM_D42B)
  or   a
  jp   z, _LABEL_AD1_85
  ld   l, a
  ld   h, $00
  add  hl, hl
  add  hl, hl
  ld   de, $7FFC
  ld   a, (RAM_D500)
  dec  a
  jr   z, _LABEL_A8C_86
  ld   de, $81C8
_LABEL_A8C_86:
  add  hl, de
  ld   a, $1F
  ld   ($FFFF), a
  ld   a, (hl)
  inc  hl
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  inc  hl
  ld   b, (hl)
  ld   ($FFFF), a
  ld   a, (RAM_D42A)
  bit  6, a
  jp   nz, _LABEL_AB5_87
  ld   a, $00
  out  ($BF), a
  ld   a, $40
  out  ($BF), a
  call _LABEL_B74_88
  ld   hl, $D42A
  res  7, (hl)
  ret

_LABEL_AB5_87:
  ld   a, $00
  out  ($BF), a
  ld   a, $40
  out  ($BF), a
  ex   de, hl
  ld   d, $01
  call _LABEL_CB9_89
  ld   hl, $D42A
  res  7, (hl)
  ret

_LABEL_AC9_92:
  ld   a, $00
  out  ($BF), a
  ld   a, $02
  jr   _LABEL_AD7_94

_LABEL_AD1_85:
  ld   a, $00
  out  ($BF), a
  ld   a, $00
_LABEL_AD7_94:
  or   $40
  out  ($BF), a
  xor  a
  ld   b, $20
-:
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  out  ($BE), a
  djnz -
  ret

;---------- function start ----------
; (middle level)
LABEL_B01:
  ld   hl, $D42A
  ld   a, (hl)
  and  $80
  ld   b, a
  ld   a, (RAM_D504)
  rlca
  rlca
  and  $40
  or   $20
  or   b
  ld   (hl), a
  ld   a, (RAM_D42C)
  cp   (hl)
  jr   z, _LABEL_B1F_157
  ld   a, (hl)
  ld   (RAM_D42C), a
  set  7, (hl)
_LABEL_B1F_157:
  ld   a, (RAM_D42B)
  ld   b, a
  ld   a, (RAM_D506)
  cp   b
  ret  z

  ld   (RAM_D42B), a
  set  7, (hl)
  ret

;---------- function start ----------
; (middle level)
LABEL_B2E:
  ld   a, (RAM_D540)
  cp   $08
  jr   nz, _LABEL_B6F_159
  ld   a, (RAM_DBDA)
  or   a
  jr   nz, _LABEL_B6F_159
  ld   a, (RAM_D544)
  bit  6, a
  jr   nz, _LABEL_B6F_159
  ld   hl, $D42D
  ld   a, (hl)
  and  $80
  ld   b, a
  ld   a, (RAM_D544)
  rlca
  rlca
  and  $40
  or   $20
  or   b
  ld   (hl), a
  ld   a, (RAM_D42F)
  cp   (hl)
  jr   z, _LABEL_B60_160
  ld   a, (hl)
  ld   (RAM_D42F), a
  set  7, (hl)
_LABEL_B60_160:
  ld   a, (RAM_D42E)
  ld   b, a
  ld   a, (RAM_D546)
  cp   b
  ret  z

  ld   (RAM_D42E), a
  set  7, (hl)
  ret

_LABEL_B6F_159:
  xor  a
  ld   (RAM_D42D), a
  ret

;---------- function start ----------
; (low level)
_LABEL_B74_88:
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  ld   a, (de)
  out  ($BE), a
  inc  de
  nop
  dec  b
  jp   nz, _LABEL_B74_88
  ret

;---------- function start ----------
; (low level)
_LABEL_CB9_89:
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  ld   e, (hl)
  ld   a, (de)
  out  ($BE), a
  inc  hl
  dec  b
  jp   nz, _LABEL_CB9_89
  ret

; =============================================================================
;  Engine_ChangeGameMode
; -----------------------------------------------------------------------------
;  Jumps to a handler routine for the current game mode. 
; -----------------------------------------------------------------------------
;
; -----------------------------------------------------------------------------
Engine_ChangeGameMode:    ;$0DFE
  call Engine_FadeMusicOut
  call LABEL_3310
  ld   a, (GameMode)
  add  a, a
  ld   hl, Data_GameModeHandlers
  ld   d, $00
  ld   e, a
  add  hl, de
  ex   de, hl
  ld   a, (de)
  ld   l, a
  inc  de
  ld   a, (de)
  ;check for invalid game mode
  ld   h, a
  ld   a, $FF
  cp   h
  jp   z, Engine_ErrorTrap
  ;jump to mode handler
  di
  jp   (hl)


;pointers to game mode handlers
Data_GameModeHandlers:    ;$0E1D
.dw SegaScreen_Init             ; $00
.dw Intro_Init                  ; $01
.dw LABEL_FED                 ; $02 - title screen
.dw LABEL_1167                  ; $03
.dw GameMode_TitleCard          ; $04 - title card
.dw LABEL_1256                  ; $05 - main game mode
.dw GameMode_ScoreCard          ; $06 - score card
.dw LABEL_13C8                  ; $07
.dw LABEL_15A1                  ; $08
.dw LABEL_15A1                  ; $09 - emerald special stage
.dw GameMode_EmeraldScoreCard   ; $0A - emerald score card
.dw LABEL_1672                  ; $0B
.dw LABEL_1672                  ; $0C
.dw LABEL_1672                  ; $0D
.dw LABEL_174B                  ; $0E
.dw LABEL_174B                  ; $0F
.dw LABEL_174B                  ; $10
.dw LABEL_174B                  ; $12
.dw LABEL_17F4                  ; $13
.dw LABEL_17F4                  ; $14
.dw LABEL_17F4                  ; $15
.dw LABEL_19B8                  ; $16
.dw LABEL_1A27                  ; $17
.dw LABEL_1AC8                  ; $18 - Player Select
.dw LABEL_1AD0
.dw LABEL_1BFE
.dw $FFFF


SegaScreen_Init:    ;$0E53
  xor  a
  ld   ($d144), a
  
  ld   a, $00   ;FIXME: this is not required
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  
  ;wait for $18 frames
  ld   b, $18
  call Engine_WaitForCountInterrupts
  
  di
  call Engine_ClearWorkingSAT
  call Engine_ClearLevelAttributes
  call LABEL_327E     ;more RAM clearing
  
  
  ;set background & foreground palettes to pure white
  ld   a, $00
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $03
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $03
  
  ld   b, $18
  call Engine_WaitForCountInterrupts

  ;set CurrentLevel to the SEGA screen
  ld   a, Level_Sega
  ld   (CurrentLevel), a

  call VDP_ClearLevelTilesAndSAT
  call LABEL_341C
  
  ;fade both palettes into colour from white
  call Engine_ClearWorkingPaletteToWhite
  ld   a, $03
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $01
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $0F
  
  ;wait a while
  ld   bc, $0014
  call Engine_Wait
  
  ;instantiate a "Sonic" object
  ld   a, $01
  ld   ($D500), a

  ld   a, $02
  ld   ($D144), a
  ld   bc, $00B4

-:  push bc
  call Engine_WaitForInterrupt
  ;make the sonic object run across the SEGA text
  call LABEL_3431
  pop  bc
  
  dec  bc
  ld   a, b
  or   c
  jr   nz, -

  xor  a
  ld   ($D144), a
  
  di
  ld   a, $FF
  ld   ($d133), a

  ;play "SEGA" sound
  ld   a, $1A
  call Engine_SwapFrame2
  call $8000

  call Engine_WaitForInterrupt
  xor  a
  ld   ($d133), a

  ld   bc, $001E
  call Engine_Wait

  ;fade both palettes out
  ld   a, $01
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  
  ld   bc, $003C
  call Engine_Wait
  call LABEL_32A8
  xor  a
  ld   (CurrentLevel), a
  ld   (CurrentAct), a
  ld   a, $01
  ld   (PlayerObjectID), a
  
  ld   a, GameMode_Intro
  ld   (GameMode), a
  ;test for the debug switch
  ld   hl, Data_DebugSwitch
  bit  0, (hl)
  jp   z, Engine_ChangeGameMode
  ;if the debug switch is active, skip the intro and
  ;title screen and go to the player selection screen.
  ld   a, GameMode_PlayerSelect
  ld   (GameMode), a
  jp   Engine_ChangeGameMode


;intro screen initialiser
Intro_Init:   ;$0F3E
  xor  a
  ld   ($d17a), a
  ld   ($d171), a
  
  ;set the current level
  ld   a, Level_Intro
  ld   (CurrentLevel), a
  xor  a
  ld   (CurrentAct), a
  ld   ($D144), a
  
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearLevelAttributes
  call LABEL_3167   ;probably level load routine
  call LABEL_4CC3
  call Engine_LoadLevelPalette
  call Engine_ChangeLevelMusic
  
  ;set up the objects
  ld   a, Object_Sonic
  ld   (Player), a
  ld   a, Object_IntroEmeralds
  ld   (ObjectSlotsBadniks), a
  ld   a, Object_KnucklesIntro
  ld   (ObjectSlotsBadniks + $40), a

  ld   a, $02
  ld   ($d144), a
  call LABEL_4CCE
  ld   hl, (CameraLimit_Y)
  ld   ($d3a4), hl
  ld   ($d3a2), hl
  ld   bc, $0546
  ;fall through

;intro screen main loop
Intro_MainLoop:   ;$0F86
  push bc
  call Engine_WaitForInterrupt
  ld   ix, LevelHeader
  bit  6, (ix+$00)
  jp   nz, +
  call LABEL_3F81
  call LABEL_3431
  call LABEL_4F3F
  call LABEL_7C15

+: pop  bc
  ;check the controller data for start
  ;or button 1/2 presses
  ld   a, ($D139)
  and  $B0
  jr   nz, Intro_MainLoop_ButtonPress
  ;check for counter expiry
  dec  bc
  ld   a, b
  or   c
  ;loop back if counter != 0
  jr   nz, Intro_MainLoop
  jr   Intro_MainLoop_CounterExpired

Intro_MainLoop_ButtonPress:   ;$0FB0
  ld   bc, $0008
  call Engine_Wait
  call Engine_FadeMusicOut
  jr   Intro_MainLoop_FadeOut

Intro_MainLoop_CounterExpired:    ;$0FBB
  ld   bc, $0008
  call Engine_Wait
  ;fall through

Intro_MainLoop_FadeOut:   ;$0FC1
  ;flag FG and BG palettes to fade out to white
  ld   a, $02
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  
  ld   bc, $0078
  call Engine_Wait
  
  xor  a
  ld   ($D171), a
  ld   ($D17A), a
  ld   a, GameMode_TitleScreen
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

;title screen
LABEL_FED:
  xor  a
  ld   ($d144), a
  call LABEL_327E
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearWorkingPaletteToWhite
  call LABEL_77A8
  
  ;set up the palettes
  ld   a, $02
  ld   (PaletteTimerExpiry), a
  ;set BG palette to fade in from white
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  ;set BG palette index to $05
  inc  hl
  ld   (hl), $05
  ;set FG palette to fade in from white
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  ;set FG palette index to $06
  inc  hl
  ld   (hl), $06
  
  ;wait a while
  ld   bc, $001E
  call Engine_Wait
  ld   bc, $003C
  call Engine_InterruptableWait

  ;set both palettes to fade out to white
  ld   a, $02
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  
  ;wait a while
  ld   bc, $003C
  call Engine_Wait

LABEL_1043:
  xor  a
  ld   ($d144), a
  ld   (CurrentLevel), a
  ld   (CurrentAct), a
  ld   ($d171), a
  ld   ($d17a), a
  call LABEL_327E
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearWorkingPaletteToWhite
  call LABEL_7727
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $05
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $06
  ld   a, Music_Title
  ld   (Sound_Music_Trigger), a
  ld   bc, $003c
  call Engine_Wait
  ld   a, $02
  ld   ($d144), a
  ld   a, $00
  ld   ($d168), a
  ld   bc, $0294
  ld   ($c001), bc
LABEL_1098:
  push bc
  ld   hl, ($C001)
  dec  hl
  ld   ($C001), hl
  call Engine_WaitForInterrupt
  call LABEL_3431
  call LABEL_4F3F
  call LABEL_228E
  pop  bc
  ld   a, (LevelDynamic1)
  or   a
  jr   z, LABEL_10BD
  
.ifeq SMS_VERSION 1
  ld   a, (ControllerData)
  and  BTN_1 | BTN_2
.else
  ld   a, ($d139)
  and  $80
.endif
  
  jr   nz, LABEL_10C4
  call LABEL_1110
LABEL_10BD:
  dec  bc
  ld   a, b
  or   c
  jr   nz, LABEL_1098
  jr   LABEL_10DA

LABEL_10C4:
  ld   a, GameMode_PlayerSelect
  ld   (GameMode), a
  call Engine_FadeMusicOut
  ld   a, ($d168)
  cp   $02
  jr   nz, LABEL_10E1
  ld   a, GameMode_SoundTest
  ld   (GameMode), a
  jr   LABEL_10ED

LABEL_10DA:
  ld   a, $03   ;FIXME: ?? seems to be invalid
  ld   (GameMode), a
  jr   LABEL_10ED

LABEL_10E1:
  ld   a, ($D168)
  cp   $01
  jr   nz, LABEL_10ED
  ld   a, GameMode_PlayerSelect
  ld   (GameMode), a
LABEL_10ED:
  xor  a
  ld   (CurrentLevel), a
  
  ;set both palettes to fade to white
  ld   a, $03
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  
  ;wait a bit then change the game mode
  ld   b, $78
  call Engine_WaitForCountInterrupts
  jp   Engine_ChangeGameMode


LABEL_1110:
  ld   a, ($D43F)
  or   a
  ret  nz
  
  ld   a, ($D139)
  and  $03
  jr   z, +

  ld   h, a
  bit  0, h
  jr   z, LABEL_1137

  ld   a, $C0
  ld   (Sound_SFX_Trigger), a
  ld   a, ($D168)
  sub  $01
  ld   ($D168), a
  jr   nc, +

  ld   a, $02
  ld   ($D168), a
  jr   ++

LABEL_1137:
  bit  1, h
  jr   z, +

  ld   a, $C0
  ld   (Sound_SFX_Trigger), a
  ld   a, ($D168)
  add  a, $01
  ld   ($D168), a
  cp   $03
  jr   c, +
  
  ld   a, $00
  ld   ($D168), a

++: ld   hl, $00B4
  xor  a
  sbc  hl, bc
  jr   c, +
  
  ld   bc, $0258

+:  ld   a, $17
  call Engine_SwapFrame2
  push bc
  call $bfbb
  pop  bc
  ret

LABEL_1167:
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   a, $02
  ld   ($d4fe), a
  ld   bc, $003c
  call Engine_Wait
  ld   a, $01
  ld   ($d140), a
  call Engine_RunDemoSequence
  ld   hl, $0000
  ld   ($d3e7), hl
  call Engine_FadeMusicOut
  ld   a, (DemoFinishedFlag)
  or   a
  jr   nz, LABEL_11B8
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_11B8:
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, GameMode_TitleScreen
  ld   (GameMode), a
  jp   LABEL_1043


GameMode_TitleCard:     ; $110D
  xor  a
  ld   ($D144), a
  
  ;set both palettes to fade to black
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  
  ld   b, $3C
  call Engine_WaitForCountInterrupts
  
  xor  a
  ld   ($D3EA), a
  
  ld   a, (CurrentLevel)
  cp   Level_06
  jr   nc, LABEL_124E
  
  call VDP_ClearLevelTilesAndSAT
  call LABEL_327E
  call TitleCard_LoadTiles
  
  ;set the foreground palette
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $37
  ld   a, $01
  ld   (PaletteTimerExpiry), a
  
  ;change music
  ld   a, Music_TitleCard
  ld   (Sound_Music_Trigger), a
  
  ld   a, $02
  ld   ($d144), a
  ld   bc, $00f0
.ifeq SMS_VERSION 1
    call Engine_ClearWorkingSAT
.endif
LABEL_1222:
  push bc
  call Engine_WaitForInterrupt
  call LABEL_3431
  call LABEL_4F3F
  call LABEL_228E
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jr   nz, LABEL_1222
  ld   a, $01
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
LABEL_124E:
  ld   a, $05
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1256:
  di
  xor  a
  ld   ($D144), a
  call LABEL_1327
  ld   a, $02
  ld   ($D144), a
  ld   a, $03
  ld   ($D4FE), a
  ld   a, ($DC99)
  ld   ($D159), a
  xor  a
  ld   ($DC99), a
  call LABEL_33BA
  call LABEL_1F2E
  call LABEL_21D6
  ld   hl, $D144
  res  1, (hl)
  ld   bc, $0008
  call Engine_Wait
  ld   hl, $D144
  bit  5, (hl)
  jr   z, LABEL_1292
  ld   a, $d3
  ld   (Sound_SFX_Trigger), a
LABEL_1292:
  ld   bc, $003c
  call Engine_Wait
  
  ;fade all palettes to black
  ld   a, $01
  ld   (PaletteTimerExpiry), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)  
  ld   hl, AltBgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, AltFgPaletteControl
  ld   (hl), $00
  set  6, (hl)

  ld   bc, $003C
  call Engine_Wait
  ld   a, $ff
  ld   ($d130), a
  ld   bc, $000a
  call Engine_Wait
  di
  xor  a
  ld   ($d12f), a
  ld   hl, $dc56
  set  7, (hl)
  xor  a
  ld   ($d3ea), a
  ld   a, ($d144)
  bit  2, a
  jr   nz, LABEL_12EB
  bit  5, a
  jr   nz, LABEL_12F3
  ld   a, $06
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_12EB:
  ld   a, $07
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_12F3:
  di
  ld   a, (CurrentLevel)
  ld   ($D146), a
  ld   a, (CurrentAct)
  ld   ($D148), a
  ld   a, ($D17E) ;emerald count?
  add  a, a
  ld   hl, DATA_1317
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   a, (hl)
  ld   (GameMode), a
  inc  hl
  ld   a, (hl)
  ld   (CurrentAct), a
  jp   Engine_ChangeGameMode

DATA_1317:
.db $09, $00, $0D, $00, $09, $01, $0D, $01
.db $09, $02, $00, $00, $00, $00, $00, $00


LABEL_1327:
    xor  a
    ld   (DisableInput), a
    ld   ($d130), a
    call VDP_DisableDisplay
    call VDP_ClearLevelTilesAndSAT
    call Engine_ClearLevelAttributes
    call LABEL_3167
    call Engine_ChangeLevelMusic
    ld   a, (CurrentLevel)
    cp   $09
    jr   z, +
    cp   $08
    jr   z, +
    call Engine_LoadLevelPalette
+:  di
    call LABEL_4CC3
    jp   VDP_EnableDisplay


GameMode_ScoreCard:     ; $1352
  di
  xor  a
  ld   ($d144), a
  call LABEL_32F4
  call LABEL_3302
.ifeq SMS_VERSION 1
  ld   de, $003B
  call VDP_ClearLevelTilesAndSAT_WithValue
.else 
  call VDP_ClearLevelTilesAndSAT
.endif
  call Engine_ClearWorkingSAT
  call Engine_ClearLevelAttributes
  call LABEL_25B6
  call LABEL_3310
  ld   a, (CurrentLevel)
  cp   $05
  jr   nz, LABEL_1381
  ld   a, (CurrentAct)
  cp   $02
  jr   nz, LABEL_1381
  ld   a, ($D17E)
  inc  a
  ld   ($D17E), a
LABEL_1381:
  call LABEL_7871
  call LABEL_240B
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, (CurrentAct)
  inc  a
  ld   (CurrentAct), a
  cp   $03
  jr   c, LABEL_13C0
  xor  a
  ld   (CurrentAct), a
  ld   hl, CurrentLevel
  inc  (hl)
  ld   a, (hl)
  cp   $06
  jr   c, LABEL_13C0
  ld   a, $14
  jr   LABEL_13C2
LABEL_13C0:
  ld   a, $04
LABEL_13C2:
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_13C8:
  xor  a
  ld   (DisableInput), a
  xor  a
  ld   ($d144), a
  call VDP_DisableDisplay
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearLevelAttributes
  call VDP_EnableDisplay
  ld   a, ($d140)
  sub  $01
  daa
  ld   ($d140), a
  jr   c, LABEL_1418
  ld   a, ($d3cf)
  cp   $80
  jr   nz, LABEL_1410
  call LABEL_7ABB
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   bc, $00b4
  call Engine_Wait
LABEL_1410:
  ld   a, $04
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1418:
  ld   a, ($d14b)
  or   a
  jp   z, LABEL_156B
  ld   a, $1a
  call Engine_SwapFrame2
  call $BA18
  call Engine_ClearWorkingSAT
  call VDP_ClearVRAM
  call Engine_WaitForInterrupt
  di
  ld   de, $0100
  ld   hl, $3800
  ld   bc, $0380
  call VDP_WriteToVRAM
  call LABEL_7B0B
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0f
  ld   a, $02
  ld   ($d144), a
  ld   a, $09
  ld   ($d534), a
  ld   a, $01
  ld   ($d535), a
  xor  a
  ld   ($d537), a
  ld   bc, $0276
LABEL_146F:
  push bc
  ld   a, ($d535)
  dec  a
  ld   ($d535), a
  jr   nz, LABEL_14A9
  ld   a, $3c
  ld   ($d535), a
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   a, ($d534)
  add  a, a
  ld   hl, $9a8a
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   bc, $0302
  ld   hl, $3b1e
  call LABEL_2C3B
  ld   a, ($d534)
  sub  $01
  ld   ($d534), a
  jr   nc, LABEL_14A9
  xor  a
  ld   ($d534), a
LABEL_14A9:
  call Engine_WaitForInterrupt
  call LABEL_3431
  ld   a, ($d12e)
  and  $07
  jr   nz, LABEL_14E9
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   a, ($d12e)
  bit  3, a
  jr   nz, LABEL_14D7
  ld   hl, $3B18
  ld   de, $9A7A
  call LABEL_159B
  ld   hl, $3b24
  ld   de, $9a82
  call LABEL_159B
  jr   LABEL_14E9

LABEL_14D7:
  ld   hl, $3b18
  ld   de, $9a82
  call LABEL_159B
  ld   hl, $3b24
  ld   de, $9a7a
  call LABEL_159B
LABEL_14E9:
  ld   a, ($d536)
  or   a
  jr   z, LABEL_14F9
  ld   a, ($d511)
  cp   $e0
  jr   nc, LABEL_153B
  jp   z, LABEL_146F
LABEL_14F9:
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jp   nz, LABEL_146F
  ld   a, ($d537)
  or   a
  jp   nz, LABEL_146F
  ld   a, $ff
  ld   ($d539), a
  ld   bc, $00b4
LABEL_150F:
  push bc
  call Engine_WaitForInterrupt
  call LABEL_3431
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jr   nz, LABEL_150F
  call Engine_FadeMusicOut
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  jp   LABEL_156B

LABEL_153B:
  call Engine_FadeMusicOut
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, $03
  ld   ($d140), a
  ld   a, ($d14b)
  dec  a
  ld   ($d14b), a
  ld   a, $04
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_156B:
  call Engine_ClearWorkingSAT
  call VDP_ClearVRAM
  call LABEL_7AE0
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   bc, $01e0
  call Engine_Wait
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_159B:
  ld   bc, $0202
  jp   LABEL_2C3B

LABEL_15A1:
  di
  xor  a
  ld   ($d144), a
  ld   a, $06
  ld   (CurrentLevel), a
  xor  a
  ld   ($d181), a
  call LABEL_1327
  ld   a, $02
  ld   ($d144), a
  ld   a, $03
  ld   ($d4fe), a
  call LABEL_33BA
  ld   a, $30
  ld   ($d15d), a
  ld   a, $01
  ld   ($d15e), a
  ld   a, $ff
  ld   ($d181), a
  call LABEL_3035
  call LABEL_1F2E
  call LABEL_21D6
  call Engine_FadeMusicOut
  ld   hl, $d144
  bit  6, (hl)
  jr   nz, LABEL_15F6
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  jr   LABEL_160D

LABEL_15F6:
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
LABEL_160D:
  ld   bc, $0078
  call Engine_Wait
  ld   a, ($d146)
  ld   (CurrentLevel), a
  ld   a, ($d148)
  ld   (CurrentAct), a
  ld   hl, $d144
  bit  6, (hl)
  jr   nz, LABEL_162E
  ld   a, $05
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_162E:
  ld   a, $0a
  ld   (GameMode), a
    ; FALL THROUGH
    
GameMode_EmeraldScoreCard:      ; $1633
.ifeq SMS_VERSION 1
  ld   de, $003B
  call VDP_ClearLevelTilesAndSAT_WithValue
.else
  call VDP_ClearLevelTilesAndSAT
.endif
  call Engine_ClearWorkingSAT
  call LABEL_327E
  call Engine_ClearLevelAttributes
  call Engine_ClearWorkingPaletteToWhite
  ld   a, $40
  ld   ($d144), a
  call LABEL_7914
  call LABEL_240B
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $0078
  call Engine_Wait
  xor  a
  ld   ($d144), a
  ld   a, $05
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1672:
  di
  xor  a
  ld   ($d144), a
  ld   a, $08
  ld   (CurrentLevel), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  xor  a
  ld   ($d181), a
  di
  call LABEL_1327
  ld   a, $1a
  call Engine_SwapFrame2
  call $BA58
  call LABEL_76A7
  ld   a, $02
  ld   ($d4fe), a
  ld   bc, $001e
  call Engine_Wait
  ld   a, $02
  ld   ($d144), a
  call LABEL_1DED
  xor  a
  ld   ($dc52), a
  call LABEL_21D6
  call Engine_FadeMusicOut
  ld   a, ($d168)
  cp   $03
  jr   z, LABEL_1727
  ld   a, ($d180)
  or   a
  call nz, LABEL_1EB2
  ld   a, ($d146)
  ld   (CurrentLevel), a
  ld   a, ($d148)
  ld   (CurrentAct), a
  ld   hl, $d144
  bit  6, (hl)
  jr   z, LABEL_1706
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   a, $01
  ld   ($d4fe), a
  ld   bc, $0078
  call Engine_Wait
  ld   a, $0a
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1706:
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   a, $01
  ld   ($d4fe), a
  ld   bc, $0078
  call Engine_Wait
  ld   a, $05
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1727:
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   a, $01
  ld   ($d4fe), a
  call Engine_FadeMusicOut
  ld   bc, $0078
  call Engine_Wait
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_174B:
  di
  xor  a
  ld   ($d144), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearWorkingSAT
  call LABEL_327E
  call Engine_ClearLevelAttributes
  di
  ld   a, $0a
  ld   (CurrentLevel), a
  xor  a
  ld   (CurrentAct), a
  call LABEL_1327
  ld   a, $02
  ld   ($d144), a
  ld   a, $03
  ld   ($d4fe), a
  xor  a
  ld   ($dbc6), a
  ld   ($dbc7), a
  ld   ($dbc8), a
  ld   ($d15d), a
  ld   ($d15e), a
  ld   ($d15f), a
  call LABEL_3035
  call LABEL_33BA
  ld   a, $ff
  ld   ($dbd6), a
  ld   ($dbcb), a
  call LABEL_1F2E
  ld   a, ($dbcd)
  cp   $02
  jr   z, LABEL_17BC
  cp   $18
  jr   z, LABEL_17BC
  ld   bc, $0078
  call Engine_Wait
LABEL_17BC:
  call LABEL_21D6
  ld   a, $03
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, ($dbcd)
  or   a
  jr   z, LABEL_17EC
  cp   $02
  jr   z, LABEL_17EC
  ld   (GameMode), a
  jp  Engine_ChangeGameMode

LABEL_17EC:
  ld   a, $02
  ld   (GameMode), a
  jp   LABEL_1043

LABEL_17F4:
  xor  a
  ld   ($d144), a
  ld   ($d130), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  di
  call VDP_ClearLevelTilesAndSAT
  call VDP_ClearVRAM
  call LABEL_327E
  call Engine_ClearLevelAttributes
  ld   a, ($d17e)
  cp   $06
  jp   nz, LABEL_1949
  xor  a
  ld   (CurrentAct), a
  call LABEL_7BB5
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $003c
  call Engine_Wait
  ld   a, $03
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $33
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $34
  ld   a, Music_Ending2
  ld   (Sound_Music_Trigger), a
  ld   a, $01
  ld   ($dbe1), a
  ld   a, $0f
  call Engine_SwapFrame2
  call $AFC7
LABEL_1872:
  call Engine_WaitForInterrupt
  call LABEL_4F9C
  call LABEL_18C3
  ld   hl, ($dc24)
  inc  hl
  ld   ($dc24), hl
  ld   a, ($d12e)
  and  $03
  cp   $03
  jp   nz, LABEL_1872
  call LABEL_190F
  ld   a, ($dbe1)
  or   a
  jr   z, LABEL_1898
  jp   LABEL_1872

LABEL_1898:
  ld   bc, $0708
  call Engine_Wait
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, $d4fa
  ld   (hl), $00
  set  6, (hl)
  ld   hl, $d4fc
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  jp   _START

LABEL_18C3:
  ld   a, ($d12e)
  ld   b, a
  and  $07
  jr   nz, LABEL_18EE
  ld   hl, ($d3e5)
  ld   a, h
  or   l
  jr   z, LABEL_18EE
  push hl
  pop  iy
  ld   a, ($dc51)
  cp   $2c
  jr   c, LABEL_18EE
  dec  a
  ld   ($dc51), a
  ld   h, (iy+$15)
  ld   l, (iy+$14)
  dec  hl
  dec  hl
  ld   (iy+$15), h
  ld   (iy+$14), l
LABEL_18EE:
  ld   a, b
  and  $07
  cp   $07
  ret  nz
  ld   a, $15
  call Engine_SwapFrame2
  ld   de, $bda9
  bit  3, b
  jr   z, LABEL_1903
  ld   de, $bdfd
LABEL_1903:
  ld   hl, $3ada
  ld   bc, $0607
  di
  call LABEL_2C3B
  ei
  ret

LABEL_190F:
  ld   hl, ($dc24)
  ld   de, $2600
  xor  a
  sbc  hl, de
  jr   nc, LABEL_193E
  ld   a, ($d50a)
  bit  0, a
  jr   nz, LABEL_1930
  ld   a, ($d198)
  dec  a
  ld   ($d198), a
  cp   $f8
  ret  nc
  ld   hl, $d50a
  inc  (hl)
  ret

LABEL_1930:
  ld   a, ($d198)
  inc  a
  ld   ($d198), a
  or   a
  ret  nz
  ld   hl, $d50a
  inc  (hl)
  ret

LABEL_193E:
  ld   a, ($d198)
  cp   $90
  ret  z
  dec  a
  ld   ($d198), a
  ret

LABEL_1949:
  call LABEL_7B36
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0f
  ld   a, Music_NacksTheme
  ld   (Sound_Music_Trigger), a
  ld   a, $0a
  ld   (CurrentLevel), a
  ld   a, $08
  ld   ($d540), a
  ld   a, $02
  ld   ($d144), a
  ld   bc, $01e0
LABEL_196D:
  push bc
  call Engine_WaitForInterrupt
  call LABEL_34B9
  call LABEL_4F3F
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jr   nz, LABEL_196D
  ld   a, Music_A3
  ld   (Sound_Music_Trigger), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  ld   (hl), $0b
  ld   bc, $0258
LABEL_198F:
  push bc
  call Engine_WaitForInterrupt
  call LABEL_4F3F
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jr   nz, LABEL_198F
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  call Engine_Wait
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_19B8:
  xor  a
  ld   ($d144), a
  call VDP_ClearLevelTilesAndSAT
  call LABEL_76D0
  call Engine_ClearWorkingPaletteToWhite
  ld   a, $01
  ld   ($d3f1), a
  ld   hl, $38cc
  ld   de, DATA_19D8
  ld   bc, $0009
  call VDP_DrawText
  jr   LABEL_19E1

DATA_19D8:
.db $47, $41, $4D, $45, $20, $4F, $56, $45, $52 


LABEL_19E1:
  ld   a, $00
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $02
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $02
  ld   b, $78
  call Engine_WaitForCountInterrupts
  ld   a, $00
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   b, $3c
  call Engine_WaitForCountInterrupts
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1A27:
  ld   a, ($dc98)
  or   a
  jr   nz, LABEL_1A35
  ld   a, (Data_DebugSwitch)
  and  $04
  jp   z, LABEL_1AB6
LABEL_1A35:
  xor  a
  ld   ($d144), a
  call LABEL_32C3
  call VDP_ClearLevelTilesAndSAT
  call LABEL_76D0
  call Engine_ClearWorkingPaletteToWhite
  ld   b, $30
  call Engine_WaitForCountInterrupts
  call VDP_ClearLevelTilesAndSAT
  ld   a, $00
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $02
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $02
  ld   a, $10
  call Engine_SwapFrame2
  call $BCCC
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, (CurrentLevel)
  cp   $08
  jr   z, LABEL_1AAE
  cp   $06
  jp   nz, LABEL_1AB6
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $0078
  call Engine_Wait
  ld   a, $09
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1AAE:
  ld   a, $0d
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1AB6:
  ld   a, $04
  ld   (GameMode), a
  jp   Engine_ChangeGameMode


DATA_1ABE:
.db $04, $08, $0C, $11, $14, $17, $19, $20
.db $3E, $20

LABEL_1AC8:
  ld   a, $00
LABEL_1ACA:
  ld   (GameMode), a
  jp   Engine_ChangeGameMode

LABEL_1AD0:
  xor  a
  ld   ($d144), a
  call VDP_ClearLevelTilesAndSAT
  call LABEL_327E
  call Engine_ClearWorkingPaletteToWhite
  call LABEL_77D0
  ld   a, $87
  ld   (Sound_SFX_Trigger), a
  ld   a, $03
  ld   ($d140), a
  ld   a, $01
  ld   ($d14b), a
  xor  a
  ld   ($d17e), a
  ld   ($d171), a
  ld   ($d17a), a
  ld   (DemoSequenceIdx), a
  ld   a, (PlayerObjectID)
  or   a
  jr   nz, LABEL_1B07
  ld   a, $01
  ld   (PlayerObjectID), a
LABEL_1B07:
  ld   a, (PlayerObjectID)
  cp   $01
  jr   nz, LABEL_1B2D
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $07
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $0a
  jr   LABEL_1B4A

LABEL_1B2D:
  ld   a, $01
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $09
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $08
LABEL_1B4A:
  ld   b, $1e
  call Engine_WaitForCountInterrupts
  call Engine_WaitForInterrupt
  ld   a, (PlayerObjectID)
  cp   $01
  jr   nz, LABEL_1B6B
LABEL_1B59:
  ld   hl, BgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $07
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $0a
  jr   LABEL_1B7B

LABEL_1B6B:
  ld   hl, BgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $09
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $08
LABEL_1B7B:
  call Engine_WaitForInterrupt
  ld   hl, $D139
  ld   a, (hl)
  and  $B0
  jr   nz, LABEL_1BB7
  ld   a, (hl)
  bit  2, a
  jr   z, LABEL_1B9E
  ld   b, $01
  ld   a, (PlayerObjectID)
  cp   b
  jr   z, LABEL_1BB5
  ld   a, b
  ld   (PlayerObjectID), a
  ld   a, $c0
  ld   (Sound_SFX_Trigger), a
  jr   LABEL_1B59

LABEL_1B9E:
  bit  3, a
  jr   z, LABEL_1BB5
  ld   b, $02
  ld   a, (PlayerObjectID)
  cp   b
  jr   z, LABEL_1BB5
  ld   a, b
  ld   (PlayerObjectID), a
  ld   a, $c0
  ld   (Sound_SFX_Trigger), a
  jr   LABEL_1B6B
LABEL_1BB5:
  jr   LABEL_1B7B
LABEL_1BB7:
  ld   bc, $0014
  call Engine_Wait
  call Engine_FadeMusicOut
  ld   a, $03
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $005a
  call Engine_Wait
  ld   a, ($d168)
  cp   $01
  jr   z, LABEL_1BEF
  cp   $03
  jr   z, LABEL_1BF6
  ld   a, $16
  ld   (GameMode), a
  jr   LABEL_1BFB

LABEL_1BEF:
  ld   a, $11
  ld   (GameMode), a
  jr   LABEL_1BFB

LABEL_1BF6:
  ld   a, $0d
  ld   (GameMode), a
LABEL_1BFB:
  jp   Engine_ChangeGameMode

LABEL_1BFE:
  xor  a
  ld   ($d144), a
  call VDP_ClearLevelTilesAndSAT
  call Engine_ClearWorkingSAT
  call LABEL_327E
  call Engine_ClearLevelAttributes
  call VDP_ClearLevelTilesAndSAT
  call LABEL_76E3
  call LABEL_76F6
  call Engine_ClearWorkingPaletteToWhite
  ld   a, $03
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $31
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $32
  ld   c, $5f
  ld   h, $01
  call Engine_AllocateLinkedObject
  ld   a, $01
  ld   ($d500), a
  ld   a, $44
  ld   ($d53f), a
  ld   a, $80
  ld   ($dbee), a
  ld   a, $b0
  ld   ($dbef), a
  ld   a, $02
  ld   ($d144), a
LABEL_1C57:
  call Engine_WaitForInterrupt
  call LABEL_3431
  call LABEL_4F9C
  ld   a, ($d139)
  and  $80
  jr   z, LABEL_1C57
  call Engine_FadeMusicOut
  ld   a, $00
  ld   (GameMode), a
  jp   Engine_ChangeGameMode


;---------- function start ----------
; (middle level)
_LABEL_1C72_268:
  ld   hl, $DC56
  bit  7, (hl)
  ret  nz

  bit  0, (hl)
  jr   z, _LABEL_1C8D_269
  res  0, (hl)
  ld   a, $FF
  ld   (RAM_D3EA), a
  ld   (RAM_D12F), a
  ld   (RAM_D3E4), a
  ld   (RAM_D428), a
  ret

_LABEL_1C8D_269:
  bit  1, (hl)
  jr   z, _LABEL_1CB6_270
  ld   a, (RAM_DBEB)
  cp   $D8
  ret  c

  res  1, (hl)
  set  3, (hl)
  ld   a, (RAM_D188)
  call Engine_SwapFrame2
  ld   a, (RAM_D18A)
  ld   c, a
  ld   a, (RAM_D18B)
  ld   b, a
  ld   hl, $01C6
  add  hl, bc
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, $3900
  jp   LABEL_1DE4

_LABEL_1CB6_270:
  bit  2, (hl)
  ret  z

  ld   a, (RAM_DBEB)
  cp   $D8
  ret  c

  res  2, (hl)
  res  3, (hl)
  ld   a, (RAM_D188)
  call Engine_SwapFrame2
  ld   a, (RAM_D18A)
  ld   c, a
  ld   a, (RAM_D18B)
  ld   b, a
  ld   hl, $0140
  add  hl, bc
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, $3900
  jp   LABEL_1DE4


LABEL_1CDE:
    ld   a,($d188)
    call Engine_SwapFrame2
    ld   a,($d18a)
    ld   c,a
    ld   a,($d18b)
    ld   b,a
    ld   hl,$0000
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ret

LABEL_1CF4:
    call LABEL_1CDE
    ld   hl,$3820
    jp   LABEL_1DE4

LABEL_1CFD:
    call LABEL_1CDE
    ld   hl,$3828
    jp   LABEL_1DE4

LABEL_1D06:
    call LABEL_1CDE
    ld   hl,$3830
    jp   LABEL_1DE4

LABEL_1D0F:
    call LABEL_1CDE
    ld   hl,$3838
    jp   LABEL_1DE4

LABEL_1D18:
    call LABEL_1CDE
    ld   hl,$3800
    jp   LABEL_1DE4

LABEL_1D21:
    call LABEL_1CDE
    ld   hl,$3808
    jp   LABEL_1DE4

LABEL_1D2A:
    call LABEL_1CDE
    ld   hl,$3810
    jp   LABEL_1DE4

LABEL_1D33:
    call LABEL_1CDE
    ld   hl,$3818
    jp   LABEL_1DE4

LABEL_1D3C:
    ld   a,($d188)
    call Engine_SwapFrame2
    ld   a,($d18a)
    ld   c,a
    ld   a,($d18b)
    ld   b,a
    push bc
    ld   hl,$01e0
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3800
    call LABEL_1DE4
    pop  bc
    ld   hl,$013c
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3900
    jp   LABEL_1DE4

LABEL_1D66:
    ld   a,($d188)
    call Engine_SwapFrame2
    ld   a,($d18a)
    ld   c,a
    ld   a,($d18b)
    ld   b,a
    push bc
    ld   hl,$01e2
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3808
    call LABEL_1DE4
    pop  bc
    ld   hl,$013e
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3908
    jp   LABEL_1DE4

LABEL_1D90:
    ld   a,($d188)
    call Engine_SwapFrame2
    ld   a,($d18a)
    ld   c,a
    ld   a,($d18b)
    ld   b,a
    push bc
    ld   hl,$01e4
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3810
    call LABEL_1DE4
    pop  bc
    ld   hl,$0032
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3910
    jp   LABEL_1DE4

LABEL_1DBA:
    ld   a,($d188)
    call Engine_SwapFrame2
    ld   a,($d18a)
    ld   c,a
    ld   a,($d18b)
    ld   b,a
    push bc
    ld   hl,$0000
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3818
    call LABEL_1DE4
    pop  bc
    ld   hl,$0000
    add  hl,bc
    ld   e,(hl)
    inc  hl
    ld   d,(hl)
    ld   hl,$3918
    jp   LABEL_1DE4


LABEL_1DE4:
  ld   bc, $0404
  di
  call Engine_CopyScreenMappings
  ei
  ret

LABEL_1DED:
  call Engine_WaitForInterrupt
  call LABEL_20E2
  ld   a, ($d5c0)
  cp   $41
  jr   z, LABEL_1DFF
  call LABEL_4F9C
  jr   LABEL_1E02

LABEL_1DFF:
  call LABEL_4F3F
LABEL_1E02:
  ld   a, $03
  call Engine_SwapFrame2
  call $BDE2
  call LABEL_40D6
  call LABEL_20C7
  ld   a, ($d5c0)
  cp   $41
  jp   z, LABEL_1DED
  ld   a, ($dc4a)
  inc  a
  ld   ($dc4a), a
  cp   $07
  jr   c, +
  xor  a
  ld   ($dc4a), a
  ld   a, ($dc49)
  inc  a
  and  $03
  ld   ($dc49), a
+:  ld   hl, $d141
  inc  (hl)
  ld   a, (hl)
  and  $03
  cp   $02
  jp   c, +
  ld   a, ($dbd5)
  or   a
  jr   nz, +
  ld   a, $1c
  call Engine_SwapFrame2
  call $97AC
+:  ld   hl, ($dc47)
  ld   d, $00
  ld   a, ($dc46)
  ld   e, a
  add  hl, de
  ld   ($dc47), hl
  ld   a, ($d47f)
  cp   $01
  jr   nz, +
  ld   a, ($dbd5)
  or   a
  jr   nz, +
  ld   a, ($d12e)
  rrca
  jr   nc, +
  ld   a, ($dc51)
  cp   $40
  jr   c, +
  dec  a
  ld   ($dc51), a
+:  ld   a, ($d180)
  or   a
  jr   nz, LABEL_1E85
  ld   hl, $d144
  bit  7, (hl)
  jr   nz, LABEL_1E8C
  jp   LABEL_1DED

LABEL_1E85:
  ld   a, $d3
  ld   (Sound_SFX_Trigger), a
  jr   +

LABEL_1E8C:
  ld   a, $e2
  ld   (Sound_SFX_Trigger), a
  ld   b, $04
-:  call LABEL_1EA9
  djnz -
  ld   a, $de
  ld   (Sound_SFX_Trigger), a
+:  ld   bc, $005a
-:  call LABEL_1EA9
  dec  bc
  ld   a, b
  or   c
  jr   nz, -
  ret

LABEL_1EA9:
  push bc
  call Engine_WaitForInterrupt
  call LABEL_40D6
  pop  bc
  ret

LABEL_1EB2:
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   a, $02
  ld   ($d4fe), a
  ld   bc, $0078
  call Engine_Wait
  di
  xor  a
  ld   ($d144), a
  ld   a, $09
  ld   (GameMode), a
  ld   a, $09
  ld   (CurrentLevel), a
  call LABEL_1327
  call Engine_ClearWorkingPaletteToWhite
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $26
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $1d
  ld   a, $03
  ld   ($d4fe), a
  call LABEL_4CCE
  ld   a, $02
  ld   ($d144), a
  ld   hl, $0080
  ld   ($d159), hl
  ld   a, $ff
  ld   ($dbd7), a
  call LABEL_1F2E
  call Engine_FadeMusicOut
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  ld   bc, $003c
  jp   Engine_Wait


; main loop iteration for a level
LABEL_1F2E:
    ld   a, ($d12f)
    or   a
    jp   nz, LABEL_2069
    call Engine_WaitForInterrupt
    call LABEL_20E2
    ld   ix, LevelHeader
    bit  6, (ix+$00)
    jp   nz, LABEL_1F86
    ld   a, ($d3e4)
    cp   $05
    call nz, LABEL_3F81
    call LABEL_3431
    call LABEL_34B9
    call LABEL_4F3F
    call LABEL_7C15
    call LABEL_20C7
    ld   a, ($dbd8)
    or   a
    jr   z, LABEL_1F72
    ld   a, $ff
    ld   ($d130), a
    call LABEL_797B
    call Engine_LoadNackSwitchArt
    xor  a
    ld   ($dbd8), a
LABEL_1F72:
    ld   hl, $d141
    inc  (hl)
    ld   a, (hl)
    cp   $08
    jp   c, LABEL_1F86
    xor  a
    ld   (hl), a
    ld   a, $1c
    call Engine_SwapFrame2
    call $8000
LABEL_1F86:
    ld   a, ($DBCD)
    or   a
    ret  nz
    call LABEL_1FC6
    ld   a, ($D144)
    bit  5, a
    ret  nz
    bit  2, a
    ret  nz
    bit  4, a
    ret  nz
    ld   a, (DemoSequenceIdx)
    bit  INP_DEMO_MODE_BIT, a
    jp   z, LABEL_1F2E
    in   a, (Ports_IO1)
    or   $C0
    and  $7F
  
.ifeq SMS_VERSION 0
    ; check the GG $00 port
    ld   b, a
    in   a, (Ports_GG_Start)
    and  $80
    or   b
.endif
    cpl
    and  $80
    jr   z, _LevelLoop_DecrementDemoTimer
    ld   a, $FF
    ld   (DemoFinishedFlag), a
    ret

_LevelLoop_DecrementDemoTimer:        ; $1FB9
  ld   hl, (DemoTimer)
  dec  hl
  ld   (DemoTimer), hl
  ld   a, h
  or   l
  jp   nz, LABEL_1F2E
  ret

LABEL_1FC6:
  ld   a, ($d3fe)
  or   a
  ret  z
  ld   a, (CurrentLevel)
  cp   $05
  jp   nz, LABEL_2064
  ld   a, (CurrentAct)
  cp   $02
  jp   nz, LABEL_2064
  ld   hl, $0218
  ld   ($d3a4), hl
  ld   ($d3ac), hl
  ld   hl, ($d514)
  ld   de, $0100
  xor  a
  sbc  hl, de
  jr   nc, LABEL_2064
  ld   a, $d3
  ld   (Sound_SFX_Trigger), a
  ld   a, $00
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  set  4, (hl)
  ld   bc, $001e
  call Engine_Wait
  xor  a
  ld   ($d144), a
  ld   a, $03
  ld   (CurrentAct), a
  ld   a, $ff
  ld   ($d133), a
  call VDP_DisableDisplay
  di
  call VDP_ClearLevelTilesAndSAT
  di
  call Engine_ClearLevelAttributes
  di
  call Engine_LoadLevelHeader
  di
  call LABEL_4160
  di
  call LABEL_7B6E
  di
  call LABEL_4CC3
  call VDP_EnableDisplay
  ld   a, $02
  ld   ($d144), a
  ld   a, $02
  ld   (CurrentAct), a
  ld   a, $02
  ld   ($d4fe), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $25
  ld   hl, FgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  set  4, (hl)
  inc  hl
  ld   (hl), $14
LABEL_2064:
  xor  a
  ld   ($d3fe), a
  ret

LABEL_2069:
  call Engine_WaitForInterrupt
  call LABEL_20E2
  ld   ix, LevelHeader
  bit  6, (ix+$00)
  jp   nz, LABEL_1F86
  ld   a, ($d3ea)
  or   a
  call z, LABEL_3F81
  call LABEL_3431
  call LABEL_34B9
  call LABEL_4F3F
  call LABEL_2A14
  call LABEL_B01
  ld   a, ($d12f)
  cp   $fe
  jp   z, LABEL_20B8
  call LABEL_B2E
  call LABEL_7C15
  call LABEL_20C7
  ld   hl, $d141
  inc  (hl)
  ld   a, (hl)
  cp   $08
  jp   c, LABEL_1F86
  xor  a
  ld   (hl), a
  ld   a, $1c
  call Engine_SwapFrame2
  call $8000
  jp   LABEL_1F86

LABEL_20B8:
  ld   a, ($d144)
  bit  5, a
  ret  nz
  bit  2, a
  ret  nz
  bit  4, a
  ret  nz
  jp   LABEL_2069

LABEL_20C7:
  ld   hl, ($d3ee)
  ld   a, h
  or   l
  ret  z
  dec  hl
  ld   ($d3ee), hl
  ld   a, h
  or   l
  ret  nz
  ld   a, ($d501)
  cp   $29
  jp   nz, Engine_ChangeLevelMusic
  ld   a, Music_SeaFox
  ld   (Sound_Music_Trigger), a
  ret

LABEL_20E2:
  ld   a, ($dbcb)
  or   a
  ret  nz
  ld   a, ($d3eb)
  or   a
  jr   nz, LABEL_20F5
  ld   a, (GameMode)
  cp   $11
  jp   z, LABEL_212D
LABEL_20F5:
  ld   a, ($D139)
  bit  7, a
  ret  z
  
.ifeq SMS_VERSION 1
  ;SMS MOD - clear the start button flag
  ld   a, ($D139)
  xor  $80
  ld   ($D139), a
  ;END SMS MOD
.endif 
  
  ld   a, $80
  ld   ($DE08), a
  ld   hl, $D144
  set  0, (hl)
  call LABEL_21AA
  ld   a, ($D3EA)
  or   a
  jr   nz, LABEL_210E   ;FIXME: more pointless code

;pause busy loop
LABEL_210E:
  ;pause code
  call Engine_WaitForInterrupt
  ld   a, ($D139)
  bit  7, a
  jr   z, LABEL_210E
  
  xor  a
  ld   ($DE08), a
  call LABEL_21AF
  ld   hl, $D144
  res  0, (hl)
  call Engine_WaitForInterrupt
  ld   a, ($D3EA)
  or   a
  ret  nz
  ret

LABEL_212D:
  ld   a, ($dbc9)
  or   a
  ret  z
  ld   a, $80
  ld   ($de08), a
  ld   a, $ff
  ld   ($d182), a
  ld   hl, BgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $38
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $39
  di
  ld   a, $03
  call Engine_SwapFrame2
  call $BEAD
LABEL_2155:
  ld   a, ($dbcd)
  or   a
  jr   nz, LABEL_218E
  call Engine_WaitForInterrupt
  call LABEL_34B9
  call LABEL_4F3F
  ld   a, ($d139)
  bit  7, a
  jr   z, LABEL_2155
  xor  a
  ld   ($de08), a
  ld   ($dbc9), a
  ld   ($d182), a
  ld   hl, BgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $1e
  ld   hl, FgPaletteControl
  set  5, (hl)
  inc  hl
  ld   (hl), $0f
  di
  ld   a, $03
  call Engine_SwapFrame2
  jp   $BEB4

LABEL_218E:
  xor  a
  ld   ($de08), a
  ld   ($dbc9), a
  ld   ($d182), a
  di
  ld   a, $03
  call Engine_SwapFrame2
  call $BEB4
  ld   bc, $000a
  call Engine_Wait
  jp   Engine_FadeMusicOut

LABEL_21AA:
  xor  a
  ld   ($d181), a
  ret

LABEL_21AF:
  ld   a, ($d3e4)
  or   a
  ret  nz
  ld   a, $ff
  ld   ($d181), a
  ret

;**********************************************************
;*  Simple busywait loop.                                 *
;*                                                        *
;*  in    BC   The counter value.                         *
;*  destroys   A, BC                                      *
;**********************************************************
Engine_Wait:  ;$21BA
  push bc
  call Engine_WaitForInterrupt
  pop  bc
  dec  bc
  ld   a, b
  or   c
  jr   nz, Engine_Wait
  ret

;**********************************************************
;*  A busywait loop that can be cut short by pressing     *
;*  buttons 1, 2 or start.                                *
;*                                                        *
;*  in    BC   The counter value.                         *
;*  destroys   A, BC                                      *
;**********************************************************
Engine_InterruptableWait:   ;$21C5
  push bc
  call Engine_WaitForInterrupt
  pop  bc
  ;decrement the counter
  dec  bc
  ;check for start/1/2 buttons
  ld   a, ($D139)
  and  $B0
  ret  nz
  ;check the counter value
  ld   a, b
  or   c
  jr   nz, Engine_InterruptableWait
  ret

LABEL_21D6:
  ld   a, $ff
  ld   ($d48f), a
  ret

Engine_ChangeLevelMusic:    ;$21DC
  ld   a, ($d3eb)
  or   a
  ret  nz
  ld   a, ($d501)
  cp   $44
  ret  z
  cp   $20
  ret  z
  cp   $1f
  ret  z
  cp   $28
  ret  z
  ld   a, ($d3e4)
  or   a
  jr   nz, LABEL_2244
  ld   a, (CurrentLevel)
  ld   b, a
  add  a, a
  add  a, b
  ld   b, a
  ld   a, (CurrentAct)
  add  a, b
  ld   l, a
  ld   h, $00
  ld   de, Data_LevelMusicTracks
  add  hl, de
  ld   a, (hl)
  ld   (Sound_Music_Trigger), a
  ld   ($d3ec), a
  ld   ($d3ed), a
  call Engine_WaitForInterrupt
  ret

LABEL_2216:
  ld   a, ($D3EB)
  or   a
  ret  nz
  ld   a, ($D501)
  cp   $44
  ret  z
  cp   $20
  ret  z
  cp   PlayerState_LostLife
  ret  z
  cp   PlayerState_Drown
  ret  z
  ld   a, (CurrentLevel)
  cp   $05
  jr   nz, +
  ld   a, (CurrentAct)
  cp   $02
  ret  z
+:  ld   a, ($D3ED)
  ld   (Sound_Music_Trigger), a
  ld   ($D3EC), a
  call Engine_WaitForInterrupt
  ret

LABEL_2244:
  ld   a, (CurrentLevel)
  ld   l, a
  ld   h, $00
  ld   de, Data_BossMusicTracks
  add  hl, de
  ld   a, (hl)
  ld   ($DE04), a
  ld   ($D3EC), a
  ld   ($D3ED), a
  ret

Data_LevelMusicTracks:    ;$2259
.db $81, $81, $81
.db $82, $82, $93
.db $83, $83, $83
.db $84, $84, $84
.db $9A, $9A, $9A
.db $9B, $9B, $9B
.db $9C, $9C, $9C
.db $85, $85, $85
.db $9D, $9D, $9D
.db $97, $97, $97
.db $81, $81, $81
.db $81, $81, $81

Data_BossMusicTracks:   ;$227D
.db $8A, $8A, $8A, $8A, $8A, $8A, $8A, $8A,
.db $8A, $8A, $8A


;---------- function start ----------
; (high level)
Engine_FadeMusicOut:    ;$228
  ld   a, $E0
  ld   (Sound_Music_Trigger), a
  ret


LABEL_228E:
  ld   ix, $d43f
  ld   b, $04
LABEL_2294:
  push bc
  ld   a, (ix+$00)
  or   a
  jp   z, LABEL_2398
  cp   $ff
  jr   nz, LABEL_22AD
  push ix
  pop  hl
  ld   b, $10
  xor  a
-:  ld   (hl), a
  inc  hl
  djnz -
  jp   LABEL_2398

LABEL_22AD:
  dec  (ix+$03)
  jp   nz, LABEL_2398
  ld   a, (ix+$02)
  ld   (ix+$03), a
  ld   a, (ix+$04)
  call Engine_SwapFrame2
  di
  ld   h, (ix+$0f)
  ld   l, (ix+$0e)
  ld   ($d11c), hl
  ld   b, $01
  ld   c, (ix+$07)
  ld   ($d118), bc
  ld   d, (ix+$0d)
  ld   e, (ix+$0c)
  ld   ($d11a), de
  ld   b, (ix+$09)
-:  push bc
  ld   hl, ($d11c)
  ld   de, ($d11a)
  ld   bc, ($d118)
  call LABEL_2C3B
  ld   hl, ($d11c)
  ld   de, $0040
  add  hl, de
  ld   ($d11c), hl
  ld   hl, ($d11a)
  ld   a, (ix+$06)
  add  a, a
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   ($d11a), hl
  pop  bc
  djnz -
  ei
  dec  (ix+$05)
  jp   nz, LABEL_2317
  ld   (ix+$00), $ff
  jp   LABEL_2398

LABEL_2317:
  ld   b, (ix+$01)
  bit  0, b
  jr   z, LABEL_2354
  ld   a, (ix+$0a)
  or   a
  jr   z, LABEL_2344
  dec  (ix+$0a)
  ld   h, (ix+$0d)
  ld   l, (ix+$0c)
  dec  hl
  dec  hl
  ld   (ix+$0d), h
  ld   (ix+$0c), l
  ld   a, (ix+$06)
  cp   (ix+$07)
  jr   z, LABEL_2398
  jr   c, LABEL_2398
  inc  (ix+$07)
  jr   LABEL_2398

LABEL_2344:
  ld   h, (ix+$0f)
  ld   l, (ix+$0e)
  inc  hl
  inc  hl
  ld   (ix+$0f), h
  ld   (ix+$0e), h
  jr   LABEL_2398

LABEL_2354:
  bit  1, b
  jr   z, LABEL_2375
  ld   h, (ix+$0f)
  ld   l, (ix+$0e)
  dec  hl
  dec  hl
  ld   (ix+$0f), h
  ld   (ix+$0e), l
  ld   a, (ix+$06)
  cp   (ix+$07)
  jr   z, LABEL_2398
  jr   c, LABEL_2398
  inc  (ix+$07)
  jr   LABEL_2398

LABEL_2375:
  bit  3, b
  jr   z, LABEL_2398
  ld   h, (ix+$0f)
  ld   l, (ix+$0e)
  ld   de, $ffc0
  add  hl, de
  ld   (ix+$0f), h
  ld   (ix+$0e), l
  ld   a, (ix+$08)
  cp   (ix+$09)
  jr   z, LABEL_2398
  jr   c, LABEL_2398
  inc  (ix+$09)
  jr   LABEL_2398

LABEL_2398:
  ld   de, $0010
  add  ix, de
  pop  bc
  dec  b
  jp   nz, LABEL_2294
  ret

LABEL_23A3:
  ld   ($D110), a
  call LABEL_23F6
  or   a
  ret  nz
  
  
  ld   a, ($D110)
  add  a, a
  ld   hl, $B393
  ld   d, $00
  ld   e, a
  add  hl, de
  
  ;push the current slot 2 bank number to the stack
  ld   a, ($D12B)
  push af
  
  ;swap in the bank with the palettes
  ld   a, :Bank14
  ld   ($D12B), a
  ld   ($FFFF), a
  
  
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ex   de, hl
  push ix
  pop  de
  ld   bc, $0010
  ldir

  ld   a, (ix+$0B)
  or   a
  jr   z, LABEL_23EE

  dec  a
  add  a, a
  ld   d, $00
  ld   e, a
  ld   hl, Data_TitleCardPalettes
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  
  ;copy the colours into working CRAM
  ex   de, hl
  ld   de, WorkingCRAM + ColourSize_Bytes
  ld   bc, 4 * ColourSize_Bytes
  ldir
  ;flag for a palette update
  ld   a, $FF
  ld   (PaletteUpdatePending), a
  
LABEL_23EE:
  ;restore the previous mapper config
  pop  af
  ld   ($D12B), a
  ld   ($FFFF), a
  ret


LABEL_23F6:
  ld   ix, $D43F
  ld   b, $04
  
-:  ld   a, (ix+$00)
  or   a
  ret  z
  ld   de, $0010
  add  ix, de
  djnz -
  
  ld   a, $FF
  ret

LABEL_240B:
  di
  ld   a, $19
  call Engine_SwapFrame2
  call $bd4a
  di
  ld   a, $1a
  call Engine_SwapFrame2
  call $b9de
  di
  ld   a, $19
  call Engine_SwapFrame2
  call $bece
  ld   bc, $003c
  call Engine_Wait
  ret

LABEL_242D:
  ld   hl, DATA_249E
LABEL_2430:
  ld   a, (DemoSequenceIdx)
  or   a
  ret  nz
  ld   de, ($d14c)
  ld   ($d14f), de
  ld   a, ($d14e)
  ld   ($d151), a
  xor  a
  ld   de, $d14c
  ld   a, (de)
  adc  a, (hl)
  daa
  ld   (de), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  daa
  ld   (de), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  daa
  ld   (de), a
  jr   nc, LABEL_245D
  ld   a, $02
  jr   LABEL_245F

LABEL_245D:
  ld   a, $01
LABEL_245F:
  ld   ($d153), a
  jr   LABEL_2464

LABEL_2464:
  ld   a, ($d14e)
  and  $0f
  ld   b, a
  ld   a, ($d151)
  and  $0f
  cp   b
  ret  z
  ld   a, ($d14e)
  and  $0f
  jp   z, LABEL_247F
  cp   $05
  jp   z, LABEL_247F
  ret

LABEL_247F:
  ld   a, $d1
  ld   (Sound_SFX_Trigger), a
  ld   hl, $d14b
  inc  (hl)
  ld   a, (GameMode)
  cp   $06
  ret  nz
  call LABEL_78C2
  ret

LABEL_2492:
  or   a
  ret  z
  ld   b, $03
-:  sub  b
  jr   nc, -
  add  a, b
  ret

DATA_249B:
.db $00, $01, $00
DATA_249E:
.db $00, $00, $01


; ==============================================================================
;  Engine_RunDemoSequence
; ------------------------------------------------------------------------------
;  Reads a demo sequence structure and runs the game loop.
; ------------------------------------------------------------------------------
Engine_RunDemoSequence:            ; $24A1
    xor  a
    ld   (DemoFinishedFlag), a
    ; use the demo number to get a pointer to the descriptor
    ld   a, (DemoSequenceIdx)
    and  ~(1 << INP_DEMO_MODE_BIT)
    add  a, a
    ld   b, a
    add  a, a
    add  a, b
    ld   d, $00
    ld   e, a
    ld   hl, _Demo_Descriptors
    add  hl, de
    ; read the descriptor into RAM
    ld   a, (hl)
    ld   (PlayerObjectID), a
    inc  hl
    ld   a, (hl)
    ld   (CurrentLevel), a
    inc  hl
    ld   a, (hl)
    ld   (CurrentAct), a
    inc  hl
    ld   a, (hl)
    ld   (DemoSequenceBank), a
    inc  hl
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    push de
    ld   hl, 840  ; 14 seconds
    ld   (DemoTimer), hl
    ld   a, (DemoSequenceIdx)
    set  INP_DEMO_MODE_BIT, a
    ld   (DemoSequenceIdx), a
    xor  a
    ld   ($d171), a
    ld   ($d17a), a
    call LABEL_1327

    pop  de
    ld   (DemoSequencePtr), de
    ld   a, $02
    ld   ($d144), a
    ld   a, 3
    ld   (PaletteTimerExpiry), a
    call LABEL_1F2E
    call LABEL_21D6
    ld   hl, $0000
    ld   (DemoSequencePtr), hl
    ld   a, (DemoSequenceIdx)
    res  INP_DEMO_MODE_BIT, a
    ld   (DemoSequenceIdx), a
    ld   a, (DemoSequenceIdx)
    inc  a
    and  ~(1 << INP_DEMO_MODE_BIT)
    ld   (DemoSequenceIdx), a
    cp   2
    ret  c
    xor  a
    ld   (DemoSequenceIdx), a
    ret

_Demo_Descriptors:              ; $2519
.db Object_Sonic
  .db Level_GTZ
  .db Act_1,
  .db :DemoSequence_GTZ1
  .dw DemoSequence_GTZ1
.db Object_Tails
  .db Level_SPZ
  .db Act_2
  .db :DemoSequence_SPZ2
  .dw DemoSequence_SPZ2


;---------- function start ----------
; (middle level)
Engine_SwapFrame2:    ;$2525
  ld   (RAM_D12B), a
  ld   ($FFFF), a
  ret

;**********************************************************
;*  Wait for a number of frames.                          *
;*                                                        *
;*  in    B     The number of frames to wait.             *
;**********************************************************
Engine_WaitForCountInterrupts:    ;$252C
  ei
  call Engine_WaitForInterrupt
  djnz Engine_WaitForCountInterrupts
  ret

;---------- function start ----------
; (high level)
_LABEL_2533_4:
  ld   bc, $0A96
  in   a, ($BF)

  ;check for vblank interrupt
-:  in   a, ($BF)
  and  a
  ;loop back if no VBlank interrupt
  ;(i.e. sign is reset)
  jp   p, -

  ;loop using BC
-:  dec  bc
  ld   a, c
  or   b
  jp   nz, -

  ;read the VDP status again
  in   a, ($BF)
  and  a
  ld   a, $01
  ;jump if there was a frame interrupt
  jp   m, +
  
  dec  a
  
+:  ld   (RAM_D12D), a
  or   a
  ret  nz

  ld   a, $80
  ld   (RAM_DE91), a
  ret


;**********************************************************
;*  Enables VDP line interrupts if the palettes are       *
;*  being faded.                                          *
;**********************************************************
VDP_EnableLineInterruptIfFading:    ;$2558
  ld   a, (BgPaletteControl)
  and  $CF
  ret  nz
  ld   a, (VdpRegister0)
  or   $10
  ld   b, a
  ld   c, $00
  jp   VDP_SetRegister


VDP_DisableLineInterrupt:   ;$2569
  ld   a, (VdpRegister0)
  and  $EF
  ld   b, a
  ld   c, $00
  jp   VDP_SetRegister


;**********************************************************
;*  Fade FG & BG palettes out to black.                   *
;*                                                        *
;*  destroys  HL                                          *
;**********************************************************
Engine_FadePalettesToBlack:   ;$2574
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  6, (hl)
  inc  hl
  inc  hl
  ld   (hl), $00
  set  6, (hl)
  ret


;**********************************************************
;*  Copy FG & BG palettes to VRAM.                        *
;*                                                        *
;*  destroys  A, BC, DE, HL                               *
;**********************************************************
VDP_CopyPalettes:   ;$2582
  ld   a, (PaletteUpdatePending)
  or   a
  ret  z

  ld   a, (RAM_DBCA)
  or   a
  jr   nz, +
  
  ld   hl, WorkingCRAM
  ld   de, $C000
  ld   bc, 2 * PaletteSize_Bytes  ;$0040
  call VDP_CopyToVRAM

+:  xor  a
  ld   (PaletteUpdatePending), a
  ret


; Data from 259E to 2640 (163 bytes)
LABEL_259E:
  ld   iy, LevelDynamic1
  ld   b, $04
  ld   de, $0008
-:    ld   a, (iy+$00)
    or   a
    jr   z, LABEL_25B2
    add  iy, de
  djnz -
  ret


LABEL_25B2:
  ld   (iy+$00), C
  ret


LABEL_25B6:
  xor  a
  ld   (LevelDynamic1), a
  ld   (LevelDynamic2), a
  ld   ($D4A0), a
  ld   ($D4A8), a
  ld   hl, $0000
  ld   (LevelRingArtVRAMPtr), hl
  xor  a
  ld   ($D12F), a
  ret


LABEL_25CE:
  xor  a
  ld   (iy+$00), a
  ld   (iy+$01), a
  ld   (iy+$02), a
  ld   (iy+$03), a
  ret


;**********************************************************
;*  Fades one colour towards another by incrementing or   *
;*  decrementing each component.                          *
;*                                                        *
;*  in    HL    - Origin colour.                          *
;*        DE    - Destination colour.                     *
;*  out   HL    - Faded colour.                           *
;**********************************************************
Engine_FadeFromColourToColour:    ;$25DC

.ifeq SMS_VERSION 1
;SMS MOD ==================================================
  ;store target colour
  ld   ($D11A), de
  ld   l, a
  
  ld   a, e
  ;extract the blue component from the target colour
  and  $30
  ld   d, a
  ;extract the blue component from the origin colour
  ld   a, l
  and  $30
  ;compare the components
  cp   d
  ;jump if equal
  jp   z, +
  ;jump if D > A
  jp   c, ++
  
  ;D < A: decrement the component
  sub  $10
  and  $30
  jr   +
  
  ;D > A: increment the component
++: add  a, $10
  and  $30

  ;recombine with red & green
+:  ld   d, a
    ld   a, l
  and  $0F
  or   d
  ld   l, a
  
  ;extract the green component from the target colour
  ld   a, e
  and  $0C
  ld   d, a
  ;extract the green component from the origin colour
  ld   a, l
  and  $0C
  ;compare the components
  cp   d
  ;jump if equal
  jp   z, +
  ;jump if D > A
  jp   c, ++
  
  ;D < A: decrement component
  sub  $04
  and  $0C
  jr   +
  
  ;D > A: increment component
++: add  a, $04
  and  $0C
  
  ;recombine with blue & red components
+:  ld   d, a
  ld   a, l
  and  $33
  or   d
  ld   l, a
  
  ;extract the red component from the target colour
  ld   a, e
  and  $03
  ld   d, a
  ;extract the red component from the origin colour
  ld   a, l
  and  $03
  ;compare the components
  cp   d
  ;jump if equal
  jp   z, +
  ;jump if D > A
  jp   c, ++
  
  ;D < A: decrement component
  dec  a
  and  $03
  jr   +
  
  ;D > A: increment component
++: inc  a
  and  $03

  ;recombine with blue & green
+:  ld   d, a
  ld   a, l
  and  $3C
  or   d
  
  ld   ($D11C), hl
  ret
;END SMS MOD ==============================================
.else
  ld   ($D11C), hl
  ld   ($D11A), de
  
  ;copy blue component into A
  ld   a, h
  ;compare with component in D
  cp   d
  ;jump if equal
  jr   z, +
  ;jump if D > A
  jr   c, ++
  
  ;D < A
  ;decrement blue component
  dec  h
  ld   a, h
  ld   ($D11D), a
  jr   +

  ;D > A
  ;increment blue component
++: inc  h
  ld   a, h
  ld   ($D11D), a

  ;D == A
  ;get the green component
+:  ld   a, l
  rrca
  rrca
  rrca
  rrca
  and  $0F
  ld   b, a
  ;compare with green component in E
  ld   a, e
  rrca
  rrca
  rrca
  rrca
  and  $0F
  ld   c, a
  ld   a, b
  cp   c
  ;jump if they are equal
  jr   z, +++
  ;jump if C > A
  jr   c, +
  
  ;C < A
  dec  a
  jr   ++
  
  ;C > A
+:  inc  a

  ;rotate the component back into position
++: rlca
  rlca
  rlca
  rlca
  and  $F0
  ;recombine with the red component
  ld   b, a
  ld   a, ($D11C)
  and  $0F
  or   b
  ld   ($D11C), a

+++:
       ;get the red component
  ld   a, l
  and  $0F
  ld   b, a
  ld   a, e
  and  $0F
  ld   c, a
  ld   a, b
  ;compare with component in C
  cp   c
  ;jump if equal
  jr   z, $12
  ;jump if C > A
  jr   c, +
  
  ;C < A
  dec  a
  jr   ++

  ;C > A
+:  inc  a

++: and  $0F
  ld   b, a
  ld   a, ($D11C)
  and  $F0
  or   b
  ld   ($D11C), a
  ld   hl, ($D11C)
  ret
.endif 

;---------- function start ----------
; (high level)
VDP_InitRegisters:    ;$2641
  in   a, ($BF)
  ld   b, $0B
  ld   c, $80
  ld   de, VdpRegister0
  ld   hl, Initial_VDP_Register_Values

-:    ld   a, (hl)
    out  ($BF), a
    ld   (de), a
    ld   a, c
    out  ($BF), a
    inc  c
    inc  de
    inc  hl
  djnz -
  ret


Initial_VDP_Register_Values:    ;$265A
.ifeq SMS_VERSION 1
  .db $26, $82, $FF, $FF, $FF, $FF, $FB, $00, $00, $00, $FF
.else
  .db $06, $82, $FF, $FF, $FF, $FF, $FB, $00, $00, $00, $FF
.endif


;**********************************************************
;*  Set the value of a VDP register. Also synchronises    *
;*  the relevant RAM variable for the register.           *
;*                                                        *
;*  in    B   The register's new value.                   *
;*        C   The register number.                        *
;*  destroys  A                                           *
;**********************************************************
VDP_SetRegister:    ;$2665
  push bc
    push hl
      ;send the register's data
      ld   a, b
      out  ($BF), a
      ;send the register number
      ld   a, c
      or   $80
      out  ($BF), a
      ;update the register's RAM variable
      ld   a, b
      ld   b, $00
      ld   hl, VdpRegister0
      add  hl, bc
      ld   (hl), a
    pop  hl
  pop  bc
  ret


;****************************************************
;*  Set the VDP to write to a specific address.   *
;*  in  hl    The VRAM address.           *
;****************************************************
VDP_SetVRAMPointer:   ;$267A
  ;preserve AF
  push af
    ;set the VRAM address pointer
    ld   a, l
    out  ($BF), a
    ld   a, h
    or   $40
    out  ($BF), a
  ;restore AF
  pop  af
  ret

;NOTE:probably unused code
LABEL_2685:
  ld   a, l
  out  ($BF), a
  ld   a, h
  and  $3F
  out  ($BF), a
  push af
  pop  af
  ret

;NOTE: probably unused code
LABEL_2690:
  push af
  call VDP_SetVRAMPointer
  pop  af
  out  ($BE), a
  ret

;NOTE: probably unused code
LABEL_2698:
  push de
  push af
  call VDP_SetVRAMPointer
  pop  af
  ld   d, a
-:  ld   a, d
  out  ($BE), a
  push af
  pop  af
  in   a, ($BE)
  dec  bc
  ld   a, b
  or   c
  jp   nz, -
  pop  de
  ret
  
  
;************************************************
;*  Copy a single value to a block of VRAM.   *
;*                        *
;*  in  hl    The VRAM address to copy to.    *
;*  in  de    The vale to copy to VRAM.     *
;*  in  bc    Byte count.           *
;*  destroys  A, BC             *
;************************************************
VDP_WriteToVRAM:    ;$26AE
  call VDP_SetVRAMPointer
  ;copy DE to VRAM
-:  ld   a, e
  out  ($BE), a
  push af
  pop  af
  ld   a, d
  out  ($BE), a

  ;decrement counter and test for 0
  dec  bc
  ld   a, b
  or   c
  jp   nz, -

  ret


;probably unused
LABEL_26C0:
  push hl
    ld   hl, $D42A
    bit  7, (hl)
  pop  hl
  ret  nz


;************************************************
;*  Copy a block of data from RAM to VRAM.    *
;*                        *
;*  in  hl    The source address.       *
;*  in  de    The VRAM address.         *
;*  in  bc    Byte count.           *
;*  destroys  A, BC, DE, HL         *
;************************************************
VDP_CopyToVRAM:   ;$26C8
  ;set the VRAM address pointer
  ex   de, hl
  call VDP_SetVRAMPointer

  ;copy bytes from (de) to VRAM
-:  ld   a, (de)
  out  ($BE), a
  
  ;increment data pointer
  inc  de
  
  ;decrement counter & check for 0
  dec  bc
  ld   a, b
  or   c
  ;if counter == 0 return
  jr   nz, -
  ret


;********************************************************
;*  Sets the Display Visible bit of the VDP's mode    *
;*  control register 2 ( VDP(1) ).            *
;********************************************************
VDP_EnableDisplay:    ;$26D6
  ld   hl, VdpRegister1
  ld   a, (hl)
  or   $40
  ld   (hl), a
  out  ($BF), a
  ld   a, $81     ;write to VDP(1)
  out  ($BF), a
  ret

;********************************************************
;*  Sets the lower 5 bits of the VDP's mode control 2 *
;*  register (register 1).                *
;********************************************************
VDP_DisableDisplay:   ;$26E4
  ld   hl, VdpRegister1
  ld   a, (hl)
  and  $BF
  ld   (hl), a
  out  ($BF), a
  ld   a, $81
  out  ($BF), a
  ret

;********************************************************
;*  Set the Frame Interrupt bit in register VDP(1).     *
;********************************************************
VDP_EnableFrameInterrupt:   ;$26F2
  ld   hl, VdpRegister1
  ld   a, (hl)
  or   $20
  ld   (hl), a
  out  ($BF), a
  ld   a, $81
  out  ($BF), a
  ret


;********************************************************
;*  Reset the Frame Interrupt bit in register VDP(1).   *
;********************************************************
VDP_DisableFrameInterrupt:    ;$2700
  ld   hl, VdpRegister1
  ld   a, (hl)
  and  $DF
  ld   (hl), a
  out  ($BF), a
  ld   a, $81
  out  ($BF), a
  ret


;**********************************************
;*  DrawText
;*   Copies a string of chars into VRAM.
;*  hl  - The VRAM address.
;*  de  - Pointer to chars
;*  bc  - Char count
;**********************************************
VDP_DrawText:   ;$270E
  di
  call VDP_SetVRAMPointer
  push de
  push bc
    ;copy a char from address in DE into VRAM
-:    ld   a, (de)
    out  ($BE), a
    
    ;read the tile attribute byte and copy to VRAM
    ld   a, (RAM_D3F1)
    nop
    nop
    nop
    out  ($BE), a
    
    ;increment pointer to next char
    inc  de
    ;decrement char counter
    dec  bc
    ;if counter == 0 return
    ld   a, c
    or   b
    jr   nz, -
    
  pop  bc
  pop  de
  ret


; Data from 2728 to 273A (19 bytes)
LABEL_2728:
  ret

LABEL_2729:
  ld   de, $0000
  call Engine_WaitForInterrupt
  di
  ld   hl, $3800
  ld   bc, $0380
  call VDP_WriteToVRAM
  ;jump to clear the SAT
  jr   +

;---------- function start ----------
; (high level)
VDP_ClearLevelTilesAndSAT:    ;$273B
  ld   de, $00C0
VDP_ClearLevelTilesAndSAT_WithValue:
  call Engine_WaitForInterrupt
  di
  
  ;clear SAT
  ;copy DE to 896 words starting at the name table ($3800)
  ld   hl, $3800
  ld   bc, $0380
  call VDP_WriteToVRAM
  
  ;Clear level tile data
  ;copy $0000 to 32 words starting at VRAM address $1800
  ld   hl, $1800
  ld   bc, $0020
  ld   de, $0000
  call VDP_WriteToVRAM


Engine_ClearWorkingSAT:   ;$2757
  ;clear the working copy of the SAT
+:  ld   hl, VDP_WorkingSAT         ;address of VPOS attribs
  ld   de, VDP_WorkingSAT_HPOS    ;address of HPOS/char codes
  
  xor  a
  ld   b, $40   ;update 64 sprites

-:
.ifeq SMS_VERSION 1
  ld   (hl), $EF
.else
  ld   (hl), $F0  ;set vpos attribute = 240
.endif
  inc  hl
  ld   (de), a  ;set hpos & char code = 0
  inc  de
  ld   (de), a
  inc  de
  djnz -      ;loop to next sprite

  ;set the SAT update trigger
  ld   a, $FF
  ld   (SatUpdateTrigger), a
  
  ret


; Data from 276F to 279F (49 bytes)
LABEL_276F:
  di
  call VDP_SetVRAMPointer
  ld   a, b
  rrca
  rrca
  rrca
  rrca
  and  $0F
  or   $F0
  daa
  add  a, $A0
  adc  a, $40
  out  ($BE), a
  ld   a, ($D3F1)
  nop
  nop
  nop
  out  ($BE), a
  ld   a, b
  and  $0F
  or   $F0
  daa
  add  a, $A0
  adc  a, $40
  out  ($BE), a
  ld   a, ($D3F1)
  nop
  nop
  nop
  out  ($BE), a
  ret


;---------- function start ----------
; (middle level)
_LABEL_27A0_59:
  ld   hl, $D134
  xor  a
  or   (hl)
  ret  z

  ld   (hl), $00
  ld   hl, $D135
  inc  (hl)
  ld   a, (hl)
  rrca
  jp   c, _LABEL_280E_60
  ld   a, $00
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   a, (RAM_DBC0)
  or   a
  jp   nz, _LABEL_27E0_61
  ld   hl, $DB00
  ld   c, $BE
  call _LABEL_2871_62
  ld   a, $80
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   hl, $DB40
  ld   c, $BE
  call _LABEL_2871_62
  call _LABEL_2871_62
  ret

_LABEL_27E0_61:
  ld   hl, $DB08
  ld   c, $BE
  call _LABEL_2881_63
  ld   hl, $DB00
  ld   c, $BE
  call _LABEL_28E1_64
  ld   a, $80
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   hl, $DB50
  ld   c, $BE
  call _LABEL_2881_63
  call _LABEL_2881_63
  ld   hl, $DB40
  ld   c, $BE
  call _LABEL_28D1_65
  ret

_LABEL_280E_60:
  ld   a, $00
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   a, (RAM_DBC0)
  or   a
  jp   nz, _LABEL_2850_66
  ld   hl, $DB00
  ld   c, $BE
  call _LABEL_28E1_64
  ld   hl, $DB3F
  ld   c, $BE
  call _LABEL_2902_67
  ld   a, $80
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   hl, $DB40
  ld   c, $BE
  call _LABEL_28D1_65
  ld   hl, $DBBE
  ld   de, $FFFC
  ld   c, $BE
  call _LABEL_2987_68
  call _LABEL_2987_68
  ret

_LABEL_2850_66:
  ld   hl, $DB3F
  ld   c, $BE
  call _LABEL_28F2_69
  ld   a, $80
  out  ($BF), a
  ld   a, $3F
  or   $40
  out  ($BF), a
  ld   hl, $DBBE
  ld   de, $FFFC
  ld   c, $BE
  call _LABEL_2973_70
  call _LABEL_2973_70
  ret

;---------- function start ----------
; (middle level)
_LABEL_2871_62:
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
;---------- function start ----------
; (middle level)
_LABEL_2881_63:
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
;---------- function start ----------
; (middle level)
_LABEL_28D1_65:
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
;---------- function start ----------
; (middle level)
_LABEL_28E1_64:
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  outi
  ret

;---------- function start ----------
; (middle level)
_LABEL_28F2_69:
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
;---------- function start ----------
; (middle level)
_LABEL_2902_67:
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  outd
  ret

;---------- function start ----------
; (middle level)
_LABEL_2973_70:
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
;---------- function start ----------
; (middle level)
_LABEL_2987_68:
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  outi
  outi
  add  hl, de
  ret

;---------- function start ----------
; (middle level)
LABEL_2A14:
  ld   ix, $D500
  ld   hl, $DB00
  ld   (RAM_D48B), hl
  ld   hl, $DB40
  ld   (RAM_D48D), hl
  call _LABEL_2A88_127
  ld   b, $17
_LABEL_2A29_153:
  xor  a
  or   (ix+5)
  jp   z, _LABEL_2A4D_151
  ld   a, (ix+0)
  dec  a
  cp   $EF
  jr   nc, _LABEL_2A4D_151
  push bc
  ld   c, (ix+4)
  bit  5, c
  call nz, _LABEL_2BBA_129
  bit  7, c
  jr   nz, _LABEL_2A4C_152
  bit  6, c
  jr   nz, _LABEL_2A4C_152
  call _LABEL_2BF5_135
_LABEL_2A4C_152:
  pop  bc
_LABEL_2A4D_151:
  ld   de, $0040
  add  ix, de
  djnz _LABEL_2A29_153
  ld   a, (RAM_DBC3)
  or   a
  jr   nz, _LABEL_2A73_154
  ld   hl, (RAM_D48B)
  ld   a, $31
  sub  l
  jr   c, _LABEL_2A6D_155
  inc  a
  ld   c, a
  ld   b, $00
  ld   e, l
  ld   d, h
  inc  de
  ld   (hl), $E0
  ldir
_LABEL_2A6D_155:
  ld   a, $FF
  ld   (SatUpdateTrigger), a
  ret

_LABEL_2A73_154:
  ld   hl, (RAM_D48B)
  ld   a, $3E
  sub  l
  jr   c, _LABEL_2A6D_155
  inc  a
  ld   c, a
  ld   b, $00
  ld   e, l
  ld   d, h
  inc  de
  ld   (hl), $E0
  ldir
  jr   _LABEL_2A6D_155

;---------- function start ----------
; (middle level)
_LABEL_2A88_127:
  ld   a, (RAM_D505)
  or   a
  jr   z, _LABEL_2A9F_128
  ld   a, (RAM_D504)
  ld   c, a
  bit  5, c
  call nz, _LABEL_2BBA_129
  bit  7, c
  jr   nz, _LABEL_2A9F_128
  bit  6, c
  jr   z, _LABEL_2AA3_131
_LABEL_2A9F_128:
  ld   a, $08
  jr   _LABEL_2AAF_132

_LABEL_2AA3_131:
  call _LABEL_2BF5_135
  ld   a, (ix+5)
  sub  $08
  jr   nc, _LABEL_2AC5_150
  neg
_LABEL_2AAF_132:
  ld   b, a
  ld   c, a
  ld   hl, (RAM_D48B)
  ld   a, $E1
_LABEL_2AB6_133:
  ld   (hl), a
  inc  hl
  djnz _LABEL_2AB6_133
  ld   a, c
  add  a, a
  ld   b, a
  ld   hl, (RAM_D48D)
  xor  a
_LABEL_2AC1_134:
  ld   (hl), a
  inc  hl
  djnz _LABEL_2AC1_134
_LABEL_2AC5_150:
  ld   hl, $DB08
  ld   (RAM_D48B), hl
  ld   hl, $DB50
  ld   (RAM_D48D), hl
  ld   ix, $D540
  ret

;---------- function start ----------
; (low level)
_LABEL_2AD6_138:
  ld   a, $0F
  ld   ($FFFF), a
  ld   iy, (RAM_D48B)
  ld   h, (ix+45)
  ld   l, (ix+44)
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   l, (ix+28)
  ld   h, (ix+29)
  add  hl, de
  push hl
  exx
  ld   d, (ix+43)
  ld   e, (ix+42)
  pop  bc
  exx
  ld   b, (ix+5)
_LABEL_2AFC_142:
  exx
  ld   a, $0D
  ld   ($FFFF), a
  ld   a, (de)
  ld   l, a
  inc  de
  ld   a, (de)
  ld   h, a
  inc  de
  add  hl, bc
  ld   (iy+0), l
  ld   a, $40
  add  a, l
  ld   l, a
  jr   nc, _LABEL_2B13_139
  inc  h
_LABEL_2B13_139:
  ld   a, h
  or   a
  jr   nz, _LABEL_2B1C_140
  ld   a, l
  cp   $30
  jr   nc, _LABEL_2B20_141
_LABEL_2B1C_140:
  ld   (iy+0), $E0
_LABEL_2B20_141:
  inc  de
  inc  de
  inc  iy
  exx
  djnz _LABEL_2AFC_142
  ld (RAM_D48B), iy
  ret

;---------- function start ----------
; (low level)
_LABEL_2B2C_143:
  ld   a, $0F
  ld   ($FFFF), a
  ld   iy, (RAM_D48D)
  ld   h, (ix+45)
  ld   l, (ix+44)
  inc  hl
  inc  hl
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  bit  4, (ix+4)
  jr   z, _LABEL_2B4D_144
  dec  de
  ld   a, e
  cpl
  ld   e, a
  ld   a, d
  cpl
  ld   d, a
_LABEL_2B4D_144:
  inc  hl
  ld   c, (hl)
  inc  hl
  ld   b, (hl)
  ld   (RAM_D110), bc
  ld   l, (ix+26)
  ld   h, (ix+27)
  add  hl, de
  push hl
  exx
  ld   d, (ix+43)
  ld   e, (ix+42)
  inc  de
  inc  de
  bit  4, (ix+4)
  jr   z, _LABEL_2B71_145
  ld   hl, $13B4
  add  hl, de
  ex   de, hl
_LABEL_2B71_145:
  pop  bc
  exx
  ld   b, (ix+5)
_LABEL_2B76_149:
  ld   a, $0D
  ld   ($FFFF), a
  exx
  ld   a, (de)
  ld   l, a
  inc  de
  ld   a, (de)
  ld   h, a
  inc  de
  add  hl, bc
  ld   (iy+0), l
  ld   a, h
  or   a
  jr   z, _LABEL_2B8E_146
  ld   (iy+0), $00
_LABEL_2B8E_146:
  inc  de
  inc  de
  inc  iy
  ld   a, $0F
  ld   ($FFFF), a
  ld   hl, (RAM_D110)
  ld   a, (hl)
  bit  4, (ix+4)
  jr   z, _LABEL_2BA6_147
  add  a, (ix+9)
  jr   _LABEL_2BA9_148

_LABEL_2BA6_147:
  add  a, (ix+8)
_LABEL_2BA9_148:
  ld   (iy+0), a
  inc  hl
  ld   (RAM_D110), hl
  inc  iy
  exx
  djnz _LABEL_2B76_149
  ld (RAM_D48D), iy
  ret

;---------- function start ----------
; (low level)
_LABEL_2BBA_129:
  inc  (ix+46)
  ld   a, (ix+46)
  rrca
  rrca
  jr   c, _LABEL_2BC9_130
  res  7, (ix+4)
  ret

_LABEL_2BC9_130:
  set  7, (ix+4)
  ret

;---------- function start ----------
; (low level)
_LABEL_2BCE_137:
  ld   l, (ix+17)
  ld   h, (ix+18)
  ld   de, (RAM_D19A)
  xor  a
  sbc  hl, de
  ld   (ix+26), l
  ld   (ix+27), h
  ld   l, (ix+20)
  ld   h, (ix+21)
  ld   de, (RAM_D19C)
  xor  a
  sbc  hl, de
  ld   (ix+28), l
  ld   (ix+29), h
  ret

;---------- function start ----------
; (low level)
_LABEL_2BF5_135:
  ld   a, (RAM_D12F)
  cp   $FF
  jr   z, _LABEL_2C06_136
  call _LABEL_2BCE_137
  call _LABEL_2AD6_138
  call _LABEL_2B2C_143
  ret

_LABEL_2C06_136:
  ld   a, $FF
  ld   ($d133), a
  di
  call _LABEL_2BCE_137
  call _LABEL_2AD6_138
  call _LABEL_2B2C_143
  ei
  xor  a
  ld   ($d133), a
  ret


; Data from 2C1B to 2C3D (35 bytes)
LABEL_2C1B:
  call LABEL_2CB9
  push bc
  push hl
  ld   b, c
  call VDP_SetVRAMPointer
  ld   a, (de)
  out  ($BE), a
  inc  de
  inc  hl
  ld   a, (de)
  push af
  pop  af
  out  ($BE), a
  inc  de
  inc  hl
  djnz $F2
  pop  hl
  ld   bc, $0040
  add  hl, bc
  pop  bc
  djnz $E4
  ret


;**********************************************************
;*                                                        *
;*                                                        *
;*  in    HL    Destination VRAM address.                 *
;*        DE    Source address of mappings.               *
;*        B     Number of rows.                           *
;*        C     Number of columns.                        *
;**********************************************************
LABEL_2C3B:
    call LABEL_2CB9
Engine_CopyScreenMappings:    ;$2C3E
    push bc
    push hl
      ;copy a row of mappings
      ld   b, c
      call VDP_SetVRAMPointer
        
      ;copy a byte to VRAM
-:    ld   a, (de)
      out  ($BE), a
          
      ;increment the source & dest pointers
      inc  de
      inc  hl
          
      ;copy a byte to VRAM
      ld   a, (de)
      nop
      nop
      out  ($BE), a
          
      ;increment the source & dest pointers
      inc  de
      inc  hl
          
      ;check for column address overflow
      ld   a, l
      and  $3F
      jp   nz, +
          
      ;adjust the pointer if there was an overflow
      push de
        ld   de, $0040
        or   a
        sbc  hl, de
        call VDP_SetVRAMPointer
      pop  de
+:    djnz -
  
      pop  hl
      ld   bc, $0040
      add  hl, bc
      ld   a, h
      cp   $3F
      jp   nz, +
      ld   h, $38
+:    pop  bc
      djnz Engine_CopyScreenMappings
      ei
      ret


LABEL_2C75:
  call LABEL_2CB9
-:  push bc
  push hl
  ld   b, c
  call VDP_SetVRAMPointer
--: ld   a, (de)
  out  ($be), a
  ld   a, ($d3f1)
  push af
  pop  af
  out  ($be), a
  inc  de
  inc  hl
  call LABEL_2C9B
  djnz --
  pop  hl
  ld   bc, $0040
  add  hl, bc
  call LABEL_2CAB
  pop  bc
  djnz -
  ret

LABEL_2C9B:
  ld   a, l
  and  $3f
  ret  nz
  push de
  ld   de, $0040
  or   a
  sbc  hl, de
  call VDP_SetVRAMPointer
  pop  de
  ret

LABEL_2CAB: 
  ld   a, h
  cp   $3f
  ret  nz
  ld   h, $38
  ret

LABEL_2CB2:
  ld   a, h
  cp   $38
  ret  nc
  ld   h, $3e
  ret


;adjusts VRAM address before copying mappings to the nametable
LABEL_2CB9:
  push bc
    ld   a, l
    and  $C0
    ld   b, a
    ld   a, ($D19A)
    rrca
    rrca
    add  a, l
    and  $3E
    or   b
    ld   l, a
    
    push hl
      ld   a, (CameraLimit_Y)
      rrca
      rrca
      rrca
      and  $1f
      rlca
      ld   c, a
      ld   b, $00
      ld   hl, DATA_4765
      add  hl, bc
      ld   c, (hl)
      inc  hl
      ld   b, (hl)
    pop  hl
    
    add  hl, bc
    ld   a, h
    ;check for overflow
    cp   $3F
    jr   c, +
    or   a
    ;loop back to start of name table
    ld   bc, $0700
    sbc  hl, bc
+:  pop  bc
  ret

LABEL_2CEB:
  di
  call LABEL_2CF5
  ld   bc, $0404
  jp   Engine_CopyScreenMappings
  
LABEL_2CF5:
  ld   hl, ($d407)
  ld   a, l
  and  $e0
  ld   l, a
  srl  h
  rr   l
  srl  h
  rr   l
  ld   bc, DATA_4765
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   b, (hl)
  ld   a, ($d405)
  rrca
  rrca
  and  $38
  ld   l, a
  ld   h, $38
  add  hl, bc
  ret

LABEL_2D17:
  di
  call LABEL_2D21
  ld   bc, $0404
  jp   Engine_CopyScreenMappings

LABEL_2D21:
  ld   hl, ($d40f)
  ld   a, l
  and  $e0
  ld   l, a
  srl  h
  rr   l
  srl  h
  rr   l
  ld   bc, DATA_4765
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   b, (hl)
  ld   a, ($d40d)
  rrca
  rrca
  and  $38
  ld   l, a
  ld   h, $38
  add  hl, bc
  ret


;---------- function start ----------
; (middle level)
Engine_ReadInput:   ;$2D43
    ld   a, (DisableInput)
    or   a
    ret  nz

    ld   a, (ControllerData)
    ld   (ControllerData_Prev), a
    ld   a, (RAM_D139)
    ld   (RAM_D13A), a
    call Engine_ReadInputPort
    cpl
  
.ifeq SMS_VERSION 1
    ld   b, a
    ld   a, ($D139)
    or   b
.endif
  
    ld   (ControllerData), a
    ld   hl, ControllerData
    
    ;if both up & down are pressed reset both buttons
    and  BTN_UP | BTN_DOWN
    cp   BTN_UP | BTN_DOWN
    jr   nz, +
    
    res  BTN_UP_BIT, (hl)
    res  BTN_DOWN_BIT, (hl)

    ;if both left & right are pressed clear both buttons
+:  ld   a, (hl)
    and  BTN_LEFT | BTN_RIGHT
    cp   BTN_LEFT | BTN_RIGHT
    jr   nz, +
    res  BTN_LEFT_BIT, (hl)
    res  BTN_RIGHT_BIT, (hl)

+:  ld   a, (ControllerData_Prev)
    xor  $FF
    ld   b, a
    ld   a, (ControllerData)
    and  $BF
    ld   c, a
    xor  $FF
    xor  b
    and  c
    ld   (RAM_D139), a
    ld   (RAM_D13B), a
    
    call Engine_ReadDemoSequence
    ;disable pause if the bg palette is fading
    ld   a, (BgPaletteControl)
    and  $C0
    ret  z

    xor  a
    ld   (ControllerData), a
    ld   (RAM_D139), a
    ret

;---------- function start ----------
; (middle level)
Engine_ReadInputPort:   ;$2D9A
  in   a, (Ports_IO1)
  or   $C0
  
.ifeq SMS_VERSION 0
  and  $7F
  ld   b, a
  in   a, (Ports_GG_Start)
  and  $80
  or   b
.endif
  
  ret

; ==============================================================================
;
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_ReadDemoSequence:                ; $2DA7
    ld   a, (DemoSequenceIdx)
    bit  INP_DEMO_MODE_BIT, a
    ret  z

    ld   a, (DemoSequenceBank)
    ld   ($FFFF), a
    ld   hl, (DemoSequencePtr)
    ld   a, h
    or   l
    ret  z

    ld   a, (hl)
    ld   (ControllerData), a
    inc  hl
    ld   a, (hl)
    ld   (RAM_D139), a
    inc  hl
    ld   (DemoSequencePtr), hl
    ret

;**********************************************************
;*  Decompress a tileset to VRAM. Clears the data buffers *
;*  before and after decompression.                       *
;**********************************************************
Engine_DecompressTileset:   ;$2DC7
  push af
  call Engine_ClearDecompressionBuffers
  pop  af
  push ix
  call Engine_LoadCompressedTileset
  pop  ix
  call Engine_ClearDecompressionBuffers
  ret


.include "src/tile_loading_routines.asm"


; Data from 2F2A to 2F5E (53 bytes)
LABEL_2F2A:
  ld   d, $00
  ld   l, d
  ld   b, $08
  add  hl, hl
  jr   nc, +
  add  hl, de
+:  djnz $FA
  ret


LABEL_2F36:
  ld   b, $10
  xor  a
  add  hl, hl
  rla
  cp   e
  jr   c, $03
  sub  e
  set  0, l
  djnz $F6
  ret


LABEL_2F44:
  ld   a, $10
  sla  e
  rl   d
  adc  hl, hl
  jr   c, $09
  sbc  hl, bc
  jr   nc, $08
  add  hl, bc
  dec  a
  jr   nz, $F0
  ret


LABEL_2F57:
  or   a
  sbc  hl, bc
  inc  e
  dec  a
  jr   nz, $E8
  ret


;---------- function start ----------
; (middle level)
_LABEL_2F5F_258:
  ld   a, (RAM_D182)
  or   a
  ret  nz

  ld   a, (RAM_D181)
  or   a
  ret  z

  ld   a, (CurrentLevel)
  cp   $06
  jp   z, _LABEL_2FE8_259
  ld   a, (GameMode)
  cp   $11
  jr   z, _LABEL_2FA4_260
  ld   hl, $D183
  inc  (hl)
  ld   a, $3C
  sub  (hl)
  ret  nz

  ld   (hl), a
  ld   a, $01
  ld   hl, $D15D
  add  a, (hl)
  daa
  ld   (hl), a
  sub  $60
  jp   nz, LABEL_3035
  ld   (hl), a
  ld   a, $01
  inc  hl
  add  a, (hl)
  daa
  ld   (hl), a
  sub  $10
  jp   nz, LABEL_3035
  ld   a, $C0
  ld   (RAM_D3CF), a
  xor  a
  ld   (RAM_D181), a
  ret

_LABEL_2FA4_260:
  ld   hl, $DBC8
  ld   de, DATA_2FE7
  or   a
  ld   b, $03
_LABEL_2FAD_264:
  ld   a, (de)
  adc  a, (hl)
  daa
  ld   (hl), a
  dec  de
  dec  hl
  djnz _LABEL_2FAD_264
  jp   nc, _LABEL_306B_265
  xor  a
  ld   (RAM_DBC6), a
  ld   (RAM_DBC7), a
  ld   (RAM_DBC8), a
  ld   a, $01
  ld   hl, $D15D
  add  a, (hl)
  daa
  ld   (hl), a
  sub  $60
  jp   nz, LABEL_3035
  ld   (hl), a
  ld   a, $01
  inc  hl
  add  a, (hl)
  daa
  ld   (hl), a
  sub  $10
  jp   nz, LABEL_3035
  ld   a, $C0
  ld   (RAM_D3CF), a
  xor  a
  ld   (RAM_D181), a
  ret


; Data from 2FE5 to 2FE7 (3 bytes)
.db $01, $66
DATA_2FE7:
.db $67

_LABEL_2FE8_259:
  ld   hl, $D183
  inc  (hl)
  ld   a, $3C
  sub  (hl)
  ret  nz

  ld   (hl), a
  call _LABEL_3024_266
  ld   a, (RAM_D15D)
  sub  $01
  daa
  ld   (RAM_D15D), a
  jp   nc, LABEL_3035
  ld   a, $59
  ld   (RAM_D15D), a
  ld   a, (RAM_D15E)
  sub  $01
  daa
  ld   (RAM_D15E), a
  jp   nc, LABEL_3035
  xor  a
  ld   (RAM_D15E), a
  xor  a
  ld   (RAM_D181), a
  ld   (RAM_D15D), a
  ld   hl, $D144
  set  2, (hl)
  jp   LABEL_3035

;---------- function start ----------
; (middle level)
_LABEL_3024_266:
  ld   hl, $0010
  ld   de, (RAM_D15D)
  xor  a
  sbc  hl, de
  ret  c

  ld   a, $C0
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3035:
  ld   a, (GameMode)
  cp   $09
  jr   z, _LABEL_3042_262
  ld   a, (CurrentAct)
  cp   $02
  ret  z

_LABEL_3042_262:
  ld   a, (RAM_D15E)
  rlca
  and  $1E
  add  a, $2E
  ld   (RAM_DBB9), a
  ld   a, (RAM_D15D)
  rrca
  rrca
  rrca
  and  $1E
  add  a, $2E
  ld   (RAM_DBBD), a
  ld   a, (RAM_D15D)
  rlca
  and  $1E
  add  a, $2E
  ld   (RAM_DBBF), a
  ld   a, (GameMode)
  cp   $11
  ret  nz

_LABEL_306B_265:
  ld   a, (RAM_DBC6)
  rrca
  rrca
  rrca
  and  $1E
  add  a, $2E
  ld   (RAM_DBAB), a
  ld   a, (RAM_DBC6)
  and  $0F
  ld   l, a
  ld   h, $00
  ld   de, DATA_3098
  ld   a, (RAM_DBC6)
  bit  4, a
  jr   z, _LABEL_308D_263
  ld   de, DATA_30A8
_LABEL_308D_263:
  add  hl, de
  ld   a, (hl)
  rlca
  and  $1E
  add  a, $2E
  ld   (RAM_DBAD), a
  ret

DATA_3098:
.db $00, $01, $02, $03, $04, $05, $06, $07
.db $08, $09, $00, $00, $00, $00, $00, $00
DATA_30A8:
.db $00, $02, $02, $04, $04, $05, $07, $07
.db $09, $09, $00, $00, $00, $00, $00, $00


LABEL_30B8:
  ld   a, (ix+$0b)
  or   a
  ret  z
  ld   d, $00
  ld   e, (ix+$0a)
  ld   hl, $0200
  add  hl, de
  ld   a, (hl)
  and  a
  jp   p, LABEL_30D8
  ld   ($d100), a
  ld   a, $ff
  ld   ($d101), a
  ld   ($d106), a
  jr   LABEL_30E2

LABEL_30D8:
  ld   ($d100), a
  xor  a
  ld   ($d101), a
  ld   ($d106), a
LABEL_30E2:
  ld   hl, $0000
  ld   ($d104), hl
  ld   b, (ix+$0b)
-:  ld   hl, ($d104)
  ld   de, ($d100)
  add  hl, de
  ld   ($d104), hl
  djnz -
  push ix
  pop  hl
  ld   de, $0010
  add  hl, de
  ld   de, $d104
  xor  a
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  ld   a, (ix+$0a)
  add  a, $c0
  ld   e, a
  ld   d, $00
  ld   hl, $0200
  add  hl, de
  ld   a, (hl)
  and  a
  jp   p, LABEL_312E
  ld   ($d100), a
  ld   a, $ff
  ld   ($d101), a
  ld   ($d106), a
  jr   LABEL_3138

LABEL_312E:
  ld   ($d100), a
  xor  a
  ld   ($d101), a
  ld   ($d106), a
LABEL_3138:
  ld   hl, $0000
  ld   ($d104), hl
  ld   b, (ix+$0b)
-:  ld   hl, ($d104)
  ld   de, ($d100)
  add  hl, de
  ld   ($d104), hl
  djnz -
  push ix
  pop  hl
  ld   de, $0013
  add  hl, de
  ld   de, $d104
  xor  a
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  inc  de
  inc  hl
  ld   a, (de)
  adc  a, (hl)
  ld   (hl), a
  ret

LABEL_3167:
  di
  call VDP_ClearVRAM
  call LABEL_327E
  xor  a
  ld   ($d159), a
  ld   ($d15a), a
  di
  call Engine_ClearWorkingSAT
  di
  ld   ix, LevelHeader
  call Engine_ClearLevelAttributes
  di
  call Engine_LoadLevelHeader
  di
  call LABEL_40A5
  di
  ld   a, (GameMode)
  cp   $0d
  jp   z, LABEL_3222
  ld   a, (CurrentLevel)
  cp   $09
  jp   z, LABEL_3235
  call LABEL_3321
  ld   hl, $0480
  call Engine_LoadNackExplosionArt
  ld   a, (GameMode)
  cp   $11
  jr   z, LABEL_31AF
  call Engine_LoadLevelTiles
  jr   LABEL_31B2

LABEL_31AF:
  call LABEL_7513
LABEL_31B2:
  ld   a, (GameMode)
  cp   $01
  jp   z, LABEL_3258
  ld   hl, $0400
  call Engine_LoadInvincibilityArt
  call Engine_LoadCompressedRingArt
  ld   a, (GameMode)
  cp   $09
  jr   z, LABEL_31ED
  ld   a, (CurrentLevel)
  cp   $05
  jr   nz, LABEL_31D8
  ld   a, (CurrentAct)
  cp   $02
  jr   z, LABEL_3209
LABEL_31D8:
  ld   a, (CurrentLevel)
  cp   $04
  jr   nz, LABEL_31E6
  ld   a, (CurrentAct)
  cp   $02
  jr   z, LABEL_3201
LABEL_31E6:
  ld   a, (CurrentAct)
  cp   $02
  jr   z, LABEL_320C
LABEL_31ED:
  ld   a, $1b
  call Engine_SwapFrame2
  call $BD12
  call LABEL_3035
  call LABEL_33BA
  call LABEL_3370
  call LABEL_780B
LABEL_3201:
  call LABEL_76F6
  call LABEL_75C5
  jr   LABEL_320C

LABEL_3209:
  call LABEL_75D7
LABEL_320C:
  call LABEL_32C3
  ld   a, (PlayerObjectID)
  ld   ($d500), a
  ld   a, $1b
  call Engine_SwapFrame2
  call $BEF5
  call $BE6C
  jr   LABEL_3258

LABEL_3222:
  call LABEL_74F7
  call LABEL_76F6
  ld   a, $1b
  call Engine_SwapFrame2
  call $BE43
  call LABEL_33BA
  jr   LABEL_3258

LABEL_3235:
  call LABEL_3321
  call Engine_LoadLevelTiles
  ld   hl, $0480
  call Engine_LoadNackExplosionArt
  ld   hl, $0400
  call Engine_LoadInvincibilityArt
  call Engine_LoadCompressedRingArt
  ld   a, $1b
  call Engine_SwapFrame2
  call $BEF5
  ld   a, (PlayerObjectID)
  ld   ($d500), a
LABEL_3258:
  ld   a, (CurrentLevel)
  cp   $06
  jr   z, LABEL_326B
  ld   a, (CurrentAct)
  cp   $02
  jr   nz, LABEL_326B
  ld   a, $ff
  ld   ($dbc3), a
LABEL_326B:
  ld   a, ($d149)
  or   a
  jr   nz, LABEL_327A
  ld   a, $ff
  ld   ($d181), a
  call Engine_WaitForInterrupt
  ret

LABEL_327A:
  call Engine_WaitForInterrupt
  ret

LABEL_327E:
  ld   hl, $D181
  ld   de, $D182
  ld   bc, $0B16
  ld   (hl), $00
  ldir
  xor  a
  ld   ($D12F), a
  ret

LABEL_3290:
  ld   a, (hl)
  ld   (ix+$00), a
  inc  hl
  ld   a, (hl)
  ld   (iy+$00), a
  inc  hl
  ld   a, (hl)
  ld   (iy+$01), a
  inc  hl
  inc  ix
  inc  iy
  inc  iy
  djnz LABEL_3290
  ret

LABEL_32A8:
  xor  a
  ld   ($d14c), a
  ld   ($d14d), a
  ld   ($d14e), a
  ld   a, ($d168)
  cp   $80
  jr   z, LABEL_32C0
  xor  a
  ld   (CurrentLevel), a
  ld   (CurrentAct), a
LABEL_32C0:
  call   LABEL_32F4
LABEL_32C3:
  xor  a
  ld   ($d159), a
  ld   ($d15a), a
  ld   a, ($d17a)
  cp   $fe
  jr   z, LABEL_32DC
  xor  a
  ld   ($d15d), a
  ld   ($d15e), a
  ld   ($d15f), a
  ret

LABEL_32DC:
  ld   a, ($d17b)
  ld   ($d15d), a
  ld   a, ($d17c)
  ld   ($d15e), a
  ld   a, ($d17d)
  ld   ($d15f), a
  ld   a, $fe
  ld   ($d17a), a
  ret

LABEL_32F4:
  xor  a
  ld   hl, $0000
  ld   ($d171), a
  ld   ($d169), hl
  ld   ($d16b), hl
  ret

LABEL_3302:
  xor  a
  ld   hl, $0000
  ld   ($d17a), a
  ld   ($d172), hl
  ld   ($d174), hl
  ret


;---------- function start ----------
; (high level)
LABEL_3310:
  xor  a
  ld   (RAM_D198), a
  ld   (BackgroundYScroll), a
  ld   hl, $0000
  ld   (RAM_D19A), hl
  ld   (RAM_D19C), hl
  ret


LABEL_3321:
  ld   a, :Engine_LoadDynamicsHeader
  call Engine_SwapFrame2
  call Engine_LoadDynamicsHeader
  ret

Engine_Add100Rings:   ;$332A
  ld   a, ($D15A)
  add  a, $01
  daa
  ld   ($D15A), a
  ld   a, ($D15A)
  ld   b, a
  ld   a, ($DBDF)
  sub  b
  jp   nc, LABEL_33BA
  ld   a, b
  ld   ($DBDF), a
LABEL_3342:
  ld   a, ($D140)
  add  a, $01
  daa
  ld   ($D140), a
  ld   a, ($D532)
  cp   PowerUp_SpeedShoes
  jr   z, LABEL_3370
  cp   $05
  jr   z, LABEL_3370
  cp   $0f
  jr   z, LABEL_3370
  cp   $0a
  jr   z, LABEL_3370
  cp   $09
  jr   z, LABEL_3370
  ld   a, Music_ExtraLife
  ld   (Sound_Music_Trigger), a
  call Engine_WaitForInterrupt
  ld   hl, $0078
  ld   ($d3ee), hl

LABEL_3370:
  ld   a, (GameMode)
  cp   GameMode_TimeAttack
  jp   z, LABEL_33BA
  ld   a, ($D140)
  rrca
  rrca
  rrca
  rrca
  and  $0F
  add  a, a
  add  a, $2E
  ld   ($DBA7), a
  ld   a, ($D140)
  and  $0F
  add  a, a
  add  a, $2E
  ld   ($DBA9), a
  jp   LABEL_33BA

LABEL_3395:
  ret

Engine_Add10Rings:    ;$3396
  ;add 10 rings to the counter
  ld   a, ($D159)
  add  a, $10
  daa
  ld   ($D159), a
  call LABEL_33BA
  ;check for BCD overflow
  ld   a, ($D159)
  cp   $10
  ret  nc
  jp   Engine_Add100Rings

Engine_AddRing:     ;$33AB
  ;add a ring to the counter
  ld   a, ($D159)
  add  a, $01
  daa
  ld   ($D159), a
  ;check for BCD overflow
  or   a
  jr   nz, LABEL_33BA
  jp   Engine_Add100Rings

LABEL_33BA:
  ld   a, (GameMode)
  cp   $09
  jr   z, LABEL_33CF
  ld   a, (CurrentAct)
  cp   $02
  ret  z
  ld   a, (GameMode)
  cp   $0d
  jp   z, LABEL_33F3
LABEL_33CF:
  ld   a, ($d159)
  rrca
  rrca
  rrca
  and  $1e
  add  a, $2e
  ld   ($dbb1), a
  ld   a, ($d159)
  rlca
  and  $1e
  add  a, $2e
  ld   ($dbb3), a
  ld   a, ($d15a)
  rlca
  and  $1e
  add  a, $2e
  ld   ($dbaf), a
  ret

LABEL_33F3:
  ld   a, ($d159)
  rrca
  rrca
  rrca
  and  $1e
  add  a, $2e
  ld   ($dbbd), a
  ld   a, ($d159)
  rlca
  and  $1e
  add  a, $2e
  ld   ($dbbf), a
  ret


;**********************************************************
;*  Clear each colour entry in the working copy of colour *
;*  RAM to white. Does not synch with VDP.                *
;**********************************************************
Engine_ClearWorkingPaletteToWhite:    ;$340C
  ld   b, PaletteSize_Bytes
  ld   hl, WorkingCRAM
.ifeq SMS_VERSION 1
-:  ld   a, $3F
  ld   (hl), a
  inc  hl
  djnz -
.else
-:  ld   a, $FF
  ld   (hl), a
  inc  hl
  ld   a, $0F
  ld   (hl), a
  inc  hl
  djnz -
.endif 
  ret

LABEL_341C:
  call LABEL_7795
  ld   a, $12
  call Engine_SwapFrame2
  ld   hl, $38cc
  ld   de, $bbae
  ld   bc, $1214
  call LABEL_2C3B
  ret

LABEL_3431:
  ld   a, ($d3fc)
  or   a
  ret  nz
  ld   ix, $d500
  ld   a, ($d500)
  or   a
  ret  z
  bit  1, (ix+$24)
  jr   z, +
  ld   a, ($d526)
  or   a
  jr   z, +
  dec  (ix+$26)
  jr   nz, +
  res  1, (ix+$24)
+:  ld   a, $0c
  call Engine_SwapFrame2
  ld   a, $ff
  ld   ($d133), a
  call Engine_ProcessObjectLogic
  ld   a, $ff
  ld   ($d133), a
  ld   a, $0c
  call Engine_SwapFrame2
  call Engine_UpdateObject_CallLogicPtr
  xor  a
  ld   ($d133), a
  ld   a, ($dbd6)
  or   a
  jr   z, LABEL_347E
  ld   hl, $004a
  ld   ($d511), hl
LABEL_347E:
  ld   a, ($d428)
  or   a
  jr   nz, +
  call $BA5D
  call LABEL_37B5
  call $AFDE
  ld   a, ($d12e)
  rrca
  call nc, $AF82
+:  call LABEL_349A
  jp   $B811

LABEL_349A:
  ld   hl, ($d516)
  bit  7, h
  jr   nz, LABEL_34AD
  ld   de, $0600
  xor  a
  sbc  hl, de
  ret  c
  ld   ($d516), de
  ret

LABEL_34AD:
  ld   de, $fa00
  xor  a
  sbc  hl, de
  ret  nc
  ld   ($d516), de
  ret

LABEL_34B9:
  ld   ix, $d540
  ld   a, ($d540)
  or   a
  ret  z
  cp   $08
  jp   nz, LABEL_4FC0
  ld   a, $ff
  ld   ($d133), a
  ld   a, $16
  call Engine_SwapFrame2
  call Engine_ProcessObjectLogic
  xor  a
  ld   ($d133), a
  ld   a, $ff
  ld   ($d133), a
  ld   a, $16
  call Engine_SwapFrame2
  call Engine_UpdateObject_CallLogicPtr
  xor  a
  ld   ($d133), a
  ld   a, (ix+$01)
  or   a
  ret  z
  jp   LABEL_5456

LABEL_34F1:
  ld   a, ($d12b)
  push af
  ld   a, $0e
  ld   ($d12b), a
  ld   ($ffff), a
  ld   a, $0e
  call Engine_SwapFrame2
  ld   hl, ($d516)
  ld   de, ($d3d0)
  ld   a, ($d3d2)
  add  hl, de
  jr   nc, LABEL_3510
  inc  a
LABEL_3510:
  ld   ($d3d0), hl
  ld   ($d3d2), a
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $9B2D
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53c)
  add  hl, de
  ld   ($d514), hl
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $9EC5
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53a)
  add  hl, de
  ld   ($d511), hl
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

LABEL_3542:
  call LABEL_34F1
  ld   hl, ($d3d1)
  xor  a
  ld   de, $0090
  sbc  hl, de
  jr   nc, LABEL_3560
  ld   hl, ($d516)
  ld   de, $000a
  xor  a
  sbc  hl, de
  jr   c, LABEL_3591
  ld   ($d516), hl
  jr   LABEL_356E

LABEL_3560:
  ld   (ix+$25), $01
  ld   hl, ($d516)
  ld   de, $000c
  add  hl, de
  ld   ($d516), hl
LABEL_356E:
  ld   hl, ($d3d1)
  ld   de, $0180
  xor  a
  sbc  hl, de
  ret  c
  ld   a, $06
  bit  1, (ix+$03)
  jr   z, +
  ld   a, $09
+:  ld   (ix+$02), a
  ld   hl, ($d3d6)
  ld   ($d516), hl
  ld   a, $10
  ld   ($d3d3), a
  ret

LABEL_3591:
  ld   hl, ($d53a)
  ld   de, ($d511)
  xor  a
  sbc  hl, de
  jr   c, LABEL_35AA
  ld   hl, ($d511)
  ld   de, $0008
  add  hl, de
  ld   ($d511), hl
  jp   LABEL_3B9F

LABEL_35AA:
  ld   hl, ($d511)
  ld   de, $fffe
  add  hl, de
  ld   ($d511), hl
  jp   LABEL_3B9F

LABEL_35B7:
  ld   a, ($d12b)
  push af
  ld   a, $0e
  ld   ($d12b), a
  ld   ($ffff), a
  ld   a, $0e
  call Engine_SwapFrame2
  ld   hl, ($d516)
  ld   de, ($d3d0)
  ld   a, ($d3d2)
  add  hl, de
  jr   nc, LABEL_35D6
  inc  a
LABEL_35D6:
  ld   ($d3d0), hl
  ld   ($d3d2), a
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $a909
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53c)
  add  hl, de
  ld   ($d514), hl
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $ac6d
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53a)
  add  hl, de
  ld   ($d511), hl
  ld   hl, ($d3d1)
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  xor  a
  ld   de, $0090
  sbc  hl, de
  jr   nc, LABEL_3623
  ld   hl, ($d516)
  ld   de, $000a
  xor  a
  sbc  hl, de
  jp   c, LABEL_3591
  ld   ($d516), hl
  jr   LABEL_3631

LABEL_3623:
  ld   (ix+$25), $01
  ld   hl, ($d516)
  ld   de, $000c
  add  hl, de
  ld   ($d516), hl
LABEL_3631:
  ld   hl, ($d3d1)
  ld   de, $01a0
  xor  a
  sbc  hl, de
  ret  c
  ld   a, $0a
  ld   (ix+$02), a
  ld   hl, ($d3d6)
  ld   ($d518), hl
  ld   (ix+$16), $00
  ld   (ix+$17), $00
  set  0, (ix+$03)
  set  1, (ix+$03)
  res  1, (ix+$22)
  ret

LABEL_365B:
  ld   a, ($d12b)
  push af
  ld   a, $0e
  ld   ($d12b), a
  ld   ($ffff), a
  ld   a, $0e
  call Engine_SwapFrame2
  ld   hl, ($d516)
  dec  hl
  ld   a, h
  cpl
  ld   h, a
  ld   a, l
  cpl
  ld   l, a
  ld   de, ($d3d0)
  ld   a, ($d3d2)
  add  hl, de
  jr   nc, +
  inc  a
+:  ld   ($d3d0), hl
  ld   ($d3d2), a
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $a1db
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53c)
  add  hl, de
  ld   ($d514), hl
  ld   hl, ($d3d1)
  add  hl, hl
  ld   de, $a573
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d53a)
  add  hl, de
  ld   ($d511), hl
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

LABEL_36B3:
  call LABEL_365B
  ld   hl, ($d3d1)
  xor  a
  ld   de, $0090
  sbc  hl, de
  jr   nc, LABEL_36DF
  ld   hl, ($d516)
  dec  hl
  ld   a, h
  cpl
  ld   h, a
  ld   a, l
  cpl
  ld   l, a
  ld   de, $000a
  xor  a
  sbc  hl, de
  jr   c, LABEL_3717
  dec  hl
  ld   a, h
  cpl
  ld   h, a
  ld   a, l
  cpl
  ld   l, a
  ld   ($d516), hl
  jr   LABEL_36ED

LABEL_36DF:
  ld   (ix+$25), $00
  ld   hl, ($d516)
  ld   de, $fff4
  add  hl, de
  ld   ($d516), hl
LABEL_36ED:
  ld   hl, ($d3d1)
  ld   de, $0180
  xor  a
  sbc  hl, de
  ret  c
  ld   a, $06
  bit  1, (ix+$03)
  jr   z, +
  ld   a, $09
+:  ld   (ix+$02), a
  ld   hl, ($d3d6)
  dec  hl
  ld   a, h
  cpl
  ld   h, a
  ld   a, l
  cpl
  ld   l, a
  ld   ($d516), hl
  ld   a, $10
  ld   ($d3d3), a
  ret

LABEL_3717:
  ld   hl, ($d53a)
  ld   de, ($d511)
  xor  a
  sbc  hl, de
  jr   c, LABEL_3730
  ld   hl, ($d511)
  ld   de, $0008
  add  hl, de
  ld   ($d511), hl
  jp   LABEL_3B9F

LABEL_3730:
  ld   hl, ($d511)
  ld   de, $fffe
  add  hl, de
  ld   ($d511), hl
  jp   LABEL_3B9F

LABEL_373D:
  ld   a, (ix+$32)
  cp   $09
  ret  z
  ld   a, (ix+$01)
  cp   $16
  ret  z
  cp   $23
  ret  z
  res  4, (ix+$04)
  ld   a, (CurrentLevel)
  cp   $02
  jp   z, LABEL_37AF
  ld   (ix+$02), $0c
LABEL_375C:
  res  0, (ix+$24)
  ld   a, (ix+$11)
  and  $e0
  ld   (ix+$11), a
  ld   l, a
  ld   h, (ix+$12)
  ld   ($d53a), hl
  ld   a, (ix+$14)
  and  $e0
  add  a, $04
  ld   l, a
  ld   (ix+$14), a
  ld   h, (ix+$15)
  ld   ($d53c), hl
  xor  a
  ld   ($d3d0), a
  ld   ($d3d1), a
  ld   ($d3d2), a
  pop  af
  ret

LABEL_378C:
  ld   a, (ix+$32)
  cp   $09
  ret  z
  ld   a, (ix+$01)
  cp   $16
  ret  z
  cp   $23
  ret  z
  set  4, (ix+$04)
  ld   hl, ($d511)
  ld   bc, $0020
  add  hl, bc
  ld   ($d511), hl
  ld   (ix+$02), $0d
  jr   LABEL_375C
LABEL_37AF:
  ld   (ix+$02), $13
  jr   LABEL_375C

LABEL_37B5:
  ld   a, ($d3c4)
  or   a
  jr   z, LABEL_37D3
  ld   a, ($d520)
  cp   $4d
  jr   z, LABEL_37D3
  ld   a, ($d522)
  bit  0, a
  jr   z, LABEL_37D3
  ld   a, $ff
  ld   ($d3c1), a
  ld   a, $50
  ld   ($dbd0), a
LABEL_37D3:
  ld   a, ($d501)
  cp   $21
  jr   nz, LABEL_37E4
  xor  a
  ld   ($d3c1), a
  ld   ($d521), a
  jp   LABEL_39CF

LABEL_37E4:
  call LABEL_5A3B
  bit  6, (ix+$03)
  jp   nz, LABEL_3A33
  ld   a, ($d3c2)
  or   a
  jp   nz, LABEL_39D4
  ld   a, ($d532)
  cp   $05
  jr   nz, LABEL_3804
  xor  a
  ld   ($d3c1), a
  ld   ($d520), a
  ret

LABEL_3804:
  ld   a, ($d3c1)
  or   a
  jp   z, LABEL_3816
  xor  a
  ld   ($d3c1), a
  res  0, (ix+$23)
  jp   LABEL_384A

LABEL_3816:
  ld   a, ($d520)
  or   a
  ret  z
  cp   $25
  jp   z, LABEL_398E
  cp   $2c
  jp   z, LABEL_398E
  cp   $4d
  jp   z, LABEL_398E
  cp   $27
  jp   z, LABEL_398E
  cp   $43
  jp   z, LABEL_398E
  bit  1, (ix+$03)
  jp   nz, LABEL_3982
  cp   $06
  jp   z, LABEL_398E
  cp   $22
  jp   z, LABEL_398E
  cp   $2d
  jp   z, LABEL_398E
LABEL_384A:
  ld   a, ($d501)
  cp   $48
  jr   nz, LABEL_3857
  dec  (ix+$3f)
  jp   nz, LABEL_3A2B
LABEL_3857:
  ld   a, ($d532)
  or   a
  jr   z, LABEL_3860
  call LABEL_2216
LABEL_3860:
  ;remove any existing powerups
  ld   a, ($D532)
  ld   b, a
  xor  a
  ld   ($D532), a
  ld   ($DBE3), a
  
  ld   hl, $0000
  ld   ($DBE4), hl
  
  ;check for the existence of the speed shoes power up
  ld   a, b
  cp   PowerUp_SpeedShoes
  jr   z, LABEL_388F

  ;check the player object's state
  ld   a, ($D501)
  cp   $11
  jr   z, LABEL_388F
  cp   $16
  jr   z, LABEL_3897
  cp   $29
  jr   z, LABEL_3894
  cp   $23
  jr   z, LABEL_3897
  cp   $43
  jr   z, LABEL_388F
  jr   LABEL_389A

LABEL_388F:
  call LABEL_2216
  jr   LABEL_3897

LABEL_3894:
  call Engine_ChangeLevelMusic
LABEL_3897:
  jp   LABEL_38F2

LABEL_389A:
  ;is the current ring counter at 0?
  ld   a, ($D159)
  ld   b, a
  ld   a, ($D15A)
  or   b
  jp   z, Player_SetSate_Dead

  ld   a, b
  rrca
  rrca
  rrca
  rrca
  and  $0F
  inc  a
  cp   $08
  jr   c, LABEL_38B3
  ld   a, $07
LABEL_38B3:
  ld   b, a
  ld   c, $1E
  ld   h, $00
  ;allocate some ring objects
-:  push bc
  call Engine_AllocateLinkedObject
  pop  bc
  inc  h
  djnz -

  xor  a
  ld   ($D3C4), a
  ld   a, ($D3E4)
  or   a
  jr   nz, LABEL_38E3
.ifeq SMS_VERSION 0
  ;subtract the ring loss value from the ring counter
  ld   a, ($DBD0)
  ld   b, a
  ld   a, ($D159)
  sub  b
  daa
  ld   ($D159), a
  jr   nc, LABEL_38EA
  ld   a, ($D15A)
  sub  $01
  daa
  ld   ($D15A), a
  jr   nc, LABEL_38EA
.endif
LABEL_38E3:
  ;reset the ring counter
  xor  a
  ld   ($D159), a
  ld   ($D15A), a
LABEL_38EA:
  call LABEL_33BA
  ld   a, $B3
  ld   (Sound_SFX_Trigger), a
LABEL_38F2:
  res  7, (ix+$04)
  set  7, (ix+$03)
  set  6, (ix+$03)
  ld   a, $78
  ld   ($D3C2), a
LABEL_3903:
  ld   a, (ix+$01)
  cp   $29
  jr   nz, LABEL_390D
  call LABEL_2216
LABEL_390D:
  ld   (ix+$20), $00
  ld   (ix+$02), $1E
  set  0, (ix+$03)
  res  1, (ix+$22)
  ld   hl, $FC00
  bit  0, (ix+$22)
  jr   z, LABEL_3929
  ld   hl, $0100
LABEL_3929:
  ld   ($D518), hl
  ld   hl, $0100
  bit  3, (ix+$23)
  jr   nz, LABEL_3938
  ld   hl, $FF00
LABEL_3938:
  ld   ($D516), hl
  ld   hl, $0000
  ld   ($D3D8), hl
  ret


Player_SetSate_Dead:  ;$3942
  ld   a, (ix+$01)
  cp   PlayerState_LostLife
  ret  z
  cp   PlayerState_Drown
  ret  z
  ld   (ix+$02), PlayerState_LostLife
  
  ;is player drowning?
  bit  2, (ix+$03)
  jr   z, +
  ld   (ix+$02), PlayerState_Drown

+:  set  0, (ix+$03)
  ld   (ix+$04), $00
  ld   hl, $fb00
  ld   ($D518), hl
  ld   hl, $0000
  ld   ($D3D8), hl
  res  1, (ix+$22)
  ld   a, ($D3CF)
  and  $80
  ld   ($D3CF), a
  ld   a, Music_LostLife
  ld   (Sound_Music_Trigger), a
  call Engine_WaitForInterrupt
  ret

LABEL_3982:
  bit  4, (ix+$21)
  jr   nz, LABEL_3993
  bit  5, (ix+$21)
  jr   nz, LABEL_39A6
LABEL_398E:
  xor  a
  ld   ($D520), a
  ret

LABEL_3993:
  ld   a, ($d501)
  cp   $09
  jr   z, LABEL_398E
  set  0, (ix+$03)
  ld   hl, $0080
  ld   ($d518), hl
  jr   LABEL_398E

LABEL_39A6:
  set  0, (ix+$03)
  res  1, (ix+$22)
  res  1, (ix+$23)
  res  5, (ix+$21)
  ld   hl, $fd80
  bit  2, (ix+$03)
  jr   z, LABEL_39C2
  ld   hl, $ff00
LABEL_39C2:
  ld   ($d518), hl
  jr   LABEL_398E

LABEL_39C7:
  ld   a, ($d501)
  cp   $1e
  jp   z, LABEL_39ED
LABEL_39CF:
  ld   a, ($d3c2)
  or   a
  ret  z
LABEL_39D4:
  dec  a
  ld   ($d3c2), a
  or   a
  jr   z, LABEL_39F7
  ld   a, ($d501)
  cp   $1e
  jr   z, LABEL_39ED
  cp   $1a
  jr   z, LABEL_39ED
  ld   a, ($d3c2)
  rrca
  rrca
  jr   c, LABEL_39F2
LABEL_39ED:
  res  7, (ix+$04)
  ret

LABEL_39F2:
  set  7, (ix+$04)
  ret

LABEL_39F7
  xor  a
  ld   ($d3c1), a
  ld   (ix+$20), a
  res  7, (ix+$04)
  ld   hl, $d503
  res  7, (hl)
  res  6, (hl)
  ld   a, (ix+$00)
  cp   $01
  ld   a, ($d506)
  jr   nz, LABEL_3A1C
  cp   $06
  jp   c, LABEL_39ED
  res  1, (hl)
  jr   LABEL_39ED

LABEL_3A1C:
  res  1, (hl)
  cp   $2f
  jp   c, LABEL_39ED
  cp   $32
  jp   nc, LABEL_39ED
  set  1, (hl)
  ret

LABEL_3A2B:
  xor  a
  ld   ($d3c1), a
  ld   (ix+$20), a
  ret

LABEL_3A33:
  ld   (ix+$20), $00
  jp   LABEL_39C7

LABEL_3A3A:
  ld   a, ($d502)
  cp   $30
  ret  z
  ld   a, ($d416)
  or   a
  jr   z, LABEL_3A73
  cp   $12
  jr   z, LABEL_3A73
  cp   $14
  jr   z, LABEL_3A73
  cp   $0e
  jr   z, LABEL_3A73
  cp   $10
  jr   z, LABEL_3A73
  ld   a, (ix+$01)
  cp   $09
  ret  z
  ld   a, ($d416)
  ld   l, a
  ld   h, $00
  ld   de, $b79c
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ex   de, hl
  add  hl, hl
  add  hl, hl
  add  hl, hl
  ld   ($d516), hl
  jp   LABEL_3D7B

LABEL_3A73:
  ld   hl, $d503
  res  0, (hl)
  res  1, (hl)
  ld   (ix+$02), $01
  ld   hl, $0000
  ld   ($d516), hl
  ld   hl, $0400
  ld   ($d3d6), hl
  jp   $b7d0

LABEL_3A8D:
  bit  1, (ix+$22)
  jr   nz, LABEL_3A99
  ld   a, (ix+$01)
  cp   $31
  ret  z
LABEL_3A99:
  res  0, (ix+$03)
  res  1, (ix+$03)
  res  6, (ix+$03)
  ld   (ix+$02), $05
  ld   hl, $0400
  ld   ($d3d6), hl
  call $b7b6
  ld   a, $78
  ld   ($d3b7), a
  ret

LABEL_3AB8:
  ld   a, ($d503)
  bit  0, a
  ret  nz
  ld   a, ($d418)
  cp   $a7
  ret  z
  cp   $a8
  ret  z
  cp   $a9
  ret  z
  ld   a, (ix+$20)
  cp   $4d
  ret  z
  ld   a, ($d501)
  cp   $11
  ret  z
LABEL_3AD6:
  xor  a
  ld   ($d3c3), a
  res  6, (ix+$24)
  ld   hl, $d503
  set  0, (hl)
  set  1, (hl)
  ld   a, $0a
  ld   ($d502), a
  ld   hl, $fbc0
  ld   a, ($d503)
  bit  2, a
  jr   z, LABEL_3AF7
  ld   hl, $fcc0
LABEL_3AF7:
  ld   ($d518), hl
  ld   hl, ($d514)
  dec  hl
  ld   ($d514), hl
  ld   hl, ($d516)
  bit  2, (ix+$03)
  jr   nz, LABEL_3B21
  ld   a, ($d416)
  cp   $0c
  jr   nz, LABEL_3B16
  ld   hl, $0200
  jr   LABEL_3B1D

LABEL_3B16:
  cp   $0a
  jr   nz, LABEL_3B21
  ld   hl, $fe00
LABEL_3B1D:
  set  6, (ix+$24)
LABEL_3B21:
  ld   ($d516), hl
  ld   a, $78
  ld   ($d3b7), a
  ld   hl, $d522
  res  1, (hl)
  ld   a, (CurrentLevel)
  cp   $03
  jr   nz, LABEL_3B3B
  bit  5, (ix+$24)
  jr   nz, LABEL_3B41
LABEL_3B3B:
  ld   a, $b1
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3B41:
  ld   a, $cd
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3B47:
  ld   a, ($d501)
  cp   $0a
  ret  z
  cp   $26
  ret  z
  cp   $16
  ret  z
  cp   $41
  ret  z
  cp   $43
  ret  z
  cp   $48
  ret  z
  cp   $23
  ret  z
  cp   $29
  ret  z
LABEL_3B62:
  ld   a, ($d501)
  cp   $24
  jr   z, LABEL_3B6F
  ld   hl, $0100
  ld   ($d518), hl
LABEL_3B6F:
  ld   a, $0e
  ld   ($d502), a
  ld   hl, $d503
  set  0, (hl)
  res  1, (hl)
  ld   hl, $d504
  res  5, (hl)
  ld   hl, $d522
  res  1, (hl)
  ret

LABEL_3B86:
  ld   (ix+$02), $14
  ld   (ix+$18), $00
  ld   (ix+$19), $01
  set  0, (ix+$03)
  res  1, (ix+$03)
  res  1, (ix+$22)
  ret

LABEL_3B9F:
  ld   (ix+$02), $1d
  ld   (ix+$18), $80
  ld   (ix+$19), $00
  set  0, (ix+$03)
  res  1, (ix+$03)
  res  1, (ix+$22)
  ret

LABEL_3BB8:
  set  0, (ix+$03)
  set  1, (ix+$03)
  ld   (ix+$02), $0a
  ld   hl, $0000
  ld   ($d518), hl
  res  1, (ix+$22)
  ret

LABEL_3BCF:
  ld   hl, $0000
  ld   ($d516), hl
  ld   (ix+$02), $03
  ret

LABEL_3BDA:
  bit  1, (ix+$23)
  ret  z
  ld   hl, $0000
  ld   ($d516), hl
  res  1, (ix+$03)
  res  6, (ix+$03)
  ld   (ix+$02), $04
  ret

LABEL_3BF2:
  bit  5, (ix+$24)
  jr   z, +
  ld   a, (CurrentLevel)
  cp   $02
  ret  z
  
+:  ld   hl, $0000
  ld   ($d516), hl
  res  6, (ix+$03)
  ld   (ix+$02), $15
  ld   a, $ba
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3C12:
  ld   a, (CurrentLevel)
  cp   $02
  jr   nz, LABEL_3C1E
  bit  5, (ix+$24)
  ret  nz
LABEL_3C1E:
  ld   hl, $0700
  ld   ($d3d6), hl
  bit  4, (ix+$04)
  jr   z, LABEL_3C2D
  ld   hl, $f900
LABEL_3C2D:
  ld   ($d516), hl
  ld   (ix+$02), $1a
  ld   a, $c8
  ld   (Sound_SFX_Trigger), a
  ld   hl, $d503
  set  1, (hl)
  set  7, (hl)
  set  5, (hl)
  ld   c, $05
  ld   h, $ff
  call Engine_AllocateLinkedObject
  ret

LABEL_3C4A:
  ld   a, (CurrentLevel)
  cp   $02
  jr   nz, LABEL_3C56
  bit  5, (ix+$24)
  ret  nz
LABEL_3C56:
  ld   hl, $0000
  ld   ($d516), hl
  set  1, (ix+$03)
  res  6, (ix+$03)
  ld   (ix+$02), $0f
  ld   a, $ba
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3C6E:
  ld   hl, $0700
  ld   ($d3d6), hl
  bit  4, (ix+$04)
  jr   z, LABEL_3C7D
  ld   hl, $f900
LABEL_3C7D:
  ld   (ix+$16), l
  ld   (ix+$17), h
  set  1, (ix+$03)
  ld   (ix+$02), $10
  ld   a, $c8
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3C91:
  ld   a, (ix+$20)
  cp   $4d
  ret  z
  ld   hl, ($d514)
  ld   de, (CameraLimit_Y)
  xor  a
  sbc  hl, de
  ld   a, h
  or   a
  ret  nz
  ld   a, l
  cp   $a0
  ret  nc
  ld   a, ($d3e4)
  cp   $06
  ret  z
  ld   a, (CurrentLevel)
  cp   $03
  jr   z, LABEL_3CBA
  bit  5, (ix+$24)
  ret  nz
LABEL_3CBA:
  xor  a
  ld   ($d53f), a
  ld   a, ($d418)
  cp   $a7
  ret  z
  cp   $a8
  ret  z
  cp   $a9
  ret  z
  ld   hl, $0000
  ld   ($d516), hl
  ld   hl, $ff00
  ld   ($d518), hl
  ld   hl, ($d514)
  dec  hl
  ld   ($d514), hl
  ld   hl, $012c
  ld   ($d3c7), hl
  res  1, (ix+$03)
  res  6, (ix+$03)
  res  1, (ix+$21)
  res  1, (ix+$23)
  xor  a
  ld   ($d3c4), a
  ld   (ix+$02), $18
  ret

LABEL_3CFC:
  ld   hl, $0000
  ld   ($d516), hl
  ld   ($d518), hl
  ld   hl, $0700
  ld   ($d3d6), hl
  ld   a, (CurrentLevel)
  cp   $05
  jr   nz, LABEL_3D19
  ld   a, (CurrentAct)
  cp   $02
  jr   z, LABEL_3D2E
LABEL_3D19:
  ld   a, Music_RocketShoes
  ld   (Sound_Music_Trigger), a
  call Engine_WaitForInterrupt
LABEL_3D21:
  res  1, (ix+$03)
  res  6, (ix+$03)
  ld   (ix+$02), $11
  ret

LABEL_3D2E:
  ld   hl, $0400
  ld   ($d516), hl
  ld   hl, $d504
  res  4, (hl)
  jr   LABEL_3D21

LABEL_3D3B:
  res  0, (ix+$03)
  res  1, (ix+$03)
  ld   (ix+$02), $06
  ret

LABEL_3D48:
  bit  0, (ix+$03)
  ret  nz
  ld   a, $b0
  ld   (Sound_SFX_Trigger), a
  res  1, (ix+$03)
  ld   (ix+$02), $07
  ret

LABEL_3D5B:
  bit  0, (ix+$03)
  ret  nz
  ld   a, $b0
  ld   (Sound_SFX_Trigger), a
  res  1, (ix+$03)
  ld   (ix+$02), $08
  ret

LABEL_3D6E:
  ld   a, ($d502)
  cp   $23
  ret  z
  ld   a, ($d517)
  or   a
  jp   z, LABEL_3BDA
LABEL_3D7B:
  ld   hl, $0600
  ld   ($d3d6), hl
  ld   hl, $d503
  res  0, (hl)
  set  1, (hl)
  ld   a, ($d501)
  cp   $09
  ret  z
  ld   a, $09
  ld   ($d502), a
  ld   a, $b4
  ld   (Sound_SFX_Trigger), a
  ret

LABEL_3D99:
  xor  a
  ld   ($d53e), a
  ld   a, $1b
  ld   ($d502), a
  ld   hl, $d503
  set  0, (hl)
  set  1, (hl)
  ld   hl, $d522
  res  1, (hl)
  xor  a
  ld   ($d41b), a
  ret

LABEL_3DB3:
  ld   ($d518), hl
  ld   hl, $d503
  set  0, (hl)
  res  1, (hl)
  ld   hl, $d522
  res  1, (hl)
  ld   hl, $d523
  res  1, (hl)
  ld   hl, $d521
  res  1, (hl)
  ld   a, $b5
  ld   (Sound_SFX_Trigger), a
  ld   a, ($d501)
  cp   $16
  ret  z
  cp   $23
  ret  z
  ld   a, $0b
  ld   ($d502), a
  res  2, (ix+$24)
  ret

LABEL_3DE4:
  ld   a, ($d519)
  bit  7, a
  ret  nz
  ld   a, $1c
  ld   ($d502), a
  ld   ($d518), hl
  ld   hl, $d503
  set  0, (hl)
  set  1, (hl)
  ld   hl, $d522
  res  1, (hl)
  ret

LABEL_3DFF:
  ex   de, hl
  ld   hl, $d523
  res  3, (hl)
  ld   hl, $d522
  res  3, (hl)
  jr   LABEL_3E17

LABEL_3E0C:
  ex   de, hl
  ld   hl, $d523
  res  2, (hl)
  ld   hl, $d522
  res  2, (hl)
LABEL_3E17:
  res  1, (hl)
  ld   ($d516), de
  bit  7, d
  jr   z, LABEL_3E28
  dec  de
  ld   a, d
  cpl
  ld   d, a
  ld   a, e
  cpl
  ld   e, a
LABEL_3E28:
  ld   ($d3d6), de
  ld   a, $b5
  ld   (Sound_SFX_Trigger), a
  ld   hl, $d503
  set  1, (hl)
  res  0, (hl)
  ld   hl, $d524
  set  0, (hl)
  ld   a, ($d501)
  cp   $16
  ret  z
  cp   $23
  ret  z
  set  1, (hl)
  ld   a, $24
  ld   ($d526), a
  ld   a, ($d501)
  cp   $16
  ret  z
  cp   $23
  ret  z
  ld   a, $09
  ld   ($d502), a
  ret

LABEL_3E5C:
  ld   a, $ff
  ld   ($d182), a
  ld   a, Music_ActPassed
  ld   (Sound_Music_Trigger), a
  ld   a, ($d501)
  cp   $48
  jr   z, LABEL_3E73
  ld   a, $20
  ld   ($d502), a
  ret

LABEL_3E73:
  ld   a, $49
  ld   ($d502), a
  ret

LABEL_3E79:
  ret

LABEL_3E7A:
  ld   a, ($d524)
  bit  2, a
  ret  nz
  set  2, a
  ld   ($d524), a
  ld   a, ($d501)
  ld   ($d534), a
  ld   a, $24
  ld   ($d502), a
  ld   hl, $d503
  set  1, (hl)
  set  0, (hl)
  ld   hl, $d504
  res  7, (hl)
  ret

LABEL_3E9D:
  ld   a, (ix+$00)
  cp   $02
  ret  nz
  ld   hl, ($d514)
  ld   de, (CameraLimit_Y)
  xor  a
  sbc  hl, de
  ld   a, h
  or   a
  ret  nz
  ld   a, l
  cp   $a0
  ret  nc
  ld   a, $27
  ld   ($d502), a
  ret

LABEL_3EBA:
  ld   a, $16
  ld   ($d502), a
  set  1, (ix+$03)
  ret

LABEL_3EC4:
  ld   a, $26
  ld   ($d502), a
  ret

LABEL_3ECA:
  ld   a, $23
  ld   ($d502), a
  ret

LABEL_3ED0:
  ld   a, $2f
  ld   ($d502), a
  ret

LABEL_3ED6:
  ld   a, $30
  ld   ($d502), a
  ld   hl, $d503
  res  1, (hl)
  res  0, (hl)
  ret

LABEL_3EE3:
  ld   a, (CurrentAct)
  cp   $02
  ret  z
  ld   a, (CurrentLevel)
  cp   $02
  ret  nc
  ld   a, ($d501)
  cp   $16
  ret  z
  ld   a, ($d532)
  or   a
  ret  nz
  ld   a, ($d519)
  rlca
  ret  c
  ld   hl, $d503
  res  1, (hl)
  res  0, (hl)
  ld   a, $31
  ld   (ix+$02), a
  ld   hl, $0100
  ld   ($d518), hl
  ld   hl, $0000
  ld   ($d516), hl
  ret

LABEL_3F18:
  ld   a, $33
  ld   ($d502), a
  ret

LABEL_3F1E:
  bit  1, (ix+$22)
  ret  z
  ld   a, $3e
  ld   ($d502), a
  ret

LABEL_3F29:
  ld   a, $29
  ld   ($d502), a
  ld   hl, $0000
  ld   ($d516), hl
  ld   ($d518), hl
  ld   a, Music_SeaFox
  ld   (Sound_Music_Trigger), a
  ld   a, Music_SeaFox
  ld   ($d3ed), a
  ret

LABEL_3F42:
  ld   a, $41
  ld   ($d502), a
  ret

LABEL_3F48:
  ld   a, $ff
  ld   ($d53f), a
  ld   a, $43
  ld   (ix+$02), a
  ret

LABEL_3F53:
  ld   a, ($d501)
  cp   $18
  jp   z, LABEL_3B47
  cp   $4b
  ret  z
  ld   a, ($d522)
  bit  1, a
  ret  z
  ld   a, $44
  ld   ($d502), a
  ld   a, ($d517)
  bit  7, a
  ret  z
  ld   hl, $0000
  ld   ($d516), hl
  ret

LABEL_3F76:
  ld   a, $ff
  ld   ($dbd7), a
  ld   a, $4a
  ld   ($d502), a
  ret


LABEL_3F81:
  ld   a, ($d3ea)
  or   a
  ret  nz
  ld   ix, LevelHeader
  bit  LEVEL_F00_BIT7, (ix + Level.ix00)
  ret  z
  ; update the camera
  call LABEL_4B1D
  ; swap in the bank with the metatile data
  ld   a, (LevelHeader + Level.metatileBank)
  call Engine_SwapFrame2

  call LABEL_3FA6
  call LABEL_3FED
  call Engine_CalculateBgScroll
  set  LEVEL_F00_BIT6, (ix + Level.ix00)
  ret

LABEL_3FA6:
  bit  LEVEL_F00_BIT2, (ix + Level.ix00)
  jr   z, LABEL_3FC9
  ld   hl, ($d3b2)
  ld   a, h
  cp   $ff
  jr   z, LABEL_3FE6
  ld   de, ($d3a6)
  xor  a
  sbc  hl, de
  jr   c, LABEL_3FE6
  ld   a, ($d19a)
  ld   b, a
  ld   a, ($d3b2)
  xor  b
  jp   nz, LABEL_45E5
  ret

LABEL_3FC9:
  bit  LEVEL_F00_BIT3, (ix + Level.ix00)
  ret  z
  ld   hl, ($d3b2)
  ld   de, (Camera_maxX)
  xor  a
  sbc  hl, de
  jr   nc, LABEL_3FE6
  ld   a, ($d19a)
  ld   b, a
  ld   a, ($d3b2)
  xor  b
  jp   nz, LABEL_45E5
  ret

LABEL_3FE6:
  ld   hl, ($d19a)
  ld   ($d3b2), hl
  ret

.org $3FED
LABEL_3FED:
  bit  LEVEL_F00_BIT0, (ix + Level.ix00)
  jr   z, LABEL_4010

  ld   hl, (Camera_Y)
  ; check for overflow
  ld   a, h
  cp   $ff
  jr   z, Engine_UpdateCameraYPos_Limit
  ; check if Camera Y < minY
  ld   de, (Camera_minY)
  xor  a

.BANK 1 SLOT 1      ; sound driver gets swapped in here
.ORG 0

  sbc  hl, de
  jr   c, Engine_UpdateCameraYPos_Limit

  ld   a, (CameraLimit_Y)
  ld   b, a
  ld   a, (Camera_Y)
  xor  b
  jp   nz, LABEL_46AA
  ret

LABEL_4010:
  bit  LEVEL_F00_CAMERA_ENABLED, (ix + Level.ix00)
  ret  z
  ; is camera X >= Camera_maxX?
  ld   hl, (Camera_Y)
  ld   de, (Camera_maxY)
  xor  a
  sbc  hl, de
  jr   nc, Engine_UpdateCameraYPos_Limit
  ld   a, (CameraLimit_Y)
  ld   b, a
  ld   a, (Camera_Y)
  xor  b
  jp   nz, LABEL_46AA
  ret

Engine_UpdateCameraYPos_Limit:      ; $402D
  ld   hl, (CameraLimit_Y)
  ld   (Camera_Y), hl
  ret

Engine_CalculateBgScroll:   ;$4034
  ld   a, ($d19a)
  add  a, $01
  neg
  ld   ($D198), a
  ld   hl, (CameraLimit_Y)
  ld   de, $0011
  add  hl, de
  ld   de, $00E0    ;224 = screen height
-:  xor  a
  sbc  hl, de
  jr   nc, -
  add  hl, de
  ld   a, l
  ld   (BackgroundYScroll), a
  ret

LABEL_4053:
  ld   hl, ($d19a)
  bit  2, (ix+$00)
  jr   z, +
  ld   bc, $0008
  add  hl, bc
+:  srl  h
  rr   l
  srl  h
  rr   l
  srl  h
  rr   l
  srl  h
  rr   l
  srl  h
  rr   l
  ld   ($d196), hl

  ld   hl, (CameraLimit_Y)
  add  hl, de
  sla  l
  rl   h
  sla  l
  rl   h
  sla  l
  rl   h
  ld   (ix+$03), h

  ld   a, h
  add  a, a
  ld   l, a
  ld   h, $00
  ld   de, ($d18e)
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, ($d196)
  add  hl, de
  ld   de, $c001
  add  hl, de
  ld   ($d39e), hl
  ret

LABEL_40A3:
  ret

LABEL_40A4:
  ret

LABEL_40A5:
  di
  ld   a, (GameMode)
  cp   $0d
  jp   nz, LABEL_415D
  ld   de, $00fe
  ld   hl, $3800
  ld   bc, $0380
  call VDP_WriteToVRAM
  ld   de, $9aac
  ld   a, (PlayerObjectID)
  cp   $01
  jr   z, LABEL_40C7
  ld   de, $9c50
LABEL_40C7:
  di
  ld   a, $16
  call Engine_SwapFrame2
  ld   hl, $3ada
  ld   bc, $0607
  jp   LABEL_2C3B

LABEL_40D6:
  ld   a, ($d12e)
  ld   b, a
  and  $03
  cp   $03
  ret  nz
  ld   a, ($d180)
  or   a
  jr   z, LABEL_40F4
  ld   de, $9bfc
  ld   a, (PlayerObjectID)
  cp   $01
  jr   z, LABEL_410C
  ld   de, $9da0
  jr   LABEL_410C

LABEL_40F4:
  ld   hl, DATA_411D
  ld   a, (PlayerObjectID)
  cp   $01
  jr   z, LABEL_4101
  ld   hl, DATA_413D
LABEL_4101:
  ld   a, b
  and  $3c
  rrca
  ld   d, $00
  ld   e, a
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
LABEL_410C:
  ld   a, $16
  call Engine_SwapFrame2
  ld   hl, $3ada
  ld   bc, $0607
  di
  call LABEL_2C3B
  ei
  ret

DATA_411D:
.db $54, $9B, $A8, $9B, $54, $9B, $A8, $9B
.db $54, $9B, $A8, $9B, $54, $9B, $A8, $9B
.db $54, $9B, $A8, $9B, $54, $9B, $A8, $9B
.db $AC, $9A, $00, $9B, $AC, $9A, $00, $9B
DATA_413D:
.db $F8, $9C, $4C, $9D, $F8, $9C, $4C, $9D
.db $F8, $9C, $4C, $9D, $F8, $9C, $4C, $9D
.db $F8, $9C, $4C, $9D, $F8, $9C, $4C, $9D
.db $50, $9C, $A4, $9C, $50, $9C, $A4, $9C

LABEL_415D:
  call Engine_DecompressLevel
LABEL_4160:
    call Engine_LoadInitialCameraSettings
    ei
    xor  a
    ld   ($d133), a
    ld   hl, (Level_CameraLimitX)
    ld   (CameraLimit_X), hl
    ld   (Camera_X), hl
    ld   hl, (Level_CameraLimitY)
    ld   (CameraLimit_Y), hl
    ld   (Camera_Y), hl
    ld   b, $1d
-:  di
    push bc
    ld   hl, (CameraLimit_Y)
    ld   de, $0008
    add  hl, de
    ld   (Camera_Y), hl
    ld   a, ($d188)
    call Engine_SwapFrame2
    call LABEL_46AA
    set  6, (ix+$00)
    call Engine_WaitForInterrupt
    pop  bc
    djnz -
    ld   hl, ($d3f2)
    ld   ($d3b2), hl
    ld   ($d19a), hl
    ld   hl, ($d3f4)
    ld   ($d3b4), hl
    ld   (CameraLimit_Y), hl
    ld   de, $0010
    add  hl, de
    ld   de, $00e0
-:  xor  a
  sbc  hl, de
  jr   nc, -
  add  hl, de
  ld   a, l
  ld   (BackgroundYScroll), a
  ei
  ret


; =============================================================================
;  Engine_LoadInitialCameraSettings()
; -----------------------------------------------------------------------------
;  Loads the initial camera & player positions for the level.
; -----------------------------------------------------------------------------
;  Params:
;       ($D145) - Current Level
;       ($D147) - Current Act
; -----------------------------------------------------------------------------
Engine_LoadInitialCameraSettings:       ; $41C0
    ld   a, (GameMode)
    cp   $09
    jr   z, +
    ld   a, (CurrentLevel)
    cp   Level_06
    jr   z, +
    ld   a, ($d17a)
    or   a
    jp   nz, LABEL_424F
LABEL_41D5:
    ld   a, ($d171)
    or   a
    jp   nz, LABEL_4222

+:  ld   a, :LevelCameraHeaders
    call Engine_SwapFrame2
    
    ; get the header pointer for the current level
    ld   a, (CurrentLevel)
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   de, LevelCameraHeaders
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    
    ; get the pointer for the current act
    ld   a, (CurrentAct)
    ld   l, a
    ld   h, $00
    add  hl, hl
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    push de
    pop  iy
      
    ld   l, (iy + $00)
    ld   h, (iy + $01)
    ld   (Level_CameraLimitX), hl
    ld   l, (iy + $02)
    ld   h, (iy + $03)
    ld   (Level_CameraLimitY), hl
    
    ; read player start position
    ld   l, (iy + $04)
    ld   h, (iy + $05)
    ld   (Player + Object.X), hl
    ld   l, (iy + $06)
    ld   h, (iy + $07)
    ld   (Player + Object.Y), hl
    ret

LABEL_4222:
  xor  a
  ld   ($d171), a
  ld   hl, ($d169)
  ld   (Player + Object.X), hl
  ld   hl, ($d16b)
  ld   (Player + Object.Y), hl
  ld   hl, ($d16d)
  ld   ($d19a), hl
  ld   ($d3f2), hl
  ld   hl, ($d16f)
  ld   (CameraLimit_Y), hl
  ld   ($d3f4), hl
  xor  a
  ld   ($d15d), a
  ld   ($d15e), a
  ld   ($d15f), a
  ret

LABEL_424F:
  ld   hl, ($d172)
  ld   a, h
  or   l
  jp   z, LABEL_41D5
  ld   ($d511), hl
  ld   hl, ($d174)
  ld   ($d514), hl
  ld   hl, ($d176)
  ld   ($d19a), hl
  ld   ($d3f2), hl
  ld   hl, ($d178)
  ld   (CameraLimit_Y), hl
  ld   ($d3f4), hl
  ld   a, ($d17b)
  ld   ($d15d), a
  ld   a, ($d17c)
  ld   ($d15e), a
  ld   a, ($d17d)
  ld   ($d15f), a
  ld   hl, $0000
  ld   ($d172), hl
  ret



.include "src/level.asm"



LABEL_45E5:
  ld   de, $0008
  call LABEL_4053
  ld   de, $0008
  bit  3, (ix+$00)
  jr   nz, LABEL_45F7
  ld   de, $0000
LABEL_45F7:
  add  hl, de
  exx
  ld   de, $d19e
  exx
  ld   b, $08
LABEL_45FF:
  push bc
  push hl
  ld   e, (hl)
  ld   d, $00
  ex   de, hl
  add  hl, hl
  ld   bc, ($d18a)
  add  hl, bc
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   a, ($d19a)
  bit  2, (ix+$00)
  jr   z, +
  add  a, $08
+:  rrca
  rrca
  and  $06
  ld   l, a
  ld   h, $00
  add  hl, de
  ld   b, $04
  push hl
  exx
  pop  hl
  exx
-:  exx
  ld   a, (hl)
  ld   (de), a
  inc  hl
  inc  de
  ld   a, (hl)
  ld   (de), a
  inc  de
  ld   a, $07
  add  a, l
  ld   l, a
  exx
  djnz -
  pop  hl
  ld   de, ($d192)
  add  hl, de
  pop  bc
  djnz LABEL_45FF
  set  5, (ix+$00)
  ret


;---------- function start ----------
; (middle level)
_LABEL_4644_55:
  ld   hl, (RAM_D19C)
  ld   bc, $0008
  add  hl, bc
  srl  h
  rr   l
  srl  h
  rr   l
  srl  h
  rr   l
  add  hl, hl
  ld   bc, DATA_4765
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   b, (hl)
  ld   a, (RAM_D19A)
  bit  2, (ix+0)
  jr   z, _LABEL_466A_56
  add  a, $08
_LABEL_466A_56:
  rrca
  rrca
  and  $3E
  ld   l, a
  ld   h, $78
  add  hl, bc
  ld   bc, $0040
  ld   d, $7F
  ld   e, $07
  exx
  ld   a, (RAM_D19C)
  add  a, $08
  rrca
  rrca
  and  $06
  ld   l, a
  ld   h, $00
  ld   de, $D19E
  add  hl, de
  ld   b, $36
  ld   c, $BE
_LABEL_468E_58:
  exx
  ld   a, l
  out  ($BF), a
  ld   a, h
  out  ($BF), a
  add  hl, bc
  ld   a, h
  cp   d
  jp   c, _LABEL_469D_57
  sub  e
  ld   h, a
_LABEL_469D_57:
  exx
  outi
  outi
  jp   nz, _LABEL_468E_58
  res  5, (ix+0)
  ret


LABEL_46AA:
    ld   de, $0000
    call LABEL_4053
    ld   de, ($d194)
    bit  1, (ix+$00)
    jr   nz, +
    ld   de, $0000
+:  add  hl, de
    exx
    ld   de, $d29e
    exx
    ld   b, $09
-:  push bc
    push hl
    ld   e, (hl)
    ld   d, $00
    ex   de, hl
    add  hl, hl
    ld   bc, ($d18a)
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   a, (CameraLimit_Y)
    and  $18
    ld   l, a
    ld   h, $00
    add  hl, de
    push hl
    exx
    pop  hl
    ld   bc, $0008
    ldir
    exx
    pop  hl
    inc  hl
    pop  bc
    djnz -
    set  4, (ix+$00)
    ret


;---------- function start ----------
; (middle level)
_LABEL_46F0_49:
  ld   a, (RAM_D19A)
  bit  2, (ix+0)
  jr   z, _LABEL_46FB_50
  add  a, $08
_LABEL_46FB_50:
  rrca
  rrca
  and  $06
  ld   l, a
  ld   h, $00
  ld   de, $D29E
  add  hl, de
  ex   de, hl
  ld   hl, (RAM_D19C)
  srl  h
  rr   l
  srl  h
  rr   l
  srl  h
  rr   l
  add  hl, hl
  ld   bc, DATA_4765
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   b, (hl)
  ld   a, (RAM_D19A)
  bit  2, (ix+0)
  jr   z, _LABEL_4729_51
  add  a, $08
_LABEL_4729_51:
  rrca
  rrca
  and  $3E
  ld   l, a
  ld   h, $78
  add  hl, bc
  ld   b, $21
  bit  2, (ix+0)
  jr   z, _LABEL_473A_52
  dec  b
_LABEL_473A_52:
  ld   a, l
  out  ($BF), a
  ld   a, h
  out  ($BF), a
_LABEL_4740_54:
  ld   a, (de)
  out  ($BE), a
  inc  de
  inc  hl
  ld   a, (de)
  out  ($BE), a
  inc  de
  inc  hl
  ld   a, l
  and  $3F
  jp   nz, _LABEL_475E_53
  push de
  ld   de, $0040
  or   a
  sbc  hl, de
  ld   a, l
  out  ($BF), a
  ld   a, h
  out  ($BF), a
  pop  de
_LABEL_475E_53:
  djnz _LABEL_4740_54
  res  4, (ix+0)
  ret


DATA_4765:
.incbin "unknown/data_4765.bin"

LABEL_4B1D:
    ;clear the viewport scrolling flags
    ld   a, (LevelHeader)
    and  $F0
    ld   (LevelHeader), a

    bit  0, (ix+$01)
    jp   nz, LABEL_4C54

    call LABEL_4CA3
    call Engine_CameraAdjust
    ld   a, (Camera_adjX_centre)
    ld   b, a
    ld   a, ($d3c0)
    ld   c, a
    ld   hl, (Player + Object.X)
    ld   de, ($d3b2)
    xor  a
    sbc  hl, de
    jr   z, LABEL_4B88
    ld   a, l
    cp   b
    jr   c, LABEL_4B67
    ld   a, (Camera_adjX_right)
    ld   b, a
    ld   a, l
    cp   b
    jr   c, LABEL_4B88
    sub  b
    cp   c
    jr   c, LABEL_4B58
    dec  c
    ld   a, c
LABEL_4B58:
    ld   l, a
    ld   de, ($d19a)
    add  hl, de
    ld   ($d3b2), hl
    set  3, (ix+$00)
    jr   LABEL_4B88

LABEL_4B67:
    ld   a, c
    neg
    ld   c, a
    ld   a, (Camera_adjX_left)
    ld   b, a
    ld   a, l
    cp   b
    jr   nc, LABEL_4B88
    sub  b
    cp   c
    jr   nc, LABEL_4B79
    inc  c
    ld   a, c
LABEL_4B79:
    ld   l, a
    ld   h, $ff
    ld   de, ($d19a)
    add  hl, de
    ld   ($d3b2), hl
    set  2, (ix+$00)
LABEL_4B88:
    ld   a, (Camera_adjY_centre)
    ld   b, a
    ld   a, ($d3c0)
    ld   c, a
    ld   hl, (Player + Object.Y)
    ld   de, ($d3b4)
    xor  a
    sbc  hl, de
    ret  z
    ld   a, l
    cp   b
    jr   c, LABEL_4BBB
    ld   a, (Camera_adjY_bottom)
    ld   b, a
    ld   a, l
    cp   b
    ret  c
    sub  b
    cp   $08
    jr   c, +
    ld   a, $07
+:  ld   l, a
    ld   de, (CameraLimit_Y)
    add  hl, de
    ld   ($d3b4), hl
    set  1, (ix+$00)
    ret

LABEL_4BBB:
    ld   a, (Camera_adjY_top)
    ld   b, a
    ld   a, l
    cp   b
    ret  nc
    sub  b
    cp   $f8
    jr   nc, +
    ld   a, $f9
+:  ld   l, a
    ld   h, $ff
    ld   de, (CameraLimit_Y)
    add  hl, de
    ld   ($d3b4), hl
    set  0, (ix+$00)
    ret

; ==============================================================================
;  Engine_CameraAdjust
; ------------------------------------------------------------------------------
;  Adjust camera position to look up/dow and left/right.
; ------------------------------------------------------------------------------
Engine_CameraAdjust:    ;$4BD9
    ld   b, 120
    ld   a, ($d185)
    and  $03
    jr   nz, +
    ld   b, 96
    ld   a, (Player + Object.Flags04)
    bit  OBJ_F4_FACING_LEFT, a
    jr   z, +
    ld   b, 144
+:  ld   a, b
    ld   (Camera_adjX), a

    ; adjust camera position
    ld   a, (Camera_adjX_centre)
    ld   b, a
    ld   a, (Camera_adjX)
    cp   b
    jr   z, Engine_CameraAdjust_Vertical
    jr   c, +
    inc  b
    jr   ++
+:  dec  b
++: ld   a, b
    ld   (Camera_adjX_centre), a
    ;calculate the left edge of the camera
    sub  $08
    ld   (Camera_adjX_left), a
    ;calculate the right edge of the camera
    add  a, $10
    ld   (Camera_adjX_right), a

Engine_CameraAdjust_Vertical:   ;$4C0F
    ; move the camera pos up or down towards the adjustment
    ld   a, (Camera_adjY_centre)
    ld   b, a
    ;get the vertical camera adjustment
    ld   a, (Camera_adjY)
    cp   b
    ret  z
    jr   c, +
    inc  b
    jr   ++
+:  dec  b
++: ld   a, b
    ld   (Camera_adjY_centre), a
.ifeq SMS_VERSION 1
    sub  $10
.else
    sub  $04
.endif 
    ld   (Camera_adjY_top), a
.ifeq SMS_VERSION 1
    add  a, $20
.else
    add  a, $08
.endif
    ld   (Camera_adjY_bottom), a
    ret


; ==============================================================================
;  Engine_CalculateCameraBounds
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_CalculateCameraBounds:   ;$4C2D
    ld   a, (Camera_adjX)
    ld   (Camera_adjX_centre), a
    sub  $08
    ld   (Camera_adjX_left), a
    add  a, $10
    ld   (Camera_adjX_right), a

    ld   a, (Camera_adjY)
    ld   (Camera_adjY_centre), a
.ifeq SMS_VERSION 1
    sub  $08
.else
    sub  $04
.endif
    ld   (Camera_adjY_top), a
.ifeq SMS_VERSION 1
    add  a, $10
.else
    add  a, $08
.endif
    ld   (Camera_adjY_bottom), a

.ifeq SMS_VERSION 1
    ld   a, $10
.else
    ld   a, $08
.endif
    ld   ($D3C0), a
    ret

LABEL_4C53:
    ret

LABEL_4C54:
    ld   hl, ($d3f6)
    ld   de, ($d19a)
    xor  a
    sbc  hl, de
    jr   z, LABEL_4C82
    jr   c, LABEL_4C73
    inc  de
    ld   ($d3b2), de
    set  3, (ix+$00)
    ld   hl, ($d3f6)
    ld   ($d3a8), hl
    jr   LABEL_4C82

LABEL_4C73:
    dec  de
    ld   ($d3b2), de
    set  2, (ix+$00)
    ld   hl, ($d3f6)
    ld   ($d3a6), hl
LABEL_4C82:
    ld   hl, ($d3f8)
    ld   de, (CameraLimit_Y)
    xor  a
    sbc  hl, de
    ret  z
    jr   c, LABEL_4C99
    inc  de
    ld   ($d3b4), de
    set  1, (ix+$00)
    ret

LABEL_4C99:
    dec  de
    ld   ($d3b4), de
    set  0, (ix+$00)
    ret

LABEL_4CA3:
    bit  2, (ix+$01)
    jr   z, LABEL_4CBB
    ld   hl, ($d3bf)
    ld   de, $0008
    add  hl, de
    ld   ($d3bf), hl
    ld   a, h
    cp   $08
    ret  c
    res  2, (ix+$01)
LABEL_4CBB:
  ld   h, $08
    ld   l, $00
    ld   ($d3bf), hl
    ret

LABEL_4CC3:
    ld   hl, LevelHeader
    set  7, (hl)
    ld   hl, $d185
    res  0, (hl)
    ret

LABEL_4CCE:
    ld   hl, LevelHeader
    res  7, (hl)
    ld   hl, ($d19a)
    ld   ($d3a6), hl
    ret

LABEL_4CDA:
    ld   hl, LevelHeader
    set  7, (hl)
    ld   hl, $d185
    set  0, (hl)
    set  2, (hl)
    ld   ($d3f6), bc
    ld   ($d3f8), de
    call LABEL_4D13
    ret

LABEL_4CF2:
    ld   hl, LevelHeader
    set  7, (hl)
    ld   hl, $d185
    res  0, (hl)
    call LABEL_4D13
    ret
  
LABEL_4D00:
    call LABEL_4D13
    ld   hl, ($d3a6)
    ld   de, ($d19a)
    xor  a
    sbc  hl, de
    ret  nc
    ld   ($d3a6), de
    ret

LABEL_4D13:
    ld   hl, $d185
    set  2, (hl)
    ld   h, $02
    ld   l, $00
    ld   ($d3bf), hl
    ret

LABEL_4D20:
    call LABEL_4D13
    ld   hl, ($d3a8)
    ld   de, ($d19a)
    xor  a
    sbc  hl, de
    ret  c
    ld   ($d3a8), de
    ret


.include "src/level_width_multiples.asm"


LABEL_4F3F:
    ld   a, ($d3fc)
    or   a
    ret  nz
    xor  a
    ld   ($d521), a
    ld   hl, $d142
    inc  (hl)
    ld   ix, $d580
    ld   b, $16
    ld   a, ($d12f)
    or   a
    jp   nz, LABEL_4F75
    ld   a, ($d3eb)
    or   a
    jp   nz, LABEL_4F68
    ld   a, (GameMode)
    cp   $11
    jp   z, LABEL_4F82
LABEL_4F68:
-:  push bc
    call LABEL_4FC0
    ld   de, $0040
    add  ix, de
    pop  bc
    djnz -
    ret

LABEL_4F75:
    push bc
    call Engine_UpdateObject
    ld   de, $0040
    add  ix, de
    pop  bc
    djnz LABEL_4F75
    ret

LABEL_4F82:
    push bc
    ld   a, (ix+$00)
    cp   $19
    jr   z, LABEL_4F90
    ld   a, ($dbc9)
    or   a
    jr   nz, LABEL_4F93
LABEL_4F90:
    call LABEL_4FC0
LABEL_4F93:
    ld   de, $0040
    add  ix, de
    pop  bc
    djnz LABEL_4F68
    ret

LABEL_4F9C:
    xor  a
    ld   ($d521), a
    ld   ix, $d540
    ld   b, $0b
    ld   a, ($d12e)
    rrca
    jp   c, LABEL_4FB3
    ld   ix, $d580
    ld   b, $0b
LABEL_4FB3:
  push bc
    call LABEL_4FC0
    ld   de, $0080
    add  ix, de
    pop  bc
    djnz LABEL_4FB3
    ret

LABEL_4FC0:
    ; check the object ID
    ld   a, (ix + Object.ObjID)
    or   a
    ret  z
    ; values >= 0xF0 require special handling
    cp   $f0
    jp   nc, LABEL_509C
    call Engine_SelectObjectBank
    call Engine_ProcessObjectLogic
    ; if the object ID was changed check to see if we need to do any
    ; special handling
    ld   a, (ix + Object.ObjID)
    cp   $f0
    jp   nc, LABEL_509C
    call Engine_SelectObjectBank
    call Engine_UpdateObject_CallLogicPtr
    ld   a, (ix + Object.State)
    or   a
    ret  z
    jp   LABEL_5456

Engine_UpdateObject:        ; $4FE6
    ; check to see if the slot is occupied
    ld   a, (ix + Object.ObjID)
    or   a
    ret  z
    
    ; branch of object id >= $F0
    cp   $f0
    jp   nc, LABEL_509C
    
    ld   a, $ff
    ld   ($d133), a
    call Engine_SelectObjectBank
    call Engine_ProcessObjectLogic

    xor  a
    ld   ($d133), a
    ld   a, (ix + Object.ObjID)
    cp   $f0
    jp   nc, LABEL_509C

    ld   a, $ff
    ld   ($d133), a
    call Engine_SelectObjectBank
    call Engine_UpdateObject_CallLogicPtr
    xor  a
    ld   ($d133), a

    ld   a, (ix + Object.State)
    or   a
    ret  z
    jp   LABEL_5456


; =============================================================================
;  Engine_SelectObjectBank(uint16 obj_ptr)
; -----------------------------------------------------------------------------
;  Selects the memory bank containing the object's code into frame 2.
;  
;  Params:
;    in    IX  Object pointer.
; -----------------------------------------------------------------------------
Engine_SelectObjectBank:        ; $501E
    ld   a, (ix + Object.ObjID)
    ld   hl, _Engine_SelectObjectBank_BankData
    ld   d, $00
    ld   e, a
    add  hl, de
    ld   a, (hl)
    jp   Engine_SwapFrame2

_Engine_SelectObjectBank_BankData:
.db :Logic_Sonic    ; bank 12
.db :Logic_Tails    ; bank 12
.db $0C
.db $1E             ; $04
.db $1E             ; $05
.db $1E             ; $06
.db $1E             ; $07
.db $1E             ; $08
.db $1E             ; $09
.db $1E             ; $0A
.db $1E             ; $0B
.db $1E             ; $0C
.db $1E             ; $0D
.db $1E             ; $0E
.db $1E             ; $0F
.db $1E             ; $10
.db $1E             ; $11
.db $1E             ; $12
.db $1E             ; $13
.db $1E             ; $14 Title card numbers
.db $1E             ; $15
.db $1E             ; $16
.db $1E             ; $17
.db $1E             ; $18
.db $1E             ; $19
.db $1E             ; $1A
.db $1E             ; $1B
.db $1E             ; $1C
.db $1E             ; $1D
.db $1E             ; $1E
.db $1E             ; $1F
.db $1E             ; $20
.db $1E             ; $21
.db $17             ; $22
.db $1E             ; $23
.db $1E             ; $24
.db $1E             ; $25
.db $1E             ; $26
.db $1E             ; $27
.db $1E             ; $28
.db $1E             ; $29
.db $10             ; $2A
.db $1E             ; $2B
.db $1E             ; $2C
.db $1E             ; $2D
.db $1E             ; $2E
.db $1E             ; $2F
.db $1E             ; $30   Intro sequence emeralds
.db $1E             ; $31
.db $1E             ; $32
.db $1E             ; $33
.db $1E             ; $34
.db $1E             ; $35   Intro sequence Knuckles
.db $1E             ; $36
.db $1E             ; $37
.db $1E             ; $38
.db $1E             ; $39
.db $1E             ; $3A
.db $1E             ; $3B
.db $1E             ; $3C
.db $12             ; $3D
.db $1E             ; $3E
.db $1E             ; $3F
.db $1E             ; $40
.db $1E             ; $41
.db $17             ; $42
.db $17             ; $43
.db $17             ; $44
.db $17             ; $45
.db $17             ; $46
.db $17             ; $47
.db $17             ; $48
.db $17             ; $49
.db $17             ; $4A
.db $17             ; $4B
.db $17             ; $4C
.db $17             ; $4D
.db $17             ; $4E
.db $17             ; $4F
.db $17             ; $50
.db $17             ; $51
.db $17             ; $52
.db $17             ; $53
.db $17             ; $54
.db $17             ; $55
.db $17             ; $56
.db $17             ; $57
.db $17             ; $58
.db $17             ; $59
.db $17             ; $5A
.db $17             ; $5B
.db $17             ; $5C
.db $17             ; $5D
.db $17             ; $5E
.db $1C             ; $5F
.db $1C             ; $60
.db $1C             ; $61
.db $1C             ; $62
.db $1C             ; $63
.db $1C             ; $64
.db $1C             ; $65
.db $1C             ; $66
.db $0F             ; $67
.db $0F             ; $68
.db $1A             ; $69
.db $1A             ; $6A
.db $1A             ; $6B
.db $1A             ; $6C
.db $1A             ; $6D
.db $1A             ; $6E
.db $1A             ; $6F
.db $1A             ; $70



LABEL_509C:
    and  $0f
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, DATA_50AB
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    jp   (hl)

DATA_50AB:
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_50CB
.dw LABEL_54F1
.dw LABEL_515D


LABEL_50CB:
    ret

LABEL_50CC:
    ld   (ix+$00), $fe
    ret

LABEL_50D1:
    ld   (ix+$00), $ff
    ret

; ==============================================================================
;  Engine_UpdateObject_CallLogicPtr
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_UpdateObject_CallLogicPtr:           ; $50D6
    ld   l, (ix + Object.LogicPtr)
    ld   h, (ix + Object.LogicPtr+1)
    ld   a, h
    or   l
    ret  z
    jp   (hl)

LABEL_50E0:
    ret

; ==============================================================================
;  Engine_AllocateLinkedObject
; ------------------------------------------------------------------------------
;  Finds an open high priotity object slot and inits an object. The slot index
;  of the parent object is stored in $31
; ------------------------------------------------------------------------------
;  In:
;   C     - Object ID
;   H     - Copied into object $3F
;   IX    - Pointer to parent object.
;  Out:
;   A     - 00 on success, FF on failure.
;   IY    - Pointer to new object.
; ------------------------------------------------------------------------------
Engine_AllocateLinkedObject:              ; $50E1
    ld   iy, ObjectSlot_02
    ; look for an empty slot
    ld   b, 16
    ld   de, $0040
-:  ld   a, (iy+$00)
    or   a
    jr   z, +
    add  iy, de
    djnz -
    ; no empty slots found
    ld   a, $FF
    ret
  
+:  ld   (iy + Object.ObjID), c
    ld   (iy + Object.ix3F), h
    push hl
    push ix
    pop  hl
    call Engine_GetObjectSlotNumber
    ld   (ix + Object.ix31), a
    pop  hl
    xor  a
    ret

; ==============================================================================
;  Engine_AllocateLinkedObjectXY
; ------------------------------------------------------------------------------
;  Allocates a child object and sets its X/Y coordinates.
; ------------------------------------------------------------------------------
;  In:
;   B       - Object ID
;   C       - $31 flags
;   DE      - X-coord
;   HL      - Y-coord
;   IX      - Pointer to parent object
;
;  Out:
;   A       - Set to 00 on success, FF on failure.
;   IY      - Pointer to new object.
; ------------------------------------------------------------------------------
Engine_AllocateLinkedObjectXY:                  ; $510A
    exx
    ld   iy, $d580
    ld   b, $10
    ld   de, $0040
-:  ld   a, (iy+$00)
    or   a
    jr   z, +
    add  iy, de
    djnz -
    exx
    ld   a, $ff
    ret

+:  exx
    ld   (iy+$00), b
    ld   (iy+$3f), c
    ld   (iy+$12), d
    ld   (iy+$11), e
    ld   (iy+$15), h
    ld   (iy+$14), l
    push hl
    push ix
    pop  hl
    call Engine_GetObjectSlotNumber
    ld   (ix+$31), a
    pop  hl
    xor  a
    ret


; ==============================================================================
;  Engine_AllocateObjectLowPriority
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
;  In:
;   None
;  Out:
;   IY        - Pointer to object slot
;   <carry>   - Carry flag set on failure. clear on success.
;  Clobbers:
;   A, B, DE
; ------------------------------------------------------------------------------
Engine_AllocateObjectLowPriority:       ; $5142
    ld   iy, ObjectSlotsBadniks
    ld   de, $0040
    ld   b, 16
-:  ld   a, (iy + Object.ObjID)
    or   a
    jr   z, +
    add  iy, de
    djnz -
    ld   iy, $0000
    scf
    ret

+:  xor  a
    ret



LABEL_515D:
    ld   a, (ix+$3e)
    cp   $ff
    jr   z, +
    or   a
    jr   z, +
    ld   a, (ix+$3e)
    or   a
    jp   z, +
    dec  a
    ld   e, a
    ld   d, $00
    ld   hl, $d43f
    add  hl, de
    ld   (hl), $00
+:  push ix
    pop  hl
    ld   (hl), $00
    ld   e, l
    ld   d, h
    inc  de
    ld   bc, $003f
    ldir
    ret

LABEL_5186:
    xor  a
    ld   ($d521), a
    call LABEL_3DB3
    ld   hl, $d524
    res  2, (hl)
    ld   a, ($d501)
    cp   $23
    ret  z
    jp   LABEL_5854

; ==============================================================================
;  Engine_MoveAboveBelowPlayer
; ------------------------------------------------------------------------------
;  Moves an object to the player's X/Y coordinate with an adjustment on the
;  Y axis.
; ------------------------------------------------------------------------------
; In:
;   HL      - Vertical offset value
;   IX      - Pointer to object
; Out:
;   None
; Clobbers:
;   BC, DE, HL
; ------------------------------------------------------------------------------
Engine_MoveAboveBelowPlayer:                    ; $519B
    ld   bc, (Player + Object.X)
    ld   de, (Player + Object.Y)
    add  hl, de
    ld   (ix + Object.X), c
    ld   (ix + Object.X+1), b
    ld   (ix + Object.Y), l
    ld   (ix + Object.Y+1), h
    ret


LABEL_51B1:
    ld   a, $b9
    ld   (Sound_SFX_Trigger), a
    jr   LABEL_51D0


LABEL_51B8:
    ld   a, (ix+$21)
    and  $0f
    ret  z
    ld   a, ($d532)
    cp   $05
    jr   z, LABEL_51CB
    ld   a, ($d503)
    bit  1, a
    ret  z
LABEL_51CB:
    ld   a, $ce
    ld   (Sound_SFX_Trigger), a
LABEL_51D0:
    call LABEL_51F2
    ld   (ix+$00), $07
    xor  a
    ld   (ix+$01), a
    ld   (ix+$02), a
    ld   (ix+$04), a
    ld   (ix+$07), a
    ld   (ix+$0e), a
    ld   (ix+$0f), a
    ld   (ix+$1e), a
    res  7, (ix+$3f)
    ret

LABEL_51F2:
    ld   a, (ix+$00)
    cp   $50
    ret  nc
    ret  z
    ld   hl, DATA_249B
    jp   LABEL_2430

LABEL_51FF:
    call LABEL_58AC
    jp   LABEL_5205   ;FIXME: pointless jump

LABEL_5205:
    ld   a, (ix+$21)
    and  $0f
    ret  z
    add  a, a
    ld   e, a
    ld   d, $00
    ld   hl, DATA_5218
    add  hl, de
    ld   a, (hl)
    inc  hl
    ld   h, (hl)
    ld   l, a
    jp   (hl)

DATA_5218:
.dw LABEL_5238
.dw LABEL_5250
.dw LABEL_5239
.dw LABEL_5238
.dw LABEL_5297
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5268
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238
.dw LABEL_5238

LABEL_5238:
    ret

LABEL_5239:
    ld   a, ($d523)
    bit  1, a
    ret  nz
    ld   a, ($d529)
    ld   e, a
    ld   d, $00
    ld   l, (ix+$14)
    ld   h, (ix+$15)
    add  hl, de
    ld   ($d514), hl
    ret

LABEL_5250:
    ld   a, ($d523)
    bit  0, a
    ret  nz
    ld   e, (ix+$29)
    ld   d, $00
    ld   l, (ix+$14)
    ld   h, (ix+$15)
    xor  a
    sbc  hl, de
    ld   ($d514), hl
    ret

LABEL_5268:
    ld   a, ($d523)
    bit  3, a
    ret  nz
    ld   hl, ($d19a)
    ld   de, $0020
    add  hl, de
    ld   de, ($d511)
    xor  a
    sbc  hl, de
    ret  nc
    ld   e, (ix+$28)
    ld   d, $00
    ld   a, ($d528)
    ld   l, a
    ld   h, $00
    add  hl, de
    ex   de, hl
    ld   l, (ix+$11)
    ld   h, (ix+$12)
    xor  a
    sbc  hl, de
    ld   ($d511), hl
    ret

LABEL_5297:
    ld   a, ($d523)
    bit  2, a
    ret  nz
    ld   hl, ($d19a)
    ld   de, $00e0
    add  hl, de
    ld   de, ($d511)
    xor  a
    sbc  hl, de
    ret  c
    ld   e, (ix+$28)
    ld   d, $00
    ld   a, ($d528)
    ld   l, a
    ld   h, $00
    add  hl, de
    ex   de, hl
    ld   l, (ix+$11)
    ld   h, (ix+$12)
    add  hl, de
    ld   ($d511), hl
    ret

ObjectLogic_DoNothing:      ; $52C4
    ret

LABEL_52C5:
    xor  a
    ld   (ix+$1f), a
    ret


; ==============================================================================
;  Engine_GetObjectSlotNumber(object *ptr)
; ------------------------------------------------------------------------------
;  Calculate the slot index from its address.
; ------------------------------------------------------------------------------
;  In:
;   HL      - Slot address
;  Out:
;   A       - slot number
;  Clobbers:
;   HL
; ------------------------------------------------------------------------------
Engine_GetObjectSlotNumber:                     ; $52CA
    ld   de, ObjectSlots
    xor  a
    sbc  hl, de
    ld   a, h
    sla  l
    rla
    sla  l
    rla
    inc  a
    ret

LABEL_52D9:
    dec  a
    ld   h, a
    xor  a
    srl  h
    rra
    srl  h
    rra
    ld   l, a
    ld   de, $d500
    add  hl, de
    ret

LABEL_52E8:
    xor  a
    ld   (ix+$16), a
    ld   (ix+$17), a
    ld   (ix+$18), a
    ld   (ix+$19), a
    ld   a, (ix+$0b)
    or   a
    ret  z
    ld   d, $00
    ld   e, (ix+$0a)
    ld   hl, $0200
    add  hl, de
    ld   a, (hl)
    ld   e, a
    and  $80
    rlca
    neg
    ld   d, a
    ld   hl, $0000
    ld   b, (ix+$0b)
-:  add  hl, de
    djnz -
    ld   a, l
    sra  h
    rra
    sra  h
    rra
    sra  h
    rra
    sra  h
    rra
    ld   (ix+$16), a
    ld   (ix+$17), h
    ld   a, (ix+$0a)
    add  a, $c0
    ld   e, a
    ld   d, $00
    ld   hl, $0200
    add  hl, de
    ld   a, (hl)
    ld   e, a
    and  $80
    rlca
    neg
    ld   d, a
    ld   hl, $0000
    ld   b, (ix+$0b)
-:  add  hl, de
    djnz -
    ld   a, l
    sra  h
    rra
    sra  h
    rra
    sra  h
    rra
    sra  h
    rra
    ld   (ix+$18), a
    ld   (ix+$19), h
    ret

LABEL_5358:
    ret

LABEL_5359:
    ld   l, (ix+$10)
    ld   h, (ix+$11)
    ld   e, (ix+$16)
    ld   d, (ix+$17)
    ld   b, (ix+$12)
    ld   a, d
    and  $80
    rlca
    dec  a
    cpl
    add  hl, de
    adc  a, b
    ld   (ix+$12), a
    ld   (ix+$10), l
    ld   (ix+$11), h
LABEL_5379:
    ld   l, (ix+$13)
    ld   h, (ix+$14)
    ld   e, (ix+$18)
    ld   d, (ix+$19)
    ld   b, (ix+$15)
    ld   a, d
    and  $80
    rlca
    dec  a
    cpl
    add  hl, de
    adc  a, b
    ld   (ix+$15), a
    ld   (ix+$13), l
    ld   (ix+$14), h
    ret

LABEL_539A:
    ld   l, (ix+$10)
    ld   h, (ix+$11)
    ld   e, (ix+$16)
    ld   d, (ix+$17)
    ld   b, (ix+$12)
    ld   a, d
    and  $80
    rlca
    dec  a
    cpl
    add  hl, de
    adc  a, b
    ld   (ix+$12), a
    ld   (ix+$10), l
    ld   (ix+$11), h
    ret

LABEL_53BB:
    ld   a, ($d500)
    cp   $01
    ret  z
    ld   a, ($d502)
    cp   $18
    ret  nz
    ld   a, $0e
    ld   ($d502), a
    ret

LABEL_53CD:
    ld   de, $0000
    ld   bc, $0000
    call LABEL_709A
    ld   a, ($d411)
    bit  6, a
    jr   nz, LABEL_53E4
    bit  7, a
    jr   nz, LABEL_53E4
    ld   a, $ff
    ret

LABEL_53E4:
    ld   a, $00
    ret

LABEL_53E7:
    ld   de, $fff0
    ld   bc, $0000
    call LABEL_709A
    ld   a, ($d411)
    bit  7, a
    jr   nz, LABEL_53FA
    ld   a, $ff
    ret

LABEL_53FA:
    ld   a, $00
    ret

LABEL_53FD:
    ld   h, (ix + Object.X + 1)
    ld   l, (ix + Object.X)
    ld   de, ($d511)
    ld   bc, $000c
    call LABEL_543A
    or   a
    ret  z
    ld   h, (ix + Object.Y + 1)
    ld   l, (ix + Object.Y)
    ld   de, ($d514)
    ld   bc, $000c
    call LABEL_543A
    or   a
    ret  z
    ld   a, $ff
    ret

LABEL_5424:
    ld   h, (ix+$12)
    ld   l, (ix+$11)
    ld   de, ($d511)
    jr   LABEL_543A

LABEL_5430:
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    ld   de, ($d514)
LABEL_543A:
    xor  a
    sbc  hl, de
    ld   d, $00
    bit  7, h
    jr   z, +
  
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
    ld   d, $ff

+:  xor  a
    sbc  hl, bc
    ld   a, $00
    ret  nc
    ld   a, $ff
    ret

LABEL_5455:
    ret

LABEL_5456:
    ld   a, (GameMode)
    cp   $0d
    ret  z
    res  6, (ix+$04)
    ld   l, (ix+$11)
    ld   h, (ix+$12)
    ld   bc, $0080
    add  hl, bc
    ld   bc, ($d19a)
    xor  a
    sbc  hl, bc
    jp   c, LABEL_54C7
    srl  h
    rr   l
    ld   a, h
    or   a
    jp   nz, LABEL_54C7
    ld   e, l
    ld   l, (ix+$14)
    ld   h, (ix+$15)
    ld   bc, $0080
    add  hl, bc
    ld   bc, (CameraLimit_Y)
    xor  a
    sbc  hl, bc
    jp   c, LABEL_54C7
    srl  h
    rr   l
    ld   a, h
    or   a
    jp   nz, LABEL_54C7
    ld   a, e
    rrca
    rrca
    rrca
    and  $1f
    ld   e, a
    ld   a, l
    and  $f8
    ld   l, a
    ld   h, $00
    ld   d, $00
    add  hl, hl
    add  hl, hl
    add  hl, de
    ld   de, $8156
    add  hl, de
    ld   a, $1c
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, (hl)
    cp   $03
    jp   z, LABEL_54C7
    and  $02
    ret  z
    set  6, (ix+$04)
    ret

LABEL_54C7:
    set  6, (ix+$04)
    bit  1, (ix+$04)
    ret  nz
    ld   a, (ix+$00)
    cp   $08
    ret  z
    cp   $13
    ret  z
    ld   a, (ix+$3e)
    or   a
    jr   z, LABEL_54E8
    ld   (ix+$00), $fe
    ld   (ix+$01), $00
    ret

LABEL_54E8:
    ld   (ix+$00), $ff
    ld   (ix+$01), $00
    ret

LABEL_54F1:
    ld   (ix+$00), $ff
    ld   (ix+$01), $00
    ret

LABEL_54FA:
    ld   h, (ix+$17)
    ld   l, (ix+$16)
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
    ld   (ix+$17), h
    ld   (ix+$16), l
    ret

LABEL_550E:
    call LABEL_58AC
    ld   a, (ix+$21)
    and  $0f
    ret  z
    ld   a, $ff
    ld   ($d3c1), a
    ret

LABEL_551D:
    ld   h, (ix+$19)
    ld   l, (ix+$18)
    add  hl, de
    ld   (ix+$19), h
    ld   (ix+$18), l
    ret

LABEL_552B:
    ret

LABEL_552C:
    xor  a
    ld   (ix+$12), a
    ld   (ix+$11), a
    ld   (ix+$15), a
    ld   (ix+$14), a
    ld   (ix+$38), a
    ld   h, (ix+$35)
    ld   l, (ix+$34)
    ld   de, ($dc47)
    xor  a
    sbc  hl, de
    jp   c, LABEL_55D6
    ld   de, $0080
    xor  a
    ld   a, l
    sbc  hl, de
    ret  nc
    ld   (ix+$38), a
    sub  $10
    ret  c
    ret  z
    srl  a
    srl  a
    or   a
    jr   z, LABEL_556E
    ld   b, a
    ld   hl, $0000
    ld   de, $0020
-:  add  hl, de
    djnz -
    jr   LABEL_5571

LABEL_556E:
    ld   hl, $0000
LABEL_5571:
    ld   a, ($d12b)
    push af
    ld   a, $18
    call Engine_SwapFrame2
    ld   de, $b4d8
    add  hl, de
    ld   ($d11c), hl
    ld   b, $00
    ld   a, (ix+$36)
    bit  7, a
    jr   z, LABEL_558E
    neg
    ld   b, $ff
LABEL_558E:
    srl  a
    srl  a
    ld   d, $00
    ld   e, a
    add  hl, de
    ld   a, (hl)
    bit  0, b
    jr   z, LABEL_559D
    neg
LABEL_559D:
    add  a, $80
    ld   (ix+$11), a
    ld   (ix+$12), $00
    ld   b, $00
    ld   hl, ($d11c)
    ld   a, (ix+$37)
    bit  7, a
    jr   z, LABEL_55B6
    neg
    ld   b, $ff
LABEL_55B6:
    srl  a
    srl  a
    ld   d, $00
    ld   e, a
    add  hl, de
    ld   a, (hl)
    bit  0, b
    jr   z, +
    neg
+:  add  a, $60
    ld   (ix+$14), a
    ld   (ix+$15), $00
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_55D6:
    ld   (ix+$00), $ff
    ret

LABEL_55DB:
    xor  a
    ld   (ix+$21), a
    ld   a, (ix+$38)
    or   a
    ret  z
    cp   $22
    ret  nc
    ld   a, ($dc4e)
    sub  (ix+$14)
    jr   nc, LABEL_55F1
    neg
LABEL_55F1:
    sub  $10
    ret  nc
    ld   a, ($dc4d)
    sub  (ix+$11)
    jr   nc, LABEL_55FE
    neg
LABEL_55FE:
    sub  $10
    ret  nc
    ld   a, $ff
    ld   (ix+$21), a
    ret

LABEL_5607:
    ld   hl, ($d3e7)
    ld   a, h
    or   l
    ret  z
    add  hl, bc
    ld   d, (ix+$15)
    ld   e, (ix+$14)
    xor  a
    sbc  hl, de
    ret  nc
    ld   a, $01
    ret

LABEL_561B:
    ld   a, ($d503)
    bit  1, a
    ret  z
    ld   a, (ix+$21)
    ld   c, a
    and  $0f
    ret  z
    cp   $02
    jr   nz, LABEL_5632
    ld   a, ($d522)
    bit  1, a
    ret  nz
LABEL_5632:
    ld   hl, $0000
    ld   de, $0000
    bit  0, c
    jr   z, LABEL_5641
    ld   de, $fb00
    jr   LABEL_5648

LABEL_5641:
    bit  1, c
    jr   z, LABEL_5648
    ld   de, $0500
LABEL_5648:
    bit  3, c
    jr   z, LABEL_5651
    ld   hl, $fb00
    jr   LABEL_5658

LABEL_5651:
    bit  2, c
    jr   z, LABEL_5658
    ld   hl, $0500
LABEL_5658:
    ld   ($d516), hl
    ld   ($d518), de
    ld   a, $c1
    ld   (Sound_SFX_Trigger), a
    xor  a
    ld   ($d521), a
    ret

LABEL_5669:
    ld   a, (ix+$24)
    bit  6, a
    jr   z, LABEL_5674


LABEL_5670:
    bit  2, a
    jr   z, LABEL_5677


LABEL_5674:
    call LABEL_567F
LABEL_5677:
    ld   a, (ix+$1f)
    or   a
    ret  nz
    jp   LABEL_572E

LABEL_567F:
    ld   b, (ix+$24)
    bit  5, b
    jp   nz, LABEL_56CA
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    bit  1, b
    jr   nz, LABEL_5694
    inc  hl
    jr   LABEL_5695

LABEL_5694:
    dec  hl
LABEL_5695:
    ld   (ix+$15), h
    ld   (ix+$14), l
    bit  4, b
    jr   z, LABEL_570C
    bit  1, b
    jr   nz, LABEL_56AB
    ld   bc, $0000
    ld   de, $0010
    jr   LABEL_56B1

LABEL_56AB:
    ld   bc, $0000
    ld   de, $fff0
LABEL_56B1:
    call LABEL_709A
    ld   a, ($d411)
    bit  7, a
    ret  z
    bit  1, (ix+$24)
    jr   nz, LABEL_56C5
    set  1, (ix+$24)
    ret

LABEL_56C5:
    res  1, (ix+$24)
    ret

LABEL_56CA:
    ld   h, (ix+$12)
    ld   l, (ix+$11)
    bit  1, b
    jr   nz, LABEL_56D7
    inc  hl
    jr   LABEL_56D8

LABEL_56D7:
    dec  hl
LABEL_56D8:
    ld   (ix+$12), h
    ld   (ix+$11), l
    bit  4, b
    jr   z, LABEL_570C
    bit  1, b
    jr   nz, LABEL_56EE
    ld   bc, $0010
    ld   de, $0000
    jr   LABEL_56F4

LABEL_56EE:
    ld   bc, $fff0
    ld   de, $0000
LABEL_56F4:
    call LABEL_709A
    ld   a, ($d411)
    bit  7, a
    ret  z
    bit  1, (ix+$24)
    jr   nz, LABEL_5708
    set  1, (ix+$24)
    ret

LABEL_5708:
    res  1, (ix+$24)
LABEL_570C:
    bit  1, b
    jr   nz, LABEL_571F
    ld   d, (ix+$39)
    ld   e, (ix+$38)
    xor  a
    sbc  hl, de
    ret  c
    set  1, (ix+$24)
    ret

LABEL_571F:
    ld   d, (ix+$37)
    ld   e, (ix+$36)
    xor  a
    sbc  hl, de
    ret  nc
    res  1, (ix+$24)
    ret

LABEL_572E:
    ld   (ix+$21), $00
    bit  6, (ix+$04)
    jp   nz, LABEL_5797
    ld   a, ($d501)
    cp   $21
    ret  z
    ld   a, ($d502)
    cp   $21
    ret  z
    ld   a, ($d519)
    bit  7, a
    jp   nz, LABEL_5797
    ld   a, ($d503)
    bit  6, a
    jp   nz, LABEL_5797
    ld   a, ($d3c4)
    or   a
    jr   z, LABEL_5761
    cp   (ix+$32)
    jp   nz, LABEL_5835
LABEL_5761:
    bit  7, (ix+$19)
    jr   nz, LABEL_5770
    ld   a, ($d519)
    rlca
    jp   c, LABEL_5797
    jr   LABEL_5783

LABEL_5770:
    ld   a, ($d519)
    bit  7, a
    jr   nz, LABEL_5783
    ld   hl, ($d518)
    ld   de, $ff00
    xor  a
    sbc  hl, de
    jp   nc, LABEL_5797
LABEL_5783:
    ld   b, $00
    ld   c, (ix+$30)
    call LABEL_5430
    or   a
    jr   z, LABEL_5797
    ld   bc, $0010
    call LABEL_5424
    or   a
    jr   nz, LABEL_57BC
LABEL_5797:
    ld   a, ($d3c4)
    cp   (ix+$32)
    ret  nz
    ld   a, ($d519)
    bit  7, a
    jr   nz, LABEL_57AB
    ld   hl, $0400
    ld   ($d518), hl
LABEL_57AB:
    xor  a
    ld   ($d3c4), a
    ld   hl, $d521
    res  5, (hl)
    ld   hl, $d523
    res  1, (hl)
    jp   LABEL_5835

LABEL_57BC:
    ld   (ix+$21), $ff
    ld   a, ($d3c4)
    cp   (ix+$32)
    jr   z, LABEL_57CE
    ld   a, (ix+$32)
    ld   ($d3c4), a
LABEL_57CE:
    ld   a, $25
    ld   ($d520), a
    set  2, (ix+$24)
    ld   hl, $d521
    set  5, (hl)
    ld   hl, $0000
    ld   ($d518), hl
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    ld   d, $00
    ld   e, (ix+$30)
    dec  e
    dec  e
    xor  a
    sbc  hl, de
    ld   ($d514), hl
    ld   a, (ix+$24)
    bit  5, a
    jp   z, LABEL_580E
    ld   hl, ($d511)
    bit  1, a
    jr   nz, LABEL_5807
    inc  hl
    jr   LABEL_5808

LABEL_5807:
    dec  hl
LABEL_5808:
    ld   ($d511), hl
    jp   LABEL_580E   ;FIXME: pointless jump

LABEL_580E:
    ld   b, (ix+$24)
    bit  7, b
    ret  z
    bit  0, b
    jr   nz, LABEL_5839
    ld   a, (ix+$34)
    cp   $08
    jr   z, LABEL_5830
    inc  (ix+$34)
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    inc  hl
    ld   (ix+$15), h
    ld   (ix+$14), l
    ret

LABEL_5830:
    set  0, (ix+$24)
    ret

LABEL_5835:
    res  0, (ix+$24)
LABEL_5839:
    bit  7, (ix+$24)
    ret  z
    ld   a, (ix+$34)
    or   a
    ret  z
    dec  (ix+$34)
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    dec  hl
    ld   (ix+$15), h
    ld   (ix+$14), l
    ret

LABEL_5854:
    ld   a, ($d501)
    cp   $23
    jr   z, LABEL_5860
    cp   $26
    jr   z, LABEL_587F
    ret

LABEL_5860:
    ld   hl, $0001
    ld   ($dbe4), hl
    ld   a, (CurrentLevel)
    cp   $03
    jp   nz, LABEL_3B6F
    ld   b, $34
    ld   c, $00
    ld   de, ($d511)
    ld   hl, ($d514)
    call Engine_AllocateLinkedObjectXY
    jp   LABEL_3B6F

LABEL_587F:
    ld   hl, $0001
    ld   ($dbe4), hl
    jp   LABEL_3B47

LABEL_5888:
    ld   a, (CurrentLevel)
    add  a, a
    ld   d, $00
    ld   e, a
    ld   hl, DATA_589B
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   ($dbdc), de
    ret

DATA_589B:
.dw $0A41
.dw $07C7
.dw $045F
.dw $0C44 
.dw $084F
.dw $0F55 


LABEL_58A7:
    ld   c, $0c
    jp   LABEL_259E

LABEL_58AC:
    ld   a, $30
    ld   ($dbd0), a
    xor  a
    ld   (ix+$20), a
    ld   a, (ix+$21)
    and  $f0
    ld   (ix+$21), a
    ld   a, ($d503)
    bit  6, a
    ret  nz
    bit  6, (ix+$04)
    ret  nz
    bit  6, (ix+$03)
    ret  nz
    ld   hl, ($d511)
    ld   e, (ix+$11)
    ld   d, (ix+$12)
    xor  a
    sbc  hl, de
    jr   c, LABEL_58F1
    ld   a, h
    or   a
    jp   nz, LABEL_597D
    ld   a, ($D528)
    add  a, (ix+$28)
    sub  l
    jp   c, LABEL_597D
    ld   c, a
    set  2, (ix+$21)
    jr   LABEL_590C

LABEL_58F1:
    ld   a, h
    inc  a
    jp   nz, LABEL_597D
    ld   a, l
    neg
    jp   z, LABEL_597D
    ld   l, a
    ld   a, ($d528)
    add  a, (ix+$28)
    sub  l
    jp   c, LABEL_597D
    ld   c, a
    set  3, (ix+$21)
LABEL_590C:
    ld   hl, ($d514)
    ld   e, (ix+$14)
    ld   d, (ix+$15)
    xor  a
    sbc  hl, de
    jr   c, LABEL_592E
    ld   a, h
    or   a
    jp   nz, LABEL_597D
    ld   a, ($d529)
    sub  l
    jp   c, LABEL_597D
    ld   l, a
    set  1, (ix+$21)
    jp   LABEL_5943

LABEL_592E:
    ld   a, h
    inc  a
    jr   nz, LABEL_597D
    ld   a, l
    neg
    jr   z, LABEL_597D
    ld   l, a
    ld   a, (ix+$29)
    sub  l
    jr   c, LABEL_597D
    ld   l, a
    set  0, (ix+$21)
LABEL_5943:
    ld   a, c
    cp   l
    ld   b, $0c
    jr   c, LABEL_594B
    ld   b, $03
LABEL_594B:
    ld   a, (ix+$21)
    and  b
    ld   (ix+$21), a
    bit  7, (ix+$03)
    jr   nz, LABEL_595E
    ld   a, (ix+$00)
    ld   ($d520), a
LABEL_595E:
    ld   a, (ix+$21)
    and  $0f
    ld   b, a
    and  $03
    ld   c, $0c
    jr   z, LABEL_596C
    ld   c, $03
LABEL_596C:
    ld   a, b
    xor  c
    rlca
    rlca
    rlca
    rlca
    ld   b, a
    ld   a, ($d521)
    and  $0f
    or   b
    ld   ($d521), a
    ret

LABEL_597D
    ld   a, (ix+$21)
    and  $f0
    ld   (ix+$21), a
    ret

LABEL_5986:
    ld   a, (ix+$00)
    or   a
    ret  z
    ld   a, (iy+$00)
    or   a
    ret  z
    ld   h, (ix+$12)
    ld   l, (ix+$11)
    ld   d, (iy+$12)
    ld   e, (iy+$11)
    xor  a
    sbc  hl, de
    jr   nc, LABEL_59A8
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
LABEL_59A8:
    ld   d, $00
    ld   e, (ix+$28)
    xor  a
    sbc  hl, de
    ret  nc
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    ld   d, (iy+$15)
    ld   e, (iy+$14)
    xor  a
    sbc  hl, de
    jr   nc, LABEL_59C9
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
LABEL_59C9:
    ld   d, $00
    ld   e, (ix+$29)
    xor  a
    sbc  hl, de
    ret  nc
    ld   a, $ff
    ld   (ix+$20), a
    ld   (iy+$20), a
    ret

LABEL_59DB:
    bit  1, (ix+$25)
    ret  nz
    ld   a, ($d503)
    bit  6, a
    ret  nz
    ld   b, $00
    ld   c, (ix+$28)
    call LABEL_5424
    or   a
    ret  z
    ld   b, $00
    ld   c, (ix+$29)
    call LABEL_5430
    or   a
    ret  z
    ld   a, ($d532)
    cp   $05
    jr   z, LABEL_5A21
    ld   a, ($d503)
    bit  1, a
    jr   z, LABEL_5A2F
    bit  2, (ix+$25)
    jr   nz, LABEL_5A2F
    ld   a, ($d519)
    rlca
    jr   c, LABEL_5A21
    ld   hl, $fd00
    ld   ($d518), hl
    ld   hl, $d503
    set  1, (hl)
    set  0, (hl)
LABEL_5A21:
    bit  0, (ix+$25)
    jr   nz, LABEL_5A2A
    jp   LABEL_51CB

LABEL_5A2A:
    set  1, (ix+$25)
    ret

LABEL_5A2F:
    ld   hl, $fe00
    ld   ($d518), hl
    ld   a, $ff
    ld   ($d3c1), a
    ret

LABEL_5A3B:
    ld   b, (ix+$22)
    ld   a, ($d3c4)
    or   a
    jr   nz, +
    bit  1, (ix+$03)
    jr   nz, ++
    ld   a, ($d503)
    bit  7, a
    jr   nz, ++

+:  ld   a, (ix+$21)
    rrca
    rrca
    rrca
    rrca
    and  $0f
    or   b
    ld   b, a
  
++: ld   (ix+$23), b
    ret


; =============================================================================
;  Engine_ProcessObjectLogic(uint16 obj_ptr)
; -----------------------------------------------------------------------------
;  Processes an object's logic sequence for this frame.
; -----------------------------------------------------------------------------
;  In:
;    IX  - Pointer to object.
;  Out:
;    None.
; -----------------------------------------------------------------------------
Engine_ProcessObjectLogic:      ; $5A60
    ; fetch the logic sequence pointer
    ld   a, (ix + Object.LogicSeqPtr)
    or   (ix + Object.LogicSeqPtr + 1)
    jp   z, _Engine_ProcessObjectLogic_GetLogicPtr
    
    ; should we skip checking for a state change?
    bit  OBJ_F3_NO_STATE_CHANGE, (ix + Object.Flags03)
    jp   nz, +
    
    ; check to see if the object's state has changed
    ld   a, (ix + Object.StateNext)
    cp   (ix + Object.State)
    jp   nz, _Engine_ProcessObjectLogic_ChangeState

+:  dec  (ix + Object.FrameCounter)
    jp   z, Engine_InterpretLogicSequence
    ret

_Engine_ProcessObjectLogic_ChangeState:      ; $5A80
    ld   a, (ix + Object.StateNext)
    ld   (ix + Object.State), a

_Engine_ProcessObjectLogic_GetLogicPtr:
    ; set a dummy logic routine
    ld   hl, ObjectLogic_DoNothing
    ld   (ix + Object.LogicPtr), l
    ld   (ix + Object.LogicPtr + 1), h
    
    ; use the object ID to calculate a pointer to the logic
    ; sequence data
    ld   a, (ix + Object.ObjID)
    dec  a
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, Data_LogicPointers
    add  hl, de
    
    ; fetch the logic sequence pointer
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    
    ; calculate the logic pointer for the object's current state
    ld   a, (ix + Object.State)
    add  a, a
    ld   l, a
    ld   h, $00
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    
    ; store the pointer in the object structure
    ld   (ix + Object.LogicSeqPtr), e
    ld   (ix + Object.LogicSeqPtr + 1), d
    ; FALL THROUGH


Engine_InterpretLogicSequence:      ; $5AAF
    ; read a byte from the logic sequence
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    
    
    cp   $FF
    jp   z, Logic_ProcessCommand
    
    cp   $FE
    jp   z, Logic_ChangeAnimFrame
    
    ld   (ix+$07), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$06), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$0c), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$0d), a
    inc  hl
    ld   (ix+$0e), l
    ld   (ix+$0f), h
    jp   LABEL_5AED

Logic_ChangeAnimFrame:      ; $5ADC
    inc  hl
    ld   a, (hl)
    
    ld   (ix + Object.FrameCounter), a
    inc  hl
    
    ld   a, (hl)
    ld   (ix + Object.AnimFrame), a
    
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
LABEL_5AED:
    ld   a, $0f
    ld   ($d12b), a
    ld   ($ffff), a
    ld   l, (ix+$00)
    ld   h, $00
    add  hl, hl
    ld   de, $8000
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   l, (ix+$06)
    ld   h, $00
    add  hl, hl
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    ld   a, (hl)
    inc  hl
    ld   (ix+$05), a
    ld   a, (hl)
    ld   (ix+$28), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$29), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$2a), a
    inc  hl
    ld   a, (hl)
    ld   (ix+$2b), a
    inc  hl
    ld   (ix+$2c), l
    ld   (ix+$2d), h
    ret

Data_LogicPointers:     ; $5B2D
.dw Logic_Sonic     ; $01   Bank 12
.dw Logic_Tails     ; $02   Bank 12
.dw $8000           ; $03
.dw $8050           ; $04
.dw $8050           ; $05
.dw $8215           ; $06
.dw $848B           ; $07
.dw Logic_Nack      ; $08
.dw $8675           ; $09
.dw $877D           ; $0A
.dw $886C           ; $0B
.dw $88D9           ; $0C
.dw $89A2           ; $0D
.dw $89A2           ; $0E
.dw $8A88           ; $0F
.dw $8AF9           ; $10
.dw $8B1F           ; $11
.dw $8BEF           ; $12
.dw $8CC2           ; $13
.dw $91AE           ; $14 - title card object
.dw $9245           ; $15 - ring
.dw $92B1           ; $16
.dw $9375           ; $17
.dw $A616           ; $18
.dw $93A9           ; $19
.dw $9576           ; $1A
.dw $962D           ; $1B
.dw $96D0           ; $1C
.dw $96D0           ; $1D
.dw $9726           ; $1E
.dw $98AD           ; $1F
.dw $9BF2           ; $20
.dw $A0B1           ; $21 
.dw $9DA4           ; $22
.dw $9F37           ; $23
.dw $A1A4           ; $24
.dw $A3BE           ; $25
.dw $A469           ; $26
.dw $A616           ; $27
.dw $A7F6           ; $28
.dw $BC52           ; $29
.dw $A8D7           ; $2A
.dw $A966           ; $2B
.dw $AA5C           ; $2C
.dw $A7F6           ; $2D
.dw $AAC9           ; $2E
.dw $AC06           ; $2F
.dw $ACB9           ; $30
.dw $AEEB           ; $31
.dw $AFD0           ; $32
.dw $B11B           ; $33
.dw $B1A7           ; $34
.dw $B1DF           ; $35
.dw $B63E           ; $36
.dw $B6AF           ; $37
.dw $B71F           ; $38
.dw $B773           ; $39
.dw $B92E           ; $3A
.dw $B99D           ; $3B
.dw $BE7E           ; $3C
.dw $BB0C           ; $3D
.dw $BBD0           ; $3E
.dw $BF34           ; $3F
.dw $BF8D           ; $40
.dw $8000           ; $41
.dw $8299           ; $42
.dw $8327
.dw $8327
.dw $838F
.dw $84C6
.dw $8745
.dw $87B0
.dw $87B0
.dw $87F7
.dw $884C
.dw $884C
.dw $884C
.dw $8989
.dw $8ADA
.dw $8BD0
.dw $8FED
.dw $91DC
.dw $9B18
.dw $A292
.dw $A842
.dw $ACFB
.dw $ACFB
.dw $AF26
.dw $AFE3
.dw $B074
.dw $B4BC
.dw $B7BC
.dw $BA20
.dw $B48E
.dw $B650
.dw $B7C8
.dw $B7C8
.dw $B8D8
.dw $B8D8
.dw $BC99
.dw $BC99
.dw $B027
.dw $BF51
.dw $BAAC
.dw $BAFC
.dw $BC98
.dw $BDD1


Logic_ProcessCommand:       ; $5C03
    ; read the command byte
    inc  hl
    ld   a, (hl)
    
    ; store the updated sequence pointer
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    
    ; jump to the handler function
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, _Logic_CommandJumpTable
    add  hl, de
    ld   a, (hl)
    inc  hl
    ld   h, (hl)
    ld   l, a
    jp   (hl)

_Logic_CommandJumpTable:
.dw Logic_Cmd_ChangeStateOrRestart          ; $00
.dw Logic_Cmd_Call                          ; $01
.dw Logic_Cmd_SetSpeed                      ; $02
.dw Logic_Cmd_SetNextState                  ; $03
.dw Logic_Cmd_AllocateObject                ; $04
.dw Logic_Cmd_SetLogicFunction              ; $05
.dw Logic_Cmd_PlaySoundEffect               ; $06
.dw Logic_Cmd_SetLogicSequence              ; $07
.dw Logic_Cmd_MaybeSetLogicSequence         ; $08
.dw Logic_SetObjectProperty                 ; $09
.dw Logic_SetMemoryValue                    ; $0A
.dw Logic_AndObjectProperty                 ; $0B
.dw Logic_OrObjectProperty                  ; $0C
.dw Logic_ConditionalSetAnimFrame           ; $0D
.dw Logic_SetIX33                           ; $0E
.dw Logic_ConditionalSetLogicSequence       ; $0F
.dw LABEL_5EA3                              ; $10
.dw Logic_Cmd_PlayMusic                     ; $11


Logic_Cmd_InterpretLogicSequence:       ; $5C3D
    jp   Engine_InterpretLogicSequence


Logic_Cmd_ChangeStateOrRestart:     ; $5C40
    ld   a, (ix + Object.State)
    cp   (ix + Object.StateNext)
    jp   z, _Engine_ProcessObjectLogic_GetLogicPtr
    jp   _Engine_ProcessObjectLogic_ChangeState


Logic_Cmd_Call:     ; $5C4C
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    
    ; read the function pointer fromthe sequence
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    
    ; store updated sequence pointer
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    
    ; push as the return address
    ld   hl, Engine_InterpretLogicSequence
    push hl
    ex   de, hl
    jp   (hl)


Logic_Cmd_SetSpeed:     ; $5C62
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    
    ; read the x speed
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    
    ; read the y speed
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    inc  hl
    
    ; store the updated sequence pointer
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    
    ; if the object is facing left negate the speed value
    bit  OBJ_F4_FACING_LEFT, (ix + Object.Flags04)
    jr   z, +
    ld   hl, $0000
    xor  a
    sbc  hl, de
    ex   de, hl
    
+:  ld   (ix + Object.VelX), e
    ld   (ix + Object.VelX + 1), d
    ld   (ix + Object.VelY), c
    ld   (ix + Object.VelY + 1), b
    jp   Engine_InterpretLogicSequence


Logic_Cmd_SetNextState:     ; $5C92
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (ix + Object.StateNext), a
    jp   Engine_InterpretLogicSequence


Logic_Cmd_AllocateObject:       ; $5CA6
    call Engine_AllocateObjectLowPriority
    jr   c, _Logic_Cmd_AllocateObject_Failure
    
    ; set the object ID
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    
    ld   (iy + Object.ObjID), a
    
    ; read the new object's X-offset value
    inc  hl
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    
    ; read the object's Y-offset value
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    inc  hl
    
    ; set the object subtype
    ld   a, (hl)
    ld   (iy + Object.ix3F), a
    
    ; update the logic sequence pointer
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    
    ; negate x offset if the current object is facing left
    bit  OBJ_F4_FACING_LEFT, (ix + Object.Flags04)
    jr   z, +
    ld   hl, $0000
    xor  a
    sbc  hl, de
    ex   de, hl
    
    ; copy the current object's X coordinate to the new object,
    ; adding the X-offset value
+:  ld   l, (ix + Object.X)
    ld   h, (ix + Object.X + 1)
    add  hl, de
    ld   (iy + Object.X), l
    ld   (iy + Object.X + 1), h
    ld   (iy + Object.InitialX), l
    ld   (iy + Object.InitialX + 1), h
    
    ; copy the current object's Y coordinate to the new objet,
    ; adding the Y-offset vale
    ld   l, (ix + Object.Y)
    ld   h, (ix + Object.Y + 1)
    add  hl, bc
    ld   (iy + Object.Y), l
    ld   (iy + Object.Y + 1), h
    ld   (iy + Object.InitialY), l
    ld   (iy + Object.InitialY + 1), h
    
    ; copy current object's visibility
    ld   a, (ix + Object.Flags04)
    and  (1 << OBJ_F4_FACING_LEFT)
    ld   (iy + Object.Flags04), a
    
    ; copy VDP tile indices
    ld   a, (ix +  Object.RightFacingIdx)
    ld   (iy + Object.RightFacingIdx), a
    ld   a, (ix + Object.LeftFacingIdx)
    ld   (iy + Object.LeftFacingIdx), a
    jp   Engine_InterpretLogicSequence


_Logic_Cmd_AllocateObject_Failure:      ; $5D13
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    inc  hl
    inc  hl
    inc  hl
    inc  hl
    inc  hl
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    jp   Engine_InterpretLogicSequence


Logic_Cmd_SetLogicFunction:     ; $5D28
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    ld   a, (hl)
    ld   (ix + Object.LogicPtr), a
    inc  hl
    ld   a, (hl)
    ld   (ix + Object.LogicPtr + 1), a
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   hl, LABEL_5AED
    push hl
    ex   de, hl
    jp   (hl)


Logic_Cmd_PatternLoadCue:       ; $5D48
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (PatternLoadCue), a
    jp   Engine_InterpretLogicSequence


Logic_Cmd_PlaySoundEffect:      ; $5D5C
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (Sound_SFX_Trigger), a
    jp   Engine_InterpretLogicSequence


Logic_Cmd_PlayMusic:            ; $5D70
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (Sound_Music_Trigger), a
    jp   Engine_InterpretLogicSequence


Logic_Cmd_SetLogicSequence:     ; $5D84
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   h, (hl)
    ld   l, a
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    jp   Engine_InterpretLogicSequence


; conditioanlly sets the logic sequence pointer based on the 
; return value of a function (returned in carry flag)
Logic_Cmd_MaybeSetLogicSequence:       ; $5D97
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    
    ; read the function pointer
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    
    ; read the new logic sequence pointer
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    inc  hl
    
    ; store the incremented logic sequence pointer
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    
    ; call the function
    push bc
    ld   bc, _Logic_Cmd_MaybeSetLogicSequence_return
    push bc
    ex   de, hl
    jp   (hl)

_Logic_Cmd_MaybeSetLogicSequence_return:
    pop  bc
    ; optionally set the logic sequence pointer
    jp   nc, Engine_InterpretLogicSequence
    ld   (ix + Object.LogicSeqPtr), c
    ld   (ix + Object.LogicSeqPtr + 1), b
    jp   Engine_InterpretLogicSequence


Logic_SetObjectProperty:        ; $5DBF
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ; read the property index
    ld   c, (hl)
    inc  hl
    ; read the value
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    push ix
    pop  hl
    ld   b, $00
    add  hl, bc
    ld   (hl), a
    jp   Engine_InterpretLogicSequence


Logic_SetMemoryValue:           ; $5DD9
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ; read the pointer
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    ; read the value
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ; set the value
    ld   (de), a
    jp   Engine_InterpretLogicSequence


Logic_AndObjectProperty:     ; $5DEF
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ; read the property index
    ld   c, (hl)
    inc  hl
    ; read the mask value
    ld   e, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ; mask & set the property
    push ix
    pop  hl
    ld   b, $00
    add  hl, bc
    ld   a, (hl)
    and  e
    ld   (hl), a
    jp   Engine_InterpretLogicSequence


Logic_OrObjectProperty:     ; $5E0B
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   c, (hl)
    inc  hl
    ld   e, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    push ix
    pop  hl
    ld   b, $00
    add  hl, bc
    ld   a, (hl)
    or   e
    ld   (hl), a
    jp   Engine_InterpretLogicSequence


Logic_ConditionalSetAnimFrame:      ; $5E27
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    
    ld   c, (hl)
    inc  hl
    
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    
    ld   a, (hl)
    ld   (ix + Object.LogicPtr), a
    inc  hl
    ld   a, (hl)
    ld   (ix + Object.LogicPtr + 1), a
    inc  hl
    
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (ix + Object.FrameCounter), c
    bit  4, (ix + Object.ix0A)
    jr   z, +
    ld   e, d
    
+:  ld   (ix + Object.AnimFrame), e
    jp   LABEL_5AED


Logic_SetIX33:      ; $5E53
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    ld   (ix + Object.ix33), a
    jp   Engine_InterpretLogicSequence


Logic_ConditionalSetLogicSequence:      ; $5E67
    ld   l, (ix + Object.LogicSeqPtr)
    ld   h, (ix + Object.LogicSeqPtr + 1)
    ld   a, (ix + Object.ix33)
    dec  a
    ld   (ix + Object.ix33), a
    jr   z, +
    
    ld   a, (hl)
    inc  hl
    ld   h, (hl)
    ld   l, a
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    jp   Engine_InterpretLogicSequence

+:  ld   a, (hl)
    inc  hl
    ld   a, (hl)
    inc  hl
    ld   (ix + Object.LogicSeqPtr), l
    ld   (ix + Object.LogicSeqPtr + 1), h
    jp   Engine_InterpretLogicSequence


LABEL_5E90:
    res  OBJ_F4_FACING_LEFT, (ix + Object.Flags04)
    ret
    
    set  OBJ_F4_FACING_LEFT, (ix + Object.Flags04)
    ret


LABEL_5E9A:
    ld   a, (ix + Object.Flags04)
    xor  (1 << OBJ_F4_FACING_LEFT)
    ld   (ix + Object.Flags04), a
    ret


LABEL_5EA3:
    call Engine_AllocateObjectLowPriority
    jp   c, _Logic_Cmd_AllocateObject_Failure
    ld   l, (ix+$0e)
    ld   h, (ix+$0f)
    ld   a, (hl)
    ld   (iy+$00), a
    inc  hl
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    inc  hl
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    inc  hl
    ld   a, (hl)
    add  a, (ix+$0a)
    ld   (iy+$0a), a
    inc  hl
    ld   (ix+$0e), l
    ld   (ix+$0f), h
    bit  4, (ix+$04)
    jr   z, LABEL_5ED7
    ld   hl, $0000
    xor  a
    sbc  hl, de
    ex   de, hl
LABEL_5ED7:
    ld   l, (ix+$11)
    ld   h, (ix+$12)
    add  hl, de
    ld   (iy+$11), l
    ld   (iy+$12), h
    ld   (iy+$3a), l
    ld   (iy+$3b), h
    ld   l, (ix+$14)
    ld   h, (ix+$15)
    add  hl, bc
    ld   (iy+$14), l
    ld   (iy+$15), h
    ld   (iy+$3c), l
    ld   (iy+$3d), h
    ld   a, (ix+$04)
    and  $10
    ld   (iy+$04), a
    ld   a, (ix+$08)
    ld   (iy+$08), a
    ld   a, (ix+$09)
    ld   (iy+$09), a
    jp   Engine_InterpretLogicSequence

LABEL_5F14:
    call LABEL_5F53
    call LABEL_6780
    call LABEL_6A92
    call LABEL_6CF2
    call LABEL_6E16
    call LABEL_6516
    ret

LABEL_5F27:
    call LABEL_5F53
    call LABEL_6780
    call LABEL_6A92
    call LABEL_6E16
    ret

LABEL_5F34:
    ld   a, ($d501)
    ld   b, a
    cp   $18
    jr   z, +
    cp   $43
    ret  nz
+:  cp   $a7

    jr   z, +
    cp   $a8
    jr   z, +
    cp   $a9
    ret  nz

+:  ld   b, a
    cp   $43
    call z, LABEL_2216
    jp   LABEL_3B6F

LABEL_5F53:
    ld   a, ($d429)
    or   a
    jr   z, LABEL_5F5D
    dec  a
    ld   ($d429), a
LABEL_5F5D:
    ld   a, ($d416)
    ld   ($d417), a
    xor  a
    ld   ($d416), a
    res  5, (ix+$24)
    ld   a, ($d418)
    ld   ($d41f), a
    ld   a, ($d419)
    ld   ($d41a), a
    ld   hl, LevelHeader
    bit  7, (hl)
    jr   z, LABEL_5F9D
    ld   a, (ix+$1d)
    or   a
    jr   nz, LABEL_5F8C
    ld   a, (ix+$1c)
    cp   $a9
    jp   c, LABEL_5F9D
LABEL_5F8C:
    ld   a, ($d185)
    bit  0, a
    jr   nz, LABEL_5F9D
    xor  a
    ld   ($d411), a
    ld   ($d400), a
    jp   LABEL_5FC9

LABEL_5F9D:
    ld   bc, $0000
    ld   de, $0000
    ld   a, (ix+$00)
    cp   $01
    jr   z, LABEL_5FB0
    ld   bc, $0000
    ld   de, $0000
LABEL_5FB0:
    ld   a, (ix+$01)
    cp   $21
    jr   nz, LABEL_5FBC
    ld   de, $fff2
    jr   LABEL_5FC3

LABEL_5FBC:
    cp   $12
    jr   nz, LABEL_5FC3
    ld   de, $0008
LABEL_5FC3:
    call LABEL_6FA6
    call LABEL_5F34
LABEL_5FC9:
    ld   a, ($d400)
    ld   ($d418), a
    ld   a, ($d527)
    or   a
    jr   nz, LABEL_5FD8
    call LABEL_65AD
LABEL_5FD8:
    ld   a, ($d411)
    ld   ($d419), a
    and  $1f
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, DATA_5FF2
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    ld   de,  LABEL_5FF1
    push de
    jp   (hl)

LABEL_5FF1:
    ret

DATA_5FF2:
.dw LABEL_6487
.dw LABEL_6040
.dw LABEL_6040 
.dw LABEL_6040
.dw LABEL_6041
.dw LABEL_6207
.dw LABEL_642A
.dw LABEL_6487
.dw LABEL_6040
.dw LABEL_61B4
.dw LABEL_6030
.dw LABEL_63CE
.dw LABEL_639E
.dw LABEL_6330
.dw LABEL_636A
.dw LABEL_6044
.dw LABEL_64BA
.dw LABEL_6040
.dw LABEL_6075
.dw LABEL_6040
.dw LABEL_6040
.dw LABEL_6040
.dw LABEL_6219
.dw LABEL_6174
.dw LABEL_6054
.dw LABEL_62CE 
.dw LABEL_6040
.dw LABEL_6258
.dw LABEL_6040
.dw LABEL_6040
.dw LABEL_6040

LABEL_6030:
    ld   a, ($d501)
    cp   $29
    ret  z
    cp   $48
    ret  z
    set  0, (ix+$24)
    call LABEL_5854
LABEL_6040:
    ret

LABEL_6041:
    jp   LABEL_6B3C

LABEL_6044:
    ld   hl, ($d516)
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
    ld   de, $fb00
    jp   LABEL_72B2

LABEL_6054:
    ld   a, ($d501)
    cp   $32
    ret  z
    bit  7, (ix+$19)
    ret  nz
    bit  1, (ix+$22)
    ret  z
    ld   a, ($d511)
    and  $1f
    sub  $08
    ret  c
    sub  $10
    ret  nc
    ld   a, $32
    ld   ($d502), a
    ret

LABEL_6075:
    ld   a, ($d501)
    cp   $13
    ret  z
    cp   $0d
    ret  z
    cp   $0c
    ret  z
    cp   $11
    ret  z
    cp   $43
    ret  z
    ld   a, ($d41a)
    and  $1f
    cp   $12
    jp   nz, LABEL_6148
    bit  1, (ix+$22)
    ret  z
    ld   hl, ($d516)
    ld   a, ($d418)
    ld   b, a
    cp   $37
    jr   nc, LABEL_60D9
    bit  7, (ix+$24)
    jr   nz, LABEL_60BD
    bit  7, h
    ret  z
    ld   de, $fc80
    xor  a
    sbc  hl, de
    ret  nc
    ld   a, ($d511)
    and  $10
    ret  nz
    ld   a, b
    cp   $35
    jr   z, LABEL_6113
    ret

LABEL_60BD:
    res  4, (ix+$04)
    ld   hl, ($d516)
    ld   de, $00c0
    add  hl, de
    ex   de, hl
    bit  7, d
    jr   nz, LABEL_60D4
    ld   hl, ($d3d6)
    xor  a
    sbc  hl, de
    ret  c

LABEL_60D4:
    ld   ($d516), de
    ret

LABEL_60D9:
    bit  7, (ix+$24)
    jr   nz, LABEL_60F7
    bit  7, h
    ret  nz
    push hl
    ld   de, $0380
    xor  a
    sbc  hl, de
    pop  hl
    ret  c
    ld   a, ($d511)
    and  $10
    ret  z
    ld   a, b
    cp   $38
    jr   z, LABEL_6113
    ret

LABEL_60F7:
    set  4, (ix+$04)
    ld   hl, ($d516)
    ld   de, $ff40
    add  hl, de
    ex   de, hl
    bit  7, d
    jr   z, LABEL_610E
    ld   hl, ($d3d6)
    xor  a
    sbc  hl, de
    ret  nc
LABEL_610E:
    ld   ($d516), de
    ret

LABEL_6113:
    ld   hl, ($d516)
    bit  7, h
    jr   nz, LABEL_6121
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
LABEL_6121:
    ld   de, $fd80
    add  hl, de
    ld   ($d518), hl
    ld   hl, $0000
    ld   ($d516), hl
    ld   a, ($d501)
    cp   $23
    ret  z
    ld   a, $b1
    ld   (Sound_SFX_Trigger), a
    set  1, (ix+$03)
    res  1, (ix+$22)
    res  1, (ix+$23)
    jp   LABEL_3D99

LABEL_6148:
    ld   a, ($d527)
    or   a
    ret  nz
    ld   a, ($d418)
    cp   $34
    jr   z, LABEL_616A
    cp   $35
    jr   z, LABEL_616F
    cp   $37
    jr   z, LABEL_616A
    cp   $38
    jr   z, LABEL_616F
    cp   $36
    jr   z, LABEL_616F
    cp   $39
    jr   z, LABEL_616F
    jr   LABEL_616A   ;FIXME: pointless jump

LABEL_616A:
    set  7, (ix+$24)
    ret

LABEL_616F:
    res  7, (ix+$24)
    ret

LABEL_6174:
    ld   hl, ($d516)
    ld   a, ($d418)
    cp   $44
    jr   nz, LABEL_6190
    bit  7, h
    ret  nz
    ld   de, $0480
    xor  a
    sbc  hl, de
    ret  c
    ld   a, ($d511)
    and  $10
    ret  z
    jr   LABEL_61A2

LABEL_6190:
    bit  7, h
    ret  z
    ld   de, $fb80
    xor  a
    sbc  hl, de
    ret  nc
    ld   a, ($d511)
    and  $10
    ret  nz
    jr   LABEL_61A2   ;FIXME: pointless jump

LABEL_61A2:
    ld   hl, $fb00
    ld   ($d518), hl
    ld   a, ($d501)
    cp   $16
    ret  z
    cp   $23
    ret  z
    jp   LABEL_3D99

LABEL_61B4:
    ld   a, ($d501)
    cp   $29
    ret  z
    cp   $11
    ret  z
    cp   $43
    ret  z
    bit  1, (ix+$22)
    ret  z
    ld   a, (ix+$19)
    rlca
    ret  c
    set  0, (ix+$24)
    call LABEL_5854
    ld   a, $ff
    ld   ($d3f0), a
    ld   hl, $f900
    ld   a, (CurrentLevel)
    cp   $04
    jr   nz, LABEL_61E9
    bit  2, (ix+$03)
    jr   z, LABEL_61E9
    ld   hl, $fb00
LABEL_61E9:
    jp   LABEL_3DB3

LABEL_61EC:
    set  0, (ix+$24)
    call LABEL_5854
    ld   a, $10
    ld   ($d527), a
    ld   hl, $0700
    ld   ($d518), hl
    res  1, (ix+$22)
    res  1, (ix+$23)
    ret

LABEL_6207:
    bit  1, (ix+$22)
    ret  z
    bit  7, (ix+$03)
    ret  nz
    ld   a, $50
    ld   ($dbd0), a
    jp   LABEL_384A

LABEL_6219:
    bit  1, (ix+$03)
    ret  z
    ld   a, ($d501)
    cp   $0f
    ret  z
    cp   $10
    ret  z
    cp   $15
    ret  z
    cp   $1a
    ret  z
    ld   a, ($d522)
    and  $0c
    jr   nz, LABEL_6239
    ld   a, ($d519)
    and  a
    ret  m
LABEL_6239:
    ld   hl, $fbc0
    bit  2, (ix+$03)
    jr   z, LABEL_6245
    ld   hl, $ff00
LABEL_6245:
    ld   ($d518), hl
    res  1, (ix+$22)
    set  0, (ix+$03)
    ld   a, $b9
    ld   (Sound_SFX_Trigger), a
    jp   LABEL_71D8

LABEL_6258:
    ld   a, ($d418)
    ld   b, a
    ld   a, ($d41f)
    cp   b
    jr   z, LABEL_6277
    ld   a, (CurrentLevel)
    cp   $02
    jr   nz, LABEL_6277
    ld   b, $2f
    ld   c, $00
    ld   de, ($d511)
    ld   hl, ($d514)
    call Engine_AllocateLinkedObjectXY
LABEL_6277:
    ld   a, ($d501)
    cp   $23
    jr   z, LABEL_62B2
    ld   a, ($d519)
    rlca
    jr   c, LABEL_62C5
    set  5, (ix+$24)
    set  1, (ix+$22)
    ld   hl, $0000
    ld   ($d518), hl
    ld   a, ($d501)
    cp   $0f
    ret  z
    cp   $1a
    ret  z
    cp   $15
    ret  z
    cp   $23
    ret  z
    inc  (ix+$39)
    ld   a, (ix+$39)
    rrca
    ret  nc
    ld   hl, ($d514)
    inc  hl
    inc  hl
    ld   ($d514), hl
    ret

LABEL_62B2:
    ld   hl, $0000
    ld   ($d518), hl
    ld   hl, $d522
    set  1, (hl)
    ld   a, $1f
    ld   ($d415), a
    call LABEL_65BE
LABEL_62C5:
    res  5, (ix+$24)
    set  1, (ix+$03)
    ret

LABEL_62CE: 
    ld   b, $07
    ld   c, $04
    ld   de, ($d405)
    ld   hl, ($d407)
    call Engine_AllocateLinkedObjectXY
    or   a
    ret  nz
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, ($d188)
    call Engine_SwapFrame2
    ld   a, $91
    ld   hl, ($d401)
    ld   (iy+$39), h
    ld   (iy+$38), l
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    ld   hl, ($d405)
    ld   a, l
    and  $e0
    ld   l, a
    ld   ($d409), hl
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    ld   l, a
    ld   ($d40b), hl
    ld   a, $c5
    ld   (Sound_SFX_Trigger), a
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_6330:
    ld   a, ($d501)
    cp   $41
    ret  z
    cp   $0f
    ret  z
    cp   $10
    ret  z
    cp   $15
    ret  z
    cp   $1a
    ret  z
    cp   $29
    jr   z, LABEL_6367
    bit  1, (ix+$03)
    ret  z
    ld   a, $10
    ld   ($d3c3), a
    ld   hl, $fbc0
    bit  2, (ix+$03)
    jr   z, LABEL_635C
    ld   hl, $ff00
LABEL_635C:
    ld   ($d518), hl
    res  1, (ix+$22)
    set  0, (ix+$03)
LABEL_6367:
    jp   LABEL_7228

LABEL_636A:
    bit  1, (ix+$22)
    ret  z
    ld   a, ($d501)
    cp   $29
    ret  z
    ld   a, ($d418)
    cp   $a2
    jr   nz, LABEL_6381
    ld   hl, $0100
    jr   LABEL_6384

LABEL_6381:
    ld   hl, $ff00
LABEL_6384:
    ld   c, $00
    bit  7, h
    jr   z, LABEL_638B
    dec  c
LABEL_638B:
    xor  a
    ld   de, ($d510)
    add  hl, de
    ld   ($d510), hl
    ld   a, $00
    adc  a, c
    add  a, (ix+$12)
    ld   (ix+$12), a
    ret

LABEL_639E:
    bit  7, (ix+$19)
    ret  nz
    ld   hl, ($d401)
    ld   de, ($dbc1)
    xor  a
    sbc  hl, de
    ret  z
    ld   b, $09
    ld   c, $00
    ld   de, ($d405)
    ld   hl, ($d407)
    call Engine_AllocateLinkedObjectXY
    or   a
    ret  nz
    ld   hl, ($d401)
    ld   ($dbc1), hl
    ld   ($d403), hl
    ld   (iy+$39), h
    ld   (iy+$38), l
    ret

LABEL_63CE:
    ld   a, ($d429)
    or   a
    ret  nz
    ld   a, ($d501)
    cp   $29
    ret  z
    ld   a, ($d418)
    cp   $93
    ret  nz
    bit  1, (ix+$22)
    ret  z
    ld   b, $3f
    ld   c, $00
    ld   de, ($d405)
    ld   hl, ($d407)
    call Engine_AllocateLinkedObjectXY
    or   a
    ret  nz
    ld   hl, ($d405)
    ld   ($d40d), hl
    ld   hl, ($d407)
    ld   ($d40f), hl
    ld   c, $94
    call LABEL_6580
    ld   a, $c5
    ld   (Sound_SFX_Trigger), a
    ld   hl, ($d401)
    ld   ($d403), hl
    ld   (iy+$39), h
    ld   (iy+$38), l
    ld   a, ($d12b)
    push af
    ld   a, $18
    call Engine_SwapFrame2
    call $B858
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_642A:
    ld   a, (CurrentLevel)
    cp   $0a
    jr   z, LABEL_6434
    cp   $00
    ret  nz
LABEL_6434:
    ld   a, ($d501)
    cp   $11
    ret  z
    cp   $43
    ret  z
    ld   a, ($d501)
    cp   $0b
    ret  z
    ld   a, (ix+$11)
    and  $1f
    cp   $04
    ret  c
    cp   $1c
    ret  nc
    ld   a, ($d418)
    cp   $d5
    jr   nz, LABEL_646E
    ld   b, $21
    ld   c, $ff
    ld   de, ($d405)
    ld   hl, ($d407)
    call Engine_AllocateLinkedObjectXY
    ld   hl, $fc80
    ld   a, $00
    ld   ($d3f0), a
    jp   LABEL_5186

LABEL_646E:
    ld   b, $21
    ld   c, $fe
    ld   de, ($d405)
    ld   hl, ($d407)
    call Engine_AllocateLinkedObjectXY
    ld   hl, $f900
    ld   a, $ff
    ld   ($d3f0), a
    jp   LABEL_5186

LABEL_6487:
    bit  5, (ix+$21)
    ret  nz
    ld   a, ($d501)
    cp   $31
    ret  z
    res  1, (ix+$22)
    ld   a, ($d501)
    cp   $18
    ret  z
    cp   $11
    ret  z
    cp   $23
    ret  z
    cp   $43
    ret  z
LABEL_64A5:
    bit  1, (ix+$23)
    ret  nz
    bit  0, (ix+$03)
    ret  nz
    ld   a, (ix+$01)
    cp   $09
    jp   nz, LABEL_3B47
    jp   LABEL_3BB8

LABEL_64BA:
    ld   a, ($d418)
    sub  $6c
    ret  c
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, LABEL_64CD
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    jp   (hl)

LABEL_64CD:
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64EE
.dw LABEL_64EE
.dw LABEL_6502
.dw LABEL_6502
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED
.dw LABEL_64ED

LABEL_64ED:
    ret

LABEL_64EE:
    ld   a, (ix+$25)
    cp   $00
    ret  z
    bit  1, (ix+$22)
    ret  z
    ld   a, ($d41f)
    cp   $76
    jp   z, LABEL_378C
    ret

LABEL_6502:
    ld   a, (ix+$25)
    cp   $01
    ret  z
    bit  1, (ix+$22)
    ret  z
    ld   a, ($d41f)
    cp   $75
    jp   z, LABEL_373D
    ret

LABEL_6516:
    ld   a, (CurrentLevel)
    cp   $03
    ret  nz
    ld   a, ($d418)
    cp   $ea
    ret  nz
    ld   a, $42
    ld   ($d502), a
    ret

LABEL_6528:
    ld   a, ($d12b)
    push af
    ld   a, ($d188)
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, c
    ld   hl, ($d401)
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    jp   LABEL_64A5

LABEL_6557:
    ld   a, ($d12b)
    push af
    ld   a, ($d188)
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, c
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_6580:
    ld   a, ($d12b)
    push af
    ld   a, ($d188)
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, c
    ld   hl, ($d401)
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2D17
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_65AD:
    ld   a, ($d527)
    or   a
    ret  nz
    ld   a, ($d419)
    bit  7, a
    jr   nz, LABEL_65BE
    bit  6, a
    jr   nz, LABEL_6628
    ret

LABEL_65BE: 
    bit  7, (ix+$19)
    ret  nz
LABEL_65C3:
    ld   a, ($d415)
    bit  6, a
    ret  nz
    and  $3f
    cp   $20
    call z, LABEL_6671
    ld   a, ($d100)
    ld   ($d416), a
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    and  $3f
    add  a, c
    cp   $20
    ret  c
    sub  $20
    ld   c, a
    ld   b, $00
    ld   hl, ($d514)
    xor  a
    sbc  hl, bc
    ld   ($d514), hl
LABEL_65F3:
    set  1, (ix+$22)
    ld   a, (ix+$00)
    cp   $02
    ret  nz
    xor  a
    ld   ($dbce), a
    ret

LABEL_6602:
    ld   hl, ($d516)
    bit  7, h
    jr   z, LABEL_6610
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
LABEL_6610:
    ld   de, ($d518)
    bit  7, d
    jr   z, LABEL_661F
    dec  de
    ld   a, d
    cpl
    ld   d, a
    ld   a, e
    cpl
    ld   e, a
LABEL_661F: 
    xor  a
    sbc  hl, de
    jp   nc, LABEL_65C3
    jp   LABEL_65BE

LABEL_6628:
    and  $1f
    cp   $11
    jp   z, LABEL_6602
    cp   $03
    jp   z, LABEL_6602
    bit  7, (ix+$19)
    ret  nz
    ld   a, ($d415)
    and  $3f
    call z, LABEL_6671
    res  1, (ix+$22)
    ld   a, (ix+$19)
    add  a, $09
    ld   b, a
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    add  a, c
    cp   $20
    ret  c
    sub  $20
    cp   b
    ret  nc
    ld   c, a
    ld   b, $00
    ld   hl, ($d514)
    xor  a
    sbc  hl, bc
    ld   ($d514), hl
    ld   a, ($d100)
    ld   ($d416), a
    jp   LABEL_65F3

LABEL_6671:
    push bc
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   hl, ($d401)
    ld   de, ($d190)
    add  hl, de
    ld   a, (hl)
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   de, (CollisionDataPtr)
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    bit  5, (hl)
    jr   z, +
    ld   a, (ix+$25)
    or   a
    jr   z, +
    ld   de, $0007
    add  hl, de
+:  ld   a, (hl)
    and  $1f
    cp   $03
    jr   z, +
    ld   a, (hl)
    bit  6, a
    jr   nz, LABEL_66E5
    bit  7, a
    jr   z, LABEL_66DC
+:  inc  hl
    ld   a, (hl)
    ld   ($d100), a
    inc  hl
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    ex   de, hl
    ld   a, ($d405)
    and  $1f
    ld   l, a
    ld   h, $00
    add  hl, bc
    ld   a, (hl)
    and  $3f
    jr   z, LABEL_66DC
    ld   c, a
    ld   b, $00
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    xor  a
    sbc  hl, bc
    ld   (ix+$15), h
    ld   (ix+$14), l
LABEL_66DC: 
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    pop  bc
    ret

LABEL_66E5:
    and  $1f
    cp   $09
    jr   z, LABEL_66DC
    inc  hl
    inc  hl
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    ex   de, hl
    ld   a, ($d405)
    and  $1f
    ld   l, a
    ld   h, $00
    add  hl, bc
    ld   a, (hl)
    and  $3f
    jr   z, LABEL_66DC
    ld   b, a
    ld   a, ($d415)
    add  a, $20
    add  a, b
    ld   ($d415), a
    ld   b, a
    jp   LABEL_66DC

LABEL_670D:
    ld   a, ($d411)
    rlca
    jr   c, LABEL_6717
    rlca
    jr   c, LABEL_6747
    ret

LABEL_6717:
    ld   a, ($d415)
    and  $3f
    cp   $20
    call z, LABEL_6671
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    and  $3f
    add  a, c
    cp   $20
    ret  c
    sub  $20
    ld   c, a
    ld   b, $00
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    xor  a
    sbc  hl, bc
    ld   (ix+$15), h
    ld   (ix+$14), l
    jp   LABEL_65F3

LABEL_6747:
    bit  7, (ix+$19)
    ret  nz
    ld   a, ($d415)
    and  $3f
    call z, LABEL_6671
    ld   a, (ix+$19)
    add  a, $07
    ld   b, a
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    add  a, c
    cp   $20
    ret  c
    sub  $20
    cp   b
    ret  nc
    ld   c, a
    ld   b, $00
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    xor  a
    sbc  hl, bc
    ld   (ix+$15), h
    ld   (ix+$14), l
    jp   LABEL_65F3

LABEL_6780:
    ld   hl, $dbe0
    ld   a, (hl)
    or   a
    jr   z, LABEL_6788
    dec  (hl)
LABEL_6788:
    ld   hl, $d522
    res  2, (hl)
    res  3, (hl)
    call LABEL_6855
    call LABEL_6A2E
    call LABEL_6FA6
    call LABEL_69FB
    ld   a, ($d411)
    and  $1f
    cp   $10
    jr   nz, LABEL_67A9
    call LABEL_6B3C
    jr   LABEL_67B0

LABEL_67A9:
    cp   $04
    jr   nz, LABEL_67B0
    call LABEL_6B3C
LABEL_67B0:
    ld   a, ($d411)
    and  $1f
    cp   $05
    jp   z, LABEL_6960
    cp   $0d
    jp   z, LABEL_6910
    cp   $16
    jp   z, LABEL_69AB
    cp   $0f
    jp   z, LABEL_69E7
    cp   $18
    jp   z, LABEL_67D9
    cp   $12
    jp   z, LABEL_6A2D
    ld   a, ($d411)
    bit  7, a
    ret  z
LABEL_67D9:
    ld   a, ($d414)
    bit  6, a
    jr   nz, LABEL_67F0
    and  $3f
    ret  z
    ld   b, a
    ld   a, ($d405)
    and  $1f
    cp   b
    ret  nc
    ld   c, a
    ld   b, $00
    jr   LABEL_680D

LABEL_67F0:
    and  $3f
    ret  z
    ld   c, a
    ld   hl, ($d405)
    ld   de, $0020
    add  hl, de
    ld   a, l
    and  $e0
    ld   l, a
    ld   b, $00
    xor  a
    sbc  hl, bc
    ex   de, hl
    ld   hl, ($d405)
    sbc  hl, de
    ret  c
    ld   c, l
    ld   b, h
LABEL_680D:
    ld   hl, ($d511)
    xor  a
    sbc  hl, bc
    ld   ($d511), hl
    ld   a, ($d419)
    and  $1f
    cp   $12
    ret  z
    set  2, (ix+$22)
    bit  4, (ix+$04)
    jr   nz, LABEL_682E
    ld   hl, $0000
    ld   ($d516), hl
LABEL_682E:
    ld   a, ($d411)
    and  $1f
    cp   $0a
    ret  nz
    ld   a, ($d501)
    cp   $29
    ret  z
    ld   a, ($d501)
    cp   $11
    ret  z
    cp   $43
    ret  z
    ld   a, ($dbe0)
    or   a
    ret  nz
    ld   a, $08
    ld   ($dbe0), a
    ld   hl, $fa00
    jp   LABEL_3E0C

LABEL_6855:
    call LABEL_6A6D
    call LABEL_6FA6
    call LABEL_6A14
    ld   a, ($d411)
    and  $1f
    cp   $10
    jr   nz, LABEL_686C
    call LABEL_6B3C
    jr   LABEL_6873

LABEL_686C:
    cp   $04
    jr   nz, LABEL_6873
    call LABEL_6B3C
LABEL_6873:
    ld   a, ($d411)
    and  $1f
    cp   $05
    jp   z, LABEL_6980
    cp   $0d
    jp   z, LABEL_6937
    cp   $16
    jp   z, LABEL_69C9
    cp   $0f
    jp   z, LABEL_69F1
    cp   $18
    jr   z, LABEL_6896
    ld   a, ($d411)
    bit  7, a
    ret  z
LABEL_6896:
    ld   a, ($d414)
    bit  6, a
    jr   nz, LABEL_68B0
    and  $3f
    ret  z
    ld   b, a
    ld   a, ($d405)
    and  $1f
    sub  b
    ret  nc
    neg
    dec  a
    ld   c, a
    ld   b, $00
    jr   LABEL_68CD

LABEL_68B0:
    and  $3f
    ret  z
    ld   c, a
    ld   hl, ($d405)
    ld   de, $0020
    add  hl, de
    ld   a, l
    and  $e0
    ld   l, a
    dec  hl
    ld   de, ($d405)
    xor  a
    sbc  hl, de
    ret  c
    ld   a, l
    cp   c
    ret  nc
    ld   c, l
    ld   b, h
LABEL_68CD:
    ld   hl, ($d511)
    add  hl, bc
    ld   ($d511), hl
    ld   a, ($d419)
    and  $1f
    cp   $12
    ret  z
    set  3, (ix+$22)
    bit  4, (ix+$04)
    jr   z, LABEL_68EC
    ld   hl, $0000
    ld   ($d516), hl
LABEL_68EC:
    ld   a, ($d411)
    and  $1f
    cp   $0a
    ret  nz
    ld   a, ($d501)
    cp   $29
    ret  z
    ld   a, ($d501)
    cp   $11
    ret  z
    ld   a, ($dbe0)
    or   a
    ret  nz
    ld   a, $08
    ld   ($dbe0), a
    ld   hl, $0600
    jp   LABEL_3DFF

LABEL_6910:
    bit  1, (ix+$03)
    jp   z, LABEL_67D9
    ld   a, ($d517)
    bit  7, a
    jr   z, LABEL_6920
    neg
LABEL_6920:
    cp   $03
    jp   c, LABEL_67D9
    ld   hl, ($d516)
    ld   a, h
    cp   $07
    jr   nc, LABEL_6934
    ld   bc, $0040
    add  hl, bc
    ld   ($d516), hl
LABEL_6934:
    jp   LABEL_7228

LABEL_6937:
    bit  1, (ix+$03)
    jp   z, LABEL_6896
    ld   a, ($d517)
    bit  7, a
    jr   z, LABEL_6947
    neg
LABEL_6947:
    cp   $03
    jp   c, LABEL_6896
    ld   hl, ($d516)
    ld   a, h
    neg
    cp   $07
    jr   nc, LABEL_695D
    ld   bc, $ffc0
    add  hl, bc
    ld   ($d516), hl
LABEL_695D:
    jp   LABEL_7228

LABEL_6960:
    ld   a, ($d400)
    and  $fe
    cp   $f2
    jr   z, LABEL_6974
    ld   a, ($d502)
    cp   $16
    jp   z, LABEL_6910
    cp   $1e
    ret  z
LABEL_6974:
    call LABEL_67D9
    ld   a, ($d400)
    cp   $f4
    ret  nz
    jp   LABEL_699D

LABEL_6980:
    ld   a, ($d400)
    and  $fe
    cp   $f2
    jr   z, LABEL_6994
    ld   a, ($d502)
    cp   $16
    jp   z, LABEL_6937
    cp   $1e
    ret  z
LABEL_6994:
    call LABEL_6896
    ld   a, ($d400)
    cp   $f5
    ret  nz
LABEL_699D:
    bit  7, (ix+$03)
    ret  nz
    ld   hl, $0100
    ld   ($d518), hl
    jp   LABEL_384A

LABEL_69AB
    ld   a, ($d501)
    cp   $0f
    jp   z, LABEL_67D9
    cp   $15
    jp   z, LABEL_67D9
    bit  1, (ix+$03)
    jp   z, LABEL_67D9
    ld   a, ($d519)
    and  a
    jp   m, LABEL_67D9
    jp   LABEL_71D8

LABEL_69C9:
    ld   a, ($d501)
    cp   $0f
    jp   z, LABEL_6896
    cp   $15
    jp   z, LABEL_6896
    bit  1, (ix+$03)
    jp   z, LABEL_6896
    ld   a, ($d519)
    and  a
    jp   m, LABEL_6896
    jp   LABEL_71D8

LABEL_69E7:
    ld   hl, $fc00
    ld   de, ($d518)
    jp   LABEL_72B2

LABEL_69F1:
    ld   hl, $0400
    ld   de, ($d518)
    jp   LABEL_72B2

LABEL_69FB:
    ld   a, ($d400)
    sub  $7a
    ret  c
    sub  $05
    ret  nc
    ld   a, (ix+$01)
    cp   $18
    jr   nz, LABEL_6A0E
    call LABEL_3B62
LABEL_6A0E:
    ld   (ix+$25), $00
    pop  af
    ret

LABEL_6A14:
    ld   a, ($d400)
    sub  $7f
    ret  c
    sub  $05
    ret  nc
    ld   a, (ix+$01)
    cp   $18
    jr   nz, LABEL_6A27
    call LABEL_3B62
LABEL_6A27:
    ld   (ix+$25), $01
    pop  af
    ret
  
LABEL_6A2D:
    ret

LABEL_6A2E:
    ld   bc, ($d424)
    ld   de, ($d426)
    ld   a, ($d501)
    cp   $41
    jr   z, LABEL_6A4E
    cp   $18
    jr   z, LABEL_6A49
    cp   $29
    ret  nz
    ld   bc, $000c
    jr   LABEL_6A51

LABEL_6A49:
    ld   bc, $000e
    jr   LABEL_6A51

LABEL_6A4E:
    ld   bc, $000e
LABEL_6A51:
    ld   de, $fffc
    ld   a, ($d141)
    rrca
    ret  nc
    ld   a, ($d529)
    ld   d, $00
    ld   e, a
    dec  de
    ld   a, d
    cpl
    ld   d, a
    ld   a, e
    cpl
    ld   e, a
    ld   hl, $0001
    xor  a
    add  hl, de
    ex   de, hl
    ret

LABEL_6A6D:
    ld   bc, ($d420)
    ld   de, ($d422)
    ld   a, ($d501)
    cp   $41
    jr   z, LABEL_6A8D
    cp   $18
    jr   z, LABEL_6A88
    cp   $29
    ret  nz
    ld   bc, $fff4
    jr   LABEL_6A51

LABEL_6A88:
    ld   bc, $fff2
    jr   LABEL_6A51

LABEL_6A8D:
    ld   bc, $fff2
    jr   LABEL_6A51

LABEL_6A92:
    res  0, (ix+$22)
    ld   a, ($d3c4)
    or   a
    jr   nz, LABEL_6AB1
    ld   a, ($d501)
    cp   $29
    jr   z, LABEL_6AB1
    cp   $23
    jr   z, LABEL_6AB1
    bit  1, (ix+$22)
    ret  nz
    bit  7, (ix+$19)
    ret  z
LABEL_6AB1:
    ld   bc, $0000
    ld   a, ($d529)
    ld   d, $00
    ld   e, a
    dec  de
    ld   a, d
    cpl
    ld   d, a
    ld   a, e
    cpl
    ld   e, a
    call LABEL_6FA6
    call LABEL_6D91
    ld   a, ($d411)
    ld   b, a
    and  $1f
    cp   $0d
    jp   z, LABEL_6B70
    cp   $15
    jp   z, LABEL_6BDD
    cp   $05
    jp   z, LABEL_6BFC
    cp   $1c
    jp   z, LABEL_6C13
    cp   $0f
    jp   z, LABEL_6B9B
    cp   $09
    jp   z, LABEL_6BAB
    cp   $10
    jp   z, LABEL_6B3C
    bit  7, b
    ret  z
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    ld   l, a
    ld   de, ($d514)
    ld   a, e
    and  $e0
    ld   e, a
    xor  a
    sbc  hl, de
    ret  z
LABEL_6B06:
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    ld   b, a
    and  $3f
    ret  z
    bit  6, b
    jr   nz, LABEL_6B19
    ld   a, $20
LABEL_6B19:
    cp   c
    ret  c
    sub  c
    ld   c, a
    ld   b, $00
    ld   hl, ($d514)
    add  hl, bc
    ld   ($d514), hl
    set  0, (ix+$22)
    ld   hl, $0000
    ld   ($d518), hl
    ld   a, ($d501)
    cp   $0a
    ret  nz
    ld   a, $1b
    ld   ($d502), a
    ret

LABEL_6B3C:
    ld   a, ($d501)
    cp   $12
    jr   nz, LABEL_6B5E
    ld   hl, ($d3c9)
    ld   a, h
    or   l
    ret  z
    ld   iy, ($d3c9)
    ld   (iy+$02), $02
    ld   hl, $0000
    ld   ($d3c9), hl
    xor  a
    ld   ($d532), a
    jp   LABEL_3B6F

LABEL_6B5E:
    ld   a, (ix+$01)
    cp   $18
    jr   z, LABEL_6B68
    cp   $27
    ret  nz
LABEL_6B68:
    ld   a, $ff
    ld   ($dbce), a
    jp   LABEL_3B62

LABEL_6B70:
    ld   a, ($d3c4)
    or   a
    jp   z, LABEL_7228
    jp   Player_SetSate_Dead

LABEL_6B7A:
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    ld   l, a
    ld   a, ($d415)
    bit  6, a
    jr   nz, LABEL_6B8A
    ld   a, $20
LABEL_6B8A:
    and  $3f
    ld   e, a
    ld   d, $00
    add  hl, de
    ld   de, ($d514)
    xor  a
    sbc  hl, de
    ret  nc
    jp   LABEL_6B06

LABEL_6B9B:
    ld   hl, ($d516)
    dec  hl
    ld   a, h
    cpl
    ld   h, a
    ld   a, l
    cpl
    ld   l, a
    ld   de, $0200
    jp   LABEL_72B2

LABEL_6BAB:
    ld   a, ($d501)
    cp   $29
    ret  z
    cp   $11
    ret  z
    cp   $43
    ret  z
    bit  0, (ix+$22)
    ret  z
    ld   a, (ix+$19)
    rlca
    ret  nc
    ld   hl, $0700
    ld   ($d518), hl
    res  0, (ix+$22)
    res  0, (ix+$23)
    res  1, (ix+$22)
    res  1, (ix+$23)
    ld   a, $b5
    ld   (Sound_SFX_Trigger), a
    ret

LABEL_6BDD:
    ld   a, ($d501)
    cp   $29
    ret  z
    bit  0, (ix+$22)
    ret  z
    ld   hl, $0380
    ld   ($d518), hl
    ld   hl, $0400
    ld   ($d516), hl
    ld   a, $b5
    ld   (Sound_SFX_Trigger), a
    jp   LABEL_3D99

LABEL_6BFC:
    ld   hl, $0200
    ld   ($d518), hl
    res  0, (ix+$22)
    bit  7, (ix+$03)
    ret  nz
    ld   a, $50
    ld   ($dbd0), a
    jp   LABEL_384A

LABEL_6C13:
    ld   a, ($d501)
    cp   $13
    ret  z
    cp   $0d
    ret  z
    cp   $0c
    ret  z
    res  0, (ix+$22)
    ld   a, ($d400)
    cp   $54
    jr   z, LABEL_6C35
    cp   $5d
    jr   z, LABEL_6C35
    cp   $ae
    jr   z, LABEL_6C35
    jp   LABEL_6C5E

LABEL_6C35:
    call LABEL_6C8E
    ld   hl, ($d518)
    bit  7, h
    ret  z
    ld   de, $fd00
    xor  a
    sbc  hl, de
    jp   nc, LABEL_6C87
    ld   a, (ix+$01)
    cp   $35
    ret  z
    ld   a, ($d400)
    cp   $5d
    ret  nz
    ld   hl, $fec0
    ld   ($d516), hl
    ld   (ix+$02), $35
    ret

LABEL_6C5E:
    call LABEL_6CC7
    ld   hl, ($d518)
    bit  7, h
    ret  z
    ld   de, $fd00
    xor  a
    sbc  hl, de
    jp   nc, LABEL_6C87
    ld   a, (ix+$01)
    cp   $36
    ret  z
    ld   a, ($d400)
    cp   $fd
    ret  nz
    ld   hl, $0140
    ld   ($d516), hl
    ld   (ix+$02), $36
    ret

LABEL_6C87:
    ld   hl, $0100
    ld   ($d518), hl
    ret

LABEL_6C8E:
    ld   a, ($d414)
    and  $3f
    ret  z
    ld   c, a
    ld   hl, ($d405)
    ld   de, $0020
    add  hl, de
    ld   a, l
    and  $e0
    ld   l, a
    ld   b, $00
    xor  a
    sbc  hl, bc
    ex   de, hl
    ld   hl, ($d405)
    sbc  hl, de
    ret  c
    ld   c, l
    ld   b, h
    ld   hl, ($d511)
    xor  a
    sbc  hl, bc
    ld   ($d511), hl
    set  2, (ix+$22)
    bit  4, (ix+$04)
    ret  nz
    ld   hl, $0000
    ld   ($d516), hl
    ret

LABEL_6CC7:
    ld   a, ($d414)
    and  $3f
    ret  z
    ld   b, a
    ld   a, ($d405)
    and  $1f
    sub  b
    ret  nc
    neg
    dec  a
    ld   c, a
    ld   b, $00
    ld   hl, ($d511)
    add  hl, bc
    ld   ($d511), hl
    set  3, (ix+$22)
    bit  4, (ix+$04)
    ret  z
    ld   hl, $0000
    ld   ($d516), hl
    ret

LABEL_6CF2:
    ld   a, ($d501)
    cp   $29
    ret  z
    ld   a, ($d411)
    and  $1f
    cp   $09
    ret  z
    ld   a, ($d419)
    and  $1f
    cp   $09
    ret  z
    ld   a, ($d501)
    cp   $29
    jr   z, LABEL_6D18
    cp   $41
    jr   z, LABEL_6D18
    bit  7, (ix+$19)
    ret  z
LABEL_6D18:
    ld   bc, $0008
    bit  7, (ix+$17)
    jr   z, LABEL_6D24
    ld   bc, $fff8
LABEL_6D24:
    ld   de, $fff5
    ld   d, $00
    ld   e, (ix+$29)
    dec  de
    ld   a, d
    cpl
    ld   d, a
    ld   a, e
    cpl
    ld   e, a
    call LABEL_6FA6
    ld   a, ($d411)
    bit  7, a
    ret  z
    and  $1f
    cp   $00
    ret  z
    cp   $0b
    ret  z
    cp   $16
    ret  z
    cp   $01
    ret  z
    cp   $0d
    ret  z
    cp   $03
    ret  z
    cp   $11
    ret  z
    cp   $10
    jp   z, LABEL_6B3C
    cp   $04
    jp   z, LABEL_6B3C
    cp   $12
    ret  z
    cp   $17
    ret  z
    cp   $15
    ret  z
    cp   $0a
    ret  z
    cp   $05
    ret  z
    cp   $0f
    jp   z, LABEL_6B9B
    cp   $1c
    ret  z
    ld   hl, $0000
    ld   ($d516), hl
    ld   ($d518), hl
    set  0, (ix+$22)
    bit  7, (ix+$17)
    jr   nz, LABEL_6D8C
    set  2, (ix+$22)
    ret

LABEL_6D8C:
    set  3, (ix+$22)
    ret

LABEL_6D91:
    ld   a, ($d411)
    bit  7, a
    ret  z
    and  $1f
    cp   $0a
    ret  z
    bit  7, (ix+$19)
    ret  z
    ld   a, ($d415)
    bit  6, a
    jp   z, LABEL_6DDC
LABEL_6DA9:
    ld   a, ($d407)
    and  $1f
    ld   c, a
    ld   a, ($d415)
    and  $1f
    sub  c
    ret  c
    ld   hl, ($d514)
    ld   d, $00
    ld   e, a
    add  hl, de
    ld   ($d514), hl
    set  0, (ix+$22)
    ld   a, ($d411)
    ld   b, a
    and  $1f
    cp   $1c
    ret  z
    ld   a, ($d411)
    and  $1f
    cp   $09
    ret  z
    ld   hl, $0000
    ld   ($d518), hl
    ret

LABEL_6DDC:
    ld   a, ($d411)
    and  $1f
    cp   $01
    jr   z, LABEL_6DEA
    cp   $02
    jr   z, LABEL_6DEA
    ret

LABEL_6DEA:
    ld   a, ($d400)
    cp   $19
    ret  z
    cp   $1a
    ret  z
    cp   $a4
    jr   z, LABEL_6E0E
    cp   $a5
    jr   z, LABEL_6E0E
    cp   $a6
    jr   z, LABEL_6E0E
    cp   $a7
    jr   z, LABEL_6E0E
    cp   $a9
    jr   z, LABEL_6E0E
    ld   a, $1f
    ld   ($d415), a
    jr   LABEL_6DA9

LABEL_6E0E:
    ld   a, $20
    ld   ($d415), a
    jp   LABEL_6DA9

LABEL_6E16:
    ld   a, ($d501)
    cp   $16
    jr   z, LABEL_6E21
    cp   $17
    jr   nz, LABEL_6E29
LABEL_6E21:
    ld   bc, $0000
    ld   de, $ffe2
    jr   LABEL_6E38

LABEL_6E29:
    ld   bc, $0000
    ld   de, $ffe6
    bit  0, (ix+$07)
    jr   z, LABEL_6E38
    ld   de, $fff0
LABEL_6E38: 
    ld   a, ($d53e)
    or   a
    jr   z, LABEL_6E41
    ld   de, $fff0
LABEL_6E41:
    ld   a, ($d41d)
    ld   ($d41e), a
    call LABEL_709A
    ld   ($d41d), a
    and  $1f
    cp   $10
    jp   z, LABEL_6B3C
    cp   $04
    jp   z, LABEL_6B3C
    cp   $07
    jr   z, LABEL_6E7D
    cp   $1a
    jp   z, LABEL_6F2C
    cp   $14
    jp   z, LABEL_6F44
    cp   $13
    jp   z, LABEL_72F1
    ld   a, ($d400)
    cp   $4f
    jp   z, LABEL_72F1
    ld   a, ($d400)
    cp   $bf
    jp   z, LABEL_72F1
    ret

LABEL_6E7D:
    ld   a, ($d12b)
    push af
    ld   a, ($d188)
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, ($d400)
    sub  $64
    ld   ($d3de), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    add  hl, hl
    ld   de, DATA_6EFB
    add  hl, de
    ld   a, ($d405)
    rrca
    rrca
    rrca
    rrca
    and  $01
    ld   c, a
    ld   b, $00
    add  hl, bc
    ld   a, ($d407)
    rrca
    rrca
    rrca
    and  $02
    ld   e, a
    ld   d, $00
    add  hl, de
    ld   a, (hl)
    or   a
    jp   z, LABEL_6EF3
    ex   de, hl
    add  hl, bc
    ex   de, hl
    ld   bc, $0018
    add  hl, bc
    ld   a, (hl)
    ld   hl, ($d401)
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    ld   hl, ($d405)
    ld   ($d409), hl
    ld   hl, ($d407)
    ld   ($d40b), hl
    ld   c, $03
    ld   h, $00
    call Engine_AllocateLinkedObject
    ld   a, $c9
    ld   (Sound_SFX_Trigger), a
    call Engine_AddRing
LABEL_6EF3:
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

DATA_6EFB:
.db $FF,  $FF,  $00,  $00,  $00,  $00,  $FF,  $FF
.db $FF,  $00,  $00,  $00,  $00,  $FF,  $00,  $00
.db $00,  $00,  $FF,  $00,  $00,  $00,  $00,  $FF
.db $67,  $66,  $6A,  $6A,  $6A,  $6A,  $69,  $68
.db $6A,  $6A,  $6A,  $6A,  $6A,  $6A,  $6A,  $6A
.db $6A,  $6A,  $6A,  $6A,  $6A,  $6A,  $6A,  $6A

LABEL_6F2B:
    ret

LABEL_6F2C:
    bit  1, (ix+$22)
    ret  z
    ld   hl, $0700
    ld   ($d3d6), hl
    ld   ($d516), hl
    res  0, (ix+$03)
    ld   a, $c7
    ld   (Sound_SFX_Trigger), a
    ret

LABEL_6F44:
    ld   a, ($d501)
    cp   $29
    ret  z
    set  0, (ix+$24)
    res  2, (ix+$24)
    call LABEL_5854
    ld   a, ($d501)
    cp   $11
    ret  z
    cp   $43
    ret  z
    ld   a, ($d514)
    and  $10
    ret  nz
    ld   a, ($d519)
    bit  7, a
    ret  nz
    ld   hl, $0400
    res  4, (ix+$04)
    ld   a, ($d400)
    cp   $5a
    jr   z, LABEL_6F83
    cp   $5b
    jr   z, LABEL_6F83
    ld   hl, $fc00
    set  4, (ix+$04)
LABEL_6F83:
    ld   ($d516), hl
    ld   hl, $fa80
    ld   a, $b5
    ld   (Sound_SFX_Trigger), a
    ld   a, ($d501)
    cp   $16
    ret  z
    cp   $23
    jr   z, LABEL_6F9F
    xor  a
    ld   ($d3f0), a
    jp   LABEL_3DE4

LABEL_6F9F:
    ld   (ix+$19), h
    ld   (ix+$18), l
    ret

LABEL_6FA6:
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   h, (ix+$12)
    ld   l, (ix+$11)
    add  hl, bc
    ld   ($d405), hl
    sla  l
    rl   h
    sla  l
    rl   h
    sla  l
    rl   h
    ld   b, h
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    add  hl, de
    ld   de, $0012
    add  hl, de
    bit  7, h
    jr   z, LABEL_6FDB
    ld   hl, $0000
LABEL_6FDB:
    ld   ($d407), hl
    ld   a, h
    ld   h, $00
    rlc  l
    rla
    rlc  l
    rla
    rlc  l
    rla
    add  a, a
    ld   l, a
    ld   de, ($d18e)
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   l, b
    ld   h, $00
    add  hl, de
    ld   de, $c001
    add  hl, de
    ld   a, h
    and  $f0
    cp   $c0
    jp   nz, LABEL_7122
    ld   ($d401), hl
    ld   a, (hl)
    ld   ($d400), a
    push af
    ld   a, (ix+$00)
    cp   $02
    jr   nz, LABEL_7042
    ld   a, (ix+$01)
    cp   $18
    jr   z, LABEL_7029
    cp   $27
    jr   z, LABEL_7029
    cp   $43
    jr   nz, LABEL_7042
    ld   a, (CurrentLevel)
    cp   $02
    jr   nz, LABEL_7042
LABEL_7029: 
    ld   a, ($d400)
    cp   $a7
    jr   z, +
    cp   $a8
    jr   z, +
    cp   $a9
    jr   z, +
    jr   LABEL_7042
+:  pop  af
    ld   a, $01
    ld   ($d400), a
    jr   LABEL_7043

LABEL_7042:
    pop  af
LABEL_7043:
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   de, (CollisionDataPtr)
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    ld   a, (hl)
    ld   ($d411), a
    bit  5, a
    jr   z, +
    ld   a, (ix+$25)
    or   a
    jr   z, +
    ld   de, $0007
    add  hl, de
    ld   a, (hl)
    ld   ($d411), a
+:  inc  hl
    ld   a, (hl)
    ld   ($d100), a
    inc  hl
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    ex   de, hl
    ld   a, ($d405)
    and  $1f
    ld   l, a
    ld   h, $00
    add  hl, bc
    ld   a, (hl)
    ld   ($d415), a
    ex   de, hl
    inc  hl
    ld   c, (hl)
    inc  hl
    ld   b, (hl)
    ex   de, hl
    ld   a, ($d407)
    and  $1f
    ld   l, a
    ld   h, $00
    add  hl, bc
    ld   a, (hl)
    ld   ($d414), a
    ex   de, hl
    inc  hl
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_709A:
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   h, (ix+$12)
    ld   l, (ix+$11)
    add  hl, bc
    ld   ($d405), hl
    srl  h
    rr   l
    srl  h
    rr   l
    srl  h
    rr   l
    srl  h
    rr   l
    srl  h
    rr   l
    ld   b, h
    ld   c, l
    ld   h, (ix+$15)
    ld   l, (ix+$14)
    add  hl, de
    ld   de, $0012
    add  hl, de
    bit  7, h
    jr   z, LABEL_70D8
    ld   hl, $0000
LABEL_70D8:
    ld   ($d407), hl
    ld   a, h
    ld   h, $00
    rlc  l
    rla
    rlc  l
    rla
    rlc  l
    rla
    add  a, a
    ld   l, a
    ld   de, ($d18e)
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   l, c
    ld   h, b
    add  hl, de
    ld   de, $c001
    add  hl, de
    ld   a, h
    and  $f0
    cp   $c0
    jp   nz, LABEL_7122
    ld   ($d401), hl
    ld   a, (hl)
    ld   ($d400), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   de, (CollisionDataPtr)
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ld   a, (de)
    ld   ($d411), a
LABEL_7117:
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, ($d411)
    ret

LABEL_7122:
    ld   hl, $cfff
    ld   ($d401), hl
    ld   a, $ff
    ld   ($d400), a
    ld   a, $00
    ld   ($d411), a
    ld   a, $00
    ld   ($d414), a
    ld   ($d415), a
    jp   LABEL_7117

LABEL_713D:
    res  1, (ix+$22)
    res  6, (ix+$24)
    ld   a, (ix+$00)
    cp   $01
    jr   z, LABEL_7152
    cp   $02
    jr   z, LABEL_7152
    jr   LABEL_716A

LABEL_7152:
    ld   a, (ix+$1d)
    or   a
    jr   nz, LABEL_7160
    ld   a, (ix+$1c)
    cp   $a9
    jp   c, LABEL_716A
LABEL_7160:
    xor  a
    ld   ($d411), a
    ld   ($d400), a
    jp   LABEL_7176

LABEL_716A:
    ld   bc, $0000
    ld   de, $0000
    call LABEL_6FA6
    call LABEL_670D
LABEL_7176:
    ld   a, ($d411)
    and  $1f
    add  a, a
    ld   l, a
    ld   h, $00
    ld   de, DATA_7188
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    ex   de, hl
    jp   (hl)

DATA_7188:
.dw LABEL_71C7
.dw LABEL_71C6
.dw LABEL_71C6
.dw LABEL_71D2
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7 
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7 
.dw LABEL_71D1
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71C6
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7
.dw LABEL_71D7 
.dw LABEL_71D7
.dw LABEL_71C6
.dw LABEL_71C6
.dw LABEL_71C6
.dw LABEL_71C6
.dw LABEL_71D7
.dw LABEL_71D7

LABEL_71C6: 
    ret

LABEL_71C7:
    bit  0, (ix+$03)
    ret  nz
    set  4, (ix+$03)
    ret

LABEL_71D1
    ret

LABEL_71D2:
    set  6, (ix+$24)
    ret

LABEL_71D7
    ret

LABEL_71D8:
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, ($d188)
    call Engine_SwapFrame2
    ld   a, $6a
    ld   hl, ($d401)
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    ld   hl, ($d405)
    ld   a, l
    and  $e0
    add  a, $10
    ld   l, a
    ex   de, hl
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    add  a, $08
    ld   l, a
    ld   b, $07
    ld   c, $03
    call Engine_AllocateLinkedObjectXY
    call Engine_Add10Rings
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_7228:
    ld   a, ($d12b)
    push af
    ld   a, $0e
    ld   ($d12b), a
    ld   ($ffff), a
    ld   a, ($d188)
    call Engine_SwapFrame2
    ld   a, $8f
    ld   hl, ($d401)
    ld   (hl), a
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   a, ($d18a)
    ld   c, a
    ld   a, ($d18b)
    ld   b, a
    add  hl, bc
    ld   e, (hl)
    inc  hl
    ld   d, (hl)
    call LABEL_2CEB
    ld   hl, ($d405)
    ld   a, l
    and  $e0
    ld   l, a
    ld   ($d409), hl
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    ld   l, a
    ld   ($d40b), hl
    ld   a, $b2
    ld   (Sound_SFX_Trigger), a
    ld   c, $0a
    ld   a, ($d411)
    and  $1f
    cp   $05
    jr   nz, +
    ld   c, $2e
+:  push ix
    pop  hl
    ld   ($dbc4), hl
    ld   a, ($d400)
    ld   h, $01
    call Engine_AllocateLinkedObject
    ld   h, $02
    call Engine_AllocateLinkedObject
    ld   h, $03
    call Engine_AllocateLinkedObject
    ld   h, $04
    call Engine_AllocateLinkedObject
    ld   h, $05
    call Engine_AllocateLinkedObject
    ld   h, $06
    call Engine_AllocateLinkedObject
    ld   h, $07
    call Engine_AllocateLinkedObject
    ld   h, $08
    call Engine_AllocateLinkedObject
    pop  af
    ld   ($d12b), a
    ld   ($ffff), a
    ret

LABEL_72B2:
    ld   ($d516), hl
    ld   ($d518), de
    ld   a, $ca
    ld   (Sound_SFX_Trigger), a
    ld   a, (CurrentLevel)
    cp   $06
    jp   z, LABEL_72E1
    ld   b, $39
    ld   c, $01
    ld   de, ($d405)
    ld   a, e
    and  $e0
    add  a, $10
    ld   e, a
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    ld   l, a
    call Engine_AllocateLinkedObjectXY
    or   a
    jr   z, LABEL_72E7
LABEL_72E1:
    ld   c, $00
    call LABEL_6528
    ret

LABEL_72E7:
    ld   hl, ($d401)
    ld   (iy+$39), h
    ld   (iy+$38), l
    ret

LABEL_72F1:
    ld   a, ($d501)
    cp   $11
    ret  z
    cp   $29
    ret  z
    cp   $26
    ret  z
    cp   $43
    ret  z
    ld   a, ($d3c4)
    or   a
    ret  nz
    ld   a, ($d41b)
    ld   ($d41c), a
    ld   a, ($d400)
    ld   ($d41b), a
    cp   $4f
    jr   nz, LABEL_7319
    ld   a, $0a
    jr   LABEL_7323

LABEL_7319:
    cp   $bf
    jr   nz, LABEL_7321
    ld   a, $0a
    jr   LABEL_7323

LABEL_7321:
    sub  $b0
LABEL_7323:
    add  a, a
    ld   hl, DATA_7330
    ld   d, $00
    ld   e, a
    add  hl, de
    ld   e, (hl)
    inc  hl
    ld   h, (hl)
    ld   l, e
    jp   (hl)

DATA_7330:
.dw LABEL_734E
.dw LABEL_7354
.dw LABEL_735A
.dw LABEL_7378 
.dw LABEL_7396
.dw LABEL_73B4
.dw LABEL_73D2
.dw LABEL_73FA
.dw LABEL_7422
.dw LABEL_744A
.dw LABEL_7472
.dw LABEL_734E
.dw LABEL_734E
.dw LABEL_734E 
.dw LABEL_734E

LABEL_734E:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ret  ;FIXME: more pointless code

LABEL_7354:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ret  ;FIXME: identical to above routine. Still pointless

LABEL_735A:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    call LABEL_74B1
    ld   hl, ($d516)
    ld   a, h
    or   l
    jr   z, LABEL_7375
    jp   LABEL_740F

LABEL_7375:
  ;FIXME: just put this address in the jump table above
    jp   LABEL_7437

LABEL_7378:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    call LABEL_74B1
    ld   hl, ($d516)
    ld   a, h
    or   l
    jr   z, LABEL_7393
    jp   LABEL_740F

LABEL_7393:
    jp   LABEL_745F

LABEL_7396:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    call LABEL_74B1
    ld   hl, ($d516)
    ld   a, h
    or   l
    jr   z, LABEL_73B1
    jp   LABEL_73E7

LABEL_73B1
    jp   LABEL_7437

LABEL_73B4:
    ld   a, (ix+$3e)
    or   a
    ret  z
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    call LABEL_74B1
    ld   hl, ($d516)
    ld   a, h
    or   l
    jr   z, LABEL_73CF
    jp   LABEL_73E7

LABEL_73CF
    jp   LABEL_745F

LABEL_73D2:
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    ld   a, $b6
    call LABEL_7493
    or   a
    jp   nz, LABEL_74CF
    call LABEL_74B1
LABEL_73E7:
    ld   hl, $0000
    ld   ($d516), hl
    ld   de, $0580
    ld   ($d518), de
    ld   a, $00
    call LABEL_74D0
    ret

LABEL_73FA:
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    ld   a, $b7
    call LABEL_7493
    or   a
    jp   nz, LABEL_74CF
    call LABEL_74B1
LABEL_740F:
    ld   hl, $0000
    ld   ($d516), hl
    ld   de, $fa80
    ld   ($d518), de
    ld   a, $00
    call LABEL_74D0
    ret

LABEL_7422:
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    ld   a, $b8
    call LABEL_7493
    or   a
    jp   nz, LABEL_74CF
    call LABEL_74B1
LABEL_7437:
    ld   hl, $fa80
    ld   ($d516), hl
    ld   de, $0000
    ld   ($d518), de
    ld   a, $01
    call LABEL_74D0
    ret

LABEL_744A:
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    ld   a, $b9
    call LABEL_7493
    or   a
    jp   nz, LABEL_74CF
    call LABEL_74B1
LABEL_745F:
    ld   hl, $0580
    ld   ($d516), hl
    ld   de, $0000
    ld   ($d518), de
    ld   a, $01
    call LABEL_74D0
    ret

LABEL_7472:
    ld   a, ($d501)
    cp   $21
    ret  nz
    ld   a, ($d41c)
    ld   b, a
    ld   a, ($d41b)
    cp   b
    ret  z
    call LABEL_74B1
    ld   hl, $0000
    ld   ($d516), hl
    ld   ($d518), hl
    ld   a, $22
    ld   ($d502), a
    ret

LABEL_7493:
    cp   (ix+$3e)
    jr   z, LABEL_74A1
    ld   a, ($d501)
    cp   $21
    jr   z, LABEL_74AE
    xor  a
    ret

LABEL_74A1:
    ld   a, $c4
    ld   (Sound_SFX_Trigger), a
    ld   (ix+$02), $40
    ld   (ix+$3e), $ff
LABEL_74AE:
    ld   a, $ff
    ret

LABEL_74B1:
    ld   a, $21
    ld   ($d502), a
    ld   hl, ($d405)
    ld   a, l
    and  $e0
    or   $10
    ld   l, a
    ld   ($d511), hl
    ld   hl, ($d407)
    ld   a, l
    and  $e0
    or   $0c
    ld   l, a
    ld   ($d514), hl
    ret

LABEL_74CF:
    ret

LABEL_74D0:
    or   a
    jr   nz, LABEL_74DF
    ld   a, $b7
    bit  7, d
    jr   z, LABEL_74DB
    ld   a, $b6
LABEL_74DB:
    ld   ($d53e), a
    ret

LABEL_74DF:
    ld   a, $b8
    bit  7, h
    jr   z, LABEL_74E7
    ld   a, $b9
LABEL_74E7:
    ld   ($d53e), a
    ret

DATA_74EB:
.db $BE, $BA, $C4, $B4, $BC, $BA, $00, $00
.db $D8, $00, $00, $00 

LABEL_74F7:
    ld   a, $1a
    call Engine_SwapFrame2
    di
    call VDP_ClearVRAM
    ld   iy, $B720
    ld   a, (PlayerObjectID)
    cp   $01
    jp   z, Engine_ParseTilesetList
    ld   iy, $B727
    jp   Engine_ParseTilesetList

LABEL_7513:
    di
    ld   a, $1a
    call Engine_SwapFrame2
    ld   iy, $B743
    jp   Engine_ParseTilesetList



Engine_ClearVRAMLoadLevelTiles:      ; $7520
    di
    call VDP_ClearVRAM
    ; FALL THROUGH

; =============================================================================
;  Engine_LoadLevelTiles()
; -----------------------------------------------------------------------------
;  Parse the level/act's tileset header entries and decompress the tilesets
;  into VRAM.
; -----------------------------------------------------------------------------
;  In:
;    (CurrentLevel)
;    (CurrentAct)
;  Out:
;    None.
; -----------------------------------------------------------------------------
Engine_LoadLevelTiles:      ; $7524
    di
    ld   a, :Data_ZoneTilesets
    call Engine_SwapFrame2
    ld   a, (CurrentLevel)
    ld   b, a
    add  a, a
    add  a, b
    ld   b, a
    ld   a, (CurrentAct)
    add  a, b
    ld   l, a
    ld   h, $00
    ld   c, a
    ld   b, $00
    add  hl, hl
    add  hl, hl
    add  hl, hl
    xor  a
    sbc  hl, bc
    ld   de, Data_ZoneTilesets
    add  hl, de
    push hl
    pop  iy
    ; FALL THROUGH

; ==============================================================================
;  Engine_ParseTilesetList
; ------------------------------------------------------------------------------
;  Parse a level tileset list and read the data into VRAM.
; ------------------------------------------------------------------------------
;  In:
;    IY     - Pointer to the head of the tileset list.
;  Out:
;    None
; ------------------------------------------------------------------------------
Engine_ParseTilesetList:                        ; $7548
    ; read the main level background tiles into VRAM
    ld   l, (iy+$01)
    ld   h, (iy+$02)
    call VDP_SetVRAMPointer
    ld   l, (iy+$03)
    ld   h, (iy+$04)
    ld   a, (iy+$00)
    call Engine_SwapFrame2
    xor  a
    call Engine_DecompressTileset
    ; get the pointer to the object art chain
    ld   a, :Data_ZoneTilesets
    call Engine_SwapFrame2
    ld   l, (iy+$05)
    ld   h, (iy+$06)
    push hl
    pop  iy

-:  ; read a object art entry
    ld   a, :Data_ZoneTilesets
    call Engine_SwapFrame2
    ; check for end of list
    ld   a, (iy+$00)
    cp   $ff
    jp   z, +
    ; set the VRAM address
    ld   l, (iy+$01)
    ld   h, (iy+$02)
    di
    call VDP_SetVRAMPointer
    ; read the data pointer & bank
    ld   l, (iy+$03)
    ld   h, (iy+$04)
    ld   a, (iy+$00)
    ; swap in the data bank (ignore the hi bit mirroring flag)
    push af
    and  $1f
    call Engine_SwapFrame2
    ; restore the mirror bit & decompress
    pop  af
    and  $80
    call Engine_DecompressTileset
    ; go to the next entry
    ld   bc, $0005
    add  iy, bc
    jr   -

+:  ld   a, (CurrentLevel)
    cp   Level_ADZ
    ret  nz
    ld   a, (CurrentAct)
    cp   Act_3
    ret  nz
    ; ADZ act 3 needs some more work
    ld   hl, ($d169)
    ld   a, h
    or   l
    ret  z
    ld   hl, $0000
    ld   ($d169), hl
    call LABEL_75C5
    di
    ld   iy, $B8C8
    jp   LABEL_7A58



LABEL_75C5:
    di
    ld   a, $0a
    call Engine_SwapFrame2
    ld   hl, $9040
    ld   de, $0a00
    ld   bc, $00c0
    jp   VDP_CopyToVRAM

LABEL_75D7:
    di
    ld   a, $0a
    call Engine_SwapFrame2
    ld   hl, $9040
    ld   de, $1d00
    ld   bc, $00c0
    call VDP_CopyToVRAM
    ld   hl, $9700
    ld   a, (PlayerObjectID)
    cp   $01
    jr   z, LABEL_75F6
    ld   hl, $9B80
LABEL_75F6:
    ld   de, $1C40
    ld   bc, $00C0
    call VDP_CopyToVRAM
    ret

; ==============================================================================
;  Engine_LoadNackExplosionArt
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_LoadNackExplosionArt:                    ; $7600
    ld   a, (CurrentBank)
    push af
    di
    ld   a, :Art_NackExplosion
    call Engine_SwapFrame2
    call VDP_SetVRAMPointer
    ld   hl, Art_NackExplosion
    xor  a
    call Engine_DecompressTileset
    pop  af
    ld   (CurrentBank), a
    ld   ($ffff), a
    ret


; ==============================================================================
;  Engine_LoadInvincibilityArt
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_LoadInvincibilityArt:          ; $761C
    di
    ld   a, :Art_InvincibilityStar
    call Engine_SwapFrame2
    call VDP_SetVRAMPointer
    ld   hl, Art_InvincibilityStar
    xor  a
    jp   Engine_DecompressTileset


Engine_LoadCompressedRingArt: ;$762C
  ;save current banking
    ld   a, ($D12B)
    push af
    xor  a
    ld   ($D12C), a
  ;swap in the bank with the art
    di
    ld   a, :Bank29
    call Engine_SwapFrame2
  ;set the VRAM pointer
    ld   hl, $0200
    call VDP_SetVRAMPointer
  ;copy the art to VRAM
    di
    ld   hl, Art_cmp_Rings
    xor  a
    call Engine_DecompressTileset
  ;restore original banking
    pop  af
    ld   ($D12B), a
    ld   ($FFFF), a
    ret


Engine_LoadLevelPalette:    ;$7650
  ;swap in the bank with the palettes
    ld   a, :Bank14
    call Engine_SwapFrame2
  
  ;get the palette index for the current level/act
    ld   a, (CurrentLevel)
    ld   b, a
    add  a, a
    add  a, b
    ld   b, a
    ld   a, (CurrentAct)
    add  a, b
    ld   l, a
    ld   h, $00
    add  hl, hl
    ld   de, DATA_B14_BF1C
    add  hl, de
  
  ;set the palette indices
    ld   a, (hl)
    ld   (BgPaletteIndex), a
    inc  hl
    ld   a, (hl)
    ld   (FgPaletteIndex), a
  
  ;flag the bg palette control for a fade to colour
    ld   hl, BgPaletteControl
    ld   (hl), $00
    set  7, (hl)
  
  ;flag the fg palette control for a fade to colour
    inc  hl
    inc  hl
    ld   (hl), $00
    set  7, (hl)

  ;does this level have an underwater palette?
    ld   de, ($D3E7)
    ld   a, d
    or   e
    ret  z
  
  ;calculate index for the underwater palettes
    ld   a, (CurrentLevel)
    add  a, a
    ld   hl, DATA_B14_BF5E
    ld   d, $00
    ld   e, a
    add  hl, de
  
  ;set the palette indices
    ld   a, (hl)
    ld   (AltBgPaletteIndex), a
    inc  hl
    ld   a, (hl)
    ld   (AltFgPaletteIndex), a
  
  ;flag the palette controls for fade to colour
    ld   hl, AltBgPaletteControl
    ld   (hl), $00
    set  7, (hl)
    inc  hl
    inc  hl
    ld   (hl), $00
    set  7, (hl)
    ret

LABEL_76A7:
  ld   a, (CurrentAct)
  and  $01
  ld   b, a
  ld   a, (PlayerObjectID)
  cp   $01
  ld   a, $2B
  jr   z, +
  ld   a, $2D
+:  add  a, b
    ld   (BgPaletteIndex), a
  ld   a, $2F
  add  a, b
  ld   (FgPaletteIndex), a
  ld   hl, BgPaletteControl
  ld   (hl), $00
  set  7, (hl)
  inc  hl
  inc  hl
  ld   (hl), $00
  set  7, (hl)
  ret


;---------- function start ----------
; (high level)
LABEL_76D0:
  di
  ld   a, $03
  call Engine_SwapFrame2
  ld   hl, $2400
  call VDP_SetVRAMPointer
  ld   hl, $B7EE
  xor  a
  jp   Engine_DecompressTileset


LABEL_76E3:
  di
  ld   a, $0b
  call Engine_SwapFrame2
  ld   hl, $1000
  call VDP_SetVRAMPointer
  ld   hl, $b9c0
  xor  a
  jp   Engine_DecompressTileset

LABEL_76F6:
  ld   a, (GameMode)
  cp   $19
  jr   z, LABEL_7714
  cp   $14
  jr   z, LABEL_7714
  di
  ld   a, $0a
  call Engine_SwapFrame2
  ld   hl, $05c0
  call VDP_SetVRAMPointer
  ld   hl, $acea
  xor  a
  jp   Engine_DecompressTileset

LABEL_7714:
  di
  ld   a, :Bank29
  call Engine_SwapFrame2
  ld   hl, $05c0
  call VDP_SetVRAMPointer
  ld   hl, Art_cmp_Numbers
  xor  a
  jp   Engine_DecompressTileset

LABEL_7727:
  di
  ld   a, :Bank29
  call Engine_SwapFrame2
  ld   hl, $1400
  call VDP_SetVRAMPointer
  ld   hl, Art_cmp_Titlescreen
  xor  a
  call Engine_DecompressTileset

  di
  ld   a, :Art_TitleScreenText
  call Engine_SwapFrame2
  ld   hl, $3720
  call VDP_SetVRAMPointer
  ld   hl, Art_TitleScreenText
  xor  a
  call Engine_DecompressTileset

  di
  ld   a, $1F
  call Engine_SwapFrame2
  ld   hl, $0280
  call VDP_SetVRAMPointer
  ld   hl, $AE7C
  ld   a, $80
  call Engine_DecompressTileset

  di
  ld   a, $04
  call Engine_SwapFrame2
  ld   hl, $0200
  call VDP_SetVRAMPointer
  ld   hl, $B136
  xor  a
  call Engine_DecompressTileset

  ld   a, $10
  ld   ($d780), a
  ld   a, $10
  ld   ($d788), a
  ld   a, $35
  ld   ($d700), a
  di
  
.ifeq SMS_VERSION 1
  ld   de, $00A0
  ld   hl, $3800
  ld   bc, $380
  call VDP_WriteToVRAM
.endif
  
  ld   a, :Mappings_TitleScreen
  call Engine_SwapFrame2
  ld   hl, $38CC
  ld   de, Mappings_TitleScreen
  ld   bc, $1214
  jp   LABEL_2C3B

LABEL_7795:
  di
  ld   a, :Data_Art_Sega
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, Data_Art_Sega
  xor  a
  jp   Engine_DecompressTileset

LABEL_77A8:
  call VDP_ClearVRAM
  di
  ld   a, :Data_Art_Sega_Copyright
  call Engine_SwapFrame2
  ld   hl, $0000
  call VDP_SetVRAMPointer
  ld   hl, Data_Art_Sega_Copyright
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, :Mappings_Sega_Copyright
  call Engine_SwapFrame2
  ld   hl, $3AD6
  ld   de, Mappings_Sega_Copyright
  ld   bc, $010A
  jp   LABEL_2C3B

LABEL_77D0:
  call VDP_ClearVRAM
  di
  ld   a, :Art_PlayerSelect
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, Art_PlayerSelect
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $11
  call Engine_SwapFrame2
  di
  ld   hl, $38cc
  ld   de, $bdee
  ld   bc, $120a
  call LABEL_2C3B
  di
  ld   a, $03
  call Engine_SwapFrame2
  ld   hl, $38e0
  ld   de, $bc7a
  ld   bc, $120a
  jp   LABEL_2C3B

LABEL_780B:
  di
  ld   a, $0a
  call Engine_SwapFrame2
  ld   hl, $a040
  ld   a, (PlayerObjectID)
  cp   $01
  jr   z, LABEL_781E
  ld   hl, $9dc0
LABEL_781E:
  ld   de, $0bc0
  ld   bc, $0080
  jp   VDP_CopyToVRAM

; sets up the titlecard
TitleCard_LoadTiles:        ; $7827
  di
  ld   a, $09
  call Engine_SwapFrame2
  ld   hl, $0200
  call VDP_SetVRAMPointer
  ld   hl, $b700      ; numbers tile art
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $16
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, $91e2
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $03
  call Engine_SwapFrame2
  ld   hl, $1600
  call VDP_SetVRAMPointer
  ld   hl, $bbee
  xor  a
  call Engine_DecompressTileset
    
  ld   a, Object_TitleCardNumbers
  ld   (ObjectSlotsBadniks), a
    
  ld   a, (PlayerObjectID)
  ld   (Player), a
  ld   a, $3a
  ld   ($d53f), a
  ret

LABEL_7871:
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   hl, $0000
  call VDP_SetVRAMPointer
  ld   hl, $9b16
  ld   a, $ff
  ld   ($d12c), a
  xor  a
  call Engine_DecompressTileset
  xor  a
  ld   ($d12c), a
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   a, (PlayerObjectID)
  ld   de, $b20a
  cp   $01
  jr   z, LABEL_78A0
  ld   de, $b4da
LABEL_78A0:
  ld   hl, $38cc
  ld   bc, $1214
  di
  call LABEL_2C3B
  ld   a, (CurrentAct)
  add  a, a
  ld   d, $00
  ld   e, a
  ld   hl, DATA_790E
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   hl, $39ec
  ld   bc, $0401
  di
  call LABEL_2C3B
LABEL_78C2:
  ld   a, ($d14b)
  or   a
  ret  z
  ld   a, ($d12b)
  push af
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   a, ($d14b)
  cp   $05
  jr   c, LABEL_78DA
  ld   a, $04
LABEL_78DA:
  ld   hl, $3c5a
  ld   ($d11c), hl
  ld   b, a
-:  push bc
  ld   de, $b1ca
  ld   a, (PlayerObjectID)
  cp   $01
  jr   z, LABEL_78EF
  ld   de, $b1d6
LABEL_78EF:
  ld   hl, ($d11c)
  ld   bc, $0302
  di
  call LABEL_2C3B
  ld   hl, ($d11c)
  ld   de, $0006
  add  hl, de
  ld   ($d11c), hl
  pop  bc
  djnz -
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

DATA_790E:
.db $B2, $B1, $BA, $B1, $C2, $B1


LABEL_7914:
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   hl, $0000
  call VDP_SetVRAMPointer
  ld   hl, $9b16
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   a, (PlayerObjectID)
  ld   de, $b7aa
  cp   $01
  jr   z, LABEL_793A
  ld   de, $ba7a
LABEL_793A:
  ld   hl, $38cc
  ld   bc, $1214
  call LABEL_2C3B
  ld   a, ($d17e)
  ld   b, a
  ld   hl, $3c8e
  ld   de, $b1e2
  ld   ($d11c), hl
  ld   ($d11a), de
-:  push bc
  ld   hl, ($d11c)
  ld   de, ($d11a)
  ld   bc, $0202
  di
  call LABEL_2C3B
  ld   hl, ($d11c)
  ld   de, $0006
  add  hl, de
  ld   ($d11c), hl
  ld   hl, ($d11a)
  ld   de, $0008
  add  hl, de
  ld   ($d11a), hl
  pop  bc
  djnz -
  ret

LABEL_797B:
  ld   a, ($d12b)
  push af
  ld   a, $1f
  call Engine_SwapFrame2
  ld   hl, $0200
  call VDP_SetVRAMPointer
  ld   hl, $ae7c
  ld   a, $ff
  ld   ($d12c), a
  xor  a
  call Engine_DecompressTileset
  xor  a
  ld   ($d12c), a
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

; ==============================================================================
;  Engine_LoadNackSwitchArt
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_LoadNackSwitchArt:                   ; $79A2
  ld   a, (CurrentBank)
  push af
  ld   a, :Art_NackSwitch
  call Engine_SwapFrame2
  ld   hl, $1380
  call VDP_SetVRAMPointer
  ld   hl, Art_NackSwitch
  xor  a
  call Engine_DecompressTileset
  pop  af
  ld   (CurrentBank), a
  ld   ($ffff), a
  ret

LABEL_79C0:
  ld   a, ($d12b)
  push af
  ld   a, $09
  call Engine_SwapFrame2
  ld   hl, $1580
  call VDP_SetVRAMPointer
  ld   hl, $bebc
  ld   a, $ff
  ld   ($d12c), a
  xor  a
  call Engine_DecompressTileset
  xor  a
  ld   ($d12c), a
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret


; ==============================================================================
;  Engine_LoadNackExplosionArt2
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
Engine_LoadNackExplosionArt2:               ; $79E7
  ld   a, (CurrentBank)
  push af
  ld   a, :Art_NackExplosion
  call Engine_SwapFrame2
  ld   hl, $1440
  call VDP_SetVRAMPointer
  ld   hl, Art_NackExplosion
  ld   a, $ff
  ld   ($d12c), a
  xor  a
  call Engine_DecompressTileset
  ld   a, $06
  call Engine_SwapFrame2
  ld   hl, $1580
  call VDP_SetVRAMPointer
  ld   hl, $ba8c
  ld   a, $ff
  ld   ($d12c), a
  xor  a
  call Engine_DecompressTileset
  xor  a
  ld   ($d12c), a
  pop  af
  ld   (CurrentBank), a
  ld   ($ffff), a
  ret

LABEL_7A25:
  ld   iy, $b989
  jp   LABEL_7A58

LABEL_7A2C:
  ld   iy, $b98f
  jp   LABEL_7A58

LABEL_7A33:
  ld   iy, $b8de
  jp   LABEL_7A58

LABEL_7A3A:
  ld   iy, $b8d8
  jp   LABEL_7A58

LABEL_7A41:
  ld   iy, $b7b1
  jp   LABEL_7A58

LABEL_7A48:
  ld   a, (CurrentAct)
  or   a
  jr   nz, LABEL_7A54
  ld   iy, $b943
  jr   LABEL_7A58

LABEL_7A54:
  ld   iy, $b953
LABEL_7A58:
  ld   a, ($d12b)
  push af
  ld   a, $ff
  ld   ($d12c), a
LABEL_7A61:
  ld   a, $1a
  call Engine_SwapFrame2
  ld   a, (iy+$00)
  cp   $ff
  jr   z, LABEL_7A93
  ld   l, (iy+$01)
  ld   h, (iy+$02)
  di
  call VDP_SetVRAMPointer
  ld   l, (iy+$03)
  ld   h, (iy+$04)
  ld   a, (iy+$00)
  push af
  and  $1f
  pop  af
  call Engine_SwapFrame2
  and  $80
  call Engine_DecompressTileset
  ld   bc, $0005
  add  iy, bc
  jr   LABEL_7A61

LABEL_7A93:
  xor  a
  ld   ($d12c), a
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

LABEL_7A9F:
  ld   iy, $b9d8
  jp   LABEL_7A58

LABEL_7AA6:
  ld   iy, $b908
  jp   LABEL_7A58

LABEL_7AAD:
  ld   iy, $b918
  jp   LABEL_7A58

LABEL_7AB4:
  ld   iy, $b983
  jp   LABEL_7A58

LABEL_7ABB:
  di
  ld   a, $10
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, $af42
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $10
  call Engine_SwapFrame2
  ld   hl, $3a90
  ld   de, $b0f2
  ld   bc, $0310
  jp   LABEL_2C3B

LABEL_7AE0:
  di
  ld   a, $1a
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, $abf1
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $1f
  call Engine_SwapFrame2
  ld   hl, $3a90
  ld   de, $be8e
  ld   bc, $0311
  call LABEL_2C3B
  ld   a, Music_GameOver
  ld   (Sound_Music_Trigger), a
  ret

LABEL_7B0B:
  di
  ld   a, $14
  call Engine_SwapFrame2
  ld   hl, $2000
  call VDP_SetVRAMPointer
  ld   hl, $bbc0
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $19
  call Engine_SwapFrame2
  ld   hl, $3992
  ld   de, $9a20
  ld   bc, $030f
  call LABEL_2C3B
  ld   a, Music_Continue
  ld   (Sound_Music_Trigger), a
  ret

LABEL_7B36:
  di
  ld   a, $1a
  call Engine_SwapFrame2
  ld   hl, $1800
  call VDP_SetVRAMPointer
  ld   hl, $abf1
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $06
  call Engine_SwapFrame2
  ld   hl, $0480
  call VDP_SetVRAMPointer
  ld   hl, $ba8c
  xor  a
  call Engine_DecompressTileset
  di
  ld   a, $1f
  call Engine_SwapFrame2
  ld   hl, $3a90
  ld   de, $bef4
  ld   bc, $0310
  jp   LABEL_2C3B

LABEL_7B6E:
  ld   a, ($d12b)
  push af
  di
  ld   a, $0a
  call Engine_SwapFrame2
  ld   hl, $be3c
  ld   de, $1c20
  ld   bc, $0040
  call VDP_CopyToVRAM
  call LABEL_75C5
  di
  ld   iy, $b8c8
  call LABEL_7A58
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

LABEL_7B97:
  ld   a, ($d12b)
  push af
  di
  ld   a, $0a
  call Engine_SwapFrame2
  ld   hl, $be3c
  ld   de, $1c20
  ld   bc, $0040
  call VDP_CopyToVRAM
  pop  af
  ld   ($d12b), a
  ld   ($ffff), a
  ret

LABEL_7BB5:
  di
  ld   a, $0d
  call Engine_SwapFrame2
  ld   hl, $2000
  call VDP_SetVRAMPointer
  ld   hl, $b9b0
  xor  a
  call Engine_DecompressTileset
  ld   iy, $b995
  jp   LABEL_7A58


;---------- function start ----------
; (high level)
VDP_ClearVRAM:    ;$7BCF
  di
  call VDP_DisableDisplay
  
  ld   hl, $0000
  ld   de, $0000
  ld   bc, $0400
  call VDP_SetVRAMPointer

-:  ld   a, e
.REPT 16
  out  ($BE), a
.ENDR
  ;decrement the counter and check for 0
  dec  bc
  ld   a, b
  or   c
  jr   nz, -

  jp   VDP_EnableDisplay


;**********************************************************
;*  Reset each colour in both the foreground & background *
;*  palettes to 0.                                        *
;**********************************************************
VDP_ClearPalettes:    ;$7C08
  di
  ld   hl, $C000
  ld   de, $0000
  ld   bc, 2 * PaletteSize_Bytes  ;$0040
  jp   VDP_WriteToVRAM


LABEL_7C15:
  ld   a, (PatternLoadCue)
  or   a
  jp   z, LABEL_7CF9
  ld   a, ($d437)
  or   a
  call z, LABEL_7CA9
  ld   a, (PatternLoadCue)
  or   a
  ret  z
  ld   a, ($d438)
  or   a
  call z, LABEL_7CC0
  di
  ld   a, ($d437)
  bit  7, a
  jr   nz, LABEL_7C7A
  call Engine_SwapFrame2
  ld   hl, ($d43b)
  call VDP_SetVRAMPointer
  ld   hl, ($d439)
  ld   a, ($d438)
  or   a
  jp   z, LABEL_7CE5
  cp   $04
  jr   c, LABEL_7C50
  ld   a, $04
LABEL_7C50:
  ld   b, a
-:  ld   c, $20
  di
LABEL_7C54:
  ld   a, (hl)
  out  ($be), a
  inc  hl
  dec  c
  jp   nz, LABEL_7C54
  djnz -
  ei
LABEL_7C5F:
  ld   ($d439), hl
  ld   hl, ($d43b)
  ld   bc, $0080
  add  hl, bc
  ld   ($d43b), hl
  ld   a, ($d438)
  sub  $04
  ld   ($d438), a
  ret  nc
  xor  a
  ld   ($d438), a
  ret

LABEL_7C7A:
  and  $1f
  call Engine_SwapFrame2
  ld   hl, ($d43b)
  call VDP_SetVRAMPointer
  ld   hl, ($d439)
  ld   a, ($d438)
  or   a
  jr   z, LABEL_7CE5
  cp   $04
  jr   c, LABEL_7C94
  ld   a, $04
LABEL_7C94:
  ld   b, a
  ld   d, $01
-:  ld   c, $20
  di
LABEL_7C9A:
  ld   e, (hl)
  ld   a, (de)
  out  ($be), a
  inc  hl
  dec  c
  jp   nz, LABEL_7C9A
  djnz -
  ei
  jp   LABEL_7C5F

LABEL_7CA9:
  ld   a, (PatternLoadCue)
  sub  $10
  add  a, a
  ld   l, a
  ld   h, $00
  ld   de, DATA_7D1B
  add  hl, de
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   ($d43d), de
  jp   LABEL_7CC0

LABEL_7CC0:
  ld   hl, ($d43d)
  ld   a, (hl)
  cp   $ff
  jr   z, LABEL_7CE5
  ld   ($d437), a
  inc  hl
  ld   a, (hl)
  ld   ($d438), a
  inc  hl
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   ($d43b), de
  inc  hl
  ld   e, (hl)
  inc  hl
  ld   d, (hl)
  ld   ($d439), de
  inc  hl
  ld   ($d43d), hl
  ret

LABEL_7CE5:
  xor  a
  ld   (PatternLoadCue), a
  ld   ($d437), a
  ld   ($d438), a
  ld   hl, $0000
  ld   ($d43b), hl
  ld   ($d439), hl
  ret

LABEL_7CF9:
  ld   a, ($d3eb)
  or   a
  ret  nz
  ld   a, ($d434)
  or   a
  jr   nz, LABEL_7D0B
  ld   a, ($d435)
  or   a
  jr   nz, LABEL_7D13
  ret

LABEL_7D0B:
  ld   (PatternLoadCue), a
  xor  a
  ld   ($d434), a
  ret

LABEL_7D13:
  ld   (PatternLoadCue), a
  xor  a
  ld   ($d435), a
  ret


DATA_7D1B:
.dw DATA_7D9B
.dw DATA_7DA2
.dw DATA_7DA9
.dw DATA_7DB0
.dw DATA_7DB7
.dw DATA_7DBE
.dw DATA_7DC5
.dw DATA_7DCC
.dw DATA_7DD3
.dw DATA_7DE1
.dw DATA_7DE8
.dw DATA_7DEF
.dw DATA_7DF6
.dw DATA_7DDA
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFD
.dw DATA_7DFE
.dw DATA_7E0B
.dw DATA_7E18
.dw DATA_7E25
.dw DATA_7E32
.dw DATA_7E3F
.dw DATA_7E4C
.dw DATA_7E59
.dw DATA_7E66
.dw DATA_7E73
.dw DATA_7E80
.dw DATA_7E8D
.dw DATA_7E9A
.dw DATA_7EA7
.dw DATA_7EB4
.dw DATA_7EC1
.dw DATA_7EC2
.dw DATA_7ECF
.dw DATA_7EDC
.dw DATA_7EE9
.dw DATA_7EF6
.dw DATA_7F03
.dw DATA_7F10
.dw DATA_7F1D
.dw DATA_7F2A
.dw DATA_7F37
.dw DATA_7F44
.dw DATA_7F51
.dw DATA_7F5E
.dw DATA_7F6B
.dw DATA_7F78
.dw DATA_7F85


DATA_7D9B:
.db $0B, $2C, $80, $12, $80, $8B, $FF
DATA_7DA2:
.db $0B, $10, $80, $12, $00, $93, $FF
DATA_7DA9:
.db $0B, $10, $80, $12, $00, $95, $FF
DATA_7DB0:
.db $0B, $10, $80, $12, $00, $97, $FF
DATA_7DB7:
.db $0B, $10, $80, $12, $00, $99, $FF
DATA_7DBE:
.db $0B, $10, $80, $12, $00, $9B, $FF
DATA_7DC5:
.db $0B, $10, $80, $12, $00, $9D, $FF
DATA_7DCC:
.db $0B, $10, $80, $12, $00, $9F, $FF
DATA_7DD3:
.db $0B, $10, $80, $12, $00, $A1, $FF
DATA_7DDA:
.db $0B, $10, $80, $12, $00, $91, $FF
DATA_7DE1:
.db $88, $60, $00, $02, $22, $B3, $FF
DATA_7DE8:
.db $18, $3C, $C0, $05, $C2, $96, $FF
DATA_7DEF:
.db $18, $30, $C0, $05, $42, $9E, $FF
DATA_7DF6:
.db $0B, $16, $C0, $0D, $C0, $B5, $FF
DATA_7DFD:
.db $FF
DATA_7DFE:
.db $0A, $06, $80, $08, $00, $91, $0A, $04, $C0, $0A, $40, $9C, $FF
DATA_7E0B:
.db $0A, $06, $80, $08, $C0, $91, $0A, $04, $C0, $0A, $C0, $9C, $FF
DATA_7E18:
.db $0A, $06, $80, $08, $80, $92, $0A, $04, $C0, $0A, $40, $9D, $FF
DATA_7E25:
.db $0A, $06, $80, $08, $40, $93, $0A, $04, $C0, $0A, $C0, $9D, $FF
DATA_7E32:
.db $0A, $06, $80, $08, $00, $94, $0A, $04, $C0, $0A, $40, $9E, $FF
DATA_7E3F:
.db $0A, $06, $80, $08, $C0, $94, $0A, $04, $C0, $0A, $86, $7F, $FF
DATA_7E4C:
.db $0A, $06, $80, $08, $80, $95, $0A, $04, $C0, $0A, $86, $7F, $FF
DATA_7E59:
.db $0A, $06, $80, $08, $40, $96, $0A, $04, $C0, $0A, $C0, $9E, $FF
DATA_7E66:
.db $0A, $06, $80, $08, $00, $97, $0A, $04, $C0, $0A, $40, $9F, $FF
DATA_7E73:
.db $0A, $06, $80, $08, $C0, $97, $0A, $04, $C0, $0A, $C0, $9F, $FF
DATA_7E80:
.db $0A, $06, $80, $08, $80, $98, $0A, $04, $C0, $0A, $40, $A0, $FF
DATA_7E8D:
.db $0A, $06, $80, $08, $40, $99, $0A, $04, $C0, $0A, $C0, $A0, $FF
DATA_7E9A:
.db $0A, $06, $80, $08, $00, $9A, $0A, $04, $C0, $0A, $40, $A1, $FF
DATA_7EA7:
.db $0A, $06, $80, $08, $C0, $9A, $0A, $04, $C0, $0A, $C0, $A1, $FF
DATA_7EB4:
.db $0A, $06, $80, $08, $80, $9B, $0A, $04, $C0, $0A, $40, $A2, $FF
DATA_7EC1:
.db $FF
DATA_7EC2:
.db $0A, $06, $40, $09, $00, $91, $0A, $04, $40, $0B, $40, $9C, $FF
DATA_7ECF:
.db $0A, $06, $40, $09, $C0, $91, $0A, $04, $40, $0B, $C0, $9C, $FF
DATA_7EDC:
.db $0A, $06, $40, $09, $80, $92, $0A, $04, $40, $0B, $40, $9D, $FF
DATA_7EE9:
.db $0A, $06, $40, $09, $40, $93, $0A, $04, $40, $0B, $C0, $9D, $FF
DATA_7EF6:
.db $0A, $06, $40, $09, $00, $94, $0A, $04, $40, $0B, $40, $9E, $FF
DATA_7F03:
.db $0A, $06, $40, $09, $C0, $94, $0A, $04, $40, $0B, $86, $7F, $FF
DATA_7F10:
.db $0A, $06, $40, $09, $80, $95, $0A, $04, $40, $0B, $86, $7F, $FF
DATA_7F1D:
.db $0A, $06, $40, $09, $40, $96, $0A, $04, $40, $0B, $C0, $9E, $FF
DATA_7F2A:
.db $0A, $06, $40, $09, $00, $97, $0A, $04, $40, $0B, $40, $9F, $FF
DATA_7F37:
.db $0A, $06, $40, $09, $C0, $97, $0A, $04, $40, $0B, $C0, $9F, $FF
DATA_7F44:
.db $0A, $06, $40, $09, $80, $98, $0A, $04, $40, $0B, $40, $A0, $FF
DATA_7F51:
.db $0A, $06, $40, $09, $40, $99, $0A, $04, $40, $0B, $C0, $A0, $FF
DATA_7F5E:
.db $0A, $06, $40, $09, $00, $9A, $0A, $04, $40, $0B, $40, $A1, $FF
DATA_7F6B:
.db $0A, $06, $40, $09, $C0, $9A, $0A, $04, $40, $0B, $C0, $A1, $FF
DATA_7F78:
.db $0A, $06, $40, $09, $80, $9B, $0A, $04, $40, $0B, $40, $A2, $FF
DATA_7F85:
.db $FF


; ==============================================================================
;  ROM Header
; ==============================================================================
.ORG $3FF0
.db "TMR SEGA"
.db $00, $00
.db $00, $00
.db $30, $25
.db $00, $70

;==============================================================================

.BANK 2 SLOT 1
.ORG 0


; Data from 8000 to 8C41 (3138 bytes)
.incbin "unknown/stt.gg.dat.20"

;---------- function start ----------
; (middle level)
_LABEL_8C42_170:
  ld   hl, $DE08
  ld   a, (hl)
  or   a
  jr   z, _LABEL_8C4E_171
  ret  p

  dec  (hl)
  jp   _LABEL_908F_8

_LABEL_8C4E_171:
  call _LABEL_8CC0_172
  call _LABEL_8CA8_175
  call _LABEL_8CF1_177
  call _LABEL_8D85_189
  ld   ix, $DE40
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ld   ix, $DE70
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ld   ix, $DEA0
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ld   ix, $DED0
  bit  7, (ix+0)
  call nz, _LABEL_92CE_246
  ld   ix, $DF00
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ld   ix, $DF30
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ld   ix, $DF60
  bit  7, (ix+0)
  call nz, _LABEL_91D1_201
  ret

;---------- function start ----------
; (middle level)
_LABEL_8CA8_175:
  ld   hl, $DE01
  ld   a, (hl)
  or   a
  ld   a, (RAM_DE02)
  add  a, (hl)
  ld   (hl), a
  ret  nc

  ld   hl, $DE4A
  ld   de, $0030
  ld   b, $04
_LABEL_8CBB_176:
  inc  (hl)
  add  hl, de
  djnz _LABEL_8CBB_176
  ret

;---------- function start ----------
; (middle level)
_LABEL_8CC0_172:
  ld   de, $DE04
  ld   ix, $DE0F
  ld   iy, $DE03
  call _LABEL_8CD1_173
  call _LABEL_8CD1_173
;---------- function start ----------
; (low level)
_LABEL_8CD1_173:
  ld   a, (de)
  and  $7F
  jr   z, _LABEL_8CED_174
  dec  a
  ld   hl, $4B39
  ld   c, a
  ld   b, $00
  add  hl, bc
  ld   a, (hl)
  cp   (ix+0)
  jr   c, _LABEL_8CED_174
  and  $7F
  ld   (ix+0), a
  ld   a, (de)
  ld   (iy+0), a
_LABEL_8CED_174:
  xor  a
  ld   (de), a
  inc  de
  ret

;---------- function start ----------
; (middle level)
_LABEL_8CF1_177:
  ld   a, (RAM_DE09)
  or   a
  ret  z

  ld   ix, $DED0
  bit  7, (ix+0)
  jp   z, _LABEL_8D1E_178
  push af
  ld   a, (RAM_DE11)
  out  ($7F), a
  pop  af
  ld   hl, $DED0
  res  2, (hl)
  set  4, (hl)
  or   $1F
  out  ($7F), a
  xor  a
  ld   (RAM_DE0F), a
  ld   (ix+0), a
  ld   a, $FF
  out  ($7F), a
_LABEL_8D1E_178:
  ld   hl, $DF00
  ld   b, $03
  ld   de, $0030
_LABEL_8D26_182:
  bit  7, (hl)
  jp   z, _LABEL_8D4D_179
  push hl
  inc  hl
  ld   a, (hl)
  cp   $A0
  jp   nz, _LABEL_8D3B_180
  ld   hl, $DE70
  res  2, (hl)
  jp   _LABEL_8D4A_181

_LABEL_8D3B_180:
  cp   $E0
  jp   nz, _LABEL_8D45_188
  ld   hl, $DED0
  res  2, (hl)
_LABEL_8D45_188:
  ld   hl, $DEA0
  res  2, (hl)
_LABEL_8D4A_181:
  pop  hl
  xor  a
  ld   (hl), a
_LABEL_8D4D_179:
  add  hl, de
  djnz _LABEL_8D26_182
  ld   a, (RAM_DE0A)
  dec  a
  dec  a
  jr   z, _LABEL_8D5B_183
  ld   (RAM_DE0A), a
  ret

_LABEL_8D5B_183:
  ld   a, (RAM_DE0B)
  ld   (RAM_DE0A), a
  ld   a, (RAM_DE09)
  dec  a
  dec  a
  ld   (RAM_DE09), a
  jp   z, _LABEL_9070_184
  ld   hl, $DE46
  ld   de, $0030
  ld   b, $03
_LABEL_8D74_186:
  call _LABEL_8D7D_185
  add  hl, de
  djnz _LABEL_8D74_186
  ld   hl, $DE16
;---------- function start ----------
; (low level)
_LABEL_8D7D_185:
  ld   a, (hl)
  inc  a
  inc  a
  cp   $0C
  ret  nc

  ld   (hl), a
  ret

;---------- function start ----------
; (middle level)
_LABEL_8D85_189:
  ld   a, (RAM_DE03)
  bit  7, a
  jp   z, _LABEL_9070_184
  cp   $B0
  jr   c, _LABEL_8DD8_190
  cp   $E0
  jp   c, _LABEL_8E1D_191
  cp   $E4
  jp   nc, _LABEL_9070_184
  sub  $E0
  ld   hl, $4DAA
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  jp   (hl)

; Data from 8DAA to 8DD7 (46 bytes)
.db $B0, $4D, $70, $50, $C0, $4D, $3E, $0C, $32, $09, $DE, $3E, $12, $32, $0A, $DE
.db $32, $0B, $DE, $C3, $17, $4E, $FD, $21, $00, $DF, $11, $30, $00, $06, $03, $21
.db $D7, $4D, $FD, $75, $03, $FD, $74, $04, $FD, $19, $10, $F6, $C9, $F2

_LABEL_8DD8_190:
  sub  $81
  ret  m

  ex   af, af'
  call _LABEL_9070_184
  ex   af, af'
  ld   hl, $4B9C
  ld   c, a
  ex   af, af'
  call _LABEL_90BD_198
  ld   (RAM_DE01), a
  ld   (RAM_DE02), a
  ex   af, af'
  ld   hl, $4B9C
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  inc  hl
  inc  hl
  ld   b, (hl)
  inc  hl
  inc  hl
  ld   a, (hl)
  ex   af, af'
  inc  hl
  ld   a, (hl)
  ld   (RAM_DE01), a
  ld   (RAM_DE02), a
  ld   iy, $4EE7
  inc  hl
  ld   de, $DE40
_LABEL_8E12_200:
  call _LABEL_8EAF_199
  djnz _LABEL_8E12_200
_LABEL_8E17_9:
  ld   a, $80
  ld   (RAM_DE03), a
  ret

_LABEL_8E1D_191:
  ld   (RAM_DE0D), a
  sub  $B0
  ld   hl, $4BE2
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  inc  hl
  inc  hl
  ld   a, (hl)
  inc  hl
  ex   af, af'
  ld   b, (hl)
  inc  hl
_LABEL_8E35_196:
  inc  hl
  ld   a, (hl)
  dec  hl
  cp   $A0
  jr   z, _LABEL_8E64_192
  cp   $C0
  jr   z, _LABEL_8E49_193
  ld   de, $DF60
  ld   iy, $DED0
  jr   _LABEL_8E6B_194

_LABEL_8E49_193:
  ld   iy, $DF60
  bit  6, (iy+0)
  jr   nz, _LABEL_8E5B_197
  set  2, (iy+0)
  ld   a, $FF
  out  ($7F), a
_LABEL_8E5B_197:
  ld   de, $DF30
  ld   iy, $DEA0
  jr   _LABEL_8E6B_194

_LABEL_8E64_192:
  ld   de, $DF00
  ld   iy, $DE70
_LABEL_8E6B_194:
  call _LABEL_8E72_195
  djnz _LABEL_8E35_196
  jr   _LABEL_8E17_9

;---------- function start ----------
; (low level)
_LABEL_8E72_195:
  set  2, (iy+0)
  ld   c, $36
  push de
  pop  ix
  ldi
  ldi
  ex   af, af'
  ld   (de), a
  inc  de
  ex   af, af'
  xor  a
  ldi
  ldi
  ldi
  ldi
  ld   (de), a
  inc  de
  ld   (de), a
  inc  de
  ld   a, c
  ld   (de), a
  inc  de
  ld   a, $FF
.ifeq SMS_VERSION 1
  nop
  nop
  nop
  nop
  nop
  nop
.else
  push bc
  ld   c, $06
  out  (c), a
  pop  bc
.endif
  xor  a
  ld   (ix+39), a
  ld   (ix+40), a
  ld   (ix+41), a
  inc  a
  ld   (de), a
  push hl
  ld   hl, $0026
  add  hl, de
  ex   de, hl
  pop  hl
  ret

;---------- function start ----------
; (low level)
_LABEL_8EAF_199:
  ld   c, $34
  push de
  pop  ix
  ld   a, $80
  ld   (de), a
  inc  de
  ld   a, (iy+0)
  ld   (de), a
  inc  de
  inc  iy
  ex   af, af'
  ld   (de), a
  inc  de
  ex   af, af'
  xor  a
  ldi
  ldi
  ldi
  ldi
  ld   (de), a
  inc  de
  ld   (de), a
  inc  de
  ld   a, c
  ld   (de), a
  inc  de
  xor  a
  ld   (ix+39), a
  ld   (ix+40), a
  ld   (ix+41), a
  inc  a
  ld   (de), a
  push hl
  ld   hl, $0026
  add  hl, de
  ex   de, hl
  pop  hl
  ret


; Data from 8EE7 to 8EEA (4 bytes)
.db $80, $A0, $C0, $E0

;---------- function start ----------
; (low level)
_LABEL_8EEB_220:
  bit  7, (ix+7)
  ret  z

  bit  1, (ix+0)
  ret  nz

  ld   e, (ix+16)
  ld   d, (ix+17)
  push ix
  pop  hl
  ld   b, $00
  ld   c, $14
  add  hl, bc
  ex   de, hl
  ldi
  ldi
  ldi
  ld   a, (hl)
  srl  a
  ld   (de), a
  xor  a
  ld   (ix+18), a
  ld   (ix+19), a
  ret

;---------- function start ----------
; (low level)
_LABEL_8F16_221:
  bit  7, (ix+8)
  ret  z

  bit  1, (ix+0)
  ret  nz

  bit  7, (ix+29)
  ret  nz

  ld   a, $FF
  ld   (ix+31), a
  and  $10
  or   (ix+30)
  ld   (ix+29), a
  ret

;---------- function start ----------
; (low level)
_LABEL_8F33_224:
  ld   l, (ix+11)
  ld   h, (ix+12)
  ld   a, (ix+7)
  or   a
  ret  z

  jp   p, _LABEL_8F89_225
  dec  (ix+20)
  ret  nz

  inc  (ix+20)
  push hl
  ld   l, (ix+18)
  ld   h, (ix+19)
  dec  (ix+21)
  jr   nz, _LABEL_8F74_226
  ld   e, (ix+16)
  ld   d, (ix+17)
  push de
  pop  iy
  ld   a, (iy+1)
  ld   (ix+21), a
  ld   a, (ix+22)
  ld   c, a
  and  $80
  rlca
  neg
  ld   b, a
  add  hl, bc
  ld   (ix+18), l
  ld   (ix+19), h
_LABEL_8F74_226:
  pop  bc
  add  hl, bc
  dec  (ix+23)
  ret  nz

  ld   a, (iy+3)
  ld   (ix+23), a
  ld   a, (ix+22)
  neg
  ld   (ix+22), a
  ret

_LABEL_8F89_225:
  dec  a
  ex   de, hl
  ld   hl, $4000
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  jr   _LABEL_8F9C_227

_LABEL_8F99_232:
  ld   (ix+21), a
_LABEL_8F9C_227:
  push hl
  ld   c, (ix+21)
  ld   b, $00
  add  hl, bc
  ld   a, (hl)
  pop  hl
  bit  7, a
  jr   z, _LABEL_8FC4_228
  cp   $83
  jr   z, _LABEL_8FB9_229
  jr   nc, _LABEL_8FC0_230
  cp   $80
  jr   z, _LABEL_8FBD_231
  set  5, (ix+0)
  pop  hl
  ret

_LABEL_8FB9_229:
  inc  de
  ld   a, (de)
  jr   _LABEL_8F99_232

_LABEL_8FBD_231:
  xor  a
  jr   _LABEL_8F99_232

_LABEL_8FC0_230:
  ld   h, $FF
  jr   _LABEL_8FC6_233

_LABEL_8FC4_228:
  ld   h, $00
_LABEL_8FC6_233:
  ld   l, a
  add  hl, de
  inc  (ix+21)
  ret

;---------- function start ----------
; (low level)
_LABEL_8FCC_203:
  res  1, (ix+0)
  res  4, (ix+0)
  ld   e, (ix+3)
  ld   d, (ix+4)
_LABEL_8FDA_207:
  ld   a, (de)
  inc  de
  cp   $E1
  jp   nc, _LABEL_9357_204
  bit  3, (ix+0)
  jp   nz, _LABEL_9052_205
  cp   $D8
  jr   c, _LABEL_8FF4_206
  and  $07
  ld   (ix+6), a
  jp   _LABEL_8FDA_207

_LABEL_8FF4_206:
  cp   $80
  jr   c, _LABEL_901C_208
  jr   z, _LABEL_904D_209
  ex   af, af'
  ld   a, (ix+29)
  and  $7F
  ld   (ix+29), a
  ex   af, af'
  call _LABEL_90A9_210
  ld   (ix+11), l
  ld   (ix+12), h
_LABEL_900D_215:
  ld   a, (de)
  inc  de
  or   a
  jp   p, _LABEL_901C_208
  ld   a, (ix+13)
  ld   (ix+10), a
  dec  de
  jr   _LABEL_9025_211

_LABEL_901C_208:
  call _LABEL_90C7_216
  ld   (ix+10), a
  ld   (ix+13), a
_LABEL_9025_211:
  ld   (ix+3), e
  ld   (ix+4), d
  bit  1, (ix+0)
  ret  nz

  bit  6, (ix+0)
  jr   nz, _LABEL_903A_212
  res  5, (ix+0)
_LABEL_903A_212:
  ld   a, (ix+15)
  ld   (ix+14), a
  xor  a
  ld   (ix+21), a
  bit  7, (ix+8)
  ret  nz

  ld   (ix+31), a
  ret

_LABEL_904D_209:
  call _LABEL_9263_213
  jr   _LABEL_900D_215

_LABEL_9052_205:
  ld   h, a
  ld   a, (de)
  inc  de
  ld   l, a
  or   h
  jr   z, _LABEL_9065_218
  ld   b, $00
  ld   a, (ix+5)
  or   a
  ld   c, a
  jp   p, _LABEL_9064_219
  dec  b
_LABEL_9064_219:
  add  hl, bc
_LABEL_9065_218:
  ld   (ix+11), l
  ld   (ix+12), h
  ld   a, (de)
  inc  de
  jp   _LABEL_901C_208

;---------- function start ----------
; (low level)
_LABEL_9070_184:
  push hl
  push bc
  push de
  ld   hl, $DE03
  ld   de, $DE04
  ld   bc, $018C
  ld   (hl), $00
_LABEL_907E_187:
  ldi
  jp   pe, _LABEL_907E_187
  ld   a, $FF
  ld   (RAM_DE10), a
.ifeq SMS_VERSION 1
  nop
  nop
  nop
  nop
.else
  ld   c, $06
  out  (c), a
.endif
  pop  de
  pop  bc
  pop  hl
;---------- function start ----------
; (high level)
_LABEL_908F_8:
  push hl
  push bc
  ld   hl, $509F
  ld   b, $0A
  ld   c, $7F
  otir
  pop  bc
  pop  hl
  jp   _LABEL_8E17_9


; Data from 909F to 90A8 (10 bytes)
.db $80, $00, $A0, $00, $C0, $00, $9F, $BF, $DF, $FF

;---------- function start ----------
; (low level)
_LABEL_90A9_210:
  sub  $81
  add  a, (ix+5)
  and  $7F
  ld   hl, $50D1
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  ret

;---------- function start ----------
; (low level)
_LABEL_90BD_198:
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  ret

;---------- function start ----------
; (low level)
_LABEL_90C7_216:
  ld   b, (ix+2)
  dec  b
  ret  z

  ld   c, a
_LABEL_90CD_217:
  add  a, c
  djnz _LABEL_90CD_217
  ret


; Data from 90D1 to 91D0 (256 bytes)
.db $AC, $01, $94, $01, $7C, $01, $68, $01, $53, $01, $40, $01, $2E, $01, $1D, $01
.db $0D, $01, $FE, $00, $F0, $00, $E2, $00, $D6, $00, $CA, $00, $BE, $00, $B4, $00
.db $AA, $00, $A0, $00, $97, $00, $8F, $00, $87, $00, $7F, $00, $78, $00, $71, $00
.db $6B, $00, $65, $00, $5F, $00, $5A, $00, $55, $00, $50, $00, $4C, $00, $47, $00
.db $43, $00, $F9, $03, $C0, $03, $8A, $03, $57, $03, $27, $03, $FA, $02, $CF, $02
.db $A7, $02, $81, $02, $5D, $02, $3B, $02, $1B, $02, $FC, $01, $E0, $01, $C5, $01
.db $AC, $01, $94, $01, $7C, $01, $68, $01, $53, $01, $40, $01, $2E, $01, $1D, $01
.db $0D, $01, $FE, $00, $F0, $00, $E2, $00, $D6, $00, $CA, $00, $BE, $00, $B4, $00
.db $AA, $00, $A0, $00, $97, $00, $8F, $00, $87, $00, $7F, $00, $78, $00, $71, $00
.db $6B, $00, $65, $00, $5F, $00, $5A, $00, $55, $00, $50, $00, $4C, $00, $47, $00
.db $43, $00, $40, $00, $3C, $00, $39, $00, $36, $00, $33, $00, $30, $00, $2D, $00
.db $2B, $00, $28, $00, $26, $00, $24, $00, $22, $00, $20, $00, $1F, $00, $1D, $00
.db $1B, $00, $1A, $00, $18, $00, $17, $00, $16, $00, $15, $00, $13, $00, $12, $00
.db $11, $00, $10, $00, $0F, $00, $0E, $00, $0D, $00, $0C, $00, $0B, $00, $0A, $00
.db $09, $00, $F9, $03, $C0, $03, $8A, $03, $57, $03, $27, $03, $FA, $02, $CF, $02
.db $A7, $02, $81, $02, $5D, $02, $3B, $02, $1B, $02, $FC, $01, $E0, $01, $C5, $01

;---------- function start ----------
; (middle level)
_LABEL_91D1_201:
  dec  (ix+10)
  jr   nz, _LABEL_91EB_202
  call _LABEL_8FCC_203
  bit  4, (ix+0)
  ret  nz

  bit  2, (ix+0)
  ret  nz

  call _LABEL_8EEB_220
  call _LABEL_8F16_221
  jr   _LABEL_9208_222

_LABEL_91EB_202:
  bit  2, (ix+0)
  ret  nz

  ld   a, (ix+14)
  or   a
  jr   z, _LABEL_91FC_245
  dec  (ix+14)
  call z, _LABEL_9263_213
_LABEL_91FC_245:
  ld   a, (ix+7)
  or   a
  jr   z, _LABEL_9237_223
  bit  5, (ix+0)
  jr   nz, _LABEL_9237_223
_LABEL_9208_222:
  bit  6, (ix+0)
  jr   nz, _LABEL_9237_223
  call _LABEL_8F33_224
  ld   d, $00
  ld   a, (ix+37)
  or   a
  jp   p, _LABEL_921B_234
  dec  d
_LABEL_921B_234:
  ld   e, a
  add  hl, de
  ld   a, (ix+1)
  cp   $E0
  jr   nz, _LABEL_9226_235
  ld   a, $C0
_LABEL_9226_235:
  ld   c, a
  ld   a, l
  and  $0F
  or   c
  out  ($7F), a
  ld   a, l
  and  $F0
  or   h
  rrca
  rrca
  rrca
  rrca
  out  ($7F), a
_LABEL_9237_223:
  call _LABEL_9255_236
  bit  2, (ix+0)
  ret  nz

  bit  4, (ix+0)
  ret  nz

  add  a, (ix+6)
  bit  4, a
  jr   z, _LABEL_924D_244
  ld   a, $0F
_LABEL_924D_244:
  or   (ix+1)
  add  a, $10
  out  ($7F), a
  ret

;---------- function start ----------
; (low level)
_LABEL_9255_236:
  ld   a, (ix+8)
  or   a
  ret  z

  jp   p, _LABEL_927A_237
  bit  4, (ix+29)
  jr   z, _LABEL_9263_213
;---------- function start ----------
; (low level)
_LABEL_9263_213:
  bit  1, (ix+0)
  ret  nz

  bit  7, (ix+8)
  jp   z, _LABEL_92BD_214
  ld   a, (ix+29)
  and  $0F
  or   $80
  ld   (ix+29), a
  ret

_LABEL_927A_237:
  dec  a
  ld   hl, $4151
  ld   c, a
  ld   b, $00
  add  hl, bc
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  jr   _LABEL_928C_238

_LABEL_9289_243:
  ld   (ix+31), a
_LABEL_928C_238:
  push hl
  ld   c, (ix+31)
  ld   b, $00
  add  hl, bc
  ld   a, (hl)
  pop  hl
  bit  7, a
  jr   z, _LABEL_92B9_239
  cp   $82
  jr   z, _LABEL_92A9_240
  cp   $81
  jr   z, _LABEL_92B3_241
  cp   $80
  jr   z, _LABEL_92B0_242
  inc  de
  ld   a, (de)
  jr   _LABEL_9289_243

_LABEL_92A9_240:
  set  4, (ix+0)
  pop  hl
  jr   _LABEL_92BD_214

_LABEL_92B0_242:
  xor  a
  jr   _LABEL_9289_243

_LABEL_92B3_241:
  set  4, (ix+0)
  pop  hl
  ret

_LABEL_92B9_239:
  inc  (ix+31)
  ret

_LABEL_92BD_214:
  set  4, (ix+0)
  bit  2, (ix+0)
  ret  nz

  ld   a, $1F
  add  a, (ix+1)
  out  ($7F), a
  ret

;---------- function start ----------
; (middle level)
_LABEL_92CE_246:
  dec  (ix+10)
  jp   nz, _LABEL_9237_223
  res  4, (ix+0)
  ld   e, (ix+3)
  ld   d, (ix+4)
  ld   a, (de)
  inc  de
  cp   $E0
  jr   nc, _LABEL_92EF_247
  cp   $80
  jp   c, _LABEL_901C_208
  call _LABEL_92F8_248
  jp   _LABEL_900D_215

_LABEL_92EF_247:
  ld   hl, $52F5
  jp   _LABEL_935A_256


; Data from 92F5 to 92F7 (3 bytes)
.db $13, $18, $E6

;---------- function start ----------
; (low level)
_LABEL_92F8_248:
  cp   $81
  jr   z, _LABEL_933B_249
  cp   $82
  jr   z, _LABEL_931B_250
  cp   $84
  jr   z, _LABEL_9333_251
  cp   $88
  jr   z, _LABEL_9313_252
  cp   $90
  jr   z, _LABEL_932B_253
  cp   $A0
  jr   z, _LABEL_9323_254
  jp   _LABEL_92BD_214

_LABEL_9313_252:
  ld   a, $19
  ld   b, $02
  ld   c, $E5
  jr   _LABEL_9341_255

_LABEL_931B_250:
  ld   a, $03
  ld   b, $03
  ld   c, $E4
  jr   _LABEL_9341_255

_LABEL_9323_254:
  ld   a, $11
  ld   b, $03
  ld   c, $E4
  jr   _LABEL_9341_255

_LABEL_932B_253:
  ld   a, $1A
  ld   b, $02
  ld   c, $E5
  jr   _LABEL_9341_255

_LABEL_9333_251:
  ld   a, $18
  ld   b, $02
  ld   c, $E6
  jr   _LABEL_9341_255

_LABEL_933B_249:
  ld   a, $10
  ld   b, $03
  ld   c, $E4
_LABEL_9341_255:
  ld   (ix+8), a
  ld   a, b
  ld   (ix+6), a
  bit  2, (ix+0)
  ret  nz

  ld   a, (RAM_DE15)
  add  a, c
  ld   (RAM_DE11), a
  out  ($7F), a
  ret

_LABEL_9357_204:
  ld   hl, $536B
_LABEL_935A_256:
  push hl
  sub  $E1
  ld   hl, $536F
  add  a, a
  ld   c, a
  ld   b, $00
  add  hl, bc
  ld   c, (hl)
  inc  hl
  ld   h, (hl)
  ld   l, c
  ld   a, (de)
  jp   (hl)

LABEL_936B:
    inc de
    jp $4fda

DATA_936F:
.db $5E $55 $AD $53 $E9 $53 $F7 $53 $6A $55 $DA $53 $58 $55 $40 $55
.db $EA $53 $E9 $53 $E9 $53 $E9 $53 $62 $55 $E9 $53 $E9 $53 $4A $55
.db $E9 $53 $82 $54 $0D $54 $6A $54 $66 $54 $6E $54 $13 $55 $E6 $54
.db $00 $55 $09 $54 $02 $54 $2A $55 $74 $54 $E9 $53 $E9 $53


LABEL_93AD:
    ld b, (ix+38)
    ld (ix+38), a
    cpl
    ld c, a
    ld a, (ix+1)
    cp $80
    jr z, $14
    rlc b
    rlc c
    cp $a0
    jr z, $0c
    rlc b
    rlc c
    cp $c0
    jr z, $04
    rlc b
    rlc c
    ld hl, $de10
    ld a, (hl)
    or b
    and c
    ld (hl), a
.ifeq SMS_VERSION 1
    nop
    nop
.else
    out ($06), a
.endif
    ret

; Data from 936B to BFFF (11413 bytes)
.incbin "unknown/stt.gg.dat.24"


.BANK 3 SLOT 2
.ORG $0000


; Data from C000 to FFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.30"


.BANK 4
.ORG $0000


; Data from 10000 to 13FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.40"


.BANK 5
.ORG $0000


; Data from 14000 to 17FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.50"


.BANK 6
.ORG $0000


; Data from 18000 to 1BFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.60"


.BANK 7
.ORG $0000


; Data from 1C000 to 1FFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.70"


.BANK 8
.ORG $0000


; Data from 20000 to 23FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.80"


.BANK 9
.ORG $0000


; Data from 24000 to 27FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.90"


.BANK 10
.ORG $0000


; Data from 28000 to 2BFFF (16384 bytes)
;.incbin "unknown/stt.gg.dat.A0"

.include "src/includes/bank10.asm"


.BANK 11
.ORG $0000


; Data from 2C000 to 2FFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.B0"


.BANK 12
.ORG $0000

.include "src/includes/bank12.asm"
; Data from 30000 to 33FFF (16384 bytes)
;.incbin "unknown/stt.gg.dat.C0"


.BANK 13 SLOT 2
.ORG $0000


; Data from 34000 to 37FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.D0"

;==============================================================================
.BANK 14 SLOT 2
.ORG $0000
Bank14:

.ifeq SMS_VERSION 1
  .include "src/includes/bank14_sms.asm"
.else
  .include "src/includes/bank14.asm"
.endif

;==============================================================================
.BANK 15 SLOT 2
.ORG $0000


; Data from 3C000 to 3FFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.F0"

;==============================================================================
.BANK 16 SLOT 2
.ORG $0000

.include "src/includes/bank16.asm"

;==============================================================================
.BANK 17
.ORG $0000


; Data from 44000 to 47FFF (16384 bytes)
;.incbin "unknown/stt.gg.dat.110"
.include "src/includes/bank17.asm"


.BANK 18
.ORG $0000

.include "src/includes/bank18.asm"


.BANK 19
.ORG $0000

.include "src/includes/bank19.asm"


.BANK 20
.ORG $0000


; Data from 50000 to 53FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.140"


.BANK 21
.ORG $0000

.include "src/includes/bank21.asm"


.BANK 22
.ORG $0000


; Data from 58000 to 5BFFF (16384 bytes)
;.incbin "unknown/stt.gg.dat.160"
.include "src/includes/bank22.asm"

;==========================================================
.BANK 23
.ORG $0000

.include "src/includes/bank23.asm"

;==========================================================
.BANK 24 SLOT 2
.ORG $0000


; Data from 60000 to 63FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.180"


.BANK 25 SLOT 2
.ORG $0000


; Data from 64000 to 67FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.190"


.BANK 26
.ORG $0000

.include "src/includes/bank26.asm"


;==========================================================

.BANK 27
.ORG $0000

.ifeq SMS_VERSION 1
  .include "src/includes/bank27_sms.asm"
.else
  .include "src/includes/bank27.asm"
.endif

;==========================================================
.BANK 28
.ORG $0000


; Data from 70000 to 73FFF (16384 bytes)
.incbin "unknown/stt.gg.dat.1C0"

;==========================================================
.BANK 29 SLOT 2
.ORG $0000

Bank29:

.include "src/includes/bank29.asm"

;=========================================================
.BANK 30
.ORG $0000

.include "src/includes/bank30.asm"


.BANK 31 SLOT 2
.ORG $0000


; Data from 7C000 to 7FFFF (16384 bytes)
.incbin "unknown/stt.gg.dat.1F0"

