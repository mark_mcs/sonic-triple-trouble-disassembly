;**********************************
;*	Variables
;**********************************
.def	TileCount		$DC27
.def	SourcePointer	$DC2B
.def	BitFieldCount	$DC26
.def	FlagPointer		$DC29


;**********************************************************
;*  Clears the buffers used by the tile decompression.    *
;**********************************************************
Engine_ClearDecompressionBuffers:		;$2DD7
	ld   de, $DBE6
	ld   b, $4D
	xor  a
	
-:	ld   (de), a
	inc  de
	djnz -

	ret

;**************************************************
;* LoadTiles - 
;* Load compressed tile data into VRAM.
;* Expects VDP memory pointer to have been set.
;*  
;*  A	- Mirrored tiles flag
;*  HL	- Source Address
;**************************************************
Engine_LoadCompressedTileset:		;$2DE2
	ld   (RAM_DC31), a
	push hl
	inc  hl
	inc  hl
	ld   a, (hl)
	ld   (TileCount), a
	inc  hl
	ld   a, (hl)
	ld   (TileCount+1), a
	inc  hl
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	inc  hl
	ld   (SourcePointer), hl
	pop  hl
	add  hl, de
	ld   (FlagPointer), hl
	ld   hl, $DC06
	ld   de, $DC07
	ld   bc, $001F
	ld   (hl), $00
	ldir
	xor  a
	ld   (BitFieldCount), a

-:	call Engine_GetTileCompressionType

	;$00 = blank tile
	cp   $00
	jr   nz, +
	call Engine_WriteBlankTile
	jr   ++

	;$02 = compressed tile
+:	cp   $02
	jr   nz, +
	call Engine_DecompressTile
	call Engine_WriteTileToVRAM
	jr   ++

	;$03 = xor compressed tile
+:	cp   $03
	jr   nz, +
	call Engine_DecompressTile
	call Engine_XorDecodeTile
	call Engine_WriteTileToVRAM
	jr   ++

	;$01 = uncompressed tile
+:	call Engine_ReadUncompressedTile
	call Engine_WriteTileToVRAM
	
++:	ld   hl, (TileCount)
	dec  hl
	ld   (TileCount), hl
	ld   a, (RAM_D12C)
	or   a
	jr   z, +
	ld   a, l
	and  $07
	jr   nz, +
	push hl
	call Engine_WaitForInterrupt
	pop  hl

+:	di
	ld   a, l
	or   h
	jr   nz, -
	ret

;*************************************************************
;*	Load uncompressed tile (direct copy into buffer).
;*************************************************************
Engine_ReadUncompressedTile:		;$2E59
	ld   bc, $0020
	ld   hl, (SourcePointer)
	ld   de, $DBE6
	ld   bc, $0020
	ldir
	ld   (SourcePointer), hl
	ret

;*************************************************************
;* Decompress tile data into the buffer at $DBE6.
;*************************************************************
Engine_DecompressTile:		;$2E6B
	ld   ix, $DBE6
	ld   hl, (SourcePointer)
	ld   e, (hl)
	inc  hl
	ld   d, (hl)
	inc  hl
	ld   c, (hl)
	inc  hl
	ld   b, (hl)
	inc  hl
	ld   a, $20

-:	push af
	rr   b
	rr   c
	rr   d
	rr   e
	jr   c, +
	ld   (ix+0), $00
	jr   ++

+:	ld   a, (hl)
	ld   (ix+0), a
	inc  hl

++:	inc  ix
	pop  af
	dec  a
	jr   nz, -
	ld   (SourcePointer), hl
	ret

;*************************************************************
;*	Decode XOR'ed tile data.
;*************************************************************
Engine_XorDecodeTile:		;$2E9C
	ld   ix, $DBE6
	ld   b, $07
-:	ld   a, (ix+0)
	xor  (ix+2)
	ld   (ix+2), a
	ld   a, (ix+1)
	xor  (ix+3)
	ld   (ix+3), a
	ld   a, (ix+16)
	xor  (ix+18)
	ld   (ix+18), a
	ld   a, (ix+17)
	xor  (ix+19)
	ld   (ix+19), a
	inc  ix
	inc  ix
	djnz -
	ret

;**********************************************************************
;* Read the compression type bitfield for the next data block.
;* Compression type bitfield is a series of 2-bit flags that define
;* the type of compression used on the following data.
;*
;*	See: http://forums.sonicretro.org/index.php?showtopic=10063
;*
;**********************************************************************
Engine_GetTileCompressionType:		;$2ECD
	ld   a, (BitFieldCount)
	cp   $04
	jr   nz, +
	ld   hl, (FlagPointer)
	inc  hl
	ld   (FlagPointer), hl
	xor  a
	ld   (BitFieldCount), a

+:	ld   b, a
	ld   hl, (FlagPointer)
	ld   a, (hl)

-:	dec  b
	jp   m, +
	rrca
	rrca
	jp   -

+:	and  $03
	push af
	ld   a, (BitFieldCount)
	inc  a
	ld   (BitFieldCount), a
	pop  af
	ret

;*************************************************************
;*	Write a blank tile is encountered (compression type = 0) 
;*  data is copied from $D320 to VRAM.
;*************************************************************
Engine_WriteBlankTile:		;$2EF9
	;copy the tile data into the decompression buffer
	ld   hl, $DC06
	ld   de, $DBE6
	ld   bc, $0020
	ldir

;*************************************************************
;*	Write the decompressed tile data (at $DBE6) to VRAM
;*************************************************************
Engine_WriteTileToVRAM:		;$2F04
	ld   a, (RAM_DC31)
	or   a
	jp   nz, Engine_WriteMirroredTileToVRAM
	;copy the decompression buffer into VRAM
	ld   hl, $DBE6
	ld   b, $20
	
-:	ld   a, (hl)
	out  ($BE), a
	push hl
	pop  hl
	inc  hl
	djnz -
	
	ret

;************************************************************
;*	Write tile data to VRAM. Data at $DBE6 is treated as an	*
;*  index into the mirroring data at $0100.					*
;************************************************************
Engine_WriteMirroredTileToVRAM:		;$2F19
	ld   hl, $DBE6
	ld   b, $20
	;read a byte of tile data from the buffer
-:	ld   e, (hl)
	;mirror the byte 
	ld   d, Data_TileMirroringValues >> 8
	ld   a, (de)
	
	out  ($BE), a
	push hl
	pop  hl
	inc  hl
	djnz -
	ret
