Data_ZoneTilesets:      ; $B678
.db :Art_GTZ_LevelTiles
    .dw $1800
    .dw Art_GTZ_LevelTiles
    .dw DATA_B74A
.db :Art_GTZ_LevelTiles
    .dw $1800
    .dw Art_GTZ_LevelTiles
    .dw DATA_B74A
.db :Art_GTZ_LevelTiles
    .dw $1800
    .dw Art_GTZ_LevelTiles
    .dw DATA_B778

.db :Art_SPZ_LevelTiles
    .dw $1800
    .dw Art_SPZ_LevelTiles
    .dw DATA_B792
.db :Art_SPZ_LevelTiles
    .dw $1800
    .dw Art_SPZ_LevelTiles
    .dw DATA_B792
.db :Art_SPZ_LevelTiles
    .dw $1800
    .dw Art_SPZ_LevelTiles
    .dw DATA_B7B1

.db :Art_MJZ_LevelTiles
    .dw $1800
    .dw Art_MJZ_LevelTiles
    .dw DATA_B7BC
.db :Art_MJZ_LevelTiles
    .dw $1800
    .dw Art_MJZ_LevelTiles
    .dw DATA_B7BC
.db :Art_MJZ_LevelTiles
    .dw $1800
    .dw Art_MJZ_LevelTiles
    .dw DATA_B7E5

.db :Art_RWZ_LevelTiles
    .dw $1800
    .dw Art_RWZ_LevelTiles
    .dw DATA_B804
.db :Art_RWZ_LevelTiles
    .dw $1800
    .dw Art_RWZ_LevelTiles
    .dw DATA_B804
.db :Art_RWZ_LevelTiles
    .dw $1800
    .dw Art_RWZ_LevelTiles
    .dw DATA_B832

.db :Art_TPZ_LevelTiles
    .dw $1800
    .dw Art_TPZ_LevelTiles
    .dw DATA_B83D
.db :Art_TPZ_LevelTiles
    .dw $1800
    .dw Art_TPZ_LevelTiles
    .dw DATA_B83D
.db :Art_TPZ_LevelTiles
    .dw $1800
    .dw Art_TPZ_LevelTiles
    .dw DATA_386B

.db $1C
    .dw $1800
    .dw $9C94
    .dw DATA_B885
.db $1C
    .dw $1800
    .dw $9C94
    .dw DATA_B885
.db $1C
    .dw $1800
    .dw $9C94
    .dw DATA_B8BD

.db $16
    .dw $1800
    .dw $8000
    .dw DATA_B8E4
.db $16
    .dw $1800
    .dw $8000
    .dw DATA_B8E4
.db $16
    .dw $1800
    .dw $8000
    .dw DATA_B8E4

.db $10
    .dw $1800
    .dw $84C4
    .dw DATA_B923
.db $10
    .dw $1800
    .dw $84C4
    .dw DATA_B923
.db $10
    .dw $1800
    .dw $84C4
    .dw DATA_B923

.db :Art_Cmp_Sonic_Tornado
    .dw $2000
    .dw Art_Cmp_Sonic_Tornado
    .dw DATA_B93D
.db :Art_Cmp_Tails_Tornado
    .dw $2000
    .dw Art_Cmp_Tails_Tornado
    .dw DATA_B93D
.db :Art_Cmp_Tails_Tornado
    .dw $2000
    .dw Art_Cmp_Tails_Tornado
    .dw DATA_B93D

.db $16
    .dw $1800
    .dw $8000
    .dw DATA_B968
.db $16
    .dw $1800
    .dw $8000
    .dw DATA_B973
.db $10
    .dw $1800
    .dw $84C4
    .dw DATA_B9AF



DATA_B74A:
.db :Art_Badnik_SpringTurtle
    .dw $0FC0
    .dw Art_Badnik_SpringTurtle
.db $0A
    .dw $0C40
    .dw $A946
.db $0A
    .dw $0E80
    .dw $ABCA
.db $0A
    .dw $1380
    .dw $AB06
.db $09
    .dw $1480
    .dw $B806
.db $0A
    .dw $1600
    .dw $AA86
.db $0A
    .dw $1680
    .dw $AC8E
.db $0A
    .dw $1700
    .dw $AF3A
.db $04
    .dw $1740
    .dw $AE90
.db $FF


DATA_B778:
.db $0A
    .dw $0680
    .dw $AF3A
.db $0A
    .dw $06C0
    .dw $A586
.db $0A
    .dw $0A80
    .dw $A946
.db $0A
    .dw $0CC0
    .dw $ABCA
.db $08
    .dw $0E00
    .dw $AA40
.db $FF


DATA_B792:
.db $0A
    .dw $0C40
    .dw $AF5C
.db $06
    .dw $0C80
    .dw $B440
.db $06
    .dw $0F40
    .dw $B6BC
.db $04
    .dw $13C0
    .dw $AF10
.db $0A
    .dw $1540
    .dw $ABCA
.db $0A
    .dw $1680
    .dw $B03C
.db $FF


DATA_B7B1:
.db $09
    .dw $0C40
    .dw $B806
.db $0B
    .dw $0DC0
    .dw $A300
.db $FF


DATA_B7BC:
.db $0A 
    .dw $0C40
    .dw $AF82
.db $05
    .dw $0C80
    .dw $BCFC
.db $0A
    .dw $0F40
    .dw $ABCA
.db $07
    .dw $1080
    .dw $BC40
.db $0A
    .dw $1180
    .dw $AA86
.db $05
    .dw $1200
    .dw $B782
.db $04
    .dw $12C0
    .dw $BA64
.db $09
    .dw $1340
    .dw $BC00
.db $FF


DATA_B7E5:
.db $05
    .dw $0680
    .dw $B56E
.db $05
    .dw $0740
    .dw $B626
.db $04
    .dw $08C0
    .dw $B168
.db $05
    .dw $1780
    .dw $B782
.db $04
    .dw $1840
    .dw $BA64
.db $0A
    .dw $18C0
    .dw $A946
.db $FF


DATA_B804:
.db $0A
    .dw $0C40
    .dw $AFAA
.db $0A
    .dw $0C80
    .dw $B09C
.db $0A
    .dw $0D00
    .dw $B0C4
.db $04
    .dw $1000
    .dw $AA80
.db $0A
    .dw $1240
    .dw $ABCA
.db $0A
    .dw $1380
    .dw $AA86
.db $05
    .dw $1400
    .dw $B4C0
.db $05
    .dw $14C0
    .dw $B826
.db $0A
    .dw $1600
    .dw $AC8E
.db $FF


DATA_B832:
.db $0B
    .dw $0680
    .dw $AAA0
.db $0A
    .dw $1580
    .dw $B0C4
.db $FF


DATA_B83D:
.db $0A
    .dw $0C40
    .dw $AFD2
.db $0A
    .dw $0C80
    .dw $A946
.db $07
    .dw $0EC0
    .dw $BD44
.db $0A
    .dw $10C0
    .dw $B304
.db $0A
    .dw $1200
    .dw $B408
.db $05
    .dw $1480
    .dw $B998
.db $05
    .dw $1580
    .dw $B914
.db $08
    .dw $1600
    .dw $BF22
.db $0A
    .dw $1700
    .dw $BEDC
.db $FF


DATA_386B:
.db $18
    .dw $0C40
    .dw $A442
.db $07
    .dw $1780
    .dw $BD44
.db $0A
    .dw $1980
    .dw $A946
.db $05
    .dw $1BC0
    .dw $B914
.db $0A 
    .dw $1C40
    .dw $BEDC
.db $FF


DATA_B885:
.db $0A
    .dw $0C40
    .dw $AFFA
.db $07
    .dw $0C80
    .dw $BE20
.db $87
    .dw $0E80
    .dw $BE20
.db $0B
    .dw $1080
    .dw $B880
.db $0A
    .dw $1200
    .dw $BD26
.db $8A
    .dw $1280
    .dw $BD26
.db $09
    .dw $1300
    .dw $B986
.db $09 
    .dw $1400
    .dw $B79A
.db $06
    .dw $1500
    .dw $BF74
.db $0A
    .dw $1580
    .dw $ABCA
.db $0A
    .dw $16C0
    .dw $AC8E
.db $FF


DATA_B8BD:
.db $18
    .dw $05C0
    .dw $8180
.db $98
    .dw $1100
    .dw $8180
.db $FF


DATA_B8C8:
.db $0A
    .dw $1200
    .dw $BD26
.db $8A
    .dw $1280
    .dw $BD26
.db $09
    .dw $1300
    .dw $B986
.db $FF


DATA_B8D8:
.db $18
    .dw $05C0
    .dw $89CA
.db $FF


DATA_B8DE:
.db $03
    .dw $05C0
    .dw $B200
.db $FF


DATA_B8E4:
.db $0A
    .dw $0C40
    .dw $B01C
.db $0A
    .dw $0C80
    .dw $ABCA
.db $0A
    .dw $0DC0
    .dw $BF2E
.db $0A
    .dw $0EC0
    .dw $AA86
.db $18
    .dw $0F40
    .dw $ADC6
.db $18
    .dw $1840
    .dw $B1C6
.db $04
    .dw $1C40
    .dw $AE90
.db $FF


DATA_B908:
.db $0F
    .dw $0680
    .dw $A835
.db $8F
    .dw $1080
    .dw $A835
.db $05
    .dw $1A80
    .dw $B782
.db $FF


DATA_B918:
.db $0D
    .dw $0680
    .dw $A750
.db $8D
    .dw $12C0
    .dw $A750
.db $FF


DATA_B923:
.db $1F
    .dw $0200
    .dw $AE7C
.db $0B
    .dw $1380
    .dw $B26E
.db $04
    .dw $1800
    .dw $BA8C
.db $06
    .dw $1A00
    .dw $BA8C
.db $05
    .dw $1F00
    .dw $B782
.db $FF


DATA_B93D:
.db $05
    .dw $0C40
    .dw $BA88
.db $FF


DATA_B943:
.db $0A
    .dw $0880
    .dw $B632
.db $0A
    .dw $1200
    .dw $BAD2
.db $0E
    .dw $1840
    .dw $B197
.db $FF


DATA_B953:
.db $0A
    .dw $0880
    .dw $B632
.db $06
    .dw $1200
    .dw $BEDE
.db $04
    .dw $1800
    .dw $BBC2
.db $09
    .dw $1840
    .dw $BA3C
.db $FF


DATA_B968:
.db $0D
    .dw $0680
    .dw $B020
.db $8D
    .dw $1300
    .dw $B020
.db $FF


DATA_B973:
.db $0F
    .dw $0680
    .dw $A835
.db $10
    .dw $1080
    .dw $BA9C
.db $90
    .dw $1280
    .dw $BA9C
.db $FF


DATA_B983:
.db $0A
    .dw $0C40
    .dw $BE7C
.db $FF


DATA_B989:
.db $1B
    .dw $1080
    .dw $B060
.db $FF


DATA_B98F:
.db $09
    .dw $1D00
    .dw $BEBC
.db $FF


DATA_B995:
.db $0E
    .dw $0600
    .dw $AFE9
.db $0B
    .dw $0880
    .dw $B9C0
 .db $1D
    .dw $0F40
    .dw $BD18
.db $0E
    .dw $11C0
    .dw $B141
.db $04
    .dw $13C0
    .dw $BBC2
.db $FF


DATA_B9AF:
.db $0A
    .dw $0F40
    .dw $AF3A
.db $0A
    .dw $0F80
    .dw $ABCA
.db $0A
    .dw $10C0
    .dw $AA86
.db $04
    .dw $1140
    .dw $AE90
.db $0A
    .dw $11C0
    .dw $A586
.db $04
    .dw $1580
    .dw $AF10
.db $04
    .dw $1700
    .dw $BBE2
.db $04
    .dw $0C40
    .dw $BC8E
.db $FF


DATA_B9D8:
.db $0B
    .dw $1200
    .dw $B9C0
.db $FF
