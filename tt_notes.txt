=============================================================================
Memory Map:

  $D11F           - Copy of register VDP(1)
  
  $D132           - 
  $D133           - UNKNOWN. used in Engine_WaitForInterrupt.
                                 set before calling Engine_ProcessObjectLogic

  $D13F           - Player object number.
  
  $D143           - Game mode

  $D145           - Current level

  $D147           - Current act
  
  $D159 -> $D15A  - Ring Counter.
  
  $D168     - Title screen option number.
  
; ---- LEVEL HEADER START ----
  $D184           - Level viewport flags.
  $D185           - ??? more flags. lower bits used to skip checking for player
                    facing left when updating camera adjustment.
  $D188           - Bank containing metatile data for current
                    level.
  $D189           - Bank containing level data.
  $D18A -> $D18B  - Pointer to metatile data for current level.
  $D18C -> $D18D  - Pointer to level data.
  $D18E -> $D18F  - Pointer to table of multiples of level width.
            Used by collision routines.
  $D190 -> $D191  - 
  $D192 -> $D193  - Level width in metatiles.
  $D194 -> $D195  - Vertical offset into level data.

  $D3A2 -> $D3A3  - Minimum camera y position.
  $D3A4 -> $D3A5  - Maximum camera y position.
  $D3A6 -> $D3A7  - Minimum camera x position.
  $D3A8 -> $D3A9  - Maximum camera x position
  $D3AA -> $D3AB  - Copy of minimum camera y position.
  $D3AC -> $D3AD  - Copy of maximum camera y position.
  $D3AE -> $D3AF  - Copy of minimum camera x position.
  $D3B0 -> $D3B1  - Copy of maximum camera x position.
  
  $D3B6     - Horizontal camera adjustment.
  $D3B7     - Vertical camera adjustment.
  $D3B8     - Horizontal camera adjustment (working copy).
  $D3B9     - Right edge of camera.
  $D3BA     - Left edge of camera.
  $D3BB     - Vertical camera adjustment (working copy).
  $D3BC     - Bottom edge of camera.
  $D3BD     - Top edge of the camera.

  $D3C0     - ??? something camera related set by CalculateCameraBounds
; ---- LEVEL STRUCTURE END ----


  $D3F1     - Used by DrawText routine as the tile attribute byte
            for each character.

  $D4F6     - BG palette control byte.
  $D4F7     - BG palette index.
  $D4F8     - FG palette control byte.
  $D4F9     - FG palette index.
  
  $D4FE     - Palette fade timer expiry value.
  $D4FF     - Palette fade timers.
  
-----------------------------------------------------------------------------
Player object slot ($D500 -> $D53F)
  
  $D501     - Current player state.
  $D502     - Working player state.
  
  $D532     - Current power up.

-----------------------------------------------------------------------------
  $DB00 -> $DB7F  - Copy of the SAT stored in work RAM. $DB00->$DB40
            contains the VPOS attribs. $DB40->$DB7F contains
            the HPOS and char codes.

  $DBD0     - Ring loss value. When player is hit ring counter 
            will be decremented by this value.
  $DBE6 -> $DC05  - Buffer used by the tile decompression routines to
            store decompressed data.
  $DC06 -> $DC25  - Used by tile decompression routines. Data to be
            copied to the VDP in place of compression type 0.
            
=============================================================================
Debug Switch:
  
  The byte at $0064 is a debug flag switch. If bit 0 is set the game
  skips the usual intro/title screen sequence and enters the player
  selection screen.
  
   Bit | Action when Set      | Action when Reset |
  -----+----------------------+--------------------
    0  | No intro/title.      |
    1  | No action.           |
    2  | No action.           |
    3  | No action.           |
    4  | Level select enabled | Level select disabled
    5  | No action.           |
    6  | No action.           |
    7  | No action.           |
  -----+----------------------+---------------------               
  
=============================================================================
Palettes:
  $05   - Intro screen BG
  $06   - Intro screen FG
  
=============================================================================
Palette Control Variable:

  Lower 4 bits are used as a fade step counter.
  
   Bit | Action when Set      | Action when Reset |
  -----+----------------------+--------------------
    0  | No action.           |
    1  | No action.           |
    2  | No action.           |
    3  | No action.           |
    4  | Fade to/from white.  | Fade to/from black.
    5  | Reset to palette.    |
    6  | Fade from colour.    |
    7  | Fade to colour.      |
  -----+----------------------+---------------------

=============================================================================
Demo Sequences

    Demo sequence descriptors at $2519.

    Structure:
    --------+-------+---------------------------------------
     0x00   | byte  | Player object ID
     0x01   | byte  | Level number
     0x02   | byte  | Act number
     0x04   | byte  | bank number containing the data
     0x05   | word  | pointer to sequence data


=============================================================================
HUD icon stuff

  life icon
  $DB35 $DB36 $DB33 $DB34
  $DBAA $DBAC $DBA6 $DBA8
  $DBAB $DBAD $DBA7 $DBA9


  ring icon
  $DB3A $DB3B $DB37 $DB38 $DB39
  $DBB4 $DBB6 $DBAE $DBB0 $DBB2
  $DBB5 $DBB7 $DBAF $DBB1 $DBB3


  time icon
  $DB3C $DB3D $DB3E $DB3F
  $DBB8 $DBBA $DBBC $DBBE
  $DBB9 $DBBB $DBBD $DBBF
